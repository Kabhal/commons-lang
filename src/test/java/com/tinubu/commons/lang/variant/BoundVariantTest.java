/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import org.junit.jupiter.api.Test;

class BoundVariantTest {

   @Test
   public void testBoundVariantWhenNominal() {
      BoundVariant<Number> number = new BoundVariant<>(2, Integer.class);
      number = new BoundVariant<>(3L, Long.class);

      assertThat(number).isNotNull();
      assertThat(number.type()).isEqualTo(Long.class);

      Long value = number.value(Long.class);

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);
   }

   @Test
   public void testBoundVariantWhenObject() {
      BoundVariant<Object> number = new BoundVariant<>(3L, Long.class);

      assertThat(number).isNotNull();
      assertThat(number.type()).isEqualTo(Long.class);

      Long value = number.value(Long.class);

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);
   }

   @Test
   public void testBoundVariantWhenBadParameters() {
      assertThatNullPointerException().isThrownBy(() -> new BoundVariant<>(3L, null));
   }

   @Test
   public void testBoundVariantWhenNullValue() {
      BoundVariant<Number> number = new BoundVariant<>(null, Long.class);

      assertThat(number).isNotNull();
      assertThat(number.value()).isNull();
      assertThat(number.value(Long.class)).isNull();
   }

   @Test
   public void testBoundVariantWhenNoExplicitType() {
      BoundVariant<Number> number = new BoundVariant<>(3L);

      assertThat(number).isNotNull();
      assertThat(number.type()).isEqualTo(Long.class);

      Long value = number.value(Long.class);

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);

      assertThatExceptionOfType(VariantTypeException.class)
            .isThrownBy(() -> number.value(Integer.class))
            .withMessage("'java.lang.Integer' type is incompatible with 'java.lang.Long' variant type");
   }

   @Test
   public void testBoundVariantWhenNoExplicitTypeAndNullValue() {
      assertThatNullPointerException()
            .isThrownBy(() -> new BoundVariant<>(null))
            .withMessage("'value' must not be null");
   }

   @Test
   public void testValueWhenNullValue() {
      BoundVariant<Number> number = new BoundVariant<>(null, Long.class);

      assertThat(number).isNotNull();

      Long value = number.value(Long.class);

      assertThat(value).isNull();
   }

   @Test
   public void testValueWhenSuperType() {
      BoundVariant<Number> number = new BoundVariant<>(3L, Long.class);

      assertThat(number).isNotNull();

      Long value1 = number.value(Long.class);

      assertThat(value1).isExactlyInstanceOf(Long.class);
      assertThat(value1).isEqualTo(3L);

      Number value2 = number.value(Long.class);

      assertThat(value2).isExactlyInstanceOf(Long.class);
      assertThat(value2).isEqualTo(3L);

      Number value3 = number.value(Number.class);

      assertThat(value3).isExactlyInstanceOf(Long.class);
      assertThat(value3).isEqualTo(3L);
   }

   @Test
   public void testValueWhenIncompatibleType() {
      BoundVariant<Number> number = new BoundVariant<>(3L, Long.class);

      assertThat(number).isNotNull();

      assertThatExceptionOfType(VariantTypeException.class)
            .isThrownBy(() -> number.value(Integer.class))
            .withMessage("'java.lang.Integer' type is incompatible with 'java.lang.Long' variant type");
   }

   @Test
   public void testValue() {
      BoundVariant<Number> number = new BoundVariant<>(3L, Long.class);

      assertThat(number).isNotNull();

      Number value = number.value();

      assertThat(value).isExactlyInstanceOf(Long.class);
      assertThat(value).isEqualTo(3L);
   }

}
