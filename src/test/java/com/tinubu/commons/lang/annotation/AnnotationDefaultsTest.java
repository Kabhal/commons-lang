/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.annotation;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class AnnotationDefaultsTest {

   @Test
   public void testAnnotationDefaultWhenString() {
      assertThat(AnnotationDefaults.isDefaultValue(
            Annotation1Holder.instance().stringValueWithSystemDefault())).isTrue();
      assertThat(AnnotationDefaults.isDefaultValue(
            Annotation1Holder.instance().stringValueWithLocalDefault())).isFalse();
      assertThat(AnnotationDefaults.isDefaultValue(
            Annotation1Holder.instance().stringValueWithEmptyDefault())).isFalse();
   }

   @Test
   public void testAnnotationDefaultWhenArrayString() {
      assertThat(AnnotationDefaults.isDefaultValue(
            Annotation1Holder.instance().stringArrayValueWithSystemDefault())).isTrue();
      assertThat(AnnotationDefaults.isDefaultValue(
            Annotation1Holder.instance().stringArrayValueWithLocalDefault())).isFalse();
      assertThat(AnnotationDefaults.isDefaultValue(
            Annotation1Holder.instance().stringArrayValueWithEmptyDefault())).isFalse();
   }

   @Annotation1
   public static class Annotation1Holder {
      private static Annotation1 instance() {
         return AnnotationDefaultsTest.Annotation1Holder.class.getAnnotation(Annotation1.class);
      }
   }

}



