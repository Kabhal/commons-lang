/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.encoding;

import static com.tinubu.commons.lang.io.encoding.DefaultEncodingDetector.UTF_8_BOM;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static java.nio.charset.CodingErrorAction.REPORT;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.charset.StandardCharsets.UTF_16BE;
import static java.nio.charset.StandardCharsets.UTF_16LE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Random;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class AutoDetectEncodingInputStreamTest {

   private static final Random RANDOM = new Random();

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenNominal(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("A sample content", UTF_8),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_8)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.3f);
      });

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("A sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenExternalReadBeforeFillingDetectionBuffer(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("A sample content", UTF_8),
                                                               new DefaultEncodingDetector());

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("A sample content");

      assertThat(autoDetectStream.hasEncoding(UTF_8)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
      });

   }

   @Test
   public void autoDetectWhenBadParameters() {
      assertThatThrownBy(() -> new AutoDetectEncodingInputStream(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'inputStream' must not be null");
      assertThatThrownBy(() -> new AutoDetectEncodingInputStream(inputStream("A sample content"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'detector' must not be null");
   }

   @Test
   public void autoDetectWhenDetectionLengthTooSmall() {
      assertThatThrownBy(() -> new AutoDetectEncodingInputStream(inputStream("A sample content", UTF_8),
                                                                 new DefaultEncodingDetector(),
                                                                 0))
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("'detectionLength' must be > 0");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenAvailableBytesSmallerThatDetectionLength(int bufferSize) throws IOException {
      var autoDetectStream =
            new AutoDetectEncodingInputStream(inputStream("A", UTF_8), new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_8)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
      });

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("A");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenEmptyInputStream(int bufferSize) throws IOException {
      var autoDetectStream =
            new AutoDetectEncodingInputStream(inputStream("", UTF_8), new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_8)).isEmpty();

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenCompatibleEncoding(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("À sàmple content", UTF_8),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_8)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.3f);
      });

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("À sàmple content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenNotCompatibleEncoding(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("À sàmple content", ISO_8859_1),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_8)).isEmpty();

      assertThat(readAsString(autoDetectStream, ISO_8859_1, bufferSize)).isEqualTo("À sàmple content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenRandomBinary(int bufferSize) throws IOException {
      var autoDetectStream =
            new AutoDetectEncodingInputStream(binaryInputStream(100), new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_8)).isEmpty();
      assertThat(autoDetectStream.hasEncoding(ISO_8859_1)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(ISO_8859_1);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0f);
      });

      assertThat(readAsBytes(autoDetectStream, bufferSize)).hasSize(100);
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenUTF16BE(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("A \uD83D\uDE00 sample", UTF_16BE),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_16BE)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16BE);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.2f);
      });
      assertThat(autoDetectStream.hasEncoding(UTF_16LE)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16LE);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.2f);
      });
      assertThat(autoDetectStream.hasEncoding(UTF_16)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.2f);
      });

      assertThat(readAsString(autoDetectStream, UTF_16BE, bufferSize)).isEqualTo("A \uD83D\uDE00 sample");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenUTF16LE(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("A \uD83D\uDE00 sample", UTF_16LE),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_16BE)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16BE);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.2f);
      });
      assertThat(autoDetectStream.hasEncoding(UTF_16LE)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16LE);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.2f);
      });
      assertThat(autoDetectStream.hasEncoding(UTF_16)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.2f);
      });

      assertThat(readAsString(autoDetectStream, UTF_16LE, bufferSize)).isEqualTo("A \uD83D\uDE00 sample");
   }

   /**
    * @implNote Java UTF-16 charset encoder adds a BOM automatically.
    */
   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenUTF16(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("A \uD83D\uDE00 sample", UTF_16),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(UTF_16BE)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16BE);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16BE_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });
      assertThat(autoDetectStream.hasEncoding(UTF_16LE))
            .as("A charset whose BOM is mismatching with current BOM must not match")
            .isEmpty();
      assertThat(autoDetectStream.hasEncoding(UTF_16)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16BE_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });

      assertThat(readAsString(autoDetectStream, UTF_16, bufferSize)).isEqualTo("A \uD83D\uDE00 sample");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenMultipleCharsetsMatches(int bufferSize) throws IOException {
      var autoDetectStream =
            new AutoDetectEncodingInputStream(inputStream("A sample content"), new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(UTF_8, ISO_8859_1))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.3f);
      });

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("A sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenMultipleCharsetsMatchesAndHigherConfidenceTakesPriority(int bufferSize)
         throws IOException {
      var autoDetectStream =
            new AutoDetectEncodingInputStream(inputStream("A sample content"), new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(ISO_8859_1, UTF_8))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0.3f);
      });

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("A sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenMultipleCharsetsAndFirstFail(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("À sàmple content", ISO_8859_1),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(UTF_8, ISO_8859_1))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(ISO_8859_1);
         assertThat(detected.bom()).isEmpty();
         assertThat(detected.confidence()).isEqualTo(0f);
      });

      assertThat(readAsString(autoDetectStream, ISO_8859_1, bufferSize)).isEqualTo("À sàmple content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenUTF8Bom(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("\uFEFFA sample content", UTF_8),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(ISO_8859_1, UTF_8))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_8_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("\uFEFFA sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenUTF16LEBom(int bufferSize) throws IOException {
      var autoDetectStream =
            new AutoDetectEncodingInputStream(inputStream("\uFEFFA sample content", UTF_16LE),
                                              new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(ISO_8859_1, UTF_16LE))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16LE);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16LE_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });

      assertThat(readAsString(autoDetectStream, UTF_16LE, bufferSize)).isEqualTo("\uFEFFA sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenUTF16BEBom(int bufferSize) throws IOException {
      var autoDetectStream =
            new AutoDetectEncodingInputStream(inputStream("\uFEFFA sample content", UTF_16BE),
                                              new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(ISO_8859_1, UTF_16BE))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16BE);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16BE_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });
      assertThat(autoDetectStream.hasEncoding(list(UTF_16BE, UTF_16)))
            .as("UTF_16 alias must be correctly managed, input encodings order must be preserved")
            .hasValueSatisfying(detected -> {
               assertThat(detected.encoding()).isEqualTo(UTF_16BE);
               assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16BE_BOM);
               assertThat(detected.confidence()).isEqualTo(1f);
            });
      assertThat(autoDetectStream.hasEncoding(list(UTF_16, UTF_16BE)))
            .as("UTF_16 alias must be correctly managed, input encodings order must be preserved")
            .hasValueSatisfying(detected -> {
               assertThat(detected.encoding()).isEqualTo(UTF_16);
               assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16BE_BOM);
               assertThat(detected.confidence()).isEqualTo(1f);
            });

      assertThat(readAsString(autoDetectStream, UTF_16BE, bufferSize)).isEqualTo("\uFEFFA sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenUTF16Bom(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("\uFEFFA sample content", UTF_16),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(ISO_8859_1, UTF_16))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_16);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16BE_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });
      assertThat(autoDetectStream.hasEncoding(UTF_16BE))
            .as("UTF_16 alias must be correctly managed")
            .hasValueSatisfying(detected -> {
               assertThat(detected.encoding()).isEqualTo(UTF_16BE);
               assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_16BE_BOM);
               assertThat(detected.confidence()).isEqualTo(1f);
            });

      assertThat(readAsString(autoDetectStream, UTF_16, bufferSize)).isEqualTo("\uFEFFA sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenRemoveBom(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("\uFEFFA sample content", UTF_8),
                                                               new DefaultEncodingDetector());

      assertThat(autoDetectStream.hasEncoding(list(ISO_8859_1, UTF_8), true)).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_8_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("A sample content");
   }

   @ParameterizedTest
   @ValueSource(ints = { -1, 1, 5, 16, 20 })
   public void autoDetectWhenRemoveBomAndStreamAlreadyRead(int bufferSize) throws IOException {
      var autoDetectStream = new AutoDetectEncodingInputStream(inputStream("\uFEFFA sample content", UTF_8),
                                                               new DefaultEncodingDetector());

      assertThat(readAsString(autoDetectStream, UTF_8, bufferSize)).isEqualTo("\uFEFFA sample content");
      assertThat(autoDetectStream.hasEncoding(list(ISO_8859_1, UTF_8))).hasValueSatisfying(detected -> {
         assertThat(detected.encoding()).isEqualTo(UTF_8);
         assertThat(detected.bom()).hasValue(DefaultEncodingDetector.UTF_8_BOM);
         assertThat(detected.confidence()).isEqualTo(1f);
      });
   }

   private ByteArrayInputStream binaryInputStream(int length) {
      byte[] random = new byte[length];
      RANDOM.nextBytes(random);
      return new ByteArrayInputStream(random);
   }

   private static ByteArrayInputStream inputStream(String content, Charset charset) {
      try {
         var encode = charset
               .newEncoder()
               .onMalformedInput(REPORT)
               .onUnmappableCharacter(REPORT)
               .encode(CharBuffer.wrap(content));
         return new ByteArrayInputStream(encode.array(), encode.arrayOffset(), encode.limit());
      } catch (CharacterCodingException e) {
         throw new IllegalStateException("Can't encode test content in specified charset > " + e.getMessage(),
                                         e);
      }
   }

   private static ByteArrayInputStream inputStream(String content) {
      return inputStream(content, UTF_8);
   }

   /**
    * @param bufferSize intermediate read buffer size, or {@code -1} to use {@link InputStream#read()}
    */
   private static String readAsString(InputStream inputStream, Charset charset, int bufferSize)
         throws IOException {
      var out = new ByteArrayOutputStream();
      int read;
      if (bufferSize == -1) {
         while ((read = inputStream.read()) >= 0) {
            out.write(read);
         }
      } else {
         byte[] buffer = new byte[bufferSize];
         while ((read = inputStream.read(buffer, 0, bufferSize)) >= 0) {
            out.write(buffer, 0, read);
         }
      }
      return out.toString(charset);
   }

   private static String readAsString(InputStream inputStream, int bufferSize) throws IOException {
      return readAsString(inputStream, UTF_8, bufferSize);
   }

   private static byte[] readAsBytes(InputStream inputStream, int bufferSize) throws IOException {
      var out = new ByteArrayOutputStream();
      int read;
      if (bufferSize == -1) {
         while ((read = inputStream.read()) >= 0) {
            out.write(read);
         }
      } else {
         byte[] buffer = new byte[bufferSize];
         while ((read = inputStream.read(buffer, 0, bufferSize)) >= 0) {
            out.write(buffer, 0, read);
         }
      }
      return out.toByteArray();
   }

}