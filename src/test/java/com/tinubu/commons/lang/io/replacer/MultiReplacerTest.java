/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer;

import static com.tinubu.commons.lang.io.ReplacerTestUtils.DEFAULT_BUFFER_SIZE;
import static com.tinubu.commons.lang.io.ReplacerTestUtils.readerToString;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.StringReader;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.TokenReplacerFactory;

class MultiReplacerTest {

   @Test
   public void testReplacerWhenNominal() throws IOException {
      assertThatEquals("Some text",
                       bs -> new MultiReplacer(new StringReplacer("Some", "SOME"),
                                               new StringReplacer("text", "TEXT")),
                       "SOME TEXT");

   }

   @Test
   public void testReplacerWhenConflicting() throws IOException {
      assertThatEquals("Some text",
                       bs -> new MultiReplacer(new StringReplacer("Some", "1"),
                                               new StringReplacer("Some", "2")),
                       "1 text");
   }

   @Test
   public void testReplacerWhenFirstMatchingReplacerApplies() throws IOException {
      assertThatEquals("Some text",
                       bs -> new MultiReplacer(new StringReplacer("Some", "1"),
                                               new StringReplacer("So", "2")),
                       "2me text");
      assertThatEquals("Some text",
                       bs -> new MultiReplacer(new StringReplacer("So", "2"),
                                               new StringReplacer("Some", "1")),
                       "2me text");
      assertThatEquals("SoSomeSoSome text",
                       bs -> new MultiReplacer(new StringReplacer("Some", "1"),
                                               new StringReplacer("So", "2")),
                       "22me22me text");
      assertThatEquals("SoSomeSoSome text",
                       bs -> new MultiReplacer(new StringReplacer("So", "2"),
                                               new StringReplacer("Some", "1")),
                       "22me22me text");
   }

   @Test
   public void testReplacerWhenOverlapping() throws IOException {
      assertThatEquals("Some text",
                       bs -> new MultiReplacer(new StringReplacer("So", "Some"),
                                               new StringReplacer("Some", "SOME")),
                       "Someme text");
      assertThatEquals("Some text",
                       bs -> new MultiReplacer(new StringReplacer("Some", "SOME"),
                                               new StringReplacer("So", "Some")),
                       "Someme text");
   }

   @Test
   public void testReplacerWhenMatchStringEqualToReplacementString() throws IOException {
      assertThatEquals("Some text",
                       bs -> new MultiReplacer(new StringReplacer("So", "So"),
                                               new StringReplacer("So", "SO"),
                                               new StringReplacer("Some", "SOME")),
                       "Some text");
   }

   public static void assertThatEquals(String source,
                                       TokenReplacerFactory replacer,
                                       int bufferSize,
                                       int readLength,
                                       String expected) throws IOException {
      assertThat(readerToString(new GeneralReplacerReader(new StringReader(source), replacer, bufferSize),
                                readLength)).isEqualTo(expected);
   }

   public static void assertThatEquals(String source,
                                       TokenReplacerFactory replacer,
                                       int readLength,
                                       String expected) throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, readLength, expected);
   }

   public static void assertThatEquals(String source, TokenReplacerFactory replacer, String expected)
         throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, expected);
   }

}