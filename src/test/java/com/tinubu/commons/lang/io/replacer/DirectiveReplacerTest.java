/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer;

import static com.tinubu.commons.lang.io.ReplacerTestUtils.DEFAULT_BUFFER_SIZE;
import static com.tinubu.commons.lang.io.ReplacerTestUtils.readerToString;
import static com.tinubu.commons.lang.io.replacer.DirectiveReplacer.ErrorMode.INLINE;
import static com.tinubu.commons.lang.io.replacer.DirectiveReplacer.mapModelReplacement;
import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.io.StringReader;
import java.util.Map;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;
import org.junit.jupiter.params.provider.ArgumentsSource;

import com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader;
import com.tinubu.commons.lang.io.contentloader.StringContentLoader;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.ErrorMode;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.TokenReplacerFactory;

// FIXME test directive evaluation output (true/false/failures...)
class DirectiveReplacerTest {

   @BeforeEach
   public void contentLoaders() {
      ServiceLoaderContentLoader.loadContentLoaders(new StringContentLoader("<Injected>"));
   }

   @Nested
   public class TestDirectiveReplacer {

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenNominal(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#(Comment)} END", directiveReplacer(), readLength, "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenEmpty(int readLength) throws IOException {
         assertThatEquals("BEGIN ${} END", directiveReplacer(), readLength, "BEGIN ${} END");
         assertThatEquals("BEGIN ${} END", directiveReplacer(map()), readLength, "BEGIN ${} END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenMultipleArguments(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#(Comment1, Comment2, Comment3, ...)} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenEmptyArguments(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} Comment ${/#} END", directiveReplacer(), readLength, "BEGIN  END");
         assertThatEquals("BEGIN ${#()} Comment ${/#()} END", directiveReplacer(), readLength, "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenSuppressFollowingWhitespace(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#(Comment)-} \t\n\r\r\n\u00A0END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN \t\n\r\r\n\u00A0END");
         assertThatEquals("BEGIN ${#(Comment)-0} \t\n\r\r\n\u00A0END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  \t\n\r\r\n\u00A0END");
         assertThatEquals("BEGIN ${#(Comment)-*} \t\n\r\r\n\u00A0END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN END");
         assertThatEquals("BEGIN ${#(Comment)-5} \t\n\r\r\n\u00A0END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN \u00A0END");
         assertThatEquals("BEGIN ${#(Comment)-50} \t\n\r\r\n\u00A0END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN END");

         assertThatEquals("BEGIN ${#(Comment)-} ${#(Comment2)-} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenModelValue(int readLength) throws IOException {
         assertThatEquals("BEGIN ${unknown} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN ${unknown} END");
         assertThatEquals("BEGIN ${key} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN value END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenNoModelValueFunction(int readLength) throws IOException {
         assertThatEquals("BEGIN ${key} END", directiveReplacer(), readLength, "BEGIN ${key} END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenModelValueAndSuppressFollowingWhitespace(int readLength) throws IOException {
         assertThatEquals("BEGIN ${key-} \t\n\r\r\n\u00A0END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN value\t\n\r\r\n\u00A0END");
         assertThatEquals("BEGIN ${key-0} \t\n\r\r\n\u00A0END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN value \t\n\r\r\n\u00A0END");
         assertThatEquals("BEGIN ${key-*} \t\n\r\r\n\u00A0END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN valueEND");
         assertThatEquals("BEGIN ${key-5} \t\n\r\r\n\u00A0END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN value\u00A0END");
         assertThatEquals("BEGIN ${key-50} \t\n\r\r\n\u00A0END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN valueEND");

         assertThatEquals("BEGIN ${key-} ${key-} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN valuevalueEND");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenStopReadingAndInnerDirectivesWithError(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} ${include} ${/#} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void directiveWhenEscapeTokensAndInnerDirectivesWithError(int readLength) throws IOException {
         assertThatEquals("BEGIN ${escape} ${include} ${/escape} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  ${include}  END");
      }

      @Test
      public void directiveWhenInlineErrorMode() throws IOException {
         assertThatEquals("BEGIN ${include()} END",
                          directiveReplacer(INLINE),
                          "BEGIN ${include error: Missing URI parameter} END");
      }

      @Test
      public void directiveWithAlternativeSyntaxWhenNominal() throws IOException {
         assertThatEquals("BEGIN ${[#(Comment)]} END",
                          directiveReplacer("[", "]", "$", map(entry("key", "value"))),
                          "BEGIN  END");
         assertThatEquals("BEGIN ${[[#(Comment)]]} END",
                          directiveReplacer("[[", "]]", "$$", map(entry("key", "value"))),
                          "BEGIN  END");
      }

      @Test
      public void directiveWithAlternativeSyntaxWhenEmpty() throws IOException {
         assertThatEquals("BEGIN $#(Comment)$ END",
                          directiveReplacer("$", "$", "", "", "-", map(entry("key", "value"))),
                          "BEGIN  END");
         assertThatEquals("BEGIN $#$ Comment $/#$ END",
                          directiveReplacer("$", "$", "", "", "-", map(entry("key", "value"))),
                          "BEGIN  END");
      }

      /**
       * Placeholder parsing is not greedy so that placeholder token is parsed to
       * {#(Comment) with does not validate directive syntax.
       *
       * @implSpec It's not possible to implement a "greedy" placeholder parser or ${{directive}}} will
       *       resolve to {directive}} when the extra } could be regular template text following the
       *       placeholder.
       */
      @Test
      public void directiveWithAlternativeSyntaxWhenParsingLimitations() throws IOException {
         assertThatEquals("BEGIN ${{#(Comment)}} END",
                          directiveReplacer("{", "}", "-", map(entry("key", "value"))),
                          "BEGIN ${{#(Comment)}} END");
      }

      @Test
      public void directiveWithAlternativeSyntaxWhenSuppressWhitespace() throws IOException {
         assertThatEquals("BEGIN ${[#(Comment)]$} END",
                          directiveReplacer("[", "]", "$", map(entry("key", "value"))),
                          "BEGIN END");
         assertThatEquals("BEGIN ${[#(Comment)]$*}    END",
                          directiveReplacer("[", "]", "$", map(entry("key", "value"))),
                          "BEGIN END");
         assertThatEquals("BEGIN ${[#(Comment)$]} END",
                          directiveReplacer("[", "]", "$", map(entry("key", "value"))),
                          "BEGIN ${[#(Comment)$]} END");
         assertThatEquals("BEGIN ${[[#(Comment)]]$$*}    END",
                          directiveReplacer("[[", "]]", "$$", map(entry("key", "value"))),
                          "BEGIN END");
      }
   }

   @Nested
   public class CommentDirective {

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenNominal(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} Comment\nComment2 ${/#} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenBlock(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} Comment ${/#} END", directiveReplacer(), readLength, "BEGIN  END");
         assertThatEquals("BEGIN ${#()} Comment ${/#()} END", directiveReplacer(), readLength, "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenSingleDirective(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#(Comment)} END", directiveReplacer(), readLength, "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenInnerComment(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} ${include(test)} ${#(Inner comment)} ${#} ${key} ${/#} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenInnerDirectives(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} ${include(test)} ${key} ${/#} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenInnerDirectivesWithError(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} ${include} ${/#} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenSuppressFollowingWhitespace(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#-} Comment ${/#-} END", directiveReplacer(), readLength, "BEGIN END");
         assertThatEquals("BEGIN ${#(Comment)-} END", directiveReplacer(), readLength, "BEGIN END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenMisplacedEnd(int readLength) throws IOException {
         assertThatThrownBy(() -> assertThatEquals("${/#} BEGIN ${#} Comment END",
                                                   directiveReplacer(),
                                                   readLength,
                                                   "BEGIN "))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'/#' directive error: Misplaced directive");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void commentWhenNotEnded(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} Comment END", directiveReplacer(), readLength, "BEGIN ");
      }

   }

   @Nested
   public class IfElseDirective {

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenNominal(int readLength) throws IOException {
         assertThatEquals("BEGIN ${if(true)}IF${/if} END", directiveReplacer(), readLength, "BEGIN IF END");
         assertThatEquals("BEGIN ${if(false)}IF${/if} END", directiveReplacer(), readLength, "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenIfThenElse(int readLength) throws IOException {
         assertThatEquals("BEGIN ${if(true)}IF${else}ELSE${/if} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN IF END");
         assertThatEquals("BEGIN ${if(false)}IF${else}ELSE${/if} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN ELSE END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenNegated(int readLength) throws IOException {
         assertThatEquals("BEGIN ${if!(true)}IF${else}ELSE${/if} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN ELSE END");
         assertThatEquals("BEGIN ${if!(false)}IF${else}ELSE${/if} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN IF END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenEmptyCondition(int readLength) throws IOException {
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${if()}IF${else}ELSE${/if} END",
                                                   directiveReplacer(),
                                                   readLength,
                                                   null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'if' directive error: Missing condition");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenConditionError(int readLength) throws IOException {
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${if(&())}IF${else}ELSE${/if} END",
                                                   directiveReplacer(),
                                                   readLength,
                                                   null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'if' directive error: '&' directive error: Missing model variable name");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenNested(int readLength) throws IOException {
         assertThatEquals(
               "BEGIN ${if(has(a))}${if(has(b))}ab${else}a!b${/if}${else}${if(has(b))}!ab${else}!a!b${/if}${/if} END",
               directiveReplacer(map(entry("a", "value"), entry("b", "value"))),
               readLength,
               "BEGIN ab END");
         assertThatEquals(
               "BEGIN ${if(has(a))}${if(has(b))}ab${else}a!b${/if}${else}${if(has(b))}!ab${else}!a!b${/if}${/if} END",
               directiveReplacer(map(entry("a", "value"))),
               readLength,
               "BEGIN a!b END");
         assertThatEquals(
               "BEGIN ${if(has(a))}${if(has(b))}ab${else}a!b${/if}${else}${if(has(b))}!ab${else}!a!b${/if}${/if} END",
               directiveReplacer(map(entry("b", "value"))),
               readLength,
               "BEGIN !ab END");
         assertThatEquals(
               "BEGIN ${if(has(a))}${if(has(b))}ab${else}a!b${/if}${else}${if(has(b))}!ab${else}!a!b${/if}${/if} END",
               directiveReplacer(map()),
               readLength,
               "BEGIN !a!b END");

         assertThatEquals(
               "BEGIN ${if(has(a))}${if(has(b))}${if(has(c))}abc${else}ab!c${/if}${else}a!b${/if}${else}${if(has(b))}!ab${else}!a!b${/if}${/if} END",
               directiveReplacer(map(entry("a", "value"), entry("b", "value"))),
               readLength,
               "BEGIN ab!c END");

      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenInComment(int readLength) throws IOException {
         assertThatEquals("BEGIN ${#} ${if(has(a))}${if(has(b))}ab${else}a!b${/if}${else}!a${/if} ${/#} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  END");
         assertThatEquals("BEGIN ${#} ${/if}${if()}IF${else}ELSE ${/#} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenInEscape(int readLength) throws IOException {
         assertThatEquals("BEGIN ${escape} ${if(true)}IF${else}ELSE${/if} ${/escape} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  ${if(true)}IF${else}ELSE${/if}  END");
         assertThatEquals("BEGIN ${escape} ${/if}${if()}IF${else}ELSE ${/escape} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  ${/if}${if()}IF${else}ELSE  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenInnerDirectives(int readLength) throws IOException {
         assertThatEquals("BEGIN ${if(true)} ${include(test)} ${key} ${/if} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  <Injected> value  END");
         assertThatEquals("BEGIN ${if(true)} ${escape}${key}${/escape} ${/if} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  ${key}  END");
         assertThatEquals("BEGIN ${if(true)} ${#}${key}${/#} ${/if} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN    END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenInnerDirectivesWithError(int readLength) throws IOException {
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${if(has(key))} ${include()} ${/if} END",
                                                   directiveReplacer(map(entry("key", "value"))),
                                                   readLength,
                                                   "BEGIN  END"))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'include' directive error: Missing URI parameter");
         assertThatEquals("BEGIN ${if(has(unknown))} ${include()} ${/if} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  END");
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${if(include())}  ${/if} END",
                                                   directiveReplacer(map(entry("key", "value"))),
                                                   readLength,
                                                   "BEGIN  END"))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'if' directive error: 'include' directive error: Missing URI parameter");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenSuppressFollowingWhitespace(int readLength) throws IOException {
         assertThatEquals("BEGIN ${if(has(key))-} IF${else-} ELSE${/if-} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN IFEND");
         assertThatEquals("BEGIN ${if!(has(key))-} IF${else-} ELSE${/if-} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN ELSEEND");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenMisplacedEnd(int readLength) throws IOException {
         assertThatThrownBy(() -> assertThatEquals("${/if}BEGIN ${if(has(key))} IF",
                                                   directiveReplacer(map(entry("key", "value"))),
                                                   readLength,
                                                   null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'/if' directive error: Misplaced directive");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void ifWhenNotEnded(int readLength) throws IOException {
         assertThatEquals("BEGIN ${if(has(key))} IF",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN  IF");
      }

   }

   @Nested
   public class EscapeDirective {

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenNominal(int readLength) throws IOException {
         assertThatEquals(
               "BEGIN ${include(test)} BEFORE ${escape}${include(test)}${/escape} AFTER ${include(test)} END",
               directiveReplacer(),
               readLength,
               "BEGIN <Injected> BEFORE ${include(test)} AFTER <Injected> END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenLazyParsing(int readLength) throws IOException {
         assertThatEquals(
               "BEGIN ${include(test)} BEFORE ${escape}${ include(test,   test) - }${/escape} AFTER ${include(test)} END",
               directiveReplacer(),
               readLength,
               "BEGIN <Injected> BEFORE ${ include(test,   test) - } AFTER <Injected> END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenModelValues(int readLength) throws IOException {
         assertThatEquals("BEGIN ${key} BEFORE ${escape}${key}${/escape} AFTER ${key} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN value BEFORE ${key} AFTER value END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenError(int readLength) throws IOException {
         assertThatEquals("BEGIN ${escape(include())} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN include() END");
         assertThatEquals("BEGIN ${escape}${include()}${/escape} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN ${include()} END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenBlock(int readLength) throws IOException {
         assertThatEquals(
               "BEGIN ${include(test)} BEFORE ${escape}${include(test)}${/escape} AFTER ${include(test)} END",
               directiveReplacer(),
               readLength,
               "BEGIN <Injected> BEFORE ${include(test)} AFTER <Injected> END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenSingleDirective(int readLength) throws IOException {
         assertThatEquals("BEGIN ${escape(value\\n)} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN value\\n END");
         assertThatEquals("BEGIN ${escape(  value1,    value2,value3)} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN value1 value2 value3 END");

         /* FIXME plus vrai ?@Disabled("Placeholder is resolved to ${escape(${directive")
         assertThatEquals("BEGIN ${escape(${directive})} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN ${directive} END");
         assertThatEquals("BEGIN ${escape(  ${directive},    TEXT,${escape},${key},${#})} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN ${directive} TEXT ${escape} ${key} ${#} END");
          */
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenInnerEscape(int readLength) throws IOException {
         assertThatEquals("BEGIN ${escape} BEFORE ${escape}${include} AFTER ${/escape} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  BEFORE ${escape}${include} AFTER  END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenSuppressFollowingWhitespace(int readLength) throws IOException {
         assertThatEquals("BEGIN ${escape-} ${include}${/escape-} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN ${include}END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenBlockNotEnded(int readLength) throws IOException {
         assertThatEquals("BEGIN ${escape-} ${include} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN ${include} END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void escapeWhenFinal(int readLength) throws IOException {
         assertThatEquals(
               "BEGIN ${include(test)} BEFORE ${escape(final)}${include(test)}${/escape} AFTER ${include(test)} END",
               directiveReplacer(),
               readLength,
               "BEGIN <Injected> BEFORE ${include(test)}${/escape} AFTER ${include(test)} END");
      }
   }

   @Nested
   public class IncludeDirective {

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void includeWhenNominal(int readLength) throws IOException {
         assertThatEquals("BEGIN ${include(test)} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN <Injected> END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void includeWhenBadParameter(int readLength) throws IOException {
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${include()} END",
                                                   directiveReplacer(),
                                                   readLength,
                                                   null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'include' directive error: Missing URI parameter");
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${include(git@git.com:repo.git)} END",
                                                   directiveReplacer(),
                                                   readLength,
                                                   null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'include' directive error: Can't parse 'git@git.com:repo.git' URI");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void includeWhenInEscape(int readLength) throws IOException {
         assertThatEquals(
               "BEGIN ${include(test)} BEFORE ${escape}${include(test)}${/escape} AFTER ${include(test)} END",
               directiveReplacer(),
               readLength,
               "BEGIN <Injected> BEFORE ${include(test)} AFTER <Injected> END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void includeWhenSuppressFollowingWhitespace(int readLength) throws IOException {
         assertThatEquals("BEGIN ${include(test)-} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN <Injected>END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void includeWhenNoMatchingSource(int readLength) throws IOException {
         ServiceLoaderContentLoader.clearContentLoaders();

         assertThatThrownBy(() -> assertThatEquals("BEGIN ${include(test)} END",
                                                   directiveReplacer(),
                                                   readLength,
                                                   null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'include' directive error: 'test' resource not found");

         assertThatEquals("BEGIN ${include(test, ignoreMissingResource)} END",
                          directiveReplacer(),
                          readLength,
                          "BEGIN  END");
      }

   }

   @Nested
   public class ValueDirective {

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void valueWhenNominal(int readLength) throws IOException {
         assertThatEquals("BEGIN ${&(key)} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN value END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void valueWhenBadParameter(int readLength) {
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${&()} END", directiveReplacer(), readLength, null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'&' directive error: Missing model variable name");
         assertThatThrownBy(() -> assertThatEquals("BEGIN ${has()} END",
                                                   directiveReplacer(),
                                                   readLength,
                                                   null))
               .isInstanceOf(IllegalStateException.class)
               .hasMessage("'has' directive error: Missing model variable name");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void valueWhenInEscape(int readLength) throws IOException {
         assertThatEquals("BEGIN ${&(key)} BEFORE ${escape}${&(key)}${/escape} AFTER ${&(key)} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN value BEFORE ${&(key)} AFTER value END");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void valueWhenSuppressFollowingWhitespace(int readLength) throws IOException {
         assertThatEquals("BEGIN ${&(key)-} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN valueEND");
      }

      @ParameterizedTest
      @ArgumentsSource(ReadLengthArguments.class)
      public void valueWhenUnknown(int readLength) throws IOException {
         assertThatEquals("BEGIN ${&(unknown)} END",
                          directiveReplacer(map(entry("key", "value"))),
                          readLength,
                          "BEGIN ${&(unknown)} END");
      }

   }

   private static class ReadLengthArguments implements ArgumentsProvider {

      @Override
      public Stream<? extends Arguments> provideArguments(ExtensionContext context) throws Exception {
         return stream(Arguments.of(DEFAULT_BUFFER_SIZE),
                       Arguments.of(4096),
                       Arguments.of(1),
                       Arguments.of(2),
                       Arguments.of(4),
                       Arguments.of(-1));
      }

   }

   private void assertThatEquals(String source,
                                 TokenReplacerFactory replacer,
                                 int bufferSize,
                                 int readLength,
                                 String expected) throws IOException {
      try (GeneralReplacerReader reader = new GeneralReplacerReader(new StringReader(source),
                                                                    replacer,
                                                                    bufferSize)) {
         assertThat(readerToString(reader, readLength)).isEqualTo(expected);
      }
   }

   private void assertThatEquals(String source,
                                 TokenReplacerFactory replacer,
                                 int readLength,
                                 String expected) throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, readLength, expected);
   }

   private void assertThatEquals(String source, TokenReplacerFactory replacer, String expected)
         throws IOException {
      assertThatEquals(source, replacer, DEFAULT_BUFFER_SIZE, expected);
   }

   private static TokenReplacerFactory stringReplacer(String matchString, String replacementString) {
      return bs -> new StringReplacer(matchString, replacementString);
   }

   private static TokenReplacerFactory directiveReplacer(Map<String, Object> model) {
      return bs -> new DirectiveReplacer(bs, mapModelReplacement(model)).loadDirectivePlugins(alwaysTrue());
   }

   private static TokenReplacerFactory directiveReplacer() {
      return bs -> new DirectiveReplacer(bs).loadDirectivePlugins(alwaysTrue());
   }

   private static TokenReplacerFactory directiveReplacer(ErrorMode errorMode) {
      return bs -> new DirectiveReplacer(bs).errorMode(errorMode).loadDirectivePlugins(alwaysTrue());
   }

   private static TokenReplacerFactory directiveReplacer(String directiveBegin,
                                                         String directiveEnd,
                                                         String directiveSuppressWhitespace,
                                                         Map<String, Object> model) {
      return bs -> new DirectiveReplacer(bs, mapModelReplacement(model))
            .directiveTokens(directiveBegin, directiveEnd, directiveSuppressWhitespace)
            .loadDirectivePlugins(alwaysTrue());
   }

   private static TokenReplacerFactory directiveReplacer(String placeholderBegin,
                                                         String placeholderEnd,
                                                         String directiveBegin,
                                                         String directiveEnd,
                                                         String directiveSuppressWhitespace,
                                                         Map<String, Object> model) {
      return bs -> new DirectiveReplacer(bs, placeholderBegin, placeholderEnd, mapModelReplacement(model))
            .directiveTokens(directiveBegin, directiveEnd, directiveSuppressWhitespace)
            .loadDirectivePlugins(alwaysTrue());
   }

}