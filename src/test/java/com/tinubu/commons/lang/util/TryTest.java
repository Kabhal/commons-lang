/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.assertj.core.api.InstanceOfAssertFactories.type;

import java.io.IOException;
import java.nio.file.ClosedFileSystemException;
import java.util.concurrent.Callable;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.util.Try.ExceptionFailure;
import com.tinubu.commons.lang.util.Try.Failure;
import com.tinubu.commons.lang.util.Try.Success;

public class TryTest {

   @Test
   public void successWhenNominal() {
      Success<Integer, String> result = Success.of(42);

      assertThat(result.value()).isEqualTo(42);
   }

   @Test
   public void successWhenNull() {
      Success<Integer, String> result = Success.of(null);

      assertThat(result.value()).isNull();
   }

   @Test
   public void failureWhenNominal() {
      Failure<Integer, Integer> result = Failure.of(-1);

      assertThat(result.reason()).isEqualTo(-1);
      assertThat(result.reasonString()).isEqualTo("-1");
   }

   @Test
   public void failureWhenNull() {
      assertThatNullPointerException()
            .isThrownBy(() -> Failure.of(null))
            .withMessage("'reason' must not be null");
   }

   @Test
   public void handleExceptionWhenSuccess() {
      Try<Integer, Exception> evaluate = Try.ofThrownBy(() -> 42);

      assertThat(evaluate).asInstanceOf(type(Success.class)).satisfies(success -> {
         assertThat(success).isEqualTo(Success.of(42));
      });

   }

   @Test
   public void handleExceptionWhenNullSuccess() {
      Try<Integer, Exception> evaluate = Try.ofThrownBy(() -> null);

      assertThat(evaluate).asInstanceOf(type(Success.class)).satisfies(success -> {
         assertThat(success).isEqualTo(Success.of(null));
      });

   }

   @Test
   public void handleExceptionWhenFailure() {
      IllegalStateException error = new IllegalStateException("Error");

      Try<Integer, Exception> evaluate = Try.ofThrownBy(() -> {
         throw error;
      });

      assertThat(evaluate).asInstanceOf(type(ExceptionFailure.class)).satisfies(failure -> {
         assertThat(failure).isEqualTo(ExceptionFailure.of(error));
         assertThat(failure.reason()).isEqualTo(error);
         assertThat(failure.reasonString()).isEqualTo("Error");
      });
   }

   @Test
   public void handleExceptionWhenThrowable() {
      assertThatExceptionOfType(Throwable.class).isThrownBy(() -> Try.ofThrownBy(() -> {
         try {
            throw new Throwable("Error");
         } catch (Throwable e) {
            throw sneakyThrow(e);
         }
      })).withMessage("Error");
   }

   @Test
   public void handleExceptionWhenError() {
      assertThatExceptionOfType(Error.class).isThrownBy(() -> Try.ofThrownBy(() -> {
         throw new Error("Error");
      })).withMessage("Error");
   }

   @Test
   public void handleExceptionWithFilteredExceptionWhenExactException() throws Exception {
      IllegalStateException error = new IllegalStateException("Error");

      Try<Integer, IllegalStateException> evaluate = Try.ofThrownBy((Callable<Integer>) () -> {
         throw error;
      }, IllegalStateException.class);

      assertThat(evaluate).asInstanceOf(type(ExceptionFailure.class)).satisfies(failure -> {
         assertThat(failure).isEqualTo(ExceptionFailure.of(error));
         assertThat(failure.reason()).isEqualTo(error);
         assertThat(failure.reasonString()).isEqualTo("Error");
      });
   }

   @Test
   public void handleExceptionWithFilteredExceptionWhenChildException() {
      IllegalStateException error = new IllegalStateException("Error");

      assertThatIllegalStateException().isThrownBy(() -> Try.ofThrownBy((Runnable) () -> {
         throw error;
      }, ClosedFileSystemException.class)).withMessage("Error");
   }

   @Test
   public void handleExceptionWithFilteredExceptionWhenParentException() throws Exception {
      IllegalStateException error = new IllegalStateException("Error");

      Try<Integer, RuntimeException> evaluate = Try.ofThrownBy((Callable<Integer>) () -> {
         throw error;
      }, RuntimeException.class);

      assertThat(evaluate).asInstanceOf(type(ExceptionFailure.class)).satisfies(failure -> {
         assertThat(failure).isEqualTo(ExceptionFailure.of(error));
         assertThat(failure.reason()).isEqualTo(error);
         assertThat(failure.reasonString()).isEqualTo("Error");
      });
   }

   @Test
   public void mapWhenNominal() {
      assertThat(success(42).map(v -> v + 1)).isEqualTo(Success.of(43));
      assertThat(failure("error").map(v -> v + 1)).isEqualTo(Failure.of("error"));
   }

   @Test
   public void mapWhenNull() {
      assertThat(success(null).map(v -> null)).isEqualTo(Success.of(null));
      assertThat(success(null).map(v -> 42)).isEqualTo(Success.of(42));
      assertThat(success(42).map(v -> null)).isEqualTo(Success.of(null));
      assertThat(failure("error").map(v -> null)).isEqualTo(Failure.of("error"));
   }

   @Test
   public void flatMapWhenNominal() {
      assertThat(success(42).flatMap(v -> Success.of(v + 1))).isEqualTo(Success.of(43));
      assertThat(failure("error").flatMap(v -> Success.of(v + 1))).isEqualTo(Failure.of("error"));
   }

   @Test
   public void flatMapWhenMapToFailure() {
      assertThat(success(42).flatMap(v -> failure("map error"))).isEqualTo(Failure.of("map error"));
      assertThat(failure("error").flatMap(v -> failure("map error"))).isEqualTo(Failure.of("error"));
   }

   @Test
   public void flatMapWhenNull() {
      assertThatNullPointerException().isThrownBy(() -> success(null).flatMap(v -> null));
      assertThatNullPointerException().isThrownBy(() -> success(42).flatMap(v -> null));
      assertThat(success(null).flatMap(v -> success(42))).isEqualTo(Success.of(42));
      assertThat(failure("error").flatMap(v -> null)).isEqualTo(Failure.of("error"));
   }

   @Test
   public void orElseGetWithSupplierWhenNominal() {
      assertThat(success(42).orElseGet(() -> {
         throw new IllegalStateException("must not be called");
      })).isEqualTo(42);
      assertThat(failure("error").orElseGet(() -> 43)).isEqualTo(43);
   }

   @Test
   public void orElseGetWithSupplierWhenNull() {
      assertThat(success(null).orElseGet(() -> 42)).isEqualTo(null);
      assertThat(failure("error").orElseGet(() -> null)).isEqualTo(null);
   }

   @Test
   public void orElseGetWithMapperWhenNominal() {
      assertThat(success(42).orElseGet(r -> {
         throw new IllegalStateException("must not be called");
      })).isEqualTo(42);
      assertThat(failure("error").orElseGet(r -> 43)).isEqualTo(43);
   }

   @Test
   public void orElseGetWithMapperWhenNull() {
      assertThat(success(null).orElseGet(r -> 42)).isEqualTo(null);
      assertThat(failure("error").orElseGet(r -> null)).isEqualTo(null);
   }

   @Test
   public void orElseWhenNominal() {
      assertThat(success(42).orElse(43)).isEqualTo(42);
      assertThat(failure("error").orElse(43)).isEqualTo(43);
   }

   @Test
   public void orElseWhenNull() {
      assertThat(success(null).orElse(null)).isEqualTo(null);
      assertThat(success(null).orElse(42)).isEqualTo(null);
      assertThat(success(42).orElse(null)).isEqualTo(42);
      assertThat(failure("error").orElse(null)).isEqualTo(null);
   }

   @Test
   public void orElseThrowWhenNominal() {
      assertThat(success(42).orElseThrow()).isEqualTo(42);
      assertThatIllegalStateException().isThrownBy(() -> failure("error").orElseThrow()).withMessage("error");
   }

   @Test
   public void orElseThrowWhenExceptionFailure() {
      assertThatExceptionOfType(IllegalStateException.class)
            .isThrownBy(() -> Failure.of(new IOException("Error")).orElseThrow())
            .withMessage("java.io.IOException: Error")
            .withCause(new IOException("Error"));
   }

   @Test
   public void orElseThrowWhenRuntimeExceptionFailure() {
      assertThatExceptionOfType(IllegalArgumentException.class)
            .isThrownBy(() -> Failure.of(new IllegalArgumentException("Error")).orElseThrow())
            .withMessage("Error");
   }

   @Test
   public void orElseThrowWhenHandleExceptionFailure() {
      assertThatIllegalArgumentException().isThrownBy(() -> Try.ofThrownBy(() -> {
         throw new IllegalArgumentException("Error");
      }).orElseThrow()).withMessage("Error");
   }

   @Test
   public void orElseThrowWithExceptionWhenNominal() {
      assertThat(success(42).orElseThrow(r -> {
         throw new IllegalStateException("must not be called");
      })).isEqualTo(42);
      assertThatIllegalArgumentException()
            .isThrownBy(() -> failure("error").orElseThrow(IllegalArgumentException::new))
            .withMessage("error");
   }

   @Test
   public void orElseThrowWithExceptionWhenNull() {
      assertThatNullPointerException().isThrownBy(() -> failure("error").orElseThrow(r -> null));
   }

   @Test
   public void peekWhenNominal() {
      assertThat(success(42).peek(v -> assertThat(v).isEqualTo(42))).isEqualTo(Success.of(42));
      assertThat(failure("error").peek(v -> {
         throw new IllegalStateException("must not be called");
      })).isEqualTo(Failure.of("error"));
   }

   @Test
   public void peekWhenNull() {
      assertThat(success(null).peek(v -> assertThat(v).isNull())).isEqualTo(Success.of(null));
   }

   @Test
   public void peekFailureWhenNominal() {
      assertThat(success(42).peekFailure(r -> {
         throw new IllegalStateException("must not be called");
      })).isEqualTo(Success.of(42));
      assertThat(failure("error").peekFailure(r -> {
         assertThat(r).isEqualTo("error");
      })).isEqualTo(Failure.of("error"));
   }

   @Test
   public void peekFailureWhenNull() {
      assertThat(success(null).peekFailure(r -> {
         throw new IllegalStateException("must not be called");
      })).isEqualTo(Success.of(null));
   }

   @Test
   public void ifSuccessWhenNominal() {
      success(42).ifSuccess(v -> assertThat(v).isEqualTo(42));
      failure("error").ifSuccess(v -> {
         throw new IllegalStateException("must not be called");
      });
   }

   @Test
   public void ifSuccessOrElseWhenNominal() {
      success(42).ifSuccessOrElse(v -> assertThat(v).isEqualTo(42), r -> {
         throw new IllegalStateException("must not be called");
      });
      failure("error").ifSuccessOrElse(v -> {
         throw new IllegalStateException("must not be called");
      }, r -> assertThat(r).isEqualTo("error"));
   }

   @Test
   public void orWhenNominal() {
      assertThat(success(42).or(() -> success(43))).isEqualTo(Success.of(42));
      assertThat(failure("error").or(() -> success(43))).isEqualTo(Success.of(43));
   }

   @Test
   public void orWhenNull() {
      assertThat(success(42).or(() -> {
         throw new IllegalStateException("must not be called");
      })).isEqualTo(Success.of(42));
      assertThatNullPointerException().isThrownBy(() -> failure("error").or(() -> null));
   }

   @Test
   public void orWhenOrFailure() {
      assertThat(success(42).or(() -> failure("or error"))).isEqualTo(Success.of(42));
      assertThat(failure("error").or(() -> failure("or error"))).isEqualTo(Failure.of("or error"));
   }

   @Test
   public void streamWhenNominal() {
      assertThat(success(42).stream()).containsExactly(42);
      assertThat(failure("error").stream()).isEmpty();
   }

   @Test
   public void streamWhenNull() {
      assertThat(success(null).stream()).containsExactly((Integer) null);
   }

   @Test
   public void optionalWhenNominal() {
      assertThat(success(42).optional()).hasValue(42);
      assertThat(failure("error").optional()).isEmpty();
   }

   @Test
   public void optionalWhenNull() {
      assertThat(success(null).optional()).isEmpty();
   }

   private static Success<Integer, String> success(Integer value) {
      return Success.of(value);
   }

   private static Failure<Integer, String> failure(String reason) {
      return Failure.of(reason);
   }

}