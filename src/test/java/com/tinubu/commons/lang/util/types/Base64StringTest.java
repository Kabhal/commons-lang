/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util.types;

import static com.tinubu.commons.lang.util.types.Base64String.MAX_BASE64_DISPLAY_LENGTH;
import static com.tinubu.commons.lang.util.types.Base64String.encode;
import static com.tinubu.commons.lang.util.types.Base64String.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

import java.io.ByteArrayOutputStream;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

public class Base64StringTest {

   private byte[] PAYLOAD = new byte[] { 0, 1, 2, 3, 4 };
   private String BASE64_PAYLOAD = "AAECAwQ=";

   @Test
   public void testOfWhenNominal() {
      assertThat(of(BASE64_PAYLOAD)).satisfies(b -> {
         assertThat(b.value()).isEqualTo(BASE64_PAYLOAD);
      });
   }

   @Test
   public void testOfWhenInvalidBase64() {
      assertThatIllegalArgumentException()
            .isThrownBy(() -> of("(;+"))
            .withMessage("Can't parse '(;+' Base64 format");
   }

   @Test
   public void testOfWhenNull() {
      assertThat(of(null)).satisfies(b -> {
         assertThat(b.value()).isNull();
      });
   }

   @Test
   public void testOfWhenNoPadding() {
      assertThat(of("AAECAwQ")).satisfies(b -> {
         assertThat(b.value()).isEqualTo("AAECAwQ");
         assertThat(b.decode()).isEqualTo(PAYLOAD);
      });
   }

   @Test
   public void testEncodeWhenNominal() {
      assertThat(encode(PAYLOAD)).satisfies(b -> {
         assertThat(b.value()).isEqualTo(BASE64_PAYLOAD);
      });
   }

   @Test
   public void testEncodeWhenNull() {
      assertThat(encode(null)).satisfies(b -> {
         assertThat(b.value()).isNull();
      });
   }

   @Test
   public void testDecodeWhenNominal() {
      assertThat(encode(PAYLOAD)).satisfies(b -> {
         assertThat(b.value()).isEqualTo(BASE64_PAYLOAD);
         assertThat(b.decode()).isEqualTo(PAYLOAD);
      });
   }

   @Test
   public void testDecodeWhenNull() {
      assertThat(encode(null)).satisfies(b -> {
         assertThat(b.value()).isNull();
         assertThat(b.decode()).isNull();
      });
   }

   @Test
   public void testEqualsWhenNominal() {
      assertThat(encode(new byte[] { 0, 1, 2, 3, 4 })).isEqualTo(encode(new byte[] {
            0, 1, 2, 3, 4 }));
      assertThat(encode(new byte[] {
            1, 2, 3, 4, 0 })).isNotEqualTo(encode(new byte[] {
            0, 1, 2, 3, 4 }));
   }

   @Test
   public void testEqualsWhenNull() {
      assertThat(encode(null)).isNotEqualTo(encode(null));
   }

   @Test
   public void testToStringWhenNominal() {
      assertThat(of(BASE64_PAYLOAD)).satisfies(b -> {
         assertThat(b.toString()).isEqualTo("Base64String[value='AAECAwQ=']");
      });
   }

   @Test
   public void testToStringWhenNull() {
      assertThat(of(null)).satisfies(b -> {
         assertThat(b.toString()).isEqualTo("Base64String[value='null']");
      });
   }

   @Test
   public void testToStringWhenValueTooLong() {
      assertThat(encode(IntStream
                              .range(0, 150)
                              .mapToObj(i -> (byte) i)
                              .collect(ByteArrayOutputStream::new,
                                       ByteArrayOutputStream::write,
                                       (ba1, ba2) -> ba1.writeBytes(ba2.toByteArray()))
                              .toByteArray())).satisfies(b -> {
         String partialBase64 =
               "AAECAwQFBgcICQoLDA0ODxAREhMUFRYXGBkaGxwdHh8gISIjJCUmJygpKissLS4vMDEyMzQ1Njc4OTo7PD0+P0BBQkNERUZHSElK";
         assertThat(partialBase64.length()).isEqualTo(MAX_BASE64_DISPLAY_LENGTH);
         assertThat(b.toString()).isEqualTo("Base64String[value='" + partialBase64 + "...']");
      });
   }

}