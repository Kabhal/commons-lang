/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.XCheckedFunction.checkedFunction;
import static com.tinubu.commons.lang.util.XCheckedFunction.identity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.function.Function;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class XCheckedFunctionTest {

   @Nested
   public class InferredCreation {

      @Test
      public void checkedFunctionWhenAnonymousClass() {
         new XCheckedFunction<Integer, Integer, IOException>() {
            @Override
            public Integer applyChecked(Integer i) throws FileNotFoundException {
               return i + 1;
            }
         }.apply(0);
      }

      @Test
      public void checkedFunctionWhenUncheckedLambda() {
         XCheckedFunction<Integer, Integer, RuntimeException> inferred =
               checkedFunction((Integer a) -> a + 1);

         XCheckedFunction<Integer, Integer, Exception> inferredExtendException =
               checkedFunction((Integer a) -> a + 1);

         XCheckedFunction<Integer, Integer, RuntimeException> inferredSuperInput =
               checkedFunction((Number a) -> a.intValue() + 1);

         XCheckedFunction<Integer, Number, RuntimeException> inferredExtendReturn =
               checkedFunction((Integer a) -> a + 1);
      }

      /**
       * Sadly, the current design supports these incoherent types assignments. This is due to the fact that
       * unchecked exceptions do not need to be explicitly declared, so the compiler allows them to be handled
       * less strictly in generic type inference.
       */
      @Test
      public void checkedFunctionWhenUncheckedFunctionInferredAsAnyException() {
         XCheckedFunction<Integer, Integer, IllegalArgumentException> inferredLambdaWithAnyException =
               checkedFunction((Integer a) -> a + 1);

         XCheckedFunction<Integer, Integer, IllegalArgumentException> inferredLambdaWithMismatchingUnchecked =
               checkedFunction((Integer a) -> {throw new IllegalStateException("error");});

         XCheckedFunction<Integer, Integer, IOException> inferredLambdaWithMismatchingChecked =
               checkedFunction((Integer a) -> {throw new IllegalStateException("error");});

         CheckedFunction<Integer, Integer> checkedFunction = __ -> {throw new IOException("error");};

         XCheckedFunction<Integer, Integer, IllegalArgumentException>
               inferredFunctionWithMismatchingUnchecked =
               checkedFunction((Function<Integer, Integer>) checkedFunction);

         new XCheckedFunction<Integer, Integer, IOException>() {
            @Override
            public Integer applyChecked(Integer integer) throws IllegalStateException {
               return null;
            }
         };

      }

      @Test
      public void checkedFunctionWhenCheckedLambda() {
         XCheckedFunction<?, ?, Exception> inferred = checkedFunction(a -> {
            throw new Exception("error");
         });

         XCheckedFunction<?, ?, Exception> inferredMultipleThrows = checkedFunction(a -> {
            if (true) {
               throw new IOException("io error");
            } else {
               throw new ClassNotFoundException("class error");
            }
         });
      }

      @Test
      public void checkedFunctionWhenSubExceptionCheckedLambda() {
         XCheckedFunction<?, ?, IOException> inferred = checkedFunction(a -> {
            throw new IOException("error");
         });

         XCheckedFunction<?, ?, IOException> inferredSuperType = checkedFunction(a -> {
            throw new FileNotFoundException("error");
         });
      }

      @Test
      public void checkedFunctionWhenFunction() {
         XCheckedFunction<Integer, Integer, RuntimeException> inferred =
               checkedFunction((Function<Integer, Integer>) a -> a + 1);
      }

      @Test
      public void checkedFunctionWhenFunctionInstanceOfCheckedFunction() {
         CheckedFunction<Integer, Integer> checkedFunction1 = a -> a + 1;

         XCheckedFunction<Integer, Integer, RuntimeException> inferred1 =
               checkedFunction((Function<Integer, Integer>) checkedFunction1);

         assertThat(inferred1).isNotSameAs(checkedFunction1);

         CheckedFunction<Integer, Integer> checkedFunction2 = __ -> {throw new IOException("error");};

         XCheckedFunction<Integer, Integer, UncheckedIOException> inferred2 =
               checkedFunction((Function<Integer, Integer>) checkedFunction2);

         assertThatThrownBy(() -> inferred2.applyChecked(0))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void checkedFunctionWhenFunctionInstanceOfXCheckedFunction() {
         XCheckedFunction<Integer, Integer, ?> checkedFunction1 = a -> a + 1;

         XCheckedFunction<Integer, Integer, RuntimeException> inferred =
               checkedFunction((Function<Integer, Integer>) checkedFunction1);

         assertThat(inferred).isNotSameAs(checkedFunction1);

         XCheckedFunction<Integer, Integer, ?> checkedFunction2 = __ -> {throw new IOException("error");};

         XCheckedFunction<Integer, Integer, RuntimeException> inferred2 =
               checkedFunction((Function<Integer, Integer>) checkedFunction2);

         assertThatThrownBy(() -> inferred2.applyChecked(0))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void checkedFunctionWhenCheckedFunction() {
         XCheckedFunction<Integer, Integer, Exception> inferred =
               checkedFunction((CheckedFunction<Integer, Integer>) a -> a + 1);
         XCheckedFunction<Integer, Integer, Exception> inferred2 =
               checkedFunction((CheckedFunction<Integer, Integer>) (__ -> {throw new IOException("error");}));
      }

      @Test
      public void uncheckedWhenRuntimeThrow() {
         XCheckedFunction<Integer, Integer, Exception> checkedFunction =
               __ -> {throw new ClassNotFoundException("error");};

         assertThatThrownBy(() -> checkedFunction.unchecked(false).apply(0))
               .isInstanceOf(UncheckedException.class)
               .hasMessage("error")
               .hasCause(new ClassNotFoundException("error"));
      }

      @Test
      public void uncheckedWhenIOExceptionRuntimeThrow() {
         XCheckedFunction<Integer, Integer, IOException> checkedFunction =
               __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedFunction.unchecked(false).apply(0))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void uncheckedWhenSneakyThrow() {
         XCheckedFunction<Integer, Integer, IOException> checkedFunction =
               __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedFunction.unchecked(true).apply(0))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

      @Test
      public void autoUncheckedWhenUsedAsFunction() {
         XCheckedFunction<Integer, Object, IOException> error = __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.of(3).map(error))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void forceUncheckedWhenUsedAsFunction() {
         XCheckedFunction<Integer, Object, IOException> error = __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.of(3).map(error.unchecked(false)))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
         assertThatThrownBy(() -> Optional.of(3).map(error.unchecked(true)))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

   }

   @Nested
   @SuppressWarnings("unchecked")
   public class TryFinally {

      @Test
      public void tryFinallyWhenNominal() {
         assertThat(checkedFunction(__ -> "Value").tryFinally().applyChecked(0)).isEqualTo("Value");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new InternalError("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new Error("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new ClassNotFoundException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new Exception("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new IllegalStateException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new RuntimeException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new IOException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedFunction(__ -> "Value", Exception.class)
                          .tryFinally(finalizer1)
                          .applyChecked(0)).isEqualTo("Value");
         verify(finalizer1).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedFunction(__ -> "Value", Exception.class)
                          .tryFinally(finalizer1, finalizer2)
                          .applyChecked(0)).isEqualTo("Value");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new FileNotFoundException("Operation error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).applyChecked(0))
               .withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new InternalError("Operation error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).applyChecked(0))
               .withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedFunction(__ -> "Value", Exception.class)
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> "Value", Exception.class)
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new IllegalStateException("Function error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new Error("Function error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new Error("Function error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWithApplyUncheckedWhenNominal() {
         assertThat(checkedFunction(__ -> "Value").tryFinally().apply(0)).isEqualTo("Value");
      }

      @Test
      public void tryFinallyWithApplyUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new IllegalStateException("Function error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).apply(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Nested
   public class TryCatch {

      @Test
      public void tryCatchWhenNominal() {
         assertThat(checkedFunction(__ -> "Value").tryCatch(() -> {
            throw new IllegalStateException();
         }).applyChecked(0)).isEqualTo("Value");
      }

      @Test
      public void tryCatchWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new InternalError("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new Error("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new ClassNotFoundException("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new Exception("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new IllegalStateException("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new RuntimeException("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new IOException("Operation error");
         }).tryCatch(() -> {}).applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedFunction(__ -> "Value").tryCatch(finalizer1).applyChecked(0)).isEqualTo("Value");
         verify(finalizer1, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedFunction(__ -> "Value")
                          .tryCatch(finalizer1, finalizer2)
                          .applyChecked(0)).isEqualTo("Value");

         verify(finalizer1, never()).runChecked();
         verify(finalizer2, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryCatch(finalizer1, finalizer2).applyChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new InternalError("Operation error");
         }).tryCatch(finalizer1, finalizer2).applyChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> XCheckedFunction.<Integer, Integer, Exception>checkedFunction(__ -> {
                  throw new IllegalStateException("error");
               }).tryCatch(finalizer1, finalizer2, finalizer3).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isOfAnyClassIn(Error.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> {throw new IllegalStateException("error");})
                     .tryCatch(finalizer1, finalizer2, finalizer3)
                     .applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isExactlyInstanceOf(IllegalStateException.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new IllegalStateException("Function error");
               }).tryCatch(finalizer1, finalizer2).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new Error("Function error");
               }).tryCatch(finalizer1, finalizer2).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new Error("Function error");
               }).tryCatch(finalizer1, finalizer2).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThat(XCheckedFunction
                          .<Object, String, Exception>checkedFunction(__ -> "Value")
                          .tryCatch(IOException.class, finalizer)
                          .applyChecked(0)).isEqualTo("Value");

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithException() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new IOException("Operation error");
         }).tryCatch(IOException.class, finalizer).applyChecked(0)).withMessage("Operation error");

         verify(finalizer).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithSubException() throws Exception {
         CheckedConsumer<FileNotFoundException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(FileNotFoundException failure) {
            }
         });

         XCheckedFunction<Object, Object, IOException> function = checkedFunction(__ -> {
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> function.tryCatch(FileNotFoundException.class, finalizer).applyChecked(0))
               .withMessage("Operation error");

         verify(finalizer).acceptChecked(any(FileNotFoundException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithMismatchingSubException() throws Exception {
         CheckedConsumer<EOFException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(EOFException failure) {
            }
         });

         XCheckedFunction<Object, Object, IOException> function = checkedFunction(__ -> {
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> function.tryCatch(EOFException.class, finalizer).applyChecked(0))
               .withMessage("Operation error");

         verify(finalizer, never()).acceptChecked(any(EOFException.class));
      }

      @Test
      public void tryCatchWithExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new IllegalStateException("Function error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).applyChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithApplyUncheckedWhenNominal() {
         assertThat(checkedFunction(__ -> "Value").tryCatch(() -> {
            throw new IllegalStateException();
         }).apply(0)).isEqualTo("Value");
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithApplyUncheckedExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThat(XCheckedFunction
                          .<Object, String, Exception>checkedFunction(__ -> "Value")
                          .tryCatch(IOException.class, finalizer)
                          .apply(0)).isEqualTo("Value");

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      public void tryCatchWithApplyUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new IllegalStateException("Function error");
               }).tryCatch(finalizer1, finalizer2).apply(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithApplyUncheckedAndExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(__ -> {
                  throw new IllegalStateException("Function error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).apply(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Nested
   public class AutoCloseAutoCloseable {

      @Test
      public void testAutoCloseAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable = XCheckedFunction
               .<AutoCloseable, Exception>identity()
               .autoClose()
               .applyChecked(closeableStream);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedFunction(s -> {
            throw new Error("Function error");
         }).autoClose().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedFunction(s -> {
            throw new ClassNotFoundException("Function error");
         }).autoClose().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedFunction(s -> {
            throw new IllegalStateException("Function error");
         }).autoClose().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedFunction(s -> {
            throw new IOException("Function error");
         }).autoClose().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> identity().autoClose().applyChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> identity().autoClose().applyChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> identity().autoClose().applyChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> identity().autoClose().applyChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new InternalError("Function error");
               }).autoClose().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new ClassNotFoundException("Function error");
               }).autoClose().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new IllegalStateException("Function error");
               }).autoClose().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new IOException("Function error");
               }).autoClose().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseWithApplyUncheckedAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable =
               XCheckedFunction.<AutoCloseable, Exception>identity().autoClose().apply(closeableStream);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseWithApplyUncheckedAutoCloseableWhenOperationAndCloseException()
            throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(UncheckedException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new ClassNotFoundException("Function error");
               }).autoClose().apply(closeableStream))
               .withMessage("Function error")
               .withCauseInstanceOf(ClassNotFoundException.class)
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class AutoCloseOnThrowAutoCloseable {

      @Test
      public void testApplyAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable = XCheckedFunction
               .<AutoCloseable, Exception>identity()
               .autoCloseOnThrow()
               .applyChecked(closeableStream);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedFunction(s -> {
            throw new Error("Function error");
         }).autoCloseOnThrow().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedFunction(s -> {
            throw new ClassNotFoundException("Function error");
         }).autoCloseOnThrow().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedFunction(s -> {
            throw new IllegalStateException("Function error");
         }).autoCloseOnThrow().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedFunction(s -> {
            throw new IOException("Function error");
         }).autoCloseOnThrow().applyChecked(closeableStream)).withMessage("Function error");

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         identity().autoCloseOnThrow().applyChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         identity().autoCloseOnThrow().applyChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         identity().autoCloseOnThrow().applyChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         identity().autoCloseOnThrow().applyChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new InternalError("Function error");
               }).autoCloseOnThrow().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new ClassNotFoundException("Function error");
               }).autoCloseOnThrow().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new IllegalStateException("Function error");
               }).autoCloseOnThrow().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWhenOperationAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new IOException("Function error");
               }).autoCloseOnThrow().applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWithExceptionClassWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         XCheckedFunction<Object, Object, IOException> function = checkedFunction(s -> {
            throw new FileNotFoundException("Function error");
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> function
                     .autoCloseOnThrow(FileNotFoundException.class)
                     .applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyAutoCloseableWithMismatchingExceptionClassWhenOperationAndCloseException()
            throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         XCheckedFunction<Object, Object, IOException> function = checkedFunction(s -> {
            throw new FileNotFoundException("Function error");
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> function
                     .autoCloseOnThrow(FileNotFoundException.class)
                     .applyChecked(closeableStream))
               .withMessage("Function error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testApplyWithApplyUncheckedAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         AutoCloseable closeable = XCheckedFunction
               .<AutoCloseable, Exception>identity()
               .autoCloseOnThrow()
               .apply(closeableStream);
         assertThat(closeable).isEqualTo(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testApplyWithApplyUncheckedAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(UncheckedException.class)
               .isThrownBy(() -> checkedFunction(s -> {
                  throw new ClassNotFoundException("Function error");
               }).autoCloseOnThrow().apply(closeableStream))
               .withMessage("Function error")
               .withCauseInstanceOf(ClassNotFoundException.class)
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

}