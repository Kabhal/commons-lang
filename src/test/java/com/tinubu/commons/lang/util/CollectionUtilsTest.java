/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.collection;
import static com.tinubu.commons.lang.util.CollectionUtils.collectionConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.immutable;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.CollectionUtils.synchronizedCollection;
import static com.tinubu.commons.lang.util.CollectionUtils.synchronizedList;
import static com.tinubu.commons.lang.util.CollectionUtils.synchronizedMap;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;
import static java.util.Collections.emptySet;
import static java.util.Collections.emptySortedMap;
import static java.util.Collections.emptySortedSet;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static java.util.Collections.synchronizedSet;
import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableList;
import static java.util.Collections.unmodifiableMap;
import static java.util.Collections.unmodifiableNavigableMap;
import static java.util.Collections.unmodifiableNavigableSet;
import static java.util.Collections.unmodifiableSet;
import static java.util.Collections.unmodifiableSortedMap;
import static java.util.Collections.unmodifiableSortedSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.InstanceOfAssertFactories.type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.util.typefactory.DefaultTypeFactoryRegistry;
import com.tinubu.commons.lang.util.typefactory.TypeFactory;

class CollectionUtilsTest {

   @BeforeEach
   public void resetFactoryRegistry() {
      DefaultTypeFactoryRegistry.instance().resetRegistry();
   }

   @Nested
   public class TestCollectionWithStream {

      @Test
      public void testCollectionWhenNominal() {
         Collection<Integer> collection = collection(ArrayList::new, Stream.of(1, 2));

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testCollectionWhenNull() {
         Collection<Integer> collection = collection(ArrayList::new, (Stream<Integer>) null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).isEmpty();
      }

      @Test
      public void testCollectionConcatWhenNominal() {
         Collection<Integer> collection = collectionConcat(ArrayList::new, Stream.of(1, 2), Stream.of(3, 4));

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2, 3, 4);
      }

      @Test
      public void testCollectionConcatWhenNull() {
         Collection<Integer> collection = collectionConcat(ArrayList::new, (Stream<Integer>[]) null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).isEmpty();
      }

      @Test
      public void testCollectionConcatWhenNullStream() {
         Collection<Integer> collection = collectionConcat(ArrayList::new, Stream.of(1, 2), null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testCollectionConcatWhenNullStreamElement() {
         Collection<Integer> collection = collectionConcat(ArrayList::new, Stream.of(1, null), null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, null);
      }

   }

   @Nested
   public class TestCollection {

      @Test
      public void testCollectionWhenNominal() {
         Collection<Integer> collection = collection(ArrayList::new, Arrays.asList(1, 2));

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testCollectionWhenNull() {
         Collection<Integer> collection = collection(ArrayList::new, (Collection<Integer>) null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).isEmpty();
      }

      @Test
      public void testCollectionWhenHashSetFactory() {
         Set<Integer> collection = collection(HashSet::new, Arrays.asList(1, 2, 2));

         assertThat(collection).isInstanceOf(HashSet.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testCollectionWithMultiElementsWhenNominal() {
         Collection<Integer> collection = collection(ArrayList::new, 3, 4, 1, 2);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(3, 4, 1, 2);
      }

      @Test
      public void testCollectionWithMultiElementsWhenNull() {
         Collection<Integer> collection = collection(ArrayList::new, (Integer[]) null);

         assertThat(collection).isEmpty();
      }

      @Test
      public void testCollectionWithMultiElementsWhenNullElement() {
         Collection<Integer> collection = collection(ArrayList::new, (Integer) null, 1, 2, null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(null, 1, 2, null);
      }

      @Nested
      public class CollectionConcat {

         @Test
         public void testCollectionConcatWhenNominal() {
            Collection<Integer> collection =
                  collectionConcat(ArrayList::new, Arrays.asList(1, 2), Arrays.asList(3, 4));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2, 3, 4);
         }

         @Test
         public void testCollectionConcatWhenNull() {
            Collection<Integer> collection = collectionConcat(ArrayList::new, (Collection<Integer>[]) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionConcatWhenNoCollection() {
            @SuppressWarnings("unchecked")
            Collection<Integer> collection = collectionConcat(ArrayList::new, new ArrayList[0]);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionConcatWhenOneCollection() {
            Collection<Integer> collection = collectionConcat(ArrayList::new, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionConcatWhenNullCollection() {
            Collection<Integer> collection =
                  collectionConcat(ArrayList::new, Arrays.asList(1, 2), Arrays.asList(3, 4), null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2, 3, 4);
         }

         @Test
         public void testCollectionConcatWhenNullCollectionNoCollection() {
            Collection<Integer> collection = collectionConcat(ArrayList::new, (Collection<Integer>) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionConcatWhenNullCollectionOneCollection() {
            Collection<Integer> collection = collectionConcat(ArrayList::new, null, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionConcatWhenNullCollectionElement() {
            Collection<Integer> collection = collectionConcat(ArrayList::new, Arrays.asList(1, null), null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, null);
         }

         @Test
         public void testCollectionConcatWhenDuplicates() {
            Collection<Integer> collection = collectionConcat(ArrayList::new,
                                                              Arrays.asList(1, 1, 2, 2, null, null),
                                                              Arrays.asList(2, 2, 3, 3, null, null));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 1, 2, 2, null, null, 2, 2, 3, 3, null, null);
         }
      }

      @Nested
      public class CollectionUnion {

         @Test
         public void testCollectionUnionWhenNominal() {
            Collection<Integer> collection =
                  CollectionUtils.collectionUnion(ArrayList::new, Arrays.asList(1, 2), Arrays.asList(3, 4));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2, 3, 4);
         }

         @Test
         public void testCollectionUnionWhenNull() {
            Collection<Integer> collection =
                  CollectionUtils.collectionUnion(ArrayList::new, (Collection<Integer>[]) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionUnionWhenNoCollection() {
            @SuppressWarnings("unchecked")
            Collection<Integer> collection =
                  CollectionUtils.collectionUnion(ArrayList::new, new ArrayList[0]);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionUnionWhenOneCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionUnion(ArrayList::new, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionUnionWhenNullCollection() {
            Collection<Integer> collection = CollectionUtils.collectionUnion(ArrayList::new,
                                                                             Arrays.asList(1, 2),
                                                                             Arrays.asList(3, 4),
                                                                             null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2, 3, 4);
         }

         @Test
         public void testCollectionUnionWhenNullCollectionNoCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionUnion(ArrayList::new, (Collection<Integer>) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionUnionWhenNullCollectionOneCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionUnion(ArrayList::new, null, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionUnionWhenNullCollectionElement() {
            Collection<Integer> collection =
                  CollectionUtils.collectionUnion(ArrayList::new, Arrays.asList(1, null), null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, null);
         }

         @Test
         public void testCollectionUnionWhenDuplicates() {
            Collection<Integer> collection = CollectionUtils.collectionUnion(ArrayList::new,
                                                                             Arrays.asList(1,
                                                                                           1,
                                                                                           2,
                                                                                           2,
                                                                                           null,
                                                                                           null),
                                                                             Arrays.asList(2,
                                                                                           2,
                                                                                           3,
                                                                                           3,
                                                                                           null,
                                                                                           null));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2, null, 3);
         }
      }

      @Nested
      public class CollectionIntersection {

         @Test
         public void testCollectionIntersectionWhenNominal() {
            Collection<Integer> collection = CollectionUtils.collectionIntersection(ArrayList::new,
                                                                                    Arrays.asList(1, 2),
                                                                                    Arrays.asList(2, 3));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(2);
         }

         @Test
         public void testCollectionIntersectionWhenNull() {
            Collection<Integer> collection =
                  CollectionUtils.collectionIntersection(ArrayList::new, (Collection<Integer>[]) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionIntersectionWhenNoCollection() {
            @SuppressWarnings("unchecked")
            Collection<Integer> collection =
                  CollectionUtils.collectionIntersection(ArrayList::new, new ArrayList[0]);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionIntersectionWhenOneCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionIntersection(ArrayList::new, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionIntersectionWhenNullCollection() {
            Collection<Integer> collection = CollectionUtils.collectionIntersection(ArrayList::new,
                                                                                    Arrays.asList(1, 2),
                                                                                    Arrays.asList(2, 3),
                                                                                    null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(2);
         }

         @Test
         public void testCollectionIntersectionWhenNullCollectionNoCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionIntersection(ArrayList::new, (Collection<Integer>) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionIntersectionWhenNullCollectionOneCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionIntersection(ArrayList::new, null, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionIntersectionWhenNullCollectionElement() {
            Collection<Integer> collection = CollectionUtils.collectionIntersection(ArrayList::new,
                                                                                    Arrays.asList(1, null),
                                                                                    Arrays.asList(1, null),
                                                                                    null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, null);
         }

         @Test
         public void testCollectionIntersectionWhenDuplicates() {
            Collection<Integer> collection = CollectionUtils.collectionIntersection(ArrayList::new,
                                                                                    Arrays.asList(1,
                                                                                                  1,
                                                                                                  2,
                                                                                                  2,
                                                                                                  null,
                                                                                                  null),
                                                                                    Arrays.asList(2,
                                                                                                  2,
                                                                                                  3,
                                                                                                  3,
                                                                                                  null,
                                                                                                  null));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(2, null);
         }
      }

      @Nested
      public class CollectionSubtraction {

         @Test
         public void testCollectionSubtractionWhenNominal() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   Arrays.asList(2, 3));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1);
         }

         @Test
         public void testCollectionSubtractionWhenNull() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   (Collection<Integer>[]) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionSubtractionWhenNoCollection() {
            @SuppressWarnings("unchecked")
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   new ArrayList[0]);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionSubtractionWhenOneCollection() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   Arrays.asList(2, 3));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1);
         }

         @Test
         public void testCollectionSubtractionWhenNullCollection() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   Arrays.asList(2, 3),
                                                                                   null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1);
         }

         @Test
         public void testCollectionSubtractionWhenNullCollectionNoCollection() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   (Collection<Integer>) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionSubtractionWhenNullCollectionOneCollection() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   null,
                                                                                   Arrays.asList(2, 3));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1);
         }

         @Test
         public void testCollectionSubtractionWhenNullCollectionElementInCollection() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, null),
                                                                                   Arrays.asList(1),
                                                                                   null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly((Integer) null);
         }

         @Test
         public void testCollectionSubtractionWhenNullCollectionElementInSubstractCollections() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1),
                                                                                   Arrays.asList(1, null),
                                                                                   null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionSubtractionWhenNullCollectionElementInBothCollections() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1, null),
                                                                                   Arrays.asList(1, null),
                                                                                   null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionSubtractionWhenDuplicates() {
            Collection<Integer> collection = CollectionUtils.collectionSubtraction(ArrayList::new,
                                                                                   Arrays.asList(1,
                                                                                                 1,
                                                                                                 2,
                                                                                                 2,
                                                                                                 null,
                                                                                                 null),
                                                                                   Arrays.asList(2,
                                                                                                 2,
                                                                                                 3,
                                                                                                 3,
                                                                                                 null,
                                                                                                 null));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1);
         }
      }

      @Nested
      public class CollectionDisjunction {

         @Test
         public void testCollectionDisjunctionWhenNominal() {
            Collection<Integer> collection = CollectionUtils.collectionDisjunction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   Arrays.asList(2, 3));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 3);
         }

         @Test
         public void testCollectionDisjunctionWhenNull() {
            Collection<Integer> collection =
                  CollectionUtils.collectionDisjunction(ArrayList::new, (Collection<Integer>[]) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionDisjunctionWhenNoCollection() {
            @SuppressWarnings("unchecked")
            Collection<Integer> collection =
                  CollectionUtils.collectionDisjunction(ArrayList::new, new ArrayList[0]);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionDisjunctionWhenOneCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionDisjunction(ArrayList::new, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionDisjunctionWhenNullCollection() {
            Collection<Integer> collection = CollectionUtils.collectionDisjunction(ArrayList::new,
                                                                                   Arrays.asList(1, 2),
                                                                                   Arrays.asList(2, 3),
                                                                                   null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 3);
         }

         @Test
         public void testCollectionDisjunctionWhenNullCollectionNoCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionDisjunction(ArrayList::new, (Collection<Integer>) null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).isEmpty();
         }

         @Test
         public void testCollectionDisjunctionWhenNullCollectionOneCollection() {
            Collection<Integer> collection =
                  CollectionUtils.collectionDisjunction(ArrayList::new, null, Arrays.asList(1, 2));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCollectionDisjunctionWhenNullCollectionElement() {
            Collection<Integer> collection = CollectionUtils.collectionDisjunction(ArrayList::new,
                                                                                   Arrays.asList(1),
                                                                                   Arrays.asList(1, null),
                                                                                   null);

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly((Integer) null);
         }

         @Test
         public void testCollectionDisjunctionWhenDuplicates() {
            Collection<Integer> collection = CollectionUtils.collectionDisjunction(ArrayList::new,
                                                                                   Arrays.asList(1,
                                                                                                 1,
                                                                                                 2,
                                                                                                 2,
                                                                                                 null,
                                                                                                 null),
                                                                                   Arrays.asList(2,
                                                                                                 2,
                                                                                                 3,
                                                                                                 3,
                                                                                                 null,
                                                                                                 null));

            assertThat(collection).isInstanceOf(ArrayList.class);
            assertThat(collection).containsExactly(1, 3);
         }
      }

   }

   @Nested
   public class TestListWithStream {

      @Test
      public void testListWhenNominal() {
         List<Integer> list = list(Stream.of(1, 2));

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).containsExactly(1, 2);
      }

      @Test
      public void testListWhenNull() {
         List<Integer> list = list((Stream<Integer>) null);

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).isEmpty();
      }

      @Test
      public void testListConcatWhenNominal() {
         Collection<Integer> collection = listConcat(Stream.of(1, 2), Stream.of(3, 4));

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2, 3, 4);
      }

      @Test
      public void testListConcatWhenNull() {
         Collection<Integer> collection = listConcat((Stream<Integer>[]) null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).isEmpty();
      }

      @Test
      public void testListConcatWhenNullStream() {
         Collection<Integer> collection = listConcat(Stream.of(1, 2), null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testListConcatWhenNullStreamElement() {
         Collection<Integer> collection = listConcat(Stream.of(1, null), null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, null);
      }

   }

   @Nested
   public class TestList {

      @Test
      public void testListWhenEmpty() {
         List<Integer> list = list();

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).isEmpty();
      }

      @Test
      public void testListWhenNominal() {
         List<Integer> list = list(new LinkedList<>(Arrays.asList(1, 2)));

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).containsExactly(1, 2);
      }

      @Test
      public void testListShouldBeRecreated() {
         List<Integer> originalList = Arrays.asList(1, 2);

         assertThat(list(originalList)).isNotSameAs(originalList).isInstanceOf(ArrayList.class);
      }

      @Test
      public void testListWhenNull() {
         List<Integer> list = list((List<Integer>) null);

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).isEmpty();
      }

      @Test
      public void testListWhenCollection() {
         List<Integer> list = list(new HashSet<>(Arrays.asList(1, 2)));

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).containsExactly(1, 2);
      }

      @Test
      public void testListWhenCollectionShouldBeRecreatedIfImmutable() {
         Collection<Integer> originalListCollection = unmodifiableCollection(Arrays.asList(1, 2));

         assertThat(list(originalListCollection))
               .isNotSameAs(originalListCollection)
               .isInstanceOf(ArrayList.class);
      }

      @Test
      public void testListWithMultiElementsWhenNominal() {
         List<Integer> list = list(3, 4, 1, 2);

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).containsExactly(3, 4, 1, 2);
      }

      @Test
      public void testListWithMultiElementsWhenNull() {
         List<Integer> list = list((Integer[]) null);

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).isEmpty();
      }

      @Test
      public void testListWithMultiElementsWhenNullElement() {
         List<Integer> list = list((Integer) null, 1, 2, null);

         assertThat(list).isInstanceOf(ArrayList.class);
         assertThat(list).containsExactly(null, 1, 2, null);
      }

      @Test
      public void testListConcatWhenNominal() {
         Collection<Integer> collection = listConcat(Stream.of(1, 2), Stream.of(3, 4));

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2, 3, 4);
      }

      @Test
      public void testListConcatWhenNull() {
         Collection<Integer> collection = listConcat((Stream<Integer>[]) null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).isEmpty();
      }

      @Test
      public void testListConcatWhenNullStream() {
         Collection<Integer> collection = listConcat(Stream.of(1, 2), null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testListConcatWhenNullStreamElement() {
         Collection<Integer> collection = listConcat(Stream.of(1, null), null);

         assertThat(collection).isInstanceOf(ArrayList.class);
         assertThat(collection).containsExactly(1, null);
      }

   }

   @Nested
   public class TestMap {

      @Test
      public void testEntryWhenNominal() {
         Entry<String, Integer> entry = entry("a", 1);

         assertThat(entry).isInstanceOf(Entry.class);
         assertThat(entry.getKey()).isEqualTo("a");
         assertThat(entry.getValue()).isEqualTo(1);
      }

      @Test
      public void testEntryWhenNullKey() {
         Entry<String, Integer> entry = entry(null, 1);

         assertThat(entry).isInstanceOf(Entry.class);
         assertThat(entry.getKey()).isNull();
         assertThat(entry.getValue()).isEqualTo(1);
      }

      @Test
      public void testEntryWhenNullValue() {
         Entry<String, Integer> entry = entry("a", null);

         assertThat(entry).isInstanceOf(Entry.class);
         assertThat(entry.getKey()).isEqualTo("a");
         assertThat(entry.getValue()).isNull();
      }

      @Test
      public void testMapWhenNominal() {
         Map<String, Integer> map = map(new HashMap<String, Integer>() {{
            put("a", 1);
            put("b", 2);
         }});

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry("a", 1), entry("b", 2));
      }

      @Test
      public void testMapShouldBeRecreated() {
         Map<String, Integer> originalMap = unmodifiableMap(new HashMap<String, Integer>() {{
            put("a", 1);
            put("b", 2);
         }});

         assertThat(map(originalMap)).isNotSameAs(originalMap).isInstanceOf(HashMap.class);
      }

      @Test
      public void testMapWhenNull() {
         Map<String, Integer> map = map((Map<String, Integer>) null);

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).isEmpty();
      }

      @Test
      public void testMapWhenMapFactory() {
         Map<String, Integer> map = map(LinkedHashMap::new, new HashMap<String, Integer>() {{
            put("a", 1);
            put("b", 2);
            put("c", 3);
         }});

         assertThat(map).isInstanceOf(LinkedHashMap.class);
         assertThat(map).containsExactly(entry("a", 1), entry("b", 2), entry("c", 3));
      }

      @Test
      public void testMapWhenMergeFunction() {
         assertThat(map((v1, v2) -> v2, () -> new HashMap<>(), entry("a", 1), entry("a", 2))).containsOnly(
               entry("a", 2));
         assertThat(map((v1, v2) -> v1, () -> new HashMap<>(), entry("a", 1), entry("a", 2))).containsOnly(
               entry("a", 1));
         assertThat(map((v1, v2) -> v2, () -> new HashMap<>(), entry("a", 1), entry("a", null))).containsOnly(
               entry("a", null));
         assertThat(map((v1, v2) -> v1, () -> new HashMap<>(), entry("a", 1), entry("a", null))).containsOnly(
               entry("a", 1));
         assertThat(map((v1, v2) -> v2, () -> new HashMap<>(), entry("a", null), entry("a", 2))).containsOnly(
               entry("a", 2));
         assertThat(map((v1, v2) -> v1, () -> new HashMap<>(), entry("a", null), entry("a", 2))).containsOnly(
               entry("a", null));
      }

      @Test
      public void testMapWhenEntryNullKey() {
         Map<String, Integer> map = map(entry(null, 1), entry("b", 2));

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry(null, 1), entry("b", 2));
      }

      @Test
      public void testMapWhenOverrideEntryWithNullKey() {
         Map<String, Integer> map = map(entry(null, 1), entry(null, 2));

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry(null, 2));
      }

      @Test
      public void testMapWhenEntryNullValue() {
         Map<String, Integer> map = map(entry("a", null), entry("b", 2));

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry("a", null), entry("b", 2));
      }

      @Test
      public void testMapWhenOverrideEntryWithNullValue() {
         Map<String, Integer> map = map(entry("a", 1), entry("a", null));

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry("a", null));
      }

      @Test
      public void testMapWhenOverrideNullValueEntryWithValue() {
         Map<String, Integer> map = map(entry("a", null), entry("a", 1));

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry("a", 1));
      }

      @Test
      public void testMapWhenEmpty() {
         Map<String, Integer> map = map();

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).isEmpty();
      }
   }

   @Nested
   public class TestMapWithEntries {

      @Test
      public void testMapWithMultiEntriesWhenNominal() {
         Map<String, Integer> map = map(entry("a", 1), entry("b", 2));

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry("a", 1), entry("b", 2));
      }

      @Test
      public void testMapWithMultiEntriesWhenNull() {
         Map<String, Integer> map = map((Entry<String, Integer>[]) null);

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).isEmpty();
      }

      @Test
      public void testMapWithMultiEntriesWhenNullElement() {
         Map<String, Integer> map = map((Entry<String, Integer>) null, entry("a", 1), entry("b", 2), null);

         assertThat(map).isInstanceOf(HashMap.class);
         assertThat(map).containsOnly(entry("a", 1), entry("b", 2));
      }

   }

   @Nested
   public class TestImmutable {

      @Test
      public void testImmutableCollectionWhenNominal() {
         Collection<Integer> collection = immutable(collection(() -> new ArrayList<>(), 1, 2));

         assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
         assertThat(collection).isInstanceOf(Collection.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testImmutableCollectionWhenNull() {
         Collection<Integer> collection = immutable((Collection<Integer>) null);

         assertThatThrownBy(() -> collection.add(1)).isInstanceOf(UnsupportedOperationException.class);
         assertThat(collection).isInstanceOf(Collection.class);
         assertThat(collection).isEmpty();
      }

      @Test
      public void testImmutableCollectionWhenAlreadyImmutable() {
         assertThatImmutableCollectionIsSameAs(unmodifiableCollection(new ArrayList<>()));
         assertThatImmutableCollectionIsSameAs(unmodifiableNavigableSet(new TreeSet<>()));
         assertThatImmutableCollectionIsSameAs(emptySortedSet());
         assertThatImmutableCollectionIsSameAs(unmodifiableSet(new HashSet<>()));
         assertThatImmutableCollectionIsSameAs(unmodifiableSortedSet(new TreeSet<>()));
         assertThatImmutableCollectionIsSameAs(singleton(1));
         assertThatImmutableCollectionIsSameAs(emptySet());
         assertThatImmutableCollectionIsSameAs(Set.of());
         assertThatImmutableCollectionIsSameAs(Set.of(1));
         assertThatImmutableCollectionIsSameAs(Set.of(1, 2));
         assertThatImmutableCollectionIsSameAs(Set.of(1, 2, 3));

         assertThatImmutableCollectionIsSameAs(unmodifiableList(new LinkedList<>()));
         assertThatImmutableCollectionIsSameAs(unmodifiableList(new ArrayList<>()));
         assertThatImmutableCollectionIsSameAs(singletonList(1));
         assertThatImmutableCollectionIsSameAs(emptyList());
         assertThatImmutableCollectionIsSameAs(Arrays.asList(1, 2));
         assertThatImmutableCollectionIsSameAs(List.of());
         assertThatImmutableCollectionIsSameAs(List.of(1));
         assertThatImmutableCollectionIsSameAs(List.of(1, 2));
         assertThatImmutableCollectionIsSameAs(List.of(1, 2, 3));

         assertThatImmutableCollectionIsNotSameAs(new ArrayList<>());
         assertThatImmutableCollectionIsNotSameAs(new LinkedList<>());
         assertThatImmutableCollectionIsNotSameAs(new TreeSet<>());
         assertThatImmutableCollectionIsNotSameAs(new HashSet<>());
      }

      private void assertThatImmutableCollectionIsSameAs(Collection<Integer> collection) {
         assertThat(immutable(collection)).isSameAs(collection);
      }

      private void assertThatImmutableCollectionIsNotSameAs(Collection<Integer> collection) {
         assertThat(immutable(collection)).isNotSameAs(collection);
      }

      @Test
      public void testImmutableListWhenNominal() {
         Collection<Integer> list = immutable(list(1, 2));

         assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
         assertThat(list).isInstanceOf(Collection.class);
         assertThat(list).containsExactly(1, 2);
      }

      @Test
      public void testImmutableListWhenNull() {
         List<Integer> list = immutable((List<Integer>) null);

         assertThatThrownBy(() -> list.add(1)).isInstanceOf(UnsupportedOperationException.class);
         assertThat(list).isInstanceOf(Collection.class);
         assertThat(list).isEmpty();
      }

      @Test
      public void testImmutableListWhenAlreadyImmutable() {
         assertThatImmutableListIsSameAs(unmodifiableList(new LinkedList<>()));
         assertThatImmutableListIsSameAs(unmodifiableList(new ArrayList<>()));
         assertThatImmutableListIsSameAs(singletonList(1));
         assertThatImmutableListIsSameAs(emptyList());
         assertThatImmutableListIsSameAs(Arrays.asList(1, 2));
         assertThatImmutableListIsSameAs(List.of());
         assertThatImmutableListIsSameAs(List.of(1));
         assertThatImmutableListIsSameAs(List.of(1, 2));
         assertThatImmutableListIsSameAs(List.of(1, 2, 3));

         assertThatImmutableListIsNotSameAs(new LinkedList<>());
         assertThatImmutableListIsNotSameAs(new ArrayList<>());
      }

      private void assertThatImmutableListIsSameAs(List<Integer> collection) {
         assertThat(immutable(collection)).isSameAs(collection);
      }

      private void assertThatImmutableListIsNotSameAs(List<Integer> collection) {
         assertThat(immutable(collection)).isNotSameAs(collection);
      }

      @Test
      public void testImmutableMapWhenNominal() {
         Map<String, Integer> map = immutable(map(entry("a", 1), entry("b", 2)));

         assertThatThrownBy(() -> map.put("c", 3)).isInstanceOf(UnsupportedOperationException.class);
         assertThat(map).isInstanceOf(Map.class);
         assertThat(map).containsOnly(entry("a", 1), entry("b", 2));
      }

      @Test
      public void testImmutableMapWhenNull() {
         Map<String, Integer> map = immutable((Map<String, Integer>) null);

         assertThatThrownBy(() -> map.put("c", 3)).isInstanceOf(UnsupportedOperationException.class);
         assertThat(map).isInstanceOf(Map.class);
         assertThat(map).isEmpty();
      }

      @Test
      public void testImmutableMapWhenAlreadyImmutable() {
         assertThatImmutableMapIsSameAs(unmodifiableMap(new HashMap<>()));
         assertThatImmutableMapIsSameAs(unmodifiableNavigableMap(new TreeMap<>()));
         assertThatImmutableMapIsSameAs(emptySortedMap());
         assertThatImmutableMapIsSameAs(unmodifiableSortedMap(new TreeMap<>()));
         assertThatImmutableMapIsSameAs(singletonMap(1, 1));
         assertThatImmutableMapIsSameAs(emptyMap());
         assertThatImmutableMapIsSameAs(Map.of());
         assertThatImmutableMapIsSameAs(Map.of(1, 1));
         assertThatImmutableMapIsSameAs(Map.of(1, 1, 2, 2));
         assertThatImmutableMapIsSameAs(Map.of(1, 1, 2, 2, 3, 3));

         assertThatImmutableMapIsNotSameAs(new HashMap<>());
         assertThatImmutableMapIsNotSameAs(new TreeMap<>());
      }

      private void assertThatImmutableMapIsSameAs(Map<Integer, Integer> map) {
         assertThat(immutable(map)).isSameAs(map);
      }

      private void assertThatImmutableMapIsNotSameAs(Map<Integer, Integer> map) {
         assertThat(immutable(map)).isNotSameAs(map);
      }

   }

   @Nested
   public class TestSynchronizedView {

      @Test
      public void testSynchronizedCollectionWhenNominal() {
         Collection<Integer> collection = synchronizedCollection(collection(ArrayList::new, 1, 2));

         assertThat(collection).isInstanceOf(Collection.class);
         assertThat(collection).containsExactly(1, 2);
      }

      @Test
      public void testSynchronizedCollectionWhenNull() {
         Collection<Integer> collection = synchronizedCollection(null);

         assertThat(collection).isInstanceOf(Collection.class);
         assertThat(collection).isEmpty();
      }

      @Test
      public void testSynchronizedListWhenNominal() {
         Collection<Integer> list = synchronizedList(list(1, 2));

         assertThat(list).isInstanceOf(Collection.class);
         assertThat(list).containsExactly(1, 2);
      }

      @Test
      public void testSynchronizedListWhenNull() {
         List<Integer> list = synchronizedList(null);

         assertThat(list).isInstanceOf(Collection.class);
         assertThat(list).isEmpty();
      }

      @Test
      public void testSynchronizedMapWhenNominal() {
         Map<String, Integer> map = synchronizedMap(map(entry("a", 1), entry("b", 2)));

         assertThat(map).isInstanceOf(Map.class);
         assertThat(map).containsOnly(entry("a", 1), entry("b", 2));
      }

      @Test
      public void testSynchronizedMapWhenNull() {
         Map<String, Integer> map = synchronizedMap(null);

         assertThat(map).isInstanceOf(Map.class);
         assertThat(map).isEmpty();
      }
   }

   @Nested
   // FIXME add TestCompatibleListFactoryByClass/TestCompatibleListFactoryByInstance
   //       add TestCompatibleMapFactoryByClass/TestCompatibleMapFactoryByInstance
   public class TestCompatibleFactory {

      @Nested
      public class TestCompatibleCollectionFactoryByClass {

         @Test
         public void testCompatibleCollectionFactoryWhenNominal() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(LinkedHashSet::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(HashSet.class, ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(HashSet.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(HashSet.class, ArrayList::new),
                             stream(1, 2));

            assertThat(collection).isOfAnyClassIn(HashSet.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenUnknownFactoryForType() {
            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(HashSet.class, ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(ArrayList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(HashSet.class, ArrayList::new),
                             stream(1, 2));

            assertThat(collection).isOfAnyClassIn(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenNoDirectFactoryForType() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(LinkedHashSet.class, ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(HashSet.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(LinkedHashSet.class, ArrayList::new),
                             stream(1, 2));

            assertThat(collection).isOfAnyClassIn(HashSet.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenCustomType() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(MyList::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(MyList.class, ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(MyList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(MyList.class, ArrayList::new),
                             stream(1, 2));

            assertThat(collection).isOfAnyClassIn(MyList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenUpperBoundType() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(Collection.class, ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(ArrayList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(Collection.class, ArrayList::new),
                             stream(1, 2));

            assertThat(collection).isOfAnyClassIn(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenInterfaceType() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(Set.class, ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(ArrayList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(Set.class, ArrayList::new),
                             stream(1, 2));

            assertThat(collection).isOfAnyClassIn(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenSynchronized() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);

            Set<Integer> set = synchronizedSet(new HashSet<>());

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(set.getClass(), ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(ArrayList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(set.getClass(), ArrayList::new),
                             stream(1, 2));

            assertThat(collection).isOfAnyClassIn(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

      }

      @Nested
      public class TestCompatibleCollectionFactoryByInstance {

         @Test
         public void testCompatibleCollectionFactoryWhenNominal() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(LinkedHashSet::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(new HashSet<>(), ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(HashSet.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(new HashSet<Integer>(),
                                                                         ArrayList::new), stream(1, 2));

            assertThat(collection).isOfAnyClassIn(HashSet.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenUnknownFactoryForType() {
            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(new HashSet<>(), ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(ArrayList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(new HashSet<Integer>(),
                                                                         ArrayList::new), stream(1, 2));

            assertThat(collection).isOfAnyClassIn(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenNoDirectFactoryForType() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(new LinkedHashSet<>(), ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(HashSet.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(new LinkedHashSet<Integer>(),
                                                                         ArrayList::new), stream(1, 2));

            assertThat(collection).isOfAnyClassIn(HashSet.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenCustomType() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(MyList::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(new MyList<>(), ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(MyList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(new MyList<Integer>(),
                                                                         ArrayList::new), stream(1, 2));

            assertThat(collection).isOfAnyClassIn(MyList.class);
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenTypeHasCharacteristicsAndRegistryHasSupport() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(new TypeFactory<MyList<?>>() {
               @Override
               public MyList<?> get() {
                  return new MyList<>();
               }

               @Override
               public MyList<?> get(MyList<?> instance) {
                  return new MyList<>(instance.characteristic());
               }
            });

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(new MyList<>(2), ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(MyList.class);
            assertThat(factory.get()).asInstanceOf(type(MyList.class)).satisfies(myList -> {
               assertThat(myList.characteristic()).isEqualTo(2);
            });

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(new MyList<Integer>(2),
                                                                         ArrayList::new), stream(1, 2));

            assertThat(collection).isOfAnyClassIn(MyList.class);
            assertThat(factory.get()).asInstanceOf(type(MyList.class)).satisfies(myList -> {
               assertThat(myList.characteristic()).isEqualTo(2);
            });
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenTypeHasCharacteristicsAndRegistryHasNoSupport() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(MyList::new);

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(new MyList<>(2), ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(MyList.class);
            assertThat(factory.get()).asInstanceOf(type(MyList.class)).satisfies(myList -> {
               assertThat(myList.characteristic()).isEqualTo(0);
            });

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(new MyList<Integer>(2),
                                                                         ArrayList::new), stream(1, 2));

            assertThat(collection).isOfAnyClassIn(MyList.class);
            assertThat(factory.get()).asInstanceOf(type(MyList.class)).satisfies(myList -> {
               assertThat(myList.characteristic()).isEqualTo(0);
            });
            assertThat(collection).containsExactly(1, 2);
         }

         @Test
         public void testCompatibleCollectionFactoryWhenSynchronized() {
            DefaultTypeFactoryRegistry.instance().registerTypeFactory(HashSet::new);

            Set<Integer> set = synchronizedSet(new HashSet<>());

            TypeFactory<? extends Collection<Integer>> factory =
                  CollectionUtils.compatibleCollectionFactory(set, ArrayList::new);

            assertThat(factory.get()).isOfAnyClassIn(ArrayList.class);

            Collection<Integer> collection =
                  collection(CollectionUtils.compatibleCollectionFactory(set, ArrayList::new), stream(1, 2));

            assertThat(collection).isOfAnyClassIn(ArrayList.class);
            assertThat(collection).containsExactly(1, 2);
         }

      }

      private class MyList<T> extends ArrayList<T> {

         private final int characteristic;

         public MyList() {
            this(0);
         }

         public MyList(int characteristic) {
            this.characteristic = characteristic;
         }

         public int characteristic() {
            return characteristic;
         }
      }

   }
}