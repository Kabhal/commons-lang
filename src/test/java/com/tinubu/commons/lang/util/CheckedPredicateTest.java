/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.CheckedPredicate.checkedPredicate;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.function.Predicate;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class CheckedPredicateTest {

   @Nested
   public class InferredCreation {

      @Test
      public void checkedPredicateWhenAnonymousClass() {
         new CheckedPredicate<Integer>() {
            @Override
            public boolean testChecked(Integer i) throws FileNotFoundException {
               return true;
            }
         }.test(0);
      }

      @Test
      public void checkedPredicateWhenUncheckedLambda() {
         CheckedPredicate<Integer> inferred = checkedPredicate((Integer a) -> true);

         CheckedPredicate<Integer> inferredExtendException = checkedPredicate((Integer a) -> true);

         CheckedPredicate<Integer> inferredSuperInput = checkedPredicate((Number a) -> true);

         CheckedPredicate<Integer> inferredExtendReturn = checkedPredicate((Integer a) -> true);
      }

      @Test
      public void checkedPredicateWhenCheckedLambda() {
         CheckedPredicate<?> inferred = checkedPredicate(a -> {
            throw new Exception("error");
         });

         CheckedPredicate<?> inferredMultipleThrows = checkedPredicate(a -> {
            if (true) {
               throw new IOException("io error");
            } else {
               throw new ClassNotFoundException("class error");
            }
         });
      }

      @Test
      public void checkedPredicateWhenSubExceptionCheckedLambda() {
         CheckedPredicate<?> inferred = checkedPredicate(a -> {
            throw new IOException("error");
         });

         CheckedPredicate<?> inferredSuperType = checkedPredicate(a -> {
            throw new FileNotFoundException("error");
         });
      }

      @Test
      public void checkedPredicateWhenPredicate() {
         CheckedPredicate<Integer> inferred = checkedPredicate((Predicate<Integer>) a -> true);
      }

      @Test
      public void checkedPredicateWhenPredicateInstanceOfCheckedPredicate() {
         CheckedPredicate<Integer> checkedPredicate1 = a -> true;

         CheckedPredicate<Integer> inferred1 = checkedPredicate((Predicate<Integer>) checkedPredicate1);

         assertThat(inferred1).isSameAs(checkedPredicate1);

         CheckedPredicate<Integer> checkedPredicate2 = __ -> {throw new IOException("error");};

         CheckedPredicate<Integer> inferred2 = checkedPredicate((Predicate<Integer>) checkedPredicate2);

         assertThatThrownBy(() -> inferred2.testChecked(0))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

      @Test
      public void checkedPredicateWhenPredicateInstanceOfXCheckedPredicate() {
         XCheckedPredicate<Integer, ?> checkedPredicate1 = a -> true;

         CheckedPredicate<Integer> inferred = checkedPredicate((Predicate<Integer>) checkedPredicate1);

         assertThat(inferred).isNotSameAs(checkedPredicate1);

         XCheckedPredicate<Integer, ?> checkedPredicate2 = __ -> {throw new IOException("error");};

         CheckedPredicate<Integer> inferred2 = checkedPredicate((Predicate<Integer>) checkedPredicate2);

         assertThatThrownBy(() -> inferred2.testChecked(0))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

      @Test
      public void checkedPredicateWhenCheckedPredicate() {
         CheckedPredicate<Integer> inferred = checkedPredicate((CheckedPredicate<Integer>) a -> true);
         CheckedPredicate<Integer> inferred2 =
               checkedPredicate((CheckedPredicate<Integer>) (__ -> {throw new IOException("error");}));
      }

      @Test
      public void uncheckedWhenRuntimeThrow() {
         CheckedPredicate<Integer> checkedPredicate = __ -> {throw new ClassNotFoundException("error");};

         assertThatThrownBy(() -> checkedPredicate.unchecked(false).test(0))
               .isInstanceOf(UncheckedException.class)
               .hasMessage("error")
               .hasCause(new ClassNotFoundException("error"));
      }

      @Test
      public void uncheckedWhenIOExceptionRuntimeThrow() {
         CheckedPredicate<Integer> checkedPredicate = __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedPredicate.unchecked(false).test(0))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void uncheckedWhenSneakyThrow() {
         CheckedPredicate<Integer> checkedPredicate = __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedPredicate.unchecked(true).test(0))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

      @Test
      public void autoUncheckedWhenUsedAsPredicate() {
         CheckedPredicate<Integer> error = __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.of(3).filter(error))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void forceUncheckedWhenUsedAsPredicate() {
         CheckedPredicate<Integer> error = __ -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.of(3).filter(error.unchecked(false)))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
         assertThatThrownBy(() -> Optional.of(3).filter(error.unchecked(true)))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

   }

   @Nested
   public class AutoCloseAutoCloseable {

      @Test
      public void testAutoCloseAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         CheckedPredicate.<AutoCloseable>alwaysTrue().autoClose().testChecked(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new Error("Predicate error");
         }).autoClose().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new ClassNotFoundException("Predicate error");
         }).autoClose().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new IllegalStateException("Predicate error");
         }).autoClose().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new IOException("Predicate error");
         }).autoClose().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> alwaysTrue().autoClose().testChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> alwaysTrue().autoClose().testChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> alwaysTrue().autoClose().testChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> alwaysTrue().autoClose().testChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new InternalError("Predicate error");
               }).autoClose().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new ClassNotFoundException("Predicate error");
               }).autoClose().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new IllegalStateException("Predicate error");
               }).autoClose().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new IOException("Predicate error");
               }).autoClose().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseWithTestUncheckedAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         CheckedPredicate.<AutoCloseable>alwaysTrue().autoClose().test(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseWithTestUncheckedAutoCloseableWhenOperationAndCloseException()
            throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(UncheckedException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new ClassNotFoundException("Predicate error");
               }).autoClose().test(closeableStream)).withMessage("Predicate error")
               .withCauseInstanceOf(ClassNotFoundException.class)
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class AutoCloseOnThrowAutoCloseable {

      @Test
      public void testTestAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         CheckedPredicate.<AutoCloseable>alwaysTrue().autoCloseOnThrow().testChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new Error("Predicate error");
         }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new ClassNotFoundException("Predicate error");
         }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new IllegalStateException("Predicate error");
         }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedPredicate(s -> {
            throw new IOException("Predicate error");
         }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error");

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         alwaysTrue().autoCloseOnThrow().testChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testTestAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         alwaysTrue().autoCloseOnThrow().testChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testTestAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         alwaysTrue().autoCloseOnThrow().testChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testTestAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         alwaysTrue().autoCloseOnThrow().testChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new InternalError("Predicate error");
               }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new ClassNotFoundException("Predicate error");
               }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new IllegalStateException("Predicate error");
               }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWhenOperationAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new IOException("Predicate error");
               }).autoCloseOnThrow().testChecked(closeableStream)).withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWithExceptionClassWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         CheckedPredicate<Object> predicate = checkedPredicate(s -> {
            throw new FileNotFoundException("Predicate error");
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> predicate
                     .autoCloseOnThrow(FileNotFoundException.class).testChecked(closeableStream))
               .withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTestAutoCloseableWithMismatchingExceptionClassWhenOperationAndCloseException()
            throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         CheckedPredicate<Object> predicate = checkedPredicate(s -> {
            throw new FileNotFoundException("Predicate error");
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> predicate
                     .autoCloseOnThrow(FileNotFoundException.class).testChecked(closeableStream))
               .withMessage("Predicate error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testTestWithTestUncheckedAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         CheckedPredicate.<AutoCloseable>alwaysTrue().autoCloseOnThrow().test(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testTestWithTestUncheckedAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(UncheckedException.class)
               .isThrownBy(() -> checkedPredicate(s -> {
                  throw new ClassNotFoundException("Predicate error");
               }).autoCloseOnThrow().test(closeableStream)).withMessage("Predicate error")
               .withCauseInstanceOf(ClassNotFoundException.class)
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   @SuppressWarnings("unchecked")
   public class TryFinally {

      @Test
      public void tryFinallyWhenNominal() throws Exception {
         assertThat(checkedPredicate(__ -> true).tryFinally().testChecked(0)).isEqualTo(true);
      }

      @Test
      public void tryFinallyWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new InternalError("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new Error("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new ClassNotFoundException("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new Exception("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new IllegalStateException("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new RuntimeException("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new IOException("Operation error");
         }).tryFinally().testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedPredicate(__ -> true).tryFinally(finalizer1).testChecked(0)).isEqualTo(true);
         verify(finalizer1).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedPredicate(__ -> true).tryFinally(finalizer1, finalizer2).testChecked(0)).isEqualTo(
               true);

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryFinally(finalizer1, finalizer2).testChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new InternalError("Operation error");
         }).tryFinally(finalizer1, finalizer2).testChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedPredicate(__ -> true)
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> true)
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new IllegalStateException("Predicate error");
               }).tryFinally(finalizer1, finalizer2).testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new Error("Predicate error");
               }).tryFinally(finalizer1, finalizer2).testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new Error("Predicate error");
               }).tryFinally(finalizer1, finalizer2).testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWithTestUncheckedWhenNominal() {
         assertThat(checkedPredicate(__ -> true).tryFinally().test(0)).isEqualTo(true);
      }

      @Test
      public void tryFinallyWithTestUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new IllegalStateException("Predicate error");
               }).tryFinally(finalizer1, finalizer2).test(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Nested
   public class TryCatch {

      @Test
      public void tryCatchWhenNominal() throws Exception {
         assertThat(checkedPredicate(__ -> true).tryCatch(() -> {}).testChecked(0)).isEqualTo(true);
      }

      @Test
      public void tryCatchWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new InternalError("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new Error("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new ClassNotFoundException("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new Exception("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new IllegalStateException("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new RuntimeException("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new IOException("Operation error");
         }).tryCatch(() -> {}).testChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedPredicate(__ -> true).tryCatch(finalizer1).testChecked(0)).isEqualTo(true);
         verify(finalizer1, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedPredicate(__ -> true).tryCatch(finalizer1, finalizer2).testChecked(0)).isEqualTo(
               true);

         verify(finalizer1, never()).runChecked();
         verify(finalizer2, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryCatch(finalizer1, finalizer2).testChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new InternalError("Operation error");
         }).tryCatch(finalizer1, finalizer2).testChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {throw new IllegalStateException("error");})
                     .tryCatch(finalizer1, finalizer2, finalizer3)
                     .testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isOfAnyClassIn(Error.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {throw new IllegalStateException("error");})
                     .tryCatch(finalizer1, finalizer2, finalizer3)
                     .testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isExactlyInstanceOf(IllegalStateException.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new IllegalStateException("Predicate error");
               }).tryCatch(finalizer1, finalizer2).testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new Error("Predicate error");
               }).tryCatch(finalizer1, finalizer2).testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new Error("Predicate error");
               }).tryCatch(finalizer1, finalizer2).testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThat(CheckedPredicate
                          .<Object, Exception>checkedPredicate(__ -> true)
                          .tryCatch(IOException.class, finalizer)
                          .testChecked(0)).isEqualTo(true);

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithException() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedPredicate(__ -> {
            throw new IOException("Operation error");
         }).tryCatch(IOException.class, finalizer).testChecked(0)).withMessage("Operation error");

         verify(finalizer).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithSubException() throws Exception {
         CheckedConsumer<FileNotFoundException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(FileNotFoundException failure) {
            }
         });

         CheckedPredicate<Object> predicate = checkedPredicate(__ -> {
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> predicate.tryCatch(FileNotFoundException.class, finalizer).testChecked(0))
               .withMessage("Operation error");

         verify(finalizer).acceptChecked(any(FileNotFoundException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithMismatchingSubException() throws Exception {
         CheckedConsumer<EOFException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(EOFException failure) {
            }
         });

         CheckedPredicate<Object> predicate = checkedPredicate(__ -> {
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> predicate.tryCatch(EOFException.class, finalizer).testChecked(0))
               .withMessage("Operation error");

         verify(finalizer, never()).acceptChecked(any(EOFException.class));
      }

      @Test
      public void tryCatchWithExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new IllegalStateException("Predicate error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).testChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithTestUncheckedWhenNominal() {
         assertThat(checkedPredicate(__ -> true).tryCatch(() -> {}).test(0)).isEqualTo(true);
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithTestUncheckedExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThat(CheckedPredicate
                          .<Object, Exception>checkedPredicate(__ -> true)
                          .tryCatch(IOException.class, finalizer)
                          .test(0)).isEqualTo(true);

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      public void tryCatchWithTestUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new IllegalStateException("Predicate error");
               }).tryCatch(finalizer1, finalizer2).test(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithTestUncheckedAndExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedPredicate(__ -> {
                  throw new IllegalStateException("Predicate error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).test(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Predicate error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

}