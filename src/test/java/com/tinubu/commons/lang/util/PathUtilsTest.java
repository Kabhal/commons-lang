/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.nio.file.Path;

import org.junit.jupiter.api.Test;

public class PathUtilsTest {

   @Test
   public void isRootWhenNominal() {
      assertThat(PathUtils.isRoot(path("/"))).isTrue();

      assertThat(PathUtils.isRoot(path("/path"))).isFalse();
      assertThat(PathUtils.isRoot(path("path"))).isFalse();
      assertThat(PathUtils.isRoot(path(""))).isFalse();
      assertThat(PathUtils.isRoot(path(" "))).isFalse();
   }

   @Test
   public void isRootWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.isRoot(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
   }

   @Test
   public void isEmptyWhenNominal() {
      assertThat(PathUtils.isEmpty(path(""))).isTrue();

      assertThat(PathUtils.isEmpty(path("/path/subpath"))).isFalse();
      assertThat(PathUtils.isEmpty(path("path/subpath"))).isFalse();
      assertThat(PathUtils.isEmpty(path(" "))).isFalse();
      assertThat(PathUtils.isEmpty(path("/"))).isFalse();
   }

   @Test
   public void isEmptyWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.isEmpty(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
   }

   @Test
   public void containsWhenNominal() {
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("subpath"))).isTrue();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("path"))).isTrue();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("otherpath"))).isTrue();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("subpath/otherpath"))).isTrue();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("path/subpath"))).isTrue();

      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("path/otherpath"))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("badpath"))).isFalse();
   }

   @Test
   public void containsWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.contains(null, path("path")))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
      assertThatThrownBy(() -> PathUtils.contains(path("path"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'subPath' must not be null");
   }

   @Test
   public void containsWhenNotNormalized() {
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("path/"))).isTrue();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("otherpath/"))).isTrue();

      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("subpath/badpath/.."))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("path/."))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("./path"))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("path/.."))).isFalse();

      assertThat(PathUtils.contains(path("path/.."), path("path"))).isTrue();
      assertThat(PathUtils.contains(path("path/../subpath"), path("path/.."))).isTrue();
   }

   @Test
   public void containsWhenRequiresBackwardRepositioningWhileSearching() {
      assertThat(PathUtils.contains(path("path/path/otherpath"), path("path/otherpath"))).isTrue();
      assertThat(PathUtils.contains(path("/path/path/otherpath"), path("/path/otherpath"))).isFalse();
      assertThat(PathUtils.contains(path("path/path/path/otherpath"), path("path/otherpath"))).isTrue();
      assertThat(PathUtils.contains(path("/path/path/path/otherpath"), path("/path/otherpath"))).isFalse();
   }

   @Test
   public void containsWhenEqual() {
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"),
                                    path("/path/subpath/otherpath"))).isTrue();
   }

   @Test
   public void containsWhenAbsolute() {
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"),
                                    path("/path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"), path("/path"))).isTrue();
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"), path("/subpath"))).isFalse();
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"), path("/otherpath"))).isFalse();

      assertThat(PathUtils.contains(path("/path/subpath/otherpath"),
                                    path("path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"), path("path"))).isTrue();
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"), path("subpath"))).isTrue();
      assertThat(PathUtils.contains(path("/path/subpath/otherpath"), path("otherpath"))).isTrue();

      assertThat(PathUtils.contains(path("path/subpath/otherpath"),
                                    path("/path/subpath/otherpath"))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("/path"))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("/subpath"))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath/otherpath"), path("/otherpath"))).isFalse();
   }

   @Test
   public void containsWhenRoot() {
      assertThat(PathUtils.contains(path("/"), path("/"))).isTrue();
      assertThat(PathUtils.contains(path("/path"), path("/"))).isTrue();

      assertThat(PathUtils.contains(path("/"), path("path"))).isFalse();
      assertThat(PathUtils.contains(path("/"), path("/path"))).isFalse();
      assertThat(PathUtils.contains(path("path"), path("/"))).isFalse();
   }

   @Test
   public void containsWhenEmpty() {
      assertThat(PathUtils.contains(path(""), path(""))).isTrue();

      assertThat(PathUtils.contains(path("/"), path(""))).isTrue();
      assertThat(PathUtils.contains(path("/path"), path(""))).isTrue();
      assertThat(PathUtils.contains(path("path"), path(""))).isTrue();

      assertThat(PathUtils.contains(path(""), path("/"))).isFalse();
      assertThat(PathUtils.contains(path(""), path("/path"))).isFalse();
      assertThat(PathUtils.contains(path(""), path("path"))).isFalse();
   }

   @Test
   public void containsWhenNotBoundedElement() {
      assertThat(PathUtils.contains(path("path/subpath"), path("path/sub"))).isFalse();
      assertThat(PathUtils.contains(path("path/subpath"), path("pa"))).isFalse();
   }

   @Test
   public void containsWhenCaseInsensitive() {
      assertThat(PathUtils.contains(path("PaTh/SuBpAtH/OtHeRpAtH"), path("sUbPaTh"), false)).isFalse();
      assertThat(PathUtils.contains(path("PaTh/SuBpAtH/OtHeRpAtH"), path("sUbPaTh"), true)).isTrue();
      assertThat(PathUtils.contains(path("PaTh/SuBpAtH/OtHeRpAtH"), path("pAtH"), false)).isFalse();
      assertThat(PathUtils.contains(path("PaTh/SuBpAtH/OtHeRpAtH"), path("pAtH"), true)).isTrue();

      assertThat(PathUtils.contains(path("path"), path(""), true)).isTrue();
      assertThat(PathUtils.contains(path(""), path(""), true)).isTrue();
      assertThat(PathUtils.contains(path("/"), path(""), true)).isTrue();
      assertThat(PathUtils.contains(path("/path"), path(""), true)).isTrue();
      assertThat(PathUtils.contains(path("/PaTh"), path("/pAtH"), true)).isTrue();
   }

   @Test
   public void indexOfWhenNominal() {
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("subpath"))).hasValue(1);
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("path"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("otherpath"))).hasValue(2);
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("subpath/otherpath"))).hasValue(1);
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("path/subpath"))).hasValue(0);

      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("path/otherpath"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("badpath"))).isEmpty();
   }

   @Test
   public void indexOfWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.indexOf(null, path("path")))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
      assertThatThrownBy(() -> PathUtils.indexOf(path("path"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'subPath' must not be null");
   }

   @Test
   public void indexOfWhenNotNormalized() {
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("path/"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("otherpath/"))).hasValue(2);

      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("subpath/badpath/.."))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("path/."))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("./path"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("path/.."))).isEmpty();

      assertThat(PathUtils.indexOf(path("path/.."), path("path"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("path/../subpath"), path("path/.."))).hasValue(0);
   }

   @Test
   public void indexOfWhenRequiresBackwardRepositioningWhileSearching() {
      assertThat(PathUtils.indexOf(path("path/path/otherpath"), path("path/otherpath"))).hasValue(1);
      assertThat(PathUtils.indexOf(path("/path/path/otherpath"), path("/path/otherpath"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/path/path/otherpath"), path("path/otherpath"))).hasValue(2);
      assertThat(PathUtils.indexOf(path("/path/path/path/otherpath"), path("/path/otherpath"))).isEmpty();
   }

   @Test
   public void indexOfWhenEqual() {
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"),
                                   path("path/subpath/otherpath"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"),
                                   path("/path/subpath/otherpath"))).hasValue(0);
   }

   @Test
   public void indexOfWhenAbsolute() {
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"),
                                   path("/path/subpath/otherpath"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"), path("/path"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"), path("/subpath"))).isEmpty();
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"), path("/otherpath"))).isEmpty();

      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"), path("path/subpath/otherpath"))).hasValue(
            0);
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"), path("path"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"), path("subpath"))).hasValue(1);
      assertThat(PathUtils.indexOf(path("/path/subpath/otherpath"), path("otherpath"))).hasValue(2);

      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"),
                                   path("/path/subpath/otherpath"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("/path"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("/subpath"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath/otherpath"), path("/otherpath"))).isEmpty();
   }

   @Test
   public void indexOfWhenRoot() {
      assertThat(PathUtils.indexOf(path("/"), path("/"))).hasValue(0);
      assertThat(PathUtils.indexOf(path("/path"), path("/"))).hasValue(0);

      assertThat(PathUtils.indexOf(path("/"), path("path"))).isEmpty();
      assertThat(PathUtils.indexOf(path("/"), path("/path"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path"), path("/"))).isEmpty();
   }

   @Test
   public void indexOfWhenEmpty() {
      assertThat(PathUtils.indexOf(path(""), path(""))).hasValue(0);

      assertThat(PathUtils.indexOf(path("/"), path(""))).hasValue(0);
      assertThat(PathUtils.indexOf(path("/path"), path(""))).hasValue(0);
      assertThat(PathUtils.indexOf(path("path"), path(""))).hasValue(0);

      assertThat(PathUtils.indexOf(path(""), path("/"))).isEmpty();
      assertThat(PathUtils.indexOf(path(""), path("/path"))).isEmpty();
      assertThat(PathUtils.indexOf(path(""), path("path"))).isEmpty();
   }

   @Test
   public void indexOfWhenNotBoundedElement() {
      assertThat(PathUtils.indexOf(path("path/subpath"), path("path/sub"))).isEmpty();
      assertThat(PathUtils.indexOf(path("path/subpath"), path("pa"))).isEmpty();
   }

   @Test
   public void indexOfWhenCaseInsensitive() {
      assertThat(PathUtils.indexOf(path("PaTh/SuBpAtH/OtHeRpAtH"), path("sUbPaTh"), false)).isEmpty();
      assertThat(PathUtils.indexOf(path("PaTh/SuBpAtH/OtHeRpAtH"), path("sUbPaTh"), true)).hasValue(1);
      assertThat(PathUtils.indexOf(path("PaTh/SuBpAtH/OtHeRpAtH"), path("pAtH"), false)).isEmpty();
      assertThat(PathUtils.indexOf(path("PaTh/SuBpAtH/OtHeRpAtH"), path("pAtH"), true)).hasValue(0);

      assertThat(PathUtils.indexOf(path("path"), path(""), true)).hasValue(0);
      assertThat(PathUtils.indexOf(path(""), path(""), true)).hasValue(0);
      assertThat(PathUtils.indexOf(path("/"), path(""), true)).hasValue(0);
      assertThat(PathUtils.indexOf(path("/path"), path(""), true)).hasValue(0);
      assertThat(PathUtils.indexOf(path("/PaTh"), path("/pAtH"), true)).hasValue(0);
   }

   @Test
   public void removeRootWhenNominal() {
      assertThat(PathUtils.removeRoot(path("/path"))).isEqualTo(path("path"));
   }

   @Test
   public void removeRootWhenRelative() {
      assertThat(PathUtils.removeRoot(path("path"))).isEqualTo(path("path"));
   }

   @Test
   public void removeRootWhenEmpty() {
      assertThat(PathUtils.removeRoot(path(""))).isEqualTo(path(""));
   }

   @Test
   public void removeRootWhenRoot() {
      assertThat(PathUtils.removeRoot(path("/"))).isEqualTo(path(""));
   }

   @Test
   public void removeRootWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.removeRoot(null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
   }

   @Test
   public void addRootWhenNominal() {
      assertThat(PathUtils.addRoot(path("path"))).isEqualTo(path("/path"));
   }

   @Test
   public void addRootWhenAbsolute() {
      assertThat(PathUtils.addRoot(path("/path"))).isEqualTo(path("/path"));
   }

   @Test
   public void addRootWhenRoot() {
      assertThat(PathUtils.addRoot(path("/"))).isEqualTo(path("/"));
   }

   @Test
   public void addRootWhenEmpty() {
      assertThat(PathUtils.addRoot(path(""))).isEqualTo(path("/"));
   }

   @Test
   public void addRootWhenBadArgument() {
      assertThatNullPointerException()
            .isThrownBy(() -> PathUtils.addRoot(null))
            .withMessage("'path' must not be null");
   }

   @Test
   public void removeStartWhenNominal() {
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("/path"))).hasValue(path("subpath"));
   }

   @Test
   public void removeStartWhenNullParameters() {
      assertThatThrownBy(() -> PathUtils.removeStart(null, path("subpath")))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
      assertThatThrownBy(() -> PathUtils.removeStart(path("/path/subpath"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'startPath' must not be null");
   }

   @Test
   public void removeStartWhenBothRelative() {
      assertThat(PathUtils.removeStart(path("path/subpath"), path("path"))).hasValue(path("subpath"));
      assertThat(PathUtils.removeStart(path("path/subpath"), path("path/subpath"))).hasValue(path(""));
   }

   @Test
   public void removeStartWhenBothAbsolute() {
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("/path/subpath"))).hasValue(path(""));
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("/path"))).hasValue(path("subpath"));
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("/subpath"))).isEmpty();
   }

   @Test
   public void removeStartWhenPathAbsolute() {
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("path"))).isEmpty();
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("subpath"))).isEmpty();
   }

   @Test
   public void removeStartWhenStartPathAbsolute() {
      assertThat(PathUtils.removeStart(path("path/subpath"), path("/subpath"))).isEmpty();
      assertThat(PathUtils.removeStart(path("path/subpath"), path("/path/subpath"))).isEmpty();
      assertThat(PathUtils.removeStart(path("path/subpath"), path("/path"))).isEmpty();
   }

   @Test
   public void removeStartWhenNotNormalized() {
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("/path/."))).isEmpty();
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("/path/.."))).isEmpty();
      assertThat(PathUtils.removeStart(path("/path/./subpath"), path("/path/."))).hasValue(path("subpath"));
      assertThat(PathUtils.removeStart(path("/path/../subpath"), path("/path/.."))).hasValue(path("subpath"));
   }

   @Test
   public void removeStartWhenRootPath() {
      assertThat(PathUtils.removeStart(path("/"), path(""))).hasValue(path("/"));
      assertThat(PathUtils.removeStart(path("/"), path("/"))).hasValue(path(""));
      assertThat(PathUtils.removeStart(path("/path"), path("/"))).hasValue(path("path"));
   }

   @Test
   public void removeStartWhenEmptyPath() {
      assertThat(PathUtils.removeStart(path(""), path(""))).hasValue(path(""));
      assertThat(PathUtils.removeStart(path(""), path("path"))).isEmpty();
      assertThat(PathUtils.removeStart(path(""), path("/"))).isEmpty();
      assertThat(PathUtils.removeStart(path("/path/subpath"), path(""))).hasValue(path("/path/subpath"));
      assertThat(PathUtils.removeStart(path("path/subpath"), path(""))).hasValue(path("path/subpath"));
      assertThat(PathUtils.removeStart(path(""), path("/path"))).isEmpty();
      assertThat(PathUtils.removeStart(path(""), path("path"))).isEmpty();
   }

   @Test
   public void removeStartWhenStartPathDoesNotStartPath() {
      assertThat(PathUtils.removeStart(path("path/subpath"), path("otherpath"))).isEmpty();
      assertThat(PathUtils.removeStart(path("path/subpath"), path("subpath"))).isEmpty();
      assertThat(PathUtils.removeStart(path("path/subpath/otherpath"), path("subpath"))).isEmpty();
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("/subpath"))).isEmpty();
      assertThat(PathUtils.removeStart(path("/path/subpath"), path("subpath"))).isEmpty();
   }

   @Test
   public void removeEndWhenNominal() {
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("subpath"))).hasValue(path("/path"));
   }

   @Test
   public void removeEndWhenNullParameters() {
      assertThatThrownBy(() -> PathUtils.removeEnd(null, path("subpath")))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
      assertThatThrownBy(() -> PathUtils.removeEnd(path("/path/subpath"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'endPath' must not be null");
   }

   @Test
   public void removeEndWhenBothRelative() {
      assertThat(PathUtils.removeEnd(path("path/subpath"), path("subpath"))).hasValue(path("path"));
      assertThat(PathUtils.removeEnd(path("path/subpath"), path("path/subpath"))).hasValue(path(""));
   }

   @Test
   public void removeEndWhenBothAbsolute() {
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("/path/subpath"))).hasValue(path(""));
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("/path"))).isEmpty();
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("/subpath"))).isEmpty();
   }

   @Test
   public void removeEndWhenPathAbsolute() {
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("subpath"))).hasValue(path("/path"));
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("path/subpath"))).hasValue(path("/"));
   }

   @Test
   public void removeEndWhenEndPathAbsolute() {
      assertThat(PathUtils.removeEnd(path("path/subpath"), path("/subpath"))).isEmpty();
      assertThat(PathUtils.removeEnd(path("path/subpath"), path("/path/subpath"))).isEmpty();
   }

   @Test
   public void removeEndWhenSameElements() {
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("/path/subpath"))).hasValue(path(""));
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("path/subpath"))).hasValue(path("/"));
   }

   @Test
   public void removeEndWhenNotNormalized() {
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("subpath/."))).isEmpty();
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("subpath/.."))).isEmpty();
      assertThat(PathUtils.removeEnd(path("/path/subpath/."), path("subpath/."))).hasValue(path("/path"));
      assertThat(PathUtils.removeEnd(path("/path/subpath/.."), path("subpath/.."))).hasValue(path("/path"));
   }

   @Test
   public void removeEndWhenRootPath() {
      assertThat(PathUtils.removeEnd(path("/"), path(""))).hasValue(path("/"));
      assertThat(PathUtils.removeEnd(path("/"), path("/"))).hasValue(path(""));
   }

   @Test
   public void removeEndWhenEmptyPath() {
      assertThat(PathUtils.removeEnd(path(""), path(""))).hasValue(path(""));
      assertThat(PathUtils.removeEnd(path(""), path("path"))).isEmpty();
      assertThat(PathUtils.removeEnd(path(""), path("/"))).isEmpty();
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path(""))).hasValue(path("/path/subpath"));
      assertThat(PathUtils.removeEnd(path("path/subpath"), path(""))).hasValue(path("path/subpath"));
      assertThat(PathUtils.removeEnd(path(""), path("/path/subpath"))).isEmpty();
      assertThat(PathUtils.removeEnd(path(""), path("path/subpath"))).isEmpty();
   }

   @Test
   public void removeEndWhenEndPathDoesNotEndPath() {
      assertThat(PathUtils.removeEnd(path("path/subpath"), path("otherpath"))).isEmpty();
      assertThat(PathUtils.removeEnd(path("path/subpath"), path("path"))).isEmpty();
      assertThat(PathUtils.removeEnd(path("path/subpath/otherpath"), path("subpath"))).isEmpty();
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("/path"))).isEmpty();
      assertThat(PathUtils.removeEnd(path("/path/subpath"), path("/subpath"))).isEmpty();
   }

   @Test
   public void equalsWhenNominal() {
      assertThat(PathUtils.equals(path("path/subpath/otherpath"), path("path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.equals(path("/path/subpath/otherpath"), path("/path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.equals(path("path/subpath/otherpath"), path("subpath"))).isFalse();
      assertThat(PathUtils.equals(path("path/subpath/otherpath"), path("path"))).isFalse();
      assertThat(PathUtils.equals(path("path/subpath/otherpath"), path("otherpath"))).isFalse();
   }

   @Test
   public void equalsWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.equals(null, path("path")))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path1' must not be null");
      assertThatThrownBy(() -> PathUtils.equals(path("path"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path2' must not be null");
   }

   @Test
   public void equalsWhenNotNormalized() {
      assertThat(PathUtils.equals(path("../path/../subpath/./otherpath/"),
                                  path("../path/../subpath/./otherpath/"))).isTrue();

      assertThat(PathUtils.equals(path("path/subpath/otherpath"),
                                  path("path/subpath/otherpath/badpath/.."))).isFalse();
      assertThat(PathUtils.equals(path("path/subpath/otherpath"),
                                  path("path/subpath/./otherpath"))).isFalse();

      assertThat(PathUtils.equals(path("path/./subpath"), path("path/./subpath"))).isTrue();
      assertThat(PathUtils.equals(path("path/../subpath"), path("path/../subpath"))).isTrue();
   }

   @Test
   public void equalsWhenAbsolute() {
      assertThat(PathUtils.equals(path("/path/subpath/otherpath"), path("/path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.equals(path("path/subpath/otherpath"), path("/path/subpath/otherpath"))).isFalse();
      assertThat(PathUtils.equals(path("/path/subpath/otherpath"), path("path/subpath/otherpath"))).isFalse();
   }

   @Test
   public void equalsWhenRoot() {
      assertThat(PathUtils.equals(path("/"), path("/"))).isTrue();
   }

   @Test
   public void equalsWhenEmpty() {
      assertThat(PathUtils.equals(path(""), path(""))).isTrue();

      assertThat(PathUtils.equals(path("/"), path(""))).isFalse();
      assertThat(PathUtils.equals(path("/path"), path(""))).isFalse();
      assertThat(PathUtils.equals(path("path"), path(""))).isFalse();

      assertThat(PathUtils.equals(path(""), path("/"))).isFalse();
      assertThat(PathUtils.equals(path(""), path("/path"))).isFalse();
      assertThat(PathUtils.equals(path(""), path("path"))).isFalse();
   }

   @Test
   public void equalsWhenNotBoundedElement() {
      assertThat(PathUtils.equals(path("path/subpath"), path("path/sub"))).isFalse();
      assertThat(PathUtils.equals(path("path/subpath"), path("pa"))).isFalse();
   }

   @Test
   public void equalsWhenCaseInsensitive() {
      assertThat(PathUtils.equals(path("PaTh/SuBpAtH"), path("pAtH/sUbPaTh"), false)).isFalse();
      assertThat(PathUtils.equals(path("PaTh/SuBpAtH"), path("pAtH/sUbPaTh"), true)).isTrue();
      assertThat(PathUtils.equals(path("/PaTh/SuBpAtH"), path("/pAtH/sUbPaTh"), false)).isFalse();
      assertThat(PathUtils.equals(path("/PaTh/SuBpAtH"), path("/pAtH/sUbPaTh"), true)).isTrue();

      assertThat(PathUtils.equals(path(""), path(""), true)).isTrue();
      assertThat(PathUtils.equals(path("/"), path("/"), true)).isTrue();
   }

   @Test
   public void startsWithWhenNominal() {
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("subpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("path"))).isTrue();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("otherpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("subpath/otherpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("path/subpath"))).isTrue();

      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("path/otherpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("badpath"))).isFalse();
   }

   @Test
   public void startsWithWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.startsWith(null, path("path")))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
      assertThatThrownBy(() -> PathUtils.startsWith(path("path"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'subPath' must not be null");
   }

   @Test
   public void startsWithWhenNotNormalized() {
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath/"), path("path/"))).isTrue();
      assertThat(PathUtils.startsWith(path("path/"), path("path/"))).isTrue();

      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("path/badpath/.."))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("path/."))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("./path"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("path/.."))).isFalse();

      assertThat(PathUtils.startsWith(path("path/./subpath"), path("path/."))).isTrue();
      assertThat(PathUtils.startsWith(path("path/../subpath"), path("path/.."))).isTrue();
   }

   @Test
   public void startsWithWhenEqual() {
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"),
                                      path("path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"),
                                      path("/path/subpath/otherpath"))).isTrue();
   }

   @Test
   public void startsWithWhenAbsolute() {
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"),
                                      path("/path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"), path("/path"))).isTrue();
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"), path("/subpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"), path("/otherpath"))).isFalse();

      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"),
                                      path("path/subpath/otherpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"), path("path"))).isFalse();
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"), path("subpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("/path/subpath/otherpath"), path("otherpath"))).isFalse();

      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"),
                                      path("/path/subpath/otherpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("/path"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("/subpath"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath/otherpath"), path("/otherpath"))).isFalse();
   }

   @Test
   public void startsWithWhenRoot() {
      assertThat(PathUtils.startsWith(path("/"), path("/"))).isTrue();
      assertThat(PathUtils.startsWith(path("/path"), path("/"))).isTrue();

      assertThat(PathUtils.startsWith(path("/"), path("path"))).isFalse();
      assertThat(PathUtils.startsWith(path("/"), path("/path"))).isFalse();
      assertThat(PathUtils.startsWith(path("path"), path("/"))).isFalse();
   }

   @Test
   public void startsWithWhenEmpty() {
      assertThat(PathUtils.startsWith(path(""), path(""))).isTrue();

      assertThat(PathUtils.startsWith(path("/"), path(""))).isTrue();
      assertThat(PathUtils.startsWith(path("/path"), path(""))).isTrue();
      assertThat(PathUtils.startsWith(path("path"), path(""))).isTrue();

      assertThat(PathUtils.startsWith(path(""), path("/"))).isFalse();
      assertThat(PathUtils.startsWith(path(""), path("/path"))).isFalse();
      assertThat(PathUtils.startsWith(path(""), path("path"))).isFalse();
   }

   @Test
   public void startsWithWhenNotBoundedElement() {
      assertThat(PathUtils.startsWith(path("path/subpath"), path("path/sub"))).isFalse();
      assertThat(PathUtils.startsWith(path("/path/subpath"), path("/path/sub"))).isFalse();
      assertThat(PathUtils.startsWith(path("path/subpath"), path("pa"))).isFalse();
      assertThat(PathUtils.startsWith(path("/path/subpath"), path("/pa"))).isFalse();
   }

   @Test
   public void startsWithWhenCaseInsensitive() {
      assertThat(PathUtils.startsWith(path("PaTh/SuBpAtH/OtHeRpAtH"), path("pAtH"), false)).isFalse();
      assertThat(PathUtils.startsWith(path("PaTh/SuBpAtH/OtHeRpAtH"), path("pAtH"), true)).isTrue();
      assertThat(PathUtils.startsWith(path("/PaTh/SuBpAtH/OtHeRpAtH"), path("/pAtH"), false)).isFalse();
      assertThat(PathUtils.startsWith(path("/PaTh/SuBpAtH/OtHeRpAtH"), path("/pAtH"), true)).isTrue();

      assertThat(PathUtils.startsWith(path("path"), path(""), true)).isTrue();
      assertThat(PathUtils.startsWith(path(""), path(""), true)).isTrue();
      assertThat(PathUtils.startsWith(path("/"), path(""), true)).isTrue();
      assertThat(PathUtils.startsWith(path("/path"), path(""), true)).isTrue();
      assertThat(PathUtils.startsWith(path("/path"), path("/"), true)).isTrue();
   }

   @Test
   public void endsWithWhenNominal() {
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("subpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("path"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("otherpath"))).isTrue();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("subpath/otherpath"))).isTrue();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("path/subpath"))).isFalse();

      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("path/otherpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("badpath"))).isFalse();
   }

   @Test
   public void endsWithWhenBadParameters() {
      assertThatThrownBy(() -> PathUtils.endsWith(null, path("path")))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'path' must not be null");
      assertThatThrownBy(() -> PathUtils.endsWith(path("path"), null))
            .isInstanceOf(NullPointerException.class)
            .hasMessage("'subPath' must not be null");
   }

   @Test
   public void endsWithWhenNotNormalized() {
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath/"), path("otherpath/"))).isTrue();
      assertThat(PathUtils.endsWith(path("path/"), path("path/"))).isTrue();

      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("otherpath/badpath/.."))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("otherpath/."))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("./otherpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("otherpath/.."))).isFalse();

      assertThat(PathUtils.endsWith(path("path/./subpath"), path("./subpath"))).isTrue();
      assertThat(PathUtils.endsWith(path("path/../subpath"), path("../subpath"))).isTrue();
   }

   @Test
   public void endsWithWhenEqual() {
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"),
                                    path("/path/subpath/otherpath"))).isTrue();
   }

   @Test
   public void endsWithWhenAbsolute() {
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"),
                                    path("/path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"), path("/path"))).isFalse();
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"), path("/subpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"), path("/otherpath"))).isFalse();

      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"),
                                    path("path/subpath/otherpath"))).isTrue();
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"), path("path"))).isFalse();
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"), path("subpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("/path/subpath/otherpath"), path("otherpath"))).isTrue();

      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"),
                                    path("/path/subpath/otherpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("/path"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("/subpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath/otherpath"), path("/otherpath"))).isFalse();
   }

   @Test
   public void endsWithWhenRoot() {
      assertThat(PathUtils.endsWith(path("/"), path("/"))).isTrue();
      assertThat(PathUtils.endsWith(path("/path"), path("/"))).isFalse();

      assertThat(PathUtils.endsWith(path("/"), path("path"))).isFalse();
      assertThat(PathUtils.endsWith(path("/"), path("/path"))).isFalse();
      assertThat(PathUtils.endsWith(path("path"), path("/"))).isFalse();
   }

   @Test
   public void endsWithWhenEmpty() {
      assertThat(PathUtils.endsWith(path(""), path(""))).isTrue();

      assertThat(PathUtils.endsWith(path("/"), path(""))).isTrue();
      assertThat(PathUtils.endsWith(path("/path"), path(""))).isTrue();
      assertThat(PathUtils.endsWith(path("path"), path(""))).isTrue();

      assertThat(PathUtils.endsWith(path(""), path("/"))).isFalse();
      assertThat(PathUtils.endsWith(path(""), path("/path"))).isFalse();
      assertThat(PathUtils.endsWith(path(""), path("path"))).isFalse();
   }

   @Test
   public void endsWithWhenNotBoundedElement() {
      assertThat(PathUtils.endsWith(path("path/subpath"), path("th/subpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("/path/subpath"), path("/th/subpath"))).isFalse();
      assertThat(PathUtils.endsWith(path("path/subpath"), path("th"))).isFalse();
      assertThat(PathUtils.endsWith(path("/path/subpath"), path("th"))).isFalse();
   }

   @Test
   public void endsWithWhenCaseInsensitive() {
      assertThat(PathUtils.endsWith(path("PaTh/SuBpAtH/OtHeRpAtH"), path("oThErPaTh"), false)).isFalse();
      assertThat(PathUtils.endsWith(path("PaTh/SuBpAtH/OtHeRpAtH"), path("oThErPaTh"), true)).isTrue();
      assertThat(PathUtils.endsWith(path("/PaTh/SuBpAtH/OtHeRpAtH"), path("oThErPaTh"), false)).isFalse();
      assertThat(PathUtils.endsWith(path("/PaTh/SuBpAtH/OtHeRpAtH"), path("oThErPaTh"), true)).isTrue();

      assertThat(PathUtils.endsWith(path("path"), path(""), true)).isTrue();
      assertThat(PathUtils.endsWith(path(""), path(""), true)).isTrue();
      assertThat(PathUtils.endsWith(path("/"), path(""), true)).isTrue();
      assertThat(PathUtils.endsWith(path("/path"), path(""), true)).isTrue();
   }

   private static Path path(String path) {
      return Path.of(path);
   }

}