/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.util.Objects;

import org.junit.jupiter.api.Test;

class NullableUtilsTest {

   @Test
   public void testNullable() {
      assertThat(NullableUtils.nullable(null)).isEmpty();
      assertThat(NullableUtils.nullable(32)).isPresent().hasValue(32);
   }

   @Test
   public void testNullableWhenDefaultValue() {
      assertThat(NullableUtils.nullable(null, 33)).isEqualTo(33);
      assertThat(NullableUtils.nullable(32, 33)).isEqualTo(32);
   }

   @Test
   public void testNullableWhenMapAndDefaultValue() {
      assertThat(NullableUtils.nullable((Integer) null, v -> v + 10, 33)).isEqualTo(33);
      assertThat(NullableUtils.nullable(32, v -> v + 10, 33)).isEqualTo(42);
   }

   @Test
   public void testNullableWhenMap() {
      assertThat(NullableUtils.nullable((Integer) null, v -> v + 10)).isNull();
      assertThat(NullableUtils.nullable(32, v -> v + 10)).isEqualTo(42);
   }

   @Test
   public void testNullablePredicateWhenNominal() {
      assertThat(NullableUtils.nullablePredicate(3, v -> v == 3)).hasValue(3);
      assertThat(NullableUtils.nullablePredicate(3, v -> v != 3)).isEmpty();
   }

   @Test
   public void testNullablePredicateWhenNullValue() {
      assertThat(NullableUtils.nullablePredicate((Integer) null, v -> v == 3)).isEmpty();
   }

   @Test
   @SuppressWarnings("ConstantValue")
   public void testNullableBooleanWhenNominal() {
      assertThat(NullableUtils.nullableBoolean(true)).isTrue();
      assertThat(NullableUtils.nullableBoolean(Boolean.TRUE)).isTrue();
      assertThat(NullableUtils.nullableBoolean(false)).isFalse();
      assertThat(NullableUtils.nullableBoolean(Boolean.FALSE)).isFalse();
   }

   @Test
   public void testNullableBooleanWhenNullValue() {
      assertThat(NullableUtils.nullableBoolean(null)).isFalse();
   }

   @Test
   public void testNullableInstanceOfSubclassWhenNominal() {
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyClass.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyParent.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyChild.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), Long.class)).isEmpty();
   }

   @Test
   public void testNullableInstanceOfSubclassWhenNull() {
      assertThat(NullableUtils.nullableInstanceOf(null, MyClass.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyParent.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyChild.class)).isEmpty();

      assertThatNullPointerException()
            .isThrownBy(() -> NullableUtils.nullableInstanceOf(new MyClass(0), (Class<MyClass>) null))
            .withMessage("'subclass' must not be null");
   }

   @Test
   public void testNullableInstanceOfSubclassWhenDefaultSupplierAndSubclassFactory() {
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), () -> null, MyChild.class, v -> {
         assertThat(v).isEqualTo(new MyClass(0));
         return new MyChild(v.value);
      })).isEqualTo(new MyChild(0));
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), () -> null, MyChild.class, v -> {
         assertThat(v).isEqualTo(new MyClass(0));
         return null;
      })).isEqualTo(null);
      assertThat(NullableUtils.nullableInstanceOf(null, () -> new MyChild(1), MyChild.class, v -> {
         throw new IllegalStateException("must not be called because value is null");
      })).isEqualTo(new MyChild(1));
      assertThat(NullableUtils.nullableInstanceOf(null, () -> null, MyChild.class, v -> {
         throw new IllegalStateException("must not be called because value is null");
      })).isEqualTo(null);
      assertThat(NullableUtils.nullableInstanceOf(null, () -> null, MyChild.class, v -> {
         throw new IllegalStateException("must not be called because value is null");
      })).isEqualTo(null);
   }

   @Test
   public void testNullableInstanceOfSubclassWhenSubclassFactory() {
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyChild.class, v -> {
         assertThat(v).isEqualTo(new MyClass(0));
         return new MyChild(v.value);
      })).isEqualTo(new MyChild(0));
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyChild.class, v -> {
         assertThat(v).isEqualTo(new MyClass(0));
         return null;
      })).isEqualTo(null);
      assertThat(NullableUtils.nullableInstanceOf(null, MyChild.class, v -> {
         throw new IllegalStateException("must not be called because value is null");
      })).isEqualTo(null);
      assertThat(NullableUtils.nullableInstanceOf(null, MyChild.class, v -> {
         throw new IllegalStateException("must not be called because value is null");
      })).isEqualTo(null);
   }

   @Test
   public void testNullableInstanceOfSubclassesWhenNominal() {
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyClass.class, String.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyParent.class, String.class)).isPresent();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), MyChild.class, String.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), Long.class, String.class)).isEmpty();

      assertThat(NullableUtils.nullableInstanceOf(new MyClass(0), String.class, MyParent.class)).isPresent();
   }

   @Test
   public void testNullableInstanceOfSubclassesWhenNull() {
      assertThat(NullableUtils.nullableInstanceOf(null, MyClass.class, String.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyParent.class, String.class)).isEmpty();
      assertThat(NullableUtils.nullableInstanceOf(null, MyChild.class, String.class)).isEmpty();

      assertThat(NullableUtils.nullableInstanceOf(null, String.class, MyParent.class)).isEmpty();

      assertThatNullPointerException()
            .isThrownBy(() -> NullableUtils.nullableInstanceOf(new MyClass(0), (Class<String>[]) null))
            .withMessage("'subclasses' must not be null");
      assertThatIllegalArgumentException()
            .isThrownBy(() -> NullableUtils.nullableInstanceOf(new MyClass(0), null, (Class<String>) null))
            .withMessage("'subclasses' must not have null elements at index : 0");
   }

   public static class MyParent { }

   public static class MyClass extends MyParent {

      int value;

      public MyClass(int value) {
         this.value = value;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         MyClass myClass = (MyClass) o;
         return value == myClass.value;
      }

      @Override
      public int hashCode() {
         return Objects.hashCode(value);
      }
   }

   public static class MyChild extends MyClass {

      public MyChild(int value) {
         super(value);
      }

   }
}