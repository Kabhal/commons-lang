/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.entryStream;
import static com.tinubu.commons.lang.util.StreamUtils.findOne;
import static com.tinubu.commons.lang.util.StreamUtils.findOneOrElse;
import static com.tinubu.commons.lang.util.StreamUtils.findOneOrElseEmpty;
import static com.tinubu.commons.lang.util.StreamUtils.inputStream;
import static com.tinubu.commons.lang.util.StreamUtils.reader;
import static com.tinubu.commons.lang.util.StreamUtils.reversedStream;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;
import static com.tinubu.commons.lang.util.StreamUtils.streamUpdateOrAdd;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatIOException;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Random;
import java.util.function.BinaryOperator;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

// FIXME add tests for streamConcat(Stream<Stream>) 
//  streamConcat(Stream<Collection>) streamConcat(Collection<Collection>)
//  intStreamConcat, longStreamConcat, doubleStreamConcat
public class StreamUtilsTest {

   @Nested
   public class ReversedStream {

      @Test
      public void testReversedStreamWhenNominal() {
         assertThat(reversedStream(list("a", "c", "b"))).containsExactly("b", "c", "a");
      }

      @Test
      public void testReversedStreamWhenNull() {
         assertThat(reversedStream(null)).isEmpty();
      }

      @Test
      public void testReversedStreamWhenEmpty() {
         assertThat(reversedStream(list())).isEmpty();
      }
   }

   @Nested
   public class FindOne {

      @Test
      public void testFindOneWhenNominal() {
         assertThat(findOne(stream("a"))).hasValue("a");
      }

      @Test
      public void testFindOneWhenEmpty() {
         assertThat(findOne(stream())).isEmpty();
      }

      @Test
      public void testFindOneWhenBadParameters() {
         assertThatNullPointerException().isThrownBy(() -> findOne(null));
      }

      @Test
      public void testFindOneWhenMoreThanOneValue() {
         assertThatIllegalArgumentException()
               .isThrownBy(() -> findOne(stream("a", "b")))
               .withMessage("'stream' must have at most one element");
      }

      @Test
      public void testFindOneOrElseWhenNominal() {
         assertThat(findOneOrElse(stream("a"), () -> optional("b"))).hasValue("a");
      }

      @Test
      public void testFindOneOrElseWhenEmpty() {
         assertThat(findOneOrElse(stream(), () -> optional("b"))).isEmpty();
      }

      @Test
      public void testFindOneOrElseWhenBadParameters() {
         assertThatNullPointerException().isThrownBy(() -> findOneOrElse(null, () -> optional("b")));
         assertThatNullPointerException().isThrownBy(() -> findOneOrElse(stream(), null));
      }

      @Test
      public void testFindOneOrElseWhenMoreThanOneValue() {
         assertThat(findOneOrElse(stream("a", "b"), () -> optional("b"))).hasValue("b");
         assertThat(findOneOrElse(stream("a", "b"), OptionalUtils::optional)).isEmpty();
         assertThatIllegalStateException().isThrownBy(() -> findOneOrElse(stream("a", "b"), () -> {
            throw new IllegalStateException("Error");
         })).withMessage("Error");
      }

      @Test
      public void testFindOneOrElseEmptyWhenNominal() {
         assertThat(findOneOrElseEmpty(stream("a"))).hasValue("a");
      }

      @Test
      public void testFindOneOrElseEmptyWhenEmpty() {
         assertThat(findOneOrElseEmpty(stream())).isEmpty();
      }

      @Test
      public void testFindOneOrElseEmptyWhenMoreThanOneValue() {
         assertThat(findOneOrElseEmpty(stream("a", "b"))).isEmpty();
      }

      @Test
      public void testFindOneWhenStreamHasOnCloseOperation() {
         Stream<String> closeableStream = spy(stream("a"));

         assertThat(findOne(closeableStream)).hasValue("a");

         verify(closeableStream, times(0)).close();
      }

      @Test
      public void testFindOneOrElseWhenStreamHasOnCloseOperation() {
         Stream<String> closeableStream = spy(stream("a"));

         assertThat(findOneOrElse(closeableStream, () -> optional("b"))).hasValue("a");

         verify(closeableStream, times(0)).close();
      }
   }

   @Nested
   public class StreamWithOptional {

      @Test
      public void testStreamWithOptionalWhenNominal() {
         assertThat(stream(Optional.of(3))).containsExactly(3);
         assertThat(stream(Optional.empty())).isEmpty();
      }

      @Test
      @SuppressWarnings("OptionalAssignedToNull")
      public void testStreamWithOptionalWhenNull() {
         assertThat(stream((Optional<Integer>) null)).isEmpty();
      }

   }

   @Nested
   public class StreamWithStream {

      @Test
      public void testStreamWithStreamWhenNominal() {
         assertThat(stream(Stream.of(1, 2))).containsExactly(1, 2);
      }

      @Test
      public void testStreamWithStreamWhenNull() {
         assertThat(stream((Stream<Integer>) null)).isEmpty();
      }

      @Test
      public void testStreamShouldNotBeRecreated() {
         Stream<Integer> originalStream = Stream.of(1, 2);

         assertThat(stream(originalStream)).isSameAs(originalStream);
      }

      @Test
      public void testStreamWithStreamWhenNullElement() {
         assertThat(stream(Stream.of(1, null, 2))).containsExactly(1, null, 2);
         assertThat(stream(Stream.of(null, null))).containsExactly(null, null);
      }

   }

   @Nested
   public class StreamWithCollection {

      @Test
      public void testStreamWithCollectionWhenNominal() {
         assertThat(stream(Arrays.asList(1, 2))).containsExactly(1, 2);
      }

      @Test
      public void testStreamWithCollectionWhenNull() {
         assertThat(stream((List<Integer>) null)).isEmpty();
      }

      @Test
      public void testStreamWithCollectionWhenNullElement() {
         assertThat(stream(Arrays.asList(1, null, 2))).containsExactly(1, null, 2);
         assertThat(stream(Arrays.asList(null, null))).containsExactly(null, null);
      }

   }

   @Nested
   public class StreamWithElements {

      @Test
      public void testStreamWithElementsWhenNominal() {
         assertThat(stream(1, 2)).containsExactly(1, 2);
      }

      @Test
      public void testStreamWithElementsWhenNull() {
         assertThat(stream((Integer[]) null)).isEmpty();
      }

      @Test
      public void testStreamWithElementsWhenNullElement() {
         assertThat(stream(1, null, 2)).containsExactly(1, null, 2);
         assertThat(stream((Integer) null, null)).containsExactly(null, null);
      }

   }

   @Nested
   public class StreamConcatWithStream {

      @Test
      public void streamConcatWhenNominal() {
         Stream<Integer> stream = streamConcat(Stream.of(1, 2), Stream.of(3, 4), Stream.of(5, 6));

         assertThat(stream).containsExactly(1, 2, 3, 4, 5, 6);
      }

      @Test
      public void streamConcatWhenNull() {
         Stream<Integer> stream = streamConcat((Stream<Integer>[]) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatShouldNotBeRecreated() {
         Stream<Integer> originalStream = Stream.of(1, 2);

         assertThat(streamConcat(originalStream)).isSameAs(originalStream);
      }

      @Test
      public void streamConcatWhenStreamNullElement() {
         Stream<Integer> stream = streamConcat(Stream.of(1, null));

         assertThat(stream).containsExactly(1, null);
      }

      @Test
      public void streamConcatWhenSingleNullElement() {
         Stream<Integer> stream = streamConcat((Stream<Integer>) null);

         assertThat(stream).isEmpty();
      }

      /**
       * Test Stream.concat optimization with 2 nulls.
       */
      @Test
      public void streamConcatWhenTwoNullElement() {
         Stream<Integer> stream = streamConcat((Stream<Integer>) null, null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenNoElement() {
         Stream<Integer> stream = streamConcat(new Stream[0]);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenOneElement() {
         Stream<Integer> stream = streamConcat(Stream.of(1, 2));

         assertThat(stream).containsExactly(1, 2);
      }

      @Test
      public void streamConcatWhenTwoElement() {
         Stream<Integer> stream = streamConcat(Stream.of(1, 2), Stream.of(3, 4));

         assertThat(stream).containsExactly(1, 2, 3, 4);
      }

      @Test
      public void streamConcatWithMultiStreamsWhenNullElement() {
         Stream<Integer> stream = streamConcat(Stream.of(1, 2), null);

         assertThat(stream).containsExactly(1, 2);
      }

      @Test
      public void streamConcatWhenDuplicates() {
         Stream<Integer> stream = streamConcat(Stream.of(1, 1, 2, 2), Stream.of(2, 2, 3, 3));

         assertThat(stream).containsExactly(1, 1, 2, 2, 2, 2, 3, 3);
      }

   }

   @Nested
   public class StreamConcatWithIntStream {

      @Test
      public void streamConcathenNominal() {
         IntStream stream = streamConcat(IntStream.of(1, 2), IntStream.of(3, 4), IntStream.of(5, 6));

         assertThat(stream).containsExactly(1, 2, 3, 4, 5, 6);
      }

      @Test
      public void streamConcatWhenNull() {
         IntStream stream = streamConcat((IntStream[]) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatShouldNotBeRecreated() {
         IntStream originalStream = IntStream.of(1, 2);

         assertThat(streamConcat(originalStream)).isSameAs(originalStream);
      }

      @Test
      public void streamConcatWhenSingleNullElement() {
         IntStream stream = streamConcat((IntStream) null);

         assertThat(stream).isEmpty();
      }

      /**
       * Test Stream.concat optimization with 2 nulls.
       */
      @Test
      public void streamConcatWhenTwoNullElement() {
         IntStream stream = streamConcat((IntStream) null, null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenNoElement() {
         IntStream stream = streamConcat(new IntStream[0]);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenOneElement() {
         IntStream stream = streamConcat(IntStream.of(1, 2));

         assertThat(stream).containsExactly(1, 2);
      }

      @Test
      public void streamConcatWhenTwoElement() {
         IntStream stream = streamConcat(IntStream.of(1, 2), IntStream.of(3, 4));

         assertThat(stream).containsExactly(1, 2, 3, 4);
      }

      @Test
      public void streamConcatWithMultiStreamsWhenNullElement() {
         IntStream stream = streamConcat(IntStream.of(1, 2), null);

         assertThat(stream).containsExactly(1, 2);
      }

      @Test
      public void streamConcatWhenDuplicates() {
         IntStream stream = streamConcat(IntStream.of(1, 1, 2, 2), IntStream.of(2, 2, 3, 3));

         assertThat(stream).containsExactly(1, 1, 2, 2, 2, 2, 3, 3);
      }

   }

   @Nested
   public class StreamConcatWithLongStream {

      @Test
      public void streamConcathenNominal() {
         LongStream stream = streamConcat(LongStream.of(1, 2), LongStream.of(3, 4), LongStream.of(5, 6));

         assertThat(stream).containsExactly(1L, 2L, 3L, 4L, 5L, 6L);
      }

      @Test
      public void streamConcatWhenNull() {
         LongStream stream = streamConcat((LongStream[]) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatShouldNotBeRecreated() {
         LongStream originalStream = LongStream.of(1, 2);

         assertThat(streamConcat(originalStream)).isSameAs(originalStream);
      }

      @Test
      public void streamConcatWhenSingleNullElement() {
         LongStream stream = streamConcat((LongStream) null);

         assertThat(stream).isEmpty();
      }

      /**
       * Test Stream.concat optimization with 2 nulls.
       */
      @Test
      public void streamConcatWhenTwoNullElement() {
         LongStream stream = streamConcat((LongStream) null, null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenNoElement() {
         LongStream stream = streamConcat(new LongStream[0]);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenOneElement() {
         LongStream stream = streamConcat(LongStream.of(1, 2));

         assertThat(stream).containsExactly(1L, 2L);
      }

      @Test
      public void streamConcatWhenTwoElement() {
         LongStream stream = streamConcat(LongStream.of(1, 2), LongStream.of(3, 4));

         assertThat(stream).containsExactly(1L, 2L, 3L, 4L);
      }

      @Test
      public void streamConcatWithMultiStreamsWhenNullElement() {
         LongStream stream = streamConcat(LongStream.of(1, 2), null);

         assertThat(stream).containsExactly(1L, 2L);
      }

      @Test
      public void streamConcatWhenDuplicates() {
         LongStream stream = streamConcat(LongStream.of(1, 1, 2, 2), LongStream.of(2, 2, 3, 3));

         assertThat(stream).containsExactly(1L, 1L, 2L, 2L, 2L, 2L, 3L, 3L);
      }

   }

   @Nested
   public class StreamConcatWithDoubleStream {

      @Test
      public void streamConcathenNominal() {
         DoubleStream stream =
               streamConcat(DoubleStream.of(1, 2), DoubleStream.of(3, 4), DoubleStream.of(5, 6));

         assertThat(stream).containsExactly(1d, 2d, 3d, 4d, 5d, 6d);
      }

      @Test
      public void streamConcatWhenNull() {
         DoubleStream stream = streamConcat((DoubleStream[]) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatShouldNotBeRecreated() {
         DoubleStream originalStream = DoubleStream.of(1, 2);

         assertThat(streamConcat(originalStream)).isSameAs(originalStream);
      }

      @Test
      public void streamConcatWhenSingleNullElement() {
         DoubleStream stream = streamConcat((DoubleStream) null);

         assertThat(stream).isEmpty();
      }

      /**
       * Test Stream.concat optimization with 2 nulls.
       */
      @Test
      public void streamConcatWhenTwoNullElement() {
         DoubleStream stream = streamConcat((DoubleStream) null, null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenNoElement() {
         DoubleStream stream = streamConcat(new DoubleStream[0]);

         assertThat(stream).isEmpty();
      }

      @Test
      public void streamConcatWhenOneElement() {
         DoubleStream stream = streamConcat(DoubleStream.of(1, 2));

         assertThat(stream).containsExactly(1d, 2d);
      }

      @Test
      public void streamConcatWhenTwoElement() {
         DoubleStream stream = streamConcat(DoubleStream.of(1, 2), DoubleStream.of(3, 4));

         assertThat(stream).containsExactly(1d, 2d, 3d, 4d);
      }

      @Test
      public void streamConcatWithMultiStreamsWhenNullElement() {
         DoubleStream stream = streamConcat(DoubleStream.of(1, 2), null);

         assertThat(stream).containsExactly(1d, 2d);
      }

      @Test
      public void streamConcatWhenDuplicates() {
         DoubleStream stream = streamConcat(DoubleStream.of(1, 1, 2, 2), DoubleStream.of(2, 2, 3, 3));

         assertThat(stream).containsExactly(1d, 1d, 2d, 2d, 2d, 2d, 3d, 3d);
      }

   }

   @Nested
   public class StreamConcatWithCollection {

      @Test
      public void testStreamConcatWithCollectionWhenNominal() {
         Stream<Integer> stream = streamConcat(Arrays.asList(1, 2), Arrays.asList(3, 4));

         assertThat(stream).containsExactly(1, 2, 3, 4);
      }

      @Test
      public void testStreamConcatWithCollectionWhenNull() {
         Stream<Integer> stream = streamConcat((Collection<Integer>[]) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testStreamConcatWhenWithCollectionNullElement() {
         Stream<Integer> stream = streamConcat(Arrays.asList(1, null));

         assertThat(stream).containsExactly(1, null);
      }

      @Test
      public void testStreamConcatWithCollectionWhenSingleNullElement() {
         Stream<Integer> stream = streamConcat((Collection<Integer>) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testStreamConcatWithCollectionWhenTwoNullElement() {
         Stream<Integer> stream = streamConcat((Collection<Integer>) null, null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testStreamConcatWithCollectionWhenNoElement() {
         Stream<Integer> stream = streamConcat(new ArrayList[0]);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testStreamConcatWithCollectionWhenOneElement() {
         Stream<Integer> stream = streamConcat(Arrays.asList(1, 2));

         assertThat(stream).containsExactly(1, 2);
      }

      @Test
      public void testStreamConcatWithMultiCollectionsWhenNullElement() {
         Stream<Integer> stream = streamConcat(Arrays.asList(1, 2), null);

         assertThat(stream).containsExactly(1, 2);
      }

      @Test
      public void testStreamConcatWithCollectionWhenDuplicates() {
         Stream<Integer> stream = streamConcat(Arrays.asList(1, 1, 2, 2), Arrays.asList(2, 2, 3, 3));

         assertThat(stream).containsExactly(1, 1, 2, 2, 2, 2, 3, 3);
      }

   }

   @Nested
   public class StreamConcatWithMap {

      @Test
      public void testStreamConcatWithMapWhenNominal() {
         Stream<Entry<String, Integer>> stream = streamConcat(map(Pair.of("a", 1), Pair.of("b", 2)));

         assertThat(stream).containsExactly(Pair.of("a", 1), Pair.of("b", 2));
      }

      @Test
      public void testStreamConcatWithMapWhenNull() {
         Stream<Entry<String, Integer>> stream = streamConcat((Map<String, Integer>[]) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testStreamConcatWhenWithMapNullEntryKeyElement() {
         Stream<Entry<String, Integer>> stream = streamConcat(map(Pair.of(null, 1), Pair.of("b", 2)));

         assertThat(stream).containsExactly(Pair.of(null, 1), Pair.of("b", 2));
      }

      @Test
      public void testStreamConcatWhenWithMapNullEntryValueElement() {
         Stream<Entry<String, Integer>> stream = streamConcat(map(Pair.of("a", null), Pair.of("b", 2)));

         assertThat(stream).containsExactly(Pair.of("a", null), Pair.of("b", 2));
      }

      @Test
      public void testStreamConcatWithMapWhenSingleNullElement() {
         Stream<Entry<String, Integer>> stream = streamConcat((Map<String, Integer>) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testStreamConcatWithMapWhenTwoNullElement() {
         Stream<Entry<String, Integer>> stream = streamConcat((Map<String, Integer>) null, null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testStreamConcatWithMultiMapsWhenNominal() {
         Stream<Entry<String, Integer>> stream =
               streamConcat(map(Pair.of("a", 1), Pair.of("b", 2)), map(Pair.of("c", 3), Pair.of("d", 4)));

         assertThat(stream).containsExactly(Pair.of("a", 1),
                                            Pair.of("b", 2),
                                            Pair.of("c", 3),
                                            Pair.of("d", 4));
      }

      @Test
      public void testStreamConcatWithMultiMapsWhenNullElement() {
         Stream<Entry<String, Integer>> stream = streamConcat(map(Pair.of("a", 1), Pair.of("b", 2)), null);

         assertThat(stream).containsExactly(Pair.of("a", 1), Pair.of("b", 2));
      }

   }

   @Nested
   public class EntryStreamWithEntries {

      @Test
      public void testEntryStreamWithEntriesWhenNominal() {
         Stream<Entry<String, Integer>> stream = entryStream(Pair.of("a", 1), Pair.of("b", 2));

         assertThat(stream).containsExactly(Pair.of("a", 1), Pair.of("b", 2));
      }

      @Test
      public void testEntryStreamWhenWithEntriesWhenNull() {
         Stream<Entry<String, Integer>> stream = entryStream((Entry<String, Integer>[]) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testEntryStreamWhenWithEntriesWhenNullElement() {
         Stream<Entry<String, Integer>> stream = entryStream(Pair.of("a", 1), null);

         assertThat(stream).containsExactly(Pair.of("a", 1));
      }

      @Test
      public void testEntryStreamWithEntriesWhenSingleNullElement() {
         Stream<Entry<String, Integer>> stream = entryStream((Entry<String, Integer>) null);

         assertThat(stream).isEmpty();
      }

      @Test
      public void testEntryStreamWithEntriesWhenTwoNullElement() {
         Stream<Entry<String, Integer>> stream = entryStream((Entry<String, Integer>) null, null);

         assertThat(stream).isEmpty();
      }
   }

   @Nested
   public class StreamOperations {

      @Test
      public void testStreamUpdateOrAddWhenNominal() {
         assertThat(streamUpdateOrAdd(stream(1, 2), 3)).containsExactly(1, 2, 3);
         assertThat(streamUpdateOrAdd(stream(1, 2), 2)).containsExactly(1, 2);
      }

      @Test
      public void testStreamUpdateOrAddWhenNullElement() {
         assertThat(streamUpdateOrAdd(stream(1, null, 2), 3)).containsExactly(1, null, 2, 3);
         assertThat(streamUpdateOrAdd(stream(1, 2), null)).containsExactly(1, 2, null);
         assertThat(streamUpdateOrAdd(stream(1, null, 2), null)).containsExactly(1, null, 2);
      }

      @Test
      public void testStreamUpdateOrAddWhenMultipleMatches() {
         assertThat(streamUpdateOrAdd(stream(1, 2, 1), 3)).containsExactly(1, 2, 1, 3);
         assertThat(streamUpdateOrAdd(stream(1, 2, 1), 1)).containsExactly(1, 2, 1);
      }

      @Test
      public void testStreamUpdateOrAddWithMatcherWhenNominal() {
         assertThat(streamUpdateOrAdd(stream(1, 2), 3, (pv, nv) -> pv == 1)).containsExactly(3, 2);
         assertThat(streamUpdateOrAdd(stream(1, 2), 2, (pv, nv) -> pv == 4)).containsExactly(1, 2, 2);
      }

      @Test
      public void testStreamUpdateOrAddWithMatcherWhenNullElement() {
         assertThat(streamUpdateOrAdd(stream(1, null, 2), 3, (pv, nv) -> pv == null)).containsExactly(1,
                                                                                                      3,
                                                                                                      2);
         assertThat(streamUpdateOrAdd(stream(1, 2),
                                      null,
                                      (pv, nv) -> pv == null && nv == null)).containsExactly(1, 2, null);
         assertThat(streamUpdateOrAdd(stream(1, null, 2), null, (pv, nv) -> pv == null)).containsExactly(1,
                                                                                                         null,
                                                                                                         2);
      }

      @Test
      public void testStreamUpdateOrAddWithMatcherWhenMultipleMatches() {
         assertThat(streamUpdateOrAdd(stream(1, 2, 1), 3, (pv, nv) -> pv == 1)).containsExactly(3, 2, 3);
         assertThat(streamUpdateOrAdd(stream(1, 2, 1), 1, (pv, nv) -> pv > 0)).containsExactly(1, 1, 1);
      }

      @Test
      public void testStreamUpdateOrAddWithMapperWhenNominal() {
         assertThat(streamUpdateOrAdd(stream(1, 2),
                                      3,
                                      (pv, nv) -> true,
                                      (pv, nv) -> pv + nv)).containsExactly(4, 5);
         assertThat(streamUpdateOrAdd(stream(1, 2),
                                      3,
                                      (pv, nv) -> false,
                                      (pv, nv) -> (pv == null ? 0 : pv) + nv)).containsExactly(1, 2, 3);
      }

      @Test
      public void testStreamUpdateOrAddWithMapperWhenNullElement() {
         BinaryOperator<Integer> nullSafeMapper = (pv, nv) -> (pv != null ? pv : 0) + (nv != null ? nv : 0);

         assertThat(streamUpdateOrAdd(stream(1, null, 2),
                                      3,
                                      (pv, nv) -> true,
                                      nullSafeMapper)).containsExactly(4, 3, 5);
         assertThat(streamUpdateOrAdd(stream(1, 2),
                                      null,
                                      (pv, nv) -> true,
                                      nullSafeMapper)).containsExactly(1, 2);
         assertThat(streamUpdateOrAdd(stream(1, null, 2),
                                      null,
                                      (pv, nv) -> true,
                                      nullSafeMapper)).containsExactly(1, 0, 2);
         assertThat(streamUpdateOrAdd(stream(1, null, 2),
                                      3,
                                      (pv, nv) -> false,
                                      nullSafeMapper)).containsExactly(1, null, 2, 3);
         assertThat(streamUpdateOrAdd(stream(1, 2), null, (pv, nv) -> false, nullSafeMapper)).containsExactly(
               1,
               2,
               0);
         assertThat(streamUpdateOrAdd(stream(1, null, 2),
                                      null,
                                      (pv, nv) -> false,
                                      nullSafeMapper)).containsExactly(1, null, 2, 0);
      }

      @Test
      public void testStreamUpdateOrAddWithoutElementWhenNominal() {
         assertThat(streamUpdateOrAdd(stream(1, 2), pv -> pv == 1, pv -> 3)).containsExactly(3, 2);
         assertThat(streamUpdateOrAdd(stream(1, 2), pv -> pv == 4, pv -> 2)).containsExactly(1, 2, 2);
         assertThat(streamUpdateOrAdd(stream(1, 2), pv -> true, pv -> pv + 3)).containsExactly(4, 5);
         assertThat(streamUpdateOrAdd(stream(1, 2),
                                      pv -> false,
                                      pv -> (pv == null ? 0 : pv) + 3)).containsExactly(1, 2, 3);
      }

      @Test
      public void testStreamUpdateOrAddWithoutElementWhenNullElement() {
         assertThat(streamUpdateOrAdd(stream(1, null, 2), pv -> pv == null, pv -> 3)).containsExactly(1,
                                                                                                      3,
                                                                                                      2);
         assertThat(streamUpdateOrAdd(stream(1, 2), pv -> pv == null, pv -> null)).containsExactly(1,
                                                                                                   2,
                                                                                                   null);
         assertThat(streamUpdateOrAdd(stream(1, null, 2), pv -> pv == null, pv -> null)).containsExactly(1,
                                                                                                         null,
                                                                                                         2);
      }

      @Test
      public void testStreamUpdateOrAddWithoutElementWhenMultipleMatches() {
         assertThat(streamUpdateOrAdd(stream(1, 2, 1), pv -> pv == 1, pv -> 3)).containsExactly(3, 2, 3);
         assertThat(streamUpdateOrAdd(stream(1, 2, 1), pv -> pv > 0, pv -> 1)).containsExactly(1, 1, 1);
      }

      @Test
      public void testStreamUpdateOrAddWhenReplaceExistingElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, "a")).containsExactly("a", "b", "c");
      }

      @Test
      public void testStreamUpdateOrAddWhenAddElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, "d")).containsExactly("a", "b", "c", "d");
      }

      @Test
      public void testStreamUpdateOrAddWhenNullStream() {
         assertThatNullPointerException()
               .isThrownBy(() -> streamUpdateOrAdd(null, "d"))
               .withMessage("'stream' must not be null");
      }

      @Test
      public void testStreamUpdateOrAddWhenAddNullElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, null)).containsExactly("a", "b", "c", null);
      }

      @Test
      public void testStreamUpdateOrAddWhenReplaceNullElement() {
         Stream<String> stream = stream("a", null, "c");

         assertThat(streamUpdateOrAdd(stream, null)).containsExactly("a", null, "c");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherWhenReplaceExistingElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, "A", (p, e) -> "a".equals(p))).containsExactly("A", "b", "c");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherWhenReplaceMultipleElements() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, "A", (p, e) -> true)).containsExactly("A", "A", "A");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherWhenAddElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, "D", (p, e) -> false)).containsExactly("a", "b", "c", "D");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherWhenNullStream() {
         assertThatNullPointerException()
               .isThrownBy(() -> streamUpdateOrAdd(null, "d", (p, e) -> true))
               .withMessage("'stream' must not be null");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherWhenNullMatcher() {
         Stream<String> stream = stream("a", "b", "c");

         assertThatNullPointerException()
               .isThrownBy(() -> streamUpdateOrAdd(stream, "d", null))
               .withMessage("'matcher' must not be null");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherWhenAddNullElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, null, (p, e) -> false)).containsExactly("a", "b", "c", null);
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherWhenReplaceNullElement() {
         Stream<String> stream = stream("a", null, "c");

         assertThat(streamUpdateOrAdd(stream, null, (p, e) -> p == null)).containsExactly("a", null, "c");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenReplaceExistingElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream,
                                      "A",
                                      (p, e) -> "a".equals(p),
                                      (p, e) -> "+" + e)).containsExactly("+A", "b", "c");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenReplaceMultipleElements() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, "A", (p, e) -> true, (p, e) -> "+" + e)).containsExactly("+A",
                                                                                                       "+A",
                                                                                                       "+A");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenAddElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, "D", (p, e) -> false, (p, e) -> "+" + e)).containsExactly("a",
                                                                                                        "b",
                                                                                                        "c",
                                                                                                        "+D");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenNullStream() {
         assertThatNullPointerException()
               .isThrownBy(() -> streamUpdateOrAdd(null, "d", (p, e) -> true, (p, e) -> "+" + e))
               .withMessage("'stream' must not be null");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenNullMatcher() {
         Stream<String> stream = stream("a", "b", "c");

         assertThatNullPointerException()
               .isThrownBy(() -> streamUpdateOrAdd(stream, "d", null, (p, e) -> "+" + e))
               .withMessage("'matcher' must not be null");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenNullMapper() {
         Stream<String> stream = stream("a", "b", "c");

         assertThatNullPointerException()
               .isThrownBy(() -> streamUpdateOrAdd(stream, "d", (p, e) -> true, null))
               .withMessage("'mapper' must not be null");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenAddNullElement() {
         Stream<String> stream = stream("a", "b", "c");

         assertThat(streamUpdateOrAdd(stream, null, (p, e) -> false, (p, e) -> "+" + e)).containsExactly("a",
                                                                                                         "b",
                                                                                                         "c",
                                                                                                         "+null");
      }

      @Test
      public void testStreamUpdateOrAddUsingMatcherAndMapperWhenReplaceNullElement() {
         Stream<String> stream = stream("a", null, "c");

         assertThat(streamUpdateOrAdd(stream, null, (p, e) -> p == null, (p, e) -> "+" + e)).containsExactly(
               "a",
               "+null",
               "c");
      }

   }

   @Nested
   public class ReaderStream {

      private final Random random = new Random();

      @Test
      public void testReaderWhenNominal() throws IOException {
         char[] buffer = new char[5];
         reader(zeroCharacterStream().limit(5)).read(buffer, 0, 5);

         assertThat(buffer).containsExactly('0', '0', '0', '0', '0');
      }

      @Test
      public void testReaderWhenBadParameters() {
         char[] buffer = new char[5];
         assertThatNullPointerException().isThrownBy(() -> reader(zeroCharacterStream().limit(5)).read(null,
                                                                                                       0,
                                                                                                       -1));
      }

      @Test
      public void testReaderWhenIndexOutOfBounds() {
         char[] buffer = new char[5];
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> reader(
               zeroCharacterStream().limit(5)).read(buffer, 0, -1));
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> reader(
               zeroCharacterStream().limit(5)).read(buffer, -1, 5));
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> reader(
               zeroCharacterStream().limit(5)).read(buffer, 0, 6));
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> reader(
               zeroCharacterStream().limit(5)).read(buffer, 1, 5));
      }

      @Test
      public void testReaderWhenClosed() throws IOException {
         char[] buffer = new char[5];
         Reader reader = reader(zeroCharacterStream().limit(5));
         reader.read(buffer, 0, 2);
         reader.close();
         reader.close();
         assertThatIOException().isThrownBy(() -> reader.read(buffer, 3, 2));
      }

      @Test
      public void testInputStreamWhenNominal() throws IOException {
         byte[] buffer = new byte[5];
         inputStream(zeroByteStream().limit(5)).read(buffer, 0, 5);

         assertThat(buffer).containsExactly(0, 0, 0, 0, 0);
      }

      @Test
      public void testInputStreamWhenBadParameters() {
         byte[] buffer = new byte[5];
         assertThatNullPointerException().isThrownBy(() -> inputStream(zeroByteStream().limit(5)).read(null,
                                                                                                       0,
                                                                                                       -1));
      }

      @Test
      public void testInputStreamWhenIndexOutOfBounds() {
         byte[] buffer = new byte[5];
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> inputStream(
               zeroByteStream().limit(5)).read(buffer, 0, -1));
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> inputStream(
               zeroByteStream().limit(5)).read(buffer, -1, 5));
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> inputStream(
               zeroByteStream().limit(5)).read(buffer, 0, 6));
         assertThatExceptionOfType(IndexOutOfBoundsException.class).isThrownBy(() -> inputStream(
               zeroByteStream().limit(5)).read(buffer, 1, 5));
      }

      @Test
      public void testInputStreamWhenClosed() throws IOException {
         byte[] buffer = new byte[5];
         InputStream inputStream = inputStream(zeroByteStream().limit(5));
         inputStream.read(buffer, 0, 2);
         inputStream.close();
         inputStream.close();
         assertThatIOException().isThrownBy(() -> inputStream.read(buffer, 3, 2));
      }

      public Stream<Character> zeroCharacterStream() {
         return IntStream.generate(() -> '0').mapToObj(i -> (char) i);
      }

      public Stream<Byte> zeroByteStream() {
         return IntStream.generate(() -> (byte) 0).mapToObj(i -> (byte) i);
      }

   }

}