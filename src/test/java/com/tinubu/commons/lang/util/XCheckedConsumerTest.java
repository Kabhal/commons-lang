/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.OptionalUtils.peek;
import static com.tinubu.commons.lang.util.XCheckedConsumer.checkedConsumer;
import static com.tinubu.commons.lang.util.XCheckedConsumer.noop;
import static com.tinubu.commons.lang.util.XCheckedFunction.checkedFunction;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.function.Consumer;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class XCheckedConsumerTest {

   @Nested
   public class InferredCreation {

      @Test
      public void checkedConsumerWhenAnonymousClass() {
         new XCheckedConsumer<Integer, IOException>() {
            @Override
            public void acceptChecked(Integer i) throws FileNotFoundException {
            }
         }.accept(0);
      }

      @Test
      public void checkedConsumerWhenUncheckedLambda() {
         XCheckedConsumer<Integer, RuntimeException> inferred = checkedConsumer((Integer a) -> { });

         XCheckedConsumer<Integer, Exception> inferredExtendException = checkedConsumer((Integer a) -> { });

         XCheckedConsumer<Integer, RuntimeException> inferredSuperInput = checkedConsumer((Number a) -> { });
      }

      /**
       * Sadly, the current design supports these incoherent types assignments. This is due to the fact that
       * unchecked exceptions do not need to be explicitly declared, so the compiler allows them to be handled
       * less strictly in generic type inference.
       */
      @Test
      public void checkedConsumerWhenUncheckedConsumerInferredAsAnyException() {
         XCheckedConsumer<Integer, IllegalArgumentException> inferredLambdaWithAnyException =
               checkedConsumer((Integer a) -> { });

         XCheckedConsumer<Integer, IllegalArgumentException> inferredLambdaWithMismatchingUnchecked =
               checkedConsumer((Integer a) -> { throw new IllegalStateException("error"); });

         XCheckedConsumer<Integer, IOException> inferredLambdaWithMismatchingChecked =
               checkedConsumer((Integer a) -> { throw new IllegalStateException("error"); });

         CheckedConsumer<Integer> checkedConsumer = __ -> { throw new IOException("error"); };

         XCheckedConsumer<Integer, IllegalArgumentException> inferredConsumerWithMismatchingUnchecked =
               checkedConsumer((Consumer<Integer>) checkedConsumer);

         new XCheckedConsumer<Integer, IOException>() {
            @Override
            public void acceptChecked(Integer integer) throws IllegalStateException {
            }
         };

      }

      @Test
      public void checkedConsumerWhenCheckedLambda() {
         XCheckedConsumer<?, Exception> inferred = checkedConsumer(a -> {
            if (true) throw new Exception("error");
         });

         XCheckedConsumer<?, Exception> inferredMultipleThrows = checkedConsumer(a -> {
            if (true) {
               throw new IOException("io error");
            } else if (true) {
               throw new ClassNotFoundException("class error");
            }
         });
      }

      @Test
      public void checkedConsumerWhenSubExceptionCheckedLambda() {
         XCheckedConsumer<?, IOException> inferred = checkedConsumer(a -> {
            if (true) throw new IOException("error");
         });

         XCheckedConsumer<?, IOException> inferredSuperType = checkedConsumer(a -> {
            if (true) throw new FileNotFoundException("error");
         });
      }

      @Test
      public void checkedConsumerWhenConsumer() {
         XCheckedConsumer<Integer, RuntimeException> inferred = checkedConsumer((Consumer<Integer>) a -> { });
      }

      @Test
      public void checkedConsumerWhenConsumerInstanceOfCheckedConsumer() {
         CheckedConsumer<Integer> checkedConsumer1 = a -> { };

         XCheckedConsumer<Integer, RuntimeException> inferred1 =
               checkedConsumer((Consumer<Integer>) checkedConsumer1);

         assertThat(inferred1).isNotSameAs(checkedConsumer1);

         CheckedConsumer<Integer> checkedConsumer2 = __ -> { throw new IOException("error"); };

         XCheckedConsumer<Integer, UncheckedIOException> inferred2 =
               checkedConsumer((Consumer<Integer>) checkedConsumer2);

         assertThatThrownBy(() -> inferred2.acceptChecked(0))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void checkedConsumerWhenConsumerInstanceOfXCheckedConsumer() {
         XCheckedConsumer<Integer, ?> checkedConsumer1 = a -> { };

         XCheckedConsumer<Integer, RuntimeException> inferred =
               checkedConsumer((Consumer<Integer>) checkedConsumer1);

         assertThat(inferred).isNotSameAs(checkedConsumer1);

         XCheckedConsumer<Integer, ?> checkedConsumer2 = __ -> { throw new IOException("error"); };

         XCheckedConsumer<Integer, RuntimeException> inferred2 =
               checkedConsumer((Consumer<Integer>) checkedConsumer2);

         assertThatThrownBy(() -> inferred2.acceptChecked(0))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void checkedConsumerWhenCheckedConsumer() {
         XCheckedConsumer<Integer, Exception> inferred = checkedConsumer((CheckedConsumer<Integer>) a -> { });
         XCheckedConsumer<Integer, Exception> inferred2 =
               checkedConsumer((CheckedConsumer<Integer>) (__ -> { throw new IOException("error"); }));
      }

      @Test
      public void uncheckedWhenRuntimeThrow() {
         XCheckedConsumer<Integer, Exception> checkedConsumer =
               __ -> { throw new ClassNotFoundException("error"); };

         assertThatThrownBy(() -> checkedConsumer.unchecked(false).accept(0))
               .isInstanceOf(UncheckedException.class)
               .hasMessage("error")
               .hasCause(new ClassNotFoundException("error"));
      }

      @Test
      public void uncheckedWhenIOExceptionRuntimeThrow() {
         XCheckedConsumer<Integer, IOException> checkedConsumer = __ -> { throw new IOException("error"); };

         assertThatThrownBy(() -> checkedConsumer.unchecked(false).accept(0))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void uncheckedWhenSneakyThrow() {
         XCheckedConsumer<Integer, IOException> checkedConsumer = __ -> { throw new IOException("error"); };

         assertThatThrownBy(() -> checkedConsumer.unchecked(true).accept(0))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

      @Test
      public void autoUncheckedWhenUsedAsConsumer() {
         XCheckedConsumer<Integer, IOException> error = __ -> { throw new IOException("error"); };

         assertThatThrownBy(() -> peek(Optional.of(3), error))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void forceUncheckedWhenUsedAsConsumer() {
         XCheckedConsumer<Integer, IOException> error = __ -> { throw new IOException("error"); };

         assertThatThrownBy(() -> peek(Optional.of(3), error.unchecked(false)))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
         assertThatThrownBy(() -> peek(Optional.of(3), error.unchecked(true)))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

   }

   @Nested
   @SuppressWarnings("unchecked")
   public class TryFinally {

      @Test
      public void tryFinallyWhenNominal() {
         assertThat(checkedFunction(__ -> "Value").tryFinally().applyChecked(0)).isEqualTo("Value");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new InternalError("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new Error("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new ClassNotFoundException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new Exception("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new IllegalStateException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new RuntimeException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new FileNotFoundException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedFunction(__ -> {
            throw new IOException("Operation error");
         }).tryFinally().applyChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedFunction(__ -> "Value", Exception.class)
                          .tryFinally(finalizer1)
                          .applyChecked(0)).isEqualTo("Value");
         verify(finalizer1).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         checkedConsumer(__ -> { }, Exception.class).tryFinally(finalizer1, finalizer2).acceptChecked(0);

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new FileNotFoundException("Operation error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).acceptChecked(0))
               .withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new InternalError("Operation error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).acceptChecked(0))
               .withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedConsumer(__ -> { }, Exception.class)
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> { }, Exception.class)
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new Error("Consumer error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new Error("Consumer error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWithAcceptUncheckedWhenNominal() {
         checkedConsumer(__ -> { }).tryFinally().accept(0);
      }

      @Test
      public void tryFinallyWithAcceptUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }, Exception.class).tryFinally(finalizer1, finalizer2).accept(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Nested
   public class TryCatch {

      @Test
      public void tryCatchWhenNominal() {
         checkedConsumer(__ -> { }).tryCatch(() -> {
            throw new IllegalStateException();
         }).acceptChecked(0);
      }

      @Test
      public void tryCatchWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new InternalError("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new Error("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new ClassNotFoundException("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new Exception("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new IllegalStateException("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new RuntimeException("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new FileNotFoundException("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new IOException("Operation error");
         }).tryCatch(() -> { }).acceptChecked(0)).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         checkedConsumer(__ -> { }).tryCatch(finalizer1).acceptChecked(0);
         verify(finalizer1, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         checkedConsumer(__ -> { }).tryCatch(finalizer1, finalizer2).acceptChecked(0);

         verify(finalizer1, never()).runChecked();
         verify(finalizer2, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new FileNotFoundException("Operation error");
         }).tryCatch(finalizer1, finalizer2).acceptChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new InternalError("Operation error");
         }).tryCatch(finalizer1, finalizer2).acceptChecked(0)).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> XCheckedConsumer.<Integer, Exception>checkedConsumer(__ -> {
                  throw new IllegalStateException("error");
               }).tryCatch(finalizer1, finalizer2, finalizer3).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isOfAnyClassIn(Error.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new IllegalStateException("error");
               }).tryCatch(finalizer1, finalizer2, finalizer3).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isExactlyInstanceOf(IllegalStateException.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }).tryCatch(finalizer1, finalizer2).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new Error("Consumer error");
               }).tryCatch(finalizer1, finalizer2).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new Error("Consumer error");
               }).tryCatch(finalizer1, finalizer2).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         XCheckedConsumer
               .<Object, Exception>checkedConsumer(__ -> { })
               .tryCatch(IOException.class, finalizer)
               .acceptChecked(0);

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithException() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedConsumer(__ -> {
            if (true) throw new IOException("Operation error");
         }).tryCatch(IOException.class, finalizer).acceptChecked(0)).withMessage("Operation error");

         verify(finalizer).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithSubException() throws Exception {
         CheckedConsumer<FileNotFoundException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(FileNotFoundException failure) {
            }
         });

         XCheckedConsumer<Object, IOException> consumer = checkedConsumer(__ -> {
            if (true) throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> consumer.tryCatch(FileNotFoundException.class, finalizer).acceptChecked(0))
               .withMessage("Operation error");

         verify(finalizer).acceptChecked(any(FileNotFoundException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithMismatchingSubException() throws Exception {
         CheckedConsumer<EOFException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(EOFException failure) {
            }
         });

         XCheckedConsumer<Object, IOException> consumer = checkedConsumer(__ -> {
            if (true) throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> consumer.tryCatch(EOFException.class, finalizer).acceptChecked(0))
               .withMessage("Operation error");

         verify(finalizer, never()).acceptChecked(any(EOFException.class));
      }

      @Test
      public void tryCatchWithExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).acceptChecked(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithAcceptUncheckedWhenNominal() {
         checkedConsumer(__ -> { }).tryCatch(() -> {
            throw new IllegalStateException();
         }).accept(0);
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithAcceptUncheckedExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         XCheckedConsumer
               .<Object, Exception>checkedConsumer(__ -> { })
               .tryCatch(IOException.class, finalizer)
               .accept(0);

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      public void tryCatchWithAcceptUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }).tryCatch(finalizer1, finalizer2).accept(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithAcceptUncheckedAndExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(__ -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).accept(0))
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Consumer error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Nested
   public class AutoCloseAutoCloseable {

      @Test
      public void testAutoCloseAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         XCheckedConsumer.<AutoCloseable, Exception>noop().autoClose().acceptChecked(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new Error("Consumer error");
         }).autoClose().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new ClassNotFoundException("Consumer error");
         }).autoClose().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new IllegalStateException("Consumer error");
         }).autoClose().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new IOException("Consumer error");
         }).autoClose().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> noop().autoClose().acceptChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> noop().autoClose().acceptChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> noop().autoClose().acceptChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> noop().autoClose().acceptChecked(closeableStream))
               .withMessage("Close error");

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new InternalError("Consumer error");
               }).autoClose().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new ClassNotFoundException("Consumer error");
               }).autoClose().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }).autoClose().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseAutoCloseableWhenOperationAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new IOException("Consumer error");
               }).autoClose().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseWithAcceptUncheckedAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         XCheckedConsumer.<AutoCloseable, Exception>noop().autoClose().accept(closeableStream);

         verify(closeableStream).close();
      }

      @Test
      public void testAutoCloseWithAcceptUncheckedAutoCloseableWhenOperationAndCloseException()
            throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(UncheckedException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new ClassNotFoundException("Consumer error");
               }).autoClose().accept(closeableStream))
               .withMessage("Consumer error")
               .withCauseInstanceOf(ClassNotFoundException.class)
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

   @Nested
   public class AutoCloseOnThrowAutoCloseable {

      @Test
      public void testAcceptAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         XCheckedConsumer.<AutoCloseable, Exception>noop().autoCloseOnThrow().acceptChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new Error("Consumer error");
         }).autoCloseOnThrow().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new ClassNotFoundException("Consumer error");
         }).autoCloseOnThrow().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new IllegalStateException("Consumer error");
         }).autoCloseOnThrow().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedConsumer(s -> {
            if (true) throw new IOException("Consumer error");
         }).autoCloseOnThrow().acceptChecked(closeableStream)).withMessage("Consumer error");

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         noop().autoCloseOnThrow().acceptChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         noop().autoCloseOnThrow().acceptChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new IllegalStateException("Close error");
            }
         });

         noop().autoCloseOnThrow().acceptChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         noop().autoCloseOnThrow().acceptChecked(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationAndCloseError() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new Error("Close error");
            }
         });

         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new InternalError("Consumer error");
               }).autoCloseOnThrow().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(Error.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new ClassNotFoundException("Consumer error");
               }).autoCloseOnThrow().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationAndCloseRuntimeException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
               throw new ArrayIndexOutOfBoundsException("Close error");
            }
         });

         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new IllegalStateException("Consumer error");
               }).autoCloseOnThrow().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(ArrayIndexOutOfBoundsException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWhenOperationAndCloseIOException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws IOException {
               throw new IOException("Close error");
            }
         });

         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new IOException("Consumer error");
               }).autoCloseOnThrow().acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(IOException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWithExceptionClassWhenOperationAndCloseException() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         XCheckedConsumer<Object, IOException> consumer = checkedConsumer(s -> {
            if (true) throw new FileNotFoundException("Consumer error");
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> consumer
                     .autoCloseOnThrow(FileNotFoundException.class)
                     .acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptAutoCloseableWithMismatchingExceptionClassWhenOperationAndCloseException()
            throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         XCheckedConsumer<Object, IOException> consumer = checkedConsumer(s -> {
            if (true) throw new FileNotFoundException("Consumer error");
         });
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> consumer
                     .autoCloseOnThrow(FileNotFoundException.class)
                     .acceptChecked(closeableStream))
               .withMessage("Consumer error")
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

      @Test
      public void testAcceptWithAcceptUncheckedAutoCloseableWhenNominal() throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() {
            }
         });

         XCheckedConsumer.<AutoCloseable, Exception>noop().autoCloseOnThrow().accept(closeableStream);

         verify(closeableStream, never()).close();
      }

      @Test
      public void testAcceptWithAcceptUncheckedAutoCloseableWhenOperationAndCloseException()
            throws Exception {
         AutoCloseable closeableStream = spy(new AutoCloseable() {
            @Override
            public void close() throws Exception {
               throw new FileNotFoundException("Close error");
            }
         });

         assertThatExceptionOfType(UncheckedException.class)
               .isThrownBy(() -> checkedConsumer(s -> {
                  if (true) throw new ClassNotFoundException("Consumer error");
               }).autoCloseOnThrow().accept(closeableStream))
               .withMessage("Consumer error")
               .withCauseInstanceOf(ClassNotFoundException.class)
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(e1 -> {
                  assertThat(e1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(e1).hasMessage("Close error");
               }));

         verify(closeableStream).close();
      }

   }

}