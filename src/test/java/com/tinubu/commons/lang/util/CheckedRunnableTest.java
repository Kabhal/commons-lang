/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CheckedRunnable.checkedRunnable;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UncheckedIOException;
import java.util.Optional;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class CheckedRunnableTest {

   @Nested
   public class InferredCreation {

      @Test
      public void checkedRunnableWhenAnonymousClass() {
         new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
            }
         }.run();
      }

      @Test
      public void checkedRunnableWhenUncheckedLambda() {
         CheckedRunnable inferred = checkedRunnable(() -> {});

         CheckedRunnable inferredExtendException = checkedRunnable(() -> {});
      }

      @Test
      public void checkedRunnableWhenCheckedLambda() {
         CheckedRunnable inferred = checkedRunnable(() -> {
            throw new Exception("error");
         });

         CheckedRunnable inferredMultipleThrows = checkedRunnable(() -> {
            if (true) {
               throw new IOException("io error");
            } else {
               throw new ClassNotFoundException("class error");
            }
         });
      }

      @Test
      public void checkedRunnableWhenSubExceptionCheckedLambda() {
         CheckedRunnable inferred = checkedRunnable(() -> {
            throw new IOException("error");
         });

         CheckedRunnable inferredSuperType = checkedRunnable(() -> {
            throw new FileNotFoundException("error");
         });
      }

      @Test
      public void checkedRunnableWhenRunnable() {
         CheckedRunnable inferred = checkedRunnable((Runnable) () -> {});
      }

      @Test
      public void checkedRunnableWhenRunnableInstanceOfCheckedRunnable() {
         CheckedRunnable checkedRunnable1 = () -> {};

         CheckedRunnable inferred1 = checkedRunnable((Runnable) checkedRunnable1);

         assertThat(inferred1).isSameAs(checkedRunnable1);

         CheckedRunnable checkedRunnable2 = () -> {throw new IOException("error");};

         CheckedRunnable inferred2 = checkedRunnable((Runnable) checkedRunnable2);

         assertThatThrownBy(() -> inferred2.runChecked()).isInstanceOf(IOException.class).hasMessage("error");
      }

      @Test
      public void checkedRunnableWhenRunnableInstanceOfXCheckedRunnable() {
         XCheckedRunnable<?> checkedRunnable1 = () -> {};

         CheckedRunnable inferred = checkedRunnable((Runnable) checkedRunnable1);

         assertThat(inferred).isNotSameAs(checkedRunnable1);

         XCheckedRunnable<?> checkedRunnable2 = () -> {throw new IOException("error");};

         CheckedRunnable inferred2 = checkedRunnable((Runnable) checkedRunnable2);

         assertThatThrownBy(() -> inferred2.runChecked()).isInstanceOf(IOException.class).hasMessage("error");
      }

      @Test
      public void checkedRunnableWhenCheckedRunnable() {
         CheckedRunnable inferred = checkedRunnable((CheckedRunnable) () -> {});
         CheckedRunnable inferred2 =
               checkedRunnable((CheckedRunnable) (() -> {throw new IOException("error");}));
      }

      @Test
      public void uncheckedWhenRuntimeThrow() {
         CheckedRunnable checkedRunnable = () -> {throw new ClassNotFoundException("error");};

         assertThatThrownBy(() -> checkedRunnable.unchecked(false).run())
               .isInstanceOf(UncheckedException.class)
               .hasMessage("error")
               .hasCause(new ClassNotFoundException("error"));
      }

      @Test
      public void uncheckedWhenIOExceptionRuntimeThrow() {
         CheckedRunnable checkedRunnable = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedRunnable.unchecked(false).run())
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void uncheckedWhenSneakyThrow() {
         CheckedRunnable checkedRunnable = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedRunnable.unchecked(true).run())
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

      @Test
      public void autoUncheckedWhenUsedAsRunnable() {
         CheckedRunnable error = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.empty().ifPresentOrElse(__ -> {}, error))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void forceUncheckedWhenUsedAsRunnable() {
         CheckedRunnable error = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.empty().ifPresentOrElse(__ -> {}, error.unchecked(false)))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
         assertThatThrownBy(() -> Optional.empty().ifPresentOrElse(__ -> {}, error.unchecked(true)))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

   }

   @Nested
   @SuppressWarnings("unchecked")
   public class TryFinally {

      @Test
      public void tryFinallyWhenNominal() throws Exception {
         checkedRunnable(() -> {}).tryFinally().run();
      }

      @Test
      public void tryFinallyWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new InternalError("Operation error");
         }).tryFinally().run()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new Error("Operation error");
         }).tryFinally().run()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new ClassNotFoundException("Operation error");
         }).tryFinally().runChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new Exception("Operation error");
         }).tryFinally().run()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new IllegalStateException("Operation error");
         }).tryFinally().run()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new RuntimeException("Operation error");
         }).tryFinally().run()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new FileNotFoundException("Operation error");
         }).tryFinally().runChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new IOException("Operation error");
         }).tryFinally().runChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         checkedRunnable(() -> {}).tryFinally(finalizer1).run();
         verify(finalizer1).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         checkedRunnable(() -> {}).tryFinally(finalizer1, finalizer2).run();

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new FileNotFoundException("Operation error");
         }).tryFinally(finalizer1, finalizer2).runChecked()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new InternalError("Operation error");
         }).tryFinally(finalizer1, finalizer2).run()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedRunnable(() -> {})
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {})
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new IllegalStateException("Runnable error");
               }).tryFinally(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new Error("Runnable error");
               }).tryFinally(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new Error("Runnable error");
               }).tryFinally(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWithRunUncheckedWhenNominal() {
         checkedRunnable(() -> {}).tryFinally().run();
      }

      @Test
      public void tryFinallyWithRunUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new IllegalStateException("Runnable error");
               }).tryFinally(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Nested
   public class TryCatch {

      @Test
      public void tryCatchWhenNominal() throws Exception {
         checkedRunnable(() -> {}).tryCatch(() -> {
            throw new IllegalStateException();
         }).run();
      }

      @Test
      public void tryCatchWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new InternalError("Operation error");
         }).tryCatch(() -> {}).run()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new Error("Operation error");
         }).tryCatch(() -> {}).run()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new ClassNotFoundException("Operation error");
         }).tryCatch(() -> {}).runChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new Exception("Operation error");
         }).tryCatch(() -> {}).run()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new IllegalStateException("Operation error");
         }).tryCatch(() -> {}).run()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new RuntimeException("Operation error");
         }).tryCatch(() -> {}).run()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new FileNotFoundException("Operation error");
         }).tryCatch(() -> {}).runChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new IOException("Operation error");
         }).tryCatch(() -> {}).runChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         checkedRunnable(() -> {}).tryCatch(finalizer1).run();
         verify(finalizer1, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         checkedRunnable(() -> {}).tryCatch(finalizer1, finalizer2).run();

         verify(finalizer1, never()).runChecked();
         verify(finalizer2, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new FileNotFoundException("Operation error");
         }).tryCatch(finalizer1, finalizer2).runChecked()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new InternalError("Operation error");
         }).tryCatch(finalizer1, finalizer2).run()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {throw new IllegalStateException("error");})
                     .tryCatch(finalizer1, finalizer2, finalizer3)
                     .run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isOfAnyClassIn(Error.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {throw new IllegalStateException("error");})
                     .tryCatch(finalizer1, finalizer2, finalizer3)
                     .run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isExactlyInstanceOf(IllegalStateException.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new IllegalStateException("Runnable error");
               }).tryCatch(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new Error("Runnable error");
               }).tryCatch(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new Error("Runnable error");
               }).tryCatch(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         CheckedRunnable.<Exception>checkedRunnable(() -> {}).tryCatch(IOException.class, finalizer).run();

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithException() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedRunnable(() -> {
            throw new IOException("Operation error");
         }).tryCatch(IOException.class, finalizer).runChecked()).withMessage("Operation error");

         verify(finalizer).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithSubException() throws Exception {
         CheckedConsumer<FileNotFoundException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(FileNotFoundException failure) {
            }
         });

         CheckedRunnable runnable = checkedRunnable(() -> {
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> runnable.tryCatch(FileNotFoundException.class, finalizer).runChecked())
               .withMessage("Operation error");

         verify(finalizer).acceptChecked(any(FileNotFoundException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithMismatchingSubException() throws Exception {
         CheckedConsumer<EOFException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(EOFException failure) {
            }
         });

         CheckedRunnable runnable = checkedRunnable(() -> {
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> runnable.tryCatch(EOFException.class, finalizer).runChecked())
               .withMessage("Operation error");

         verify(finalizer, never()).acceptChecked(any(EOFException.class));
      }

      @Test
      public void tryCatchWithExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new IllegalStateException("Runnable error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithRunUncheckedWhenNominal() {
         checkedRunnable(() -> {}).tryCatch(() -> {
            throw new IllegalStateException();
         }).run();
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithRunUncheckedExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         CheckedRunnable.<Exception>checkedRunnable(() -> {}).tryCatch(IOException.class, finalizer).run();

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      public void tryCatchWithRunUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new IllegalStateException("Runnable error");
               }).tryCatch(finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithRunUncheckedAndExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedRunnable(() -> {
                  throw new IllegalStateException("Runnable error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).run())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               })).withMessage("Runnable error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

}