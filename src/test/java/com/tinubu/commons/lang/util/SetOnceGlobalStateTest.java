/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static org.assertj.core.api.Assertions.assertThatIllegalStateException;

import org.junit.jupiter.api.Test;

class SetOnceGlobalStateTest extends AbstractGlobalStateTest {

   @Override
   protected <S> GlobalState<S> globalState(S initial) {
      return new SetOnceGlobalState<>(initial);
   }

   @Override
   protected <S> GlobalState<S> globalState() {
      return new SetOnceGlobalState<>();
   }

   @Test
   public void testWhenSetAlreadySetDefaultState() {
      GlobalState<Integer> initializedState = globalState(3);

      assertThatIllegalStateException().isThrownBy(() -> initializedState.defaultState(null));
      assertThatIllegalStateException().isThrownBy(() -> initializedState.defaultState(4));

      GlobalState<Integer> uninitializedState = globalState();

      uninitializedState.defaultState(null);
      uninitializedState.defaultState(3);
      assertThatIllegalStateException().isThrownBy(() -> uninitializedState.defaultState(null));
      assertThatIllegalStateException().isThrownBy(() -> uninitializedState.defaultState(4));

      GlobalState<Integer> nullInitializedState = globalState(null);

      nullInitializedState.defaultState(null);
      nullInitializedState.defaultState(3);
      assertThatIllegalStateException().isThrownBy(() -> nullInitializedState.defaultState(null));
      assertThatIllegalStateException().isThrownBy(() -> nullInitializedState.defaultState(4));
   }

}