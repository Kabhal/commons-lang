/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CheckedSupplier.checkedSupplier;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.UncheckedIOException;
import java.util.Optional;
import java.util.function.Supplier;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class CheckedSupplierTest {

   @Nested
   public class InferredCreation {

      @Test
      public void checkedSupplierWhenAnonymousClass() {
         new CheckedSupplier<Integer>() {
            @Override
            public Integer getChecked() throws FileNotFoundException {
               return 1;
            }
         }.get();
      }

      @Test
      public void checkedSupplierWhenUncheckedLambda() {
         CheckedSupplier<Integer> inferred = checkedSupplier(() -> 1);

         CheckedSupplier<Integer> inferredExtendException = checkedSupplier(() -> 1);

         CheckedSupplier<Number> inferredExtendReturn = checkedSupplier(() -> 1);
      }

      @Test
      public void checkedSupplierWhenCheckedLambda() {
         CheckedSupplier<?> inferred = checkedSupplier(() -> {
            if (false) return 1;
            throw new Exception("error");
         });

         CheckedSupplier<?> inferredMultipleThrows = checkedSupplier(() -> {
            if (false) return 1;
            if (true) {
               throw new IOException("io error");
            } else {
               throw new ClassNotFoundException("class error");
            }
         });
      }

      @Test
      public void checkedSupplierWhenSubExceptionCheckedLambda() {
         CheckedSupplier<?> inferred = checkedSupplier(() -> {
            if (false) return 1;
            throw new IOException("error");
         });

         CheckedSupplier<?> inferredSuperType = checkedSupplier(() -> {
            if (false) return 1;
            throw new FileNotFoundException("error");
         });
      }

      @Test
      public void checkedSupplierWhenFunction() {
         CheckedSupplier<Integer> inferred = checkedSupplier((Supplier<Integer>) () -> 1);
      }

      @Test
      public void checkedSupplierWhenFunctionInstanceOfCheckedSupplier() {
         CheckedSupplier<Integer> checkedSupplier1 = () -> 1;

         CheckedSupplier<Integer> inferred1 = checkedSupplier((Supplier<Integer>) checkedSupplier1);

         assertThat(inferred1).isSameAs(checkedSupplier1);

         CheckedSupplier<Integer> checkedSupplier2 = () -> {throw new IOException("error");};

         CheckedSupplier<Integer> inferred2 = checkedSupplier((Supplier<Integer>) checkedSupplier2);

         assertThatThrownBy(() -> inferred2.getChecked()).isInstanceOf(IOException.class).hasMessage("error");
      }

      @Test
      public void checkedSupplierWhenFunctionInstanceOfXCheckedSupplier() {
         XCheckedSupplier<Integer, ?> checkedSupplier1 = () -> 1;

         CheckedSupplier<Integer> inferred = checkedSupplier((Supplier<Integer>) checkedSupplier1);

         assertThat(inferred).isNotSameAs(checkedSupplier1);

         XCheckedSupplier<Integer, ?> checkedSupplier2 = () -> {throw new IOException("error");};

         CheckedSupplier<Integer> inferred2 = checkedSupplier((Supplier<Integer>) checkedSupplier2);

         assertThatThrownBy(() -> inferred2.getChecked()).isInstanceOf(IOException.class).hasMessage("error");
      }

      @Test
      public void checkedSupplierWhenCheckedSupplier() {
         CheckedSupplier<Integer> inferred = checkedSupplier((CheckedSupplier<Integer>) () -> 1);
         CheckedSupplier<Integer> inferred2 =
               checkedSupplier((CheckedSupplier<Integer>) (() -> {throw new IOException("error");}));
      }

      @Test
      public void uncheckedWhenRuntimeThrow() {
         CheckedSupplier<Integer> checkedSupplier = () -> {throw new ClassNotFoundException("error");};

         assertThatThrownBy(() -> checkedSupplier.unchecked(false).get())
               .isInstanceOf(UncheckedException.class)
               .hasMessage("error")
               .hasCause(new ClassNotFoundException("error"));
      }

      @Test
      public void uncheckedWhenIOExceptionRuntimeThrow() {
         CheckedSupplier<Integer> checkedSupplier = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedSupplier.unchecked(false).get())
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void uncheckedWhenSneakyThrow() {
         CheckedSupplier<Integer> checkedSupplier = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> checkedSupplier.unchecked(true).get())
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

      @Test
      public void autoUncheckedWhenUsedAsFunction() {
         CheckedSupplier<Object> error = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.empty().orElseGet(error))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
      }

      @Test
      public void forceUncheckedWhenUsedAsFunction() {
         CheckedSupplier<Object> error = () -> {throw new IOException("error");};

         assertThatThrownBy(() -> Optional.empty().orElseGet(error.unchecked(false)))
               .isInstanceOf(UncheckedIOException.class)
               .hasMessage("error");
         assertThatThrownBy(() -> Optional.empty().orElseGet(error.unchecked(true)))
               .isInstanceOf(IOException.class)
               .hasMessage("error");
      }

   }

   @Nested
   @SuppressWarnings("unchecked")
   public class TryFinally {

      @Test
      public void tryFinallyWhenNominal() throws Exception {
         assertThat(checkedSupplier(() -> "Value").tryFinally().getChecked()).isEqualTo("Value");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new InternalError("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new Error("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new ClassNotFoundException("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new Exception("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new IllegalStateException("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new RuntimeException("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new FileNotFoundException("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new IOException("Operation error");
         }).tryFinally().getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryFinallyWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedSupplier(() -> "Value").tryFinally(finalizer1).getChecked()).isEqualTo("Value");
         verify(finalizer1).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedSupplier(() -> "Value").tryFinally(finalizer1, finalizer2).getChecked()).isEqualTo(
               "Value");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new FileNotFoundException("Operation error");
         }).tryFinally(finalizer1, finalizer2).getChecked()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new InternalError("Operation error");
         }).tryFinally(finalizer1, finalizer2).getChecked()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedSupplier(() -> "Value")
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> "Value")
                     .tryFinally(finalizer1, finalizer2, finalizer3)
                     .getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("Finalizer1 error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("Function error");
               }).tryFinally(finalizer1, finalizer2).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new Error("Function error");
               }).tryFinally(finalizer1, finalizer2).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new Error("Function error");
               }).tryFinally(finalizer1, finalizer2).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryFinallyWithGetUncheckedWhenNominal() {
         assertThat(checkedSupplier(() -> "Value").tryFinally().get()).isEqualTo("Value");
      }

      @Test
      public void tryFinallyWithGetUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("Function error");
               }).tryFinally(finalizer1, finalizer2).get())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

   @Nested
   public class TryCatch {

      @Test
      public void tryCatchWhenNominal() throws Exception {
         assertThat(checkedSupplier(() -> "Value").tryCatch(() -> {}).getChecked()).isEqualTo("Value");
      }

      @Test
      public void tryCatchWhenOperationFailsWithError() {
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new InternalError("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new Error("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithException() {
         assertThatExceptionOfType(ClassNotFoundException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new ClassNotFoundException("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyException() {
         assertThatExceptionOfType(Exception.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new Exception("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new IllegalStateException("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new RuntimeException("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithIOException() {
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new FileNotFoundException("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOperationFailsWithExactlyIOException() {
         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new IOException("Operation error");
         }).tryCatch(() -> {}).getChecked()).withMessage("Operation error");
      }

      @Test
      public void tryCatchWhenOneFinalizer() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedSupplier(() -> "Value").tryCatch(finalizer1).getChecked()).isEqualTo("Value");
         verify(finalizer1, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizers() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThat(checkedSupplier(() -> "Value").tryCatch(finalizer1, finalizer2).getChecked()).isEqualTo(
               "Value");

         verify(finalizer1, never()).runChecked();
         verify(finalizer2, never()).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFails() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(FileNotFoundException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new FileNotFoundException("Operation error");
         }).tryCatch(finalizer1, finalizer2).getChecked()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithError() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() {
            }
         });
         assertThatExceptionOfType(InternalError.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new InternalError("Operation error");
         }).tryCatch(finalizer1, finalizer2).getChecked()).withMessage("Operation error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithErrorFirst() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");
            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("error");
               }).tryCatch(finalizer1, finalizer2, finalizer3).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isOfAnyClassIn(Error.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isOfAnyClassIn(IllegalStateException.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndFinalizersFailWithRuntimeExceptionFirst()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws IllegalStateException {
               throw new IllegalStateException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         CheckedRunnable finalizer3 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer3 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("error");
               }).tryCatch(finalizer1, finalizer2, finalizer3).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(f1 -> {
                  assertThat(f1).isExactlyInstanceOf(IllegalStateException.class);
                  assertThat(f1).hasMessage("Finalizer1 error");
               }, f2 -> {
                  assertThat(f2).isExactlyInstanceOf(InterruptedIOException.class);
                  assertThat(f2).hasMessage("Finalizer2 error");
               }, f3 -> {
                  assertThat(f3).isExactlyInstanceOf(Error.class);
                  assertThat(f3).hasMessage("Finalizer3 error");
               }))
               .withMessage("error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
         verify(finalizer3).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsAndFinalizersFail() throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("Function error");
               }).tryCatch(finalizer1, finalizer2).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new Error("Function error");
               }).tryCatch(finalizer1, finalizer2).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWhenMultipleFinalizersAndOperationFailsWithErrorAndFinalizersFailWithError()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(Error.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new Error("Function error");
               }).tryCatch(finalizer1, finalizer2).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThat(CheckedSupplier
                          .<String, Exception>checkedSupplier(() -> "Value")
                          .tryCatch(IOException.class, finalizer)
                          .getChecked()).isEqualTo("Value");

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithException() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThatExceptionOfType(IOException.class).isThrownBy(() -> checkedSupplier(() -> {
            if (false) return 1;
            throw new IOException("Operation error");
         }).tryCatch(IOException.class, finalizer).getChecked()).withMessage("Operation error");

         verify(finalizer).acceptChecked(any(IOException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithSubException() throws Exception {
         CheckedConsumer<FileNotFoundException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(FileNotFoundException failure) {
            }
         });

         CheckedSupplier<Object> function = checkedSupplier(() -> {
            if (false) return 1;
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> function.tryCatch(FileNotFoundException.class, finalizer).getChecked())
               .withMessage("Operation error");

         verify(finalizer).acceptChecked(any(FileNotFoundException.class));
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithExceptionClassWhenOperationFailsWithMismatchingSubException() throws Exception {
         CheckedConsumer<EOFException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(EOFException failure) {
            }
         });

         CheckedSupplier<Object> function = checkedSupplier(() -> {
            if (false) return 1;
            throw new FileNotFoundException("Operation error");
         });
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> function.tryCatch(EOFException.class, finalizer).getChecked())
               .withMessage("Operation error");

         verify(finalizer, never()).acceptChecked(any(EOFException.class));
      }

      @Test
      public void tryCatchWithExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("Function error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).getChecked())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithGetUncheckedWhenNominal() {
         assertThat(checkedSupplier(() -> "Value").tryCatch(() -> {}).get()).isEqualTo("Value");
      }

      @Test
      @SuppressWarnings("unchecked")
      public void tryCatchWithGetUncheckedExceptionClassWhenNominal() throws Exception {
         CheckedConsumer<IOException> finalizer = spy(new CheckedConsumer<>() {
            @Override
            public void acceptChecked(IOException failure) {
            }
         });

         assertThat(CheckedSupplier
                          .<String, Exception>checkedSupplier(() -> "Value")
                          .tryCatch(IOException.class, finalizer)
                          .get()).isEqualTo("Value");

         verify(finalizer, never()).acceptChecked(any(IOException.class));
      }

      @Test
      public void tryCatchWithGetUncheckedWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws FileNotFoundException {
               throw new FileNotFoundException("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("Function error");
               }).tryCatch(finalizer1, finalizer2).get())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(FileNotFoundException.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

      @Test
      public void tryCatchWithGetUncheckedAndExceptionClassWhenMultipleFinalizersAndOperationFailsAndFinalizersFail()
            throws Exception {
         CheckedRunnable finalizer1 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws Error {
               throw new Error("Finalizer1 error");

            }
         });
         CheckedRunnable finalizer2 = spy(new CheckedRunnable() {
            @Override
            public void runChecked() throws InterruptedIOException {
               throw new InterruptedIOException("Finalizer2 error");
            }
         });
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> checkedSupplier(() -> {
                  if (false) return 1;
                  throw new IllegalStateException("Function error");
               }).tryCatch(IllegalStateException.class, finalizer1, finalizer2).get())
               .satisfies(e -> assertThat(e.getSuppressed()).satisfiesExactly(s1 -> {
                  assertThat(s1).isOfAnyClassIn(Error.class);
                  assertThat(s1).hasMessage("Finalizer1 error");
               }, s2 -> {
                  assertThat(s2).isOfAnyClassIn(InterruptedIOException.class);
                  assertThat(s2).hasMessage("Finalizer2 error");
               }))
               .withMessage("Function error");

         verify(finalizer1).runChecked();
         verify(finalizer2).runChecked();
      }

   }

}