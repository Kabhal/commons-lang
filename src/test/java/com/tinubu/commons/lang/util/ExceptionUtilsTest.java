/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

// FIXME more tests + move to XChecked*Test + rename test methods
class ExceptionUtilsTest {

   @Nested
   public class SneakyThrow {

      @Test
      public void sneakyThrowWhenNominal() {
         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> sneakyThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenError() {
         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> sneakyThrow(new InternalError("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> sneakyThrow(new Error("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> sneakyThrow(new IllegalStateException("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> sneakyThrow(new RuntimeException("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenException() {
         assertThatExceptionOfType(ClassNotFoundException.class)
               .isThrownBy(() -> sneakyThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenExactlyException() {
         assertThatExceptionOfType(Exception.class)
               .isThrownBy(() -> sneakyThrow(new Exception("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenIOException() {
         assertThatExceptionOfType(FileNotFoundException.class)
               .isThrownBy(() -> sneakyThrow(new FileNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void sneakyThrowWhenExactlyIOException() {
         assertThatExceptionOfType(IOException.class)
               .isThrownBy(() -> sneakyThrow(new IOException("Error")))
               .withMessage("Error");
      }
   }

   @Nested
   public class RuntimeThrow {

      @Test
      public void runtimeThrowWhenNominal() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenError() {
         assertThatExceptionOfType(InternalError.class)
               .isThrownBy(() -> runtimeThrow(new InternalError("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenExactlyError() {
         assertThatExceptionOfType(Error.class).isThrownBy(() -> runtimeThrow(new Error("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenRuntimeException() {
         assertThatExceptionOfType(IllegalStateException.class)
               .isThrownBy(() -> runtimeThrow(new IllegalStateException("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenExactlyRuntimeException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new RuntimeException("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new ClassNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenExactlyException() {
         assertThatExceptionOfType(RuntimeException.class)
               .isThrownBy(() -> runtimeThrow(new Exception("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenIOException() {
         assertThatExceptionOfType(UncheckedIOException.class)
               .isThrownBy(() -> runtimeThrow(new FileNotFoundException("Error")))
               .withMessage("Error");
      }

      @Test
      public void runtimeThrowWhenExactlyIOException() {
         assertThatExceptionOfType(UncheckedIOException.class)
               .isThrownBy(() -> runtimeThrow(new IOException("Error")))
               .withMessage("Error");
      }
   }

}
