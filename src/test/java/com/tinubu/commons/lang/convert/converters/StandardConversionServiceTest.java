/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.converters;

import static com.tinubu.commons.lang.convert.services.StandardConversionService.clearRegisteredConverters;
import static com.tinubu.commons.lang.convert.services.StandardConversionService.registerConverter;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Base64;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.convert.Converter;
import com.tinubu.commons.lang.convert.ConverterNotFoundException;
import com.tinubu.commons.lang.convert.services.StandardConversionService;
import com.tinubu.commons.lang.util.Equator;
import com.tinubu.commons.lang.util.types.Base64String;

class StandardConversionServiceTest {

   @Test
   public void testCanConvertWhenNominal() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.canConvert("3", String.class, Integer.class)).isTrue();
      assertThat(service.canConvert(0L, Long.class, String.class)).isTrue();
   }

   @Test
   public void testCanConvertWhenBadValue() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.canConvert("", String.class, Integer.class)).isTrue();
   }

   @Test
   public void testCanConvertWhenNullValue() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.canConvert(null, String.class, Long.class)).isTrue();
   }

   @Test
   public void testCanConvertWhenSameType() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.canConvert("", String.class, String.class)).isTrue();
   }

   @Test
   public void testCanConvertWhenUnexpectedError() {
      try {
         clearRegisteredConverters(String.class, Equator.class);
         registerConverter(String.class, Equator.class, new Converter<String, Equator<String>>() {
            @Override
            public boolean canConvert(String source) {
               throw new IllegalStateException("canConvert error");
            }

            @Override
            public Equator<String> convert(String source) {
               throw new UnsupportedOperationException();
            }
         });

         StandardConversionService service = new StandardConversionService();

         assertThatExceptionOfType(ConversionFailedException.class)
               .isThrownBy(() -> service.canConvert("content", String.class, Equator.class))
               .withMessage(
                     "Conversion from 'java.lang.String' to 'com.tinubu.commons.lang.util.Equator' failed for 'content' value")
               .withCauseInstanceOf(IllegalStateException.class);
      } finally {
         clearRegisteredConverters(String.class, Equator.class);
      }
   }

   @Test
   public void testConvertWhenNominal() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert("3", String.class)).isEqualTo("3");
      assertThat(service.convert(3L, String.class)).isEqualTo("3");
   }

   @Test
   public void testConvertWhenConverterNotFound() {
      clearRegisteredConverters(String.class, Equator.class);
      StandardConversionService service = new StandardConversionService();

      assertThatExceptionOfType(ConverterNotFoundException.class)
            .isThrownBy(() -> service.convert("content", Equator.class))
            .withMessage(
                  "No converter found for conversion from 'java.lang.String' to 'com.tinubu.commons.lang.util.Equator'");
   }

   @Test
   public void testConvertWhenConversionFailed() {
      StandardConversionService service = new StandardConversionService();

      assertThatExceptionOfType(ConversionFailedException.class)
            .isThrownBy(() -> service.convert("unsupported", Long.class))
            .withMessage(
                  "Conversion from 'java.lang.String' to 'java.lang.Long' failed for 'unsupported' value")
            .withCauseInstanceOf(NumberFormatException.class);
   }

   @Test
   public void testConvertWhenConversionUnexpectedlyFailed() {
      clearRegisteredConverters(String.class, Equator.class);
      registerConverter(String.class, Equator.class, source -> {
         throw new IllegalStateException("Error");
      });

      StandardConversionService service = new StandardConversionService();

      assertThatExceptionOfType(ConversionFailedException.class)
            .isThrownBy(() -> service.convert("content", Equator.class))
            .withMessage(
                  "Conversion from 'java.lang.String' to 'com.tinubu.commons.lang.util.Equator' failed for 'content' value")
            .withCauseInstanceOf(IllegalStateException.class);
   }

   @Test
   public void testConvertWhenExplicitSourceType() {
      StandardConversionService service = new StandardConversionService();

      assertThatExceptionOfType(ConverterNotFoundException.class)
            .isThrownBy(() -> service.convert(Path.of("/path"), String.class))
            .withMessage("No converter found for conversion from 'sun.nio.fs.UnixPath' to 'java.lang.String'");
      assertThat(service.convert(Path.of("/path"), Path.class, String.class)).isEqualTo("/path");
   }

   @Test
   public void testConvertWhenNoConversionNeeded() {
      StandardConversionService service = new StandardConversionService();

      Long source = 3L;
      assertThat(service.convert(source, Long.class)).isSameAs(source);
   }

   @Test
   public void testConvertWhenNullValue() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert(null, Long.class)).isNull();
   }

   @Test
   public void testNumberConverter() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert("3", String.class)).isEqualTo("3");
      assertThat(service.convert("3", Long.class)).isEqualTo(3L);
      assertThat(service.convert("3", Integer.class)).isEqualTo(3);
      assertThat(service.convert("3", Byte.class)).isEqualTo((byte) 3);
      assertThat(service.convert("3", Short.class)).isEqualTo((short) 3);
      assertThat(service.convert("3", Float.class)).isEqualTo(3f);
      assertThat(service.convert("NaN", Float.class)).isNaN();
      assertThat(service.convert("Infinity", Float.class)).isInfinite().isPositive();
      assertThat(service.convert("+Infinity", Float.class)).isInfinite().isPositive();
      assertThat(service.convert("-Infinity", Float.class)).isInfinite().isNegative();
      assertThat(service.convert("3", Double.class)).isEqualTo(3d);
      assertThat(service.convert("NaN", Double.class)).isNaN();
      assertThat(service.convert("Infinity", Double.class)).isInfinite().isPositive();
      assertThat(service.convert("+Infinity", Double.class)).isInfinite().isPositive();
      assertThat(service.convert("-Infinity", Double.class)).isInfinite().isNegative();
      assertThat(service.convert("3", BigDecimal.class)).isEqualTo(new BigDecimal("3"));
      assertThat(service.convert("3", BigInteger.class)).isEqualTo(new BigInteger("3"));

      assertThat(service.convert(3L, String.class)).isEqualTo("3");
      assertThat(service.convert(3, String.class)).isEqualTo("3");
      assertThat(service.convert((byte) 3, String.class)).isEqualTo("3");
      assertThat(service.convert((short) 3, String.class)).isEqualTo("3");
      assertThat(service.convert(3f, String.class)).isEqualTo("3.0");
      assertThat(service.convert(3d, String.class)).isEqualTo("3.0");
      assertThat(service.convert(new BigDecimal("3.00"), String.class)).isEqualTo("3");
      assertThat(service.convert(new BigInteger("3"), String.class)).isEqualTo("3");

      assertThat(service.convert(3L, Long.class)).isEqualTo(3L);
      assertThat(service.convert(3L, Integer.class)).isEqualTo(3);
      assertThat(service.convert(3L, Byte.class)).isEqualTo((byte) 3);
      assertThat(service.convert(3L, Short.class)).isEqualTo((short) 3);
      assertThat(service.convert(3L, Float.class)).isEqualTo(3f);
      assertThat(service.convert(Float.NaN, Float.class)).isNaN();
      assertThat(service.convert(3L, Double.class)).isEqualTo(3d);
      assertThat(service.convert(Double.NaN, Double.class)).isNaN();
      assertThat(service.convert(3L, BigDecimal.class)).isEqualTo(new BigDecimal("3"));
      assertThat(service.convert(3L, BigInteger.class)).isEqualTo(new BigInteger("3"));
   }

   @Test
   public void testBooleanConverter() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert("true", Boolean.class)).isTrue();
      assertThat(service.convert("TrUe", Boolean.class)).isTrue();
      assertThat(service.convert("false", Boolean.class)).isFalse();
      assertThat(service.convert("FaLsE", Boolean.class)).isFalse();

      assertThat(service.convert(true, String.class)).isEqualTo("true");
      assertThat(service.convert(false, String.class)).isEqualTo("false");
   }

   @Test
   public void testClassConverter() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert("java.util.List", Class.class)).isEqualTo(List.class);
      assertThat(service.convert(List.class, String.class)).isEqualTo("java.util.List");
   }

   @Test
   public void testDurationConverter() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert("PT4H", Duration.class)).isEqualTo(Duration.of(4, HOURS));
      assertThat(service.convert(Duration.of(4, HOURS), String.class)).isEqualTo("PT4H");
   }

   @Test
   public void testPathConverter() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert("/path", Path.class)).isEqualTo(Path.of("/path"));
      assertThat(service.convert("path", Path.class)).isEqualTo(Path.of("path"));
      assertThat(service.convert(Path.of("/path"), Path.class, String.class)).isEqualTo("/path");
      assertThat(service.convert(Path.of("path"), Path.class, String.class)).isEqualTo("path");
   }

   @Test
   public void testUriConverter() {
      StandardConversionService service = new StandardConversionService();

      assertThat(service.convert("http://localhost/path", URI.class)).isEqualTo(URI.create(
            "http://localhost/path"));
      assertThat(service.convert(URI.create("http://localhost/path"), String.class)).isEqualTo(
            "http://localhost/path");
   }

   @Test
   public void testPatternConverter() {
      StandardConversionService service = new StandardConversionService();

      Pattern pattern = Pattern.compile("content.*");

      assertThat(service.convert("content.*", Pattern.class))
            .usingComparator(patternComparator())
            .isEqualTo(pattern);
      assertThat(service.convert(pattern, String.class)).isEqualTo("content.*");
   }

   @Test
   public void testAutoDetectBase64ToBinaryConverter() {
      try {
         StandardConversionService.clearRegisteredConverters(String.class, byte[].class);
         StandardConversionService.registerConverter(String.class,
                                                     byte[].class,
                                                     new AutoDetectBase64ToBinaryConverter());
         StandardConversionService service = new StandardConversionService();

         byte[] payload = { 0, 1, 2, 3 };
         String base64Payload = Base64.getEncoder().encodeToString(payload);

         assertThat(service.convert(base64Payload, byte[].class)).isEqualTo(payload);
         assertThatExceptionOfType(ConverterNotFoundException.class).isThrownBy(() -> service.convert(payload,
                                                                                                      String.class));
      } finally {
         StandardConversionService.clearRegisteredConverters(String.class, byte[].class);
         registerConverter(Base64String.class,
                           byte[].class,
                           new Base64ToBinaryConverter(),
                           new BinaryToBase64Converter());
      }
   }

   @Test
   public void testBase64ToBinaryConverter() {
      StandardConversionService service = new StandardConversionService();

      byte[] payload = { 0, 1, 2, 3 };
      Base64String base64Payload = Base64String.encode(payload);

      assertThat(service.convert(base64Payload, byte[].class)).isEqualTo(payload);
      assertThat(service.convert(payload, Base64String.class)).isEqualTo(base64Payload);
   }

   private static Comparator<Pattern> patternComparator() {
      return (p1, p2) -> p1.toString().equals(p2.toString()) ? 0 : -1;
   }
}