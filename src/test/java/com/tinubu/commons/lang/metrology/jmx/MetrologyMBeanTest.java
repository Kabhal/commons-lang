/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.metrology.jmx;

import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import javax.management.AttributeNotFoundException;
import javax.management.MBeanAttributeInfo;
import javax.management.MBeanFeatureInfo;

import org.junit.jupiter.api.Test;

import com.tinubu.commons.lang.metrology.Counter;

public class MetrologyMBeanTest {

   @Test
   public void metrologyMbeanWhenNominal() throws AttributeNotFoundException {
      MetrologyMBean<BaseMetrology> mBean = new MetrologyMBean<>(new BaseMetrology(), "test");

      assertThat(mBean.getMBeanInfo().getDescription()).isEqualTo("test");
      assertThatAttribute(mBean, "base", "Base", 1);
      assertThatAttribute(mBean, "counter1", "Base counter 1", 1);
      assertThatAttribute(mBean, "counter2", "Base counter 2", 1);
   }

   @Test
   public void metrologyMbeanWhenSuperclass() throws AttributeNotFoundException {
      MetrologyMBean<MetrologyWithSuperclass> mBean =
            new MetrologyMBean<>(new MetrologyWithSuperclass(), "test");

      assertThat(mBean.getMBeanInfo().getDescription()).isEqualTo("test");
      assertThatAttribute(mBean, "base", "Base", 1);
      assertThatAttribute(mBean, "counter1", "Base counter 1", 1);
      assertThatAttribute(mBean, "counter2", "Base counter 2", 1);
   }

   @Test
   public void metrologyMbeanWhenSuperclassAndInterface() throws AttributeNotFoundException {
      MetrologyMBean<MetrologyWithSuperClassAndInterface> mBean =
            new MetrologyMBean<>(new MetrologyWithSuperClassAndInterface(), "test");

      assertThatAttribute(mBean, "base", "Base", 1);
      assertThatAttribute(mBean, "interface1", "Interface 1", 2);
      assertThatAttribute(mBean, "counter1", "Base counter 1", 2);
      assertThatAttribute(mBean, "counter2", "Base counter 2", 2);
   }

   @Test
   public void metrologyMbeanWhenSuperclassAndClashingInterfaces() throws AttributeNotFoundException {
      MetrologyMBean<MetrologyWithSuperClassAndClashingInterfaces> mBean =
            new MetrologyMBean<>(new MetrologyWithSuperClassAndClashingInterfaces(), "test");

      assertThatAttribute(mBean, "interface1", "Interface 1", 2);
      assertThatAttribute(mBean, "interface2", "Interface 2", 2);
      assertThatAttribute(mBean, "counter1", "Interface 2 counter 1", 2);
      assertThatAttribute(mBean, "counter2", "Interface 2 counter 2", 2);
   }

   @Test
   public void metrologyMbeanWhenSuperclassAndInterfaceWithRenaming() throws AttributeNotFoundException {
      MetrologyMBean<MetrologyWithSuperClassAndInterfaceWithRenaming> mBean =
            new MetrologyMBean<>(new MetrologyWithSuperClassAndInterfaceWithRenaming(), "test");

      assertThatAttribute(mBean, "interface1", "Interface 1", 2);
      assertThatAttribute(mBean, "counter1", "Base counter 1", 2);
      assertThatAttribute(mBean, "renamed-interface1", "Renamed interface 1", 2);
      assertThatAttribute(mBean, "renamed-counter1", "Renamed counter 1", 2);
      assertThatAttribute(mBean, "counter2", "", 2);
   }

   private static <T> Optional<MBeanAttributeInfo> mBeanAttributeInfo(MetrologyMBean<T> mBean, String name) {
      return stream(mBean.getMBeanInfo().getAttributes()).filter(a -> a.getName().equals(name)).findFirst();
   }

   private static <T> void assertThatAttribute(MetrologyMBean<T> mBean,
                                               String name,
                                               String description,
                                               Integer value) throws AttributeNotFoundException {
      assertThat(mBean.getAttribute(name)).isEqualTo(value);
      assertThat(mBeanAttributeInfo(mBean, name)).map(MBeanFeatureInfo::getDescription).hasValue(description);
   }

}

interface InterfaceMetrology1 {

   @Counter(description = "Interface 1")
   int interface1();

   @Counter(description = "Interface 1 counter 1")
   int counter1();

   @Counter(description = "Interface 1 counter 2")
   int counter2();

}

interface InterfaceMetrology2 {

   @Counter(description = "Interface 2")
   int interface2();

   @Counter(description = "Interface 2 counter 1")
   int counter1();

   @Counter(description = "Interface 2 counter 2")
   int counter2();

}

class BaseMetrology {

   @Counter(description = "Base")
   int base() {
      return 1;
   }

   @Counter(description = "Base counter 1")
   int counter1() {
      return 1;
   }

   @Counter(description = "Base counter 2")
   int counter2() {
      return 1;
   }

}

class MetrologyWithSuperclass extends BaseMetrology {

}

class MetrologyWithSuperClassAndInterface extends BaseMetrology implements InterfaceMetrology1 {

   @Override
   public int interface1() {
      return 2;
   }

   @Override
   public int counter1() {
      return 2;
   }

   @Override
   public int counter2() {
      return 2;
   }
}

class MetrologyWithSuperClassAndClashingInterfaces implements InterfaceMetrology1, InterfaceMetrology2 {

   @Override
   public int interface1() {
      return 2;
   }

   @Override
   public int interface2() {
      return 2;
   }

   @Override
   public int counter1() {
      return 2;
   }

   @Override
   public int counter2() {
      return 2;
   }
}

class MetrologyWithSuperClassAndInterfaceWithRenaming extends BaseMetrology implements InterfaceMetrology1 {

   @Override
   @Counter(value = "renamed-interface1", description = "Renamed interface 1")
   public int interface1() {
      return 2;
   }

   @Override
   @Counter(value = "renamed-counter1", description = "Renamed counter 1")
   public int counter1() {
      return 2;
   }

   @Override
   @Counter
   public int counter2() {
      return 2;
   }
}