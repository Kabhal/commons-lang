/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.services;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Collections.emptyList;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.time.Duration;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.convert.ConversionService;
import com.tinubu.commons.lang.convert.Converter;
import com.tinubu.commons.lang.convert.ConverterNotFoundException;
import com.tinubu.commons.lang.convert.converters.Base64ToBinaryConverter;
import com.tinubu.commons.lang.convert.converters.BinaryToBase64Converter;
import com.tinubu.commons.lang.convert.converters.NumberToNumberConverter;
import com.tinubu.commons.lang.convert.converters.NumberToStringConverter;
import com.tinubu.commons.lang.convert.converters.StringToNumberConverter;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.types.Base64String;

/**
 * Default {@link ConversionService} implementation for standard types.
 */
public class StandardConversionService implements ConversionService {

   private static final Map<Pair<Class<?>, Class<?>>, List<Converter<?, ?>>> CONVERTERS;

   static {
      CONVERTERS = new HashMap<>();

      for (Class<? extends Number> nc : Arrays.asList(Long.class,
                                                      Integer.class,
                                                      Byte.class,
                                                      Short.class,
                                                      Float.class,
                                                      Double.class,
                                                      BigDecimal.class,
                                                      BigInteger.class)) {
         registerConverter(String.class, nc, new StringToNumberConverter<>(nc));
         registerConverter(nc, String.class, new NumberToStringConverter<>());

         for (Class<? extends Number> nc2 : Arrays.asList(Long.class,
                                                          Integer.class,
                                                          Byte.class,
                                                          Short.class,
                                                          Float.class,
                                                          Double.class,
                                                          BigDecimal.class,
                                                          BigInteger.class)) {
            registerConverter(nc, nc2, new NumberToNumberConverter<>(nc2));
         }
      }

      registerConverter(String.class, char[].class, source -> {
         if (source == null) {
            return null;
         }

         return source.toCharArray();
      }, source -> {
         if (source == null) {
            return null;
         }

         return new String(source);
      });
      registerConverter(String.class, Boolean.class, source -> {
         if (source == null) {
            return null;
         }

         try {
            return Boolean.parseBoolean(source);
         } catch (NumberFormatException e) {
            throw new ConversionFailedException(String.class, Boolean.class, source, e);
         }
      }, source -> {
         if (source == null) {
            return null;
         }

         return source.toString();
      });
      registerConverter(String.class, Class.class, (Converter<String, Class<?>>) source -> {
         if (source == null) {
            return null;
         }

         try {
            return Class.forName(source);
         } catch (ClassNotFoundException e) {
            throw new ConversionFailedException(String.class, Class.class, source, e);
         }
      }, (Converter<Class<?>, String>) source -> {
         if (source == null) {
            return null;
         }

         return source.getName();
      });
      registerConverter(String.class, Duration.class, source -> {
         if (source == null) {
            return null;
         }

         try {
            return Duration.parse(source);
         } catch (DateTimeParseException e) {
            throw new ConversionFailedException(String.class, Duration.class, source, e);
         }
      }, source -> {
         if (source == null) {
            return null;
         }

         return source.toString();
      });
      registerConverter(String.class, Charset.class, source -> {
         if (source == null) {
            return null;
         }

         try {
            return Charset.forName(source);
         } catch (IllegalCharsetNameException | UnsupportedCharsetException e) {
            throw new ConversionFailedException(String.class, Charset.class, source, e);
         }
      }, source -> {
         if (source == null) {
            return null;
         }

         return source.name();
      });
      registerConverter(String.class, Path.class, source -> {
         if (source == null) {
            return null;
         }

         try {
            return Path.of(source);
         } catch (InvalidPathException e) {
            throw new ConversionFailedException(String.class, Path.class, source, e);
         }
      }, source -> {
         if (source == null) {
            return null;
         }

         return source.toString();
      });
      registerConverter(String.class, URI.class, source -> {
         if (source == null) {
            return null;
         }

         try {
            return URI.create(source);
         } catch (IllegalArgumentException e) {
            throw new ConversionFailedException(String.class, URI.class, source, e);
         }
      }, source -> {
         if (source == null) {
            return null;
         }

         return source.toString();
      });
      registerConverter(String.class, Pattern.class, source -> {
         if (source == null) {
            return null;
         }

         try {
            return Pattern.compile(source);
         } catch (PatternSyntaxException e) {
            throw new ConversionFailedException(String.class, Pattern.class, source, e);
         }
      }, source -> {
         if (source == null) {
            return null;
         }

         return source.toString();
      });
      registerConverter(Base64String.class,
                        byte[].class,
                        new Base64ToBinaryConverter(),
                        new BinaryToBase64Converter());
      registerConverter(String.class, Base64String.class, Base64String::of, Base64String::value);
   }

   /**
    * Registers a new converter to registry.
    *
    * @param sourceType conversion source class
    * @param targetType conversion target class
    * @param converter converter
    * @param <S> conversion source type
    * @param <T> conversion target type
    */
   public static <S, T> void registerConverter(Class<? extends S> sourceType,
                                               Class<? extends T> targetType,
                                               Converter<? extends S, ? extends T> converter) {
      CONVERTERS.computeIfAbsent(Pair.of(sourceType, targetType), __ -> new ArrayList<>()).add(converter);
   }

   /**
    * Registers a new converter to registry and its reverse in the same operation.
    *
    * @param sourceType conversion source class
    * @param targetType conversion target class
    * @param converter converter
    * @param reverseConverter reverse target to source converter
    * @param <S> conversion source type
    * @param <T> conversion target type
    */
   public static <S, T> void registerConverter(Class<? extends S> sourceType,
                                               Class<? extends T> targetType,
                                               Converter<? extends S, ? extends T> converter,
                                               Converter<? extends T, ? extends S> reverseConverter) {
      registerConverter(sourceType, targetType, converter);
      registerConverter(targetType, sourceType, reverseConverter);
   }

   /**
    * Clear all registered converters.
    */
   public static void clearRegisteredConverters() {
      CONVERTERS.clear();
   }

   /**
    * Clear all registered converters for specified source and target.
    *
    * @param <S> conversion source type
    * @param <T> conversion target type
    */
   public static <S, T> void clearRegisteredConverters(Class<? extends S> sourceType,
                                                       Class<? extends T> targetType) {
      nullable(CONVERTERS.get(Pair.of(sourceType, targetType))).ifPresent(List::clear);
   }

   @SuppressWarnings("unchecked")
   private static <S, T> List<Converter<S, T>> registeredConverters(Class<? extends S> sourceType,
                                                                    Class<T> targetType) {
      return nullable((List<Converter<S, T>>) (List<?>) CONVERTERS.get(Pair.of(sourceType, targetType)),
                      emptyList());
   }

   @Override
   public <S, T> boolean canConvert(S source, Class<S> sourceType, Class<T> targetType) {
      notNull(sourceType, "sourceType");
      notNull(targetType, "targetType");

      if (targetType.isAssignableFrom(sourceType)) {
         return true;
      }

      List<Converter<S, T>> converters = registeredConverters(sourceType, targetType);

      return converters.stream().anyMatch(c -> {
         try {
            return c.canConvert(source);
         } catch (Exception e) {
            throw new ConversionFailedException(sourceType, targetType, source, e);
         }
      });
   }

   @Override
   @SuppressWarnings("unchecked")
   public <S, T> T convert(S source, Class<S> sourceType, Class<T> targetType) {
      notNull(sourceType, "sourceType");
      notNull(targetType, "targetType");

      if (source == null) {
         return null;
      } else if (targetType.isAssignableFrom(sourceType)) {
         return (T) source;
      }

      List<Converter<S, T>> converters = registeredConverters(sourceType, targetType);

      return converters.stream().filter(c -> c.canConvert(source)).findFirst().map(converter -> {
         try {
            return converter.convert(source);
         } catch (ConversionFailedException e) {
            throw e;
         } catch (Exception e) {
            throw new ConversionFailedException(sourceType, targetType, source, e);
         }
      }).orElseThrow(() -> new ConverterNotFoundException(sourceType, targetType));
   }

}
