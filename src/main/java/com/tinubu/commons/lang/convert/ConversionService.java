/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert;

/**
 * Conversion service abstraction.
 */
public interface ConversionService {
   /**
    * Checks if implementation supports conversion from source to target type.
    *
    * @param source source value to convert
    * @param sourceType conversion source type
    * @param targetType conversion target type
    *
    * @return {@code true} if conversion is supported
    *
    * @throws ConversionFailedException if converter unexpectedly fails
    */
   <S, T> boolean canConvert(S source, Class<S> sourceType, Class<T> targetType);

   /**
    * Converts source object to target type. If source object is {@code null}, a {@code null} should be
    * returned.
    *
    * @param source conversion source object
    * @param sourceType conversion source type
    * @param targetType conversion target type
    * @param <T> conversion target type
    *
    * @return converted object
    *
    * @throws ConverterNotFoundException if conversion not supported
    * @throws ConversionFailedException if source object conversion fails
    */
   <S, T> T convert(S source, Class<S> sourceType, Class<T> targetType);

   /**
    * Converts source object to target type. If source object is {@code null}, a {@code null} should be
    * returned. Source type is extracted from source class, use {@link #convert(Object, Class, Class)} to use
    * correct converter.
    *
    * @param source conversion source object
    * @param targetType conversion target type
    * @param <T> conversion target type
    *
    * @return converted object
    *
    * @throws ConverterNotFoundException if conversion not supported
    * @throws ConversionFailedException if source object conversion fails
    */
   @SuppressWarnings("unchecked")
   default <S, T> T convert(S source, Class<T> targetType) {
      if (source == null) {
         return null;
      }

      return convert(source, (Class<S>) source.getClass(), targetType);
   }
}
