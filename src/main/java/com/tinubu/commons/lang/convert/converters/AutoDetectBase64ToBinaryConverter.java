/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.converters;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;

import java.util.Base64;
import java.util.Optional;
import java.util.regex.Pattern;

import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.convert.Converter;
import com.tinubu.commons.lang.util.Pair;

/**
 * Base64 String to byte[] converter.
 * This converter auto-detects Base64 content.
 *
 * @see Base64ToBinaryConverter
 */
public class AutoDetectBase64ToBinaryConverter implements Converter<String, byte[]> {

   /**
    * Memorizes last decode to optimize between subsequent canConvert and convert calls
    * with same source.
    */
   private final ThreadLocal<Pair<String, byte[]>> lastDecode = new ThreadLocal<>();

   @Override
   public boolean canConvert(String source) {
      if (source == null) {
         return true;
      }

      try {
         decode(source);
      } catch (IllegalArgumentException e) {
         return false;
      }

      return true;
   }

   @Override
   public byte[] convert(String source) {
      if (source == null) {
         return null;
      }

      try {
         return lastDecodedValue(source).orElseGet(() -> decode(source));
      } catch (IllegalArgumentException e) {
         throw new ConversionFailedException(String.class, Pattern.class, source, e);
      }
   }

   private Optional<byte[]> lastDecodedValue(String source) {
      Pair<String, byte[]> lastDecodeValue = lastDecode.get();
      if (lastDecodeValue != null && lastDecodeValue.getKey().equals(source)) {
         return optional(lastDecodeValue.getValue());
      } else {
         return optional();
      }
   }

   private byte[] decode(String source) {
      byte[] decode = Base64.getDecoder().decode(source);
      lastDecode.set(Pair.of(source, decode));
      return decode;
   }
}
