/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.converters;

import java.util.Base64;

import com.tinubu.commons.lang.convert.ConversionFailedException;
import com.tinubu.commons.lang.convert.Converter;
import com.tinubu.commons.lang.util.types.Base64String;

/**
 * byte[] to Base64 String converter.
 */
public class BinaryToBase64Converter implements Converter<byte[], Base64String> {

   @Override
   public Base64String convert(byte[] source) {
      if (source == null) {
         return null;
      }

      try {
         return encode(source);
      } catch (IllegalArgumentException e) {
         throw new ConversionFailedException(byte[].class, Base64String.class, source, e);
      }
   }

   private Base64String encode(byte[] source) {
      return Base64String.of(Base64.getEncoder().encodeToString(source));
   }
}
