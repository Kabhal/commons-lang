/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.convert.services;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.List;

import com.tinubu.commons.lang.convert.ConversionService;
import com.tinubu.commons.lang.convert.ConverterNotFoundException;

/**
 * Meta-{@link ConversionService} that supports multiple conversion services.
 */
public class UnionConversionService implements ConversionService {

   private final List<ConversionService> conversionServices;

   /**
    * Creates a union of specified conversion services. Services are listed from higher to lower priority.
    *
    * @param conversionServices conversion services to use
    */
   public UnionConversionService(List<ConversionService> conversionServices) {
      this.conversionServices = notNull(conversionServices, "conversionServices");
   }

   /**
    * Creates a union of specified conversion services. Services are listed from higher to lower priority.
    *
    * @param conversionServices conversion services to use
    */
   public UnionConversionService(ConversionService... conversionServices) {
      this(list(notNull(conversionServices, "conversionServices")));
   }

   @Override
   public <S, T> boolean canConvert(S source, Class<S> sourceType, Class<T> targetType) {
      for (ConversionService conversionService : conversionServices) {
         if (conversionService.canConvert(source, sourceType, targetType)) {
            return true;
         }
      }
      return false;
   }

   @Override
   public <S, T> T convert(S source, Class<S> sourceType, Class<T> targetType) {
      if (source == null) {
         return null;
      }

      for (ConversionService conversionService : conversionServices) {
         if (conversionService.canConvert(source, sourceType, targetType)) {
            return conversionService.convert(source, sourceType, targetType);
         }
      }

      throw new ConverterNotFoundException(sourceType, targetType);
   }
}
