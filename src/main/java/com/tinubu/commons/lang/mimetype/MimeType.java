/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mimetype;

import static com.tinubu.commons.lang.mimetype.MimeTypeFactory.mimeType;
import static com.tinubu.commons.lang.util.CollectionUtils.map;

import java.nio.charset.Charset;
import java.util.Map;
import java.util.Optional;

/**
 * MIME type abstraction.
 */
public interface MimeType extends Comparable<MimeType> {

   /**
    * Type part.
    *
    * @return type part, never {@code null}
    */
   String type();

   /**
    * Subtype part.
    *
    * @return subtype part, never {@code null}
    */
   String subtype();

   /**
    * Returns a new instance of this MIME type without parameters.
    *
    * @return new MIME type instance without parameters
    */
   default MimeType strippedParameters() {
      return mimeType(this, map());
   }

   /**
    * Returns a new instance of this MIME type with stripped experimental prefix {@code x-} if any.
    *
    * @return new MIME type instance without experimental prefix on subtype
    */
   MimeType strippedExperimental();

   /**
    * Returns a new instance of this MIME type with {@code *} type and preserved parameters.
    *
    * @return new instance of this MIME type with {@code *} type and preserved parameters
    */
   default MimeType typeWildcard() {
      return mimeType("*", subtype(), parameters());
   }

   /**
    * Returns a new instance of this MIME type with {@code *} subtype and preserved parameters.
    *
    * @return new instance of this MIME type with {@code *} subtype and preserved parameters
    */
   default MimeType subtypeWildcard() {
      return mimeType(type(), "*", parameters());
   }

   /**
    * Returns a new instance of this MIME type with specified charset parameter. Existing parameters are
    * preserved, excepting {@code charset} parameter that is overridden.
    *
    * @return new instance of this MIME type with specified charset parameter and preserved existing parameters
    */
   default MimeType charset(Charset charset) {
      Map<String, String> parameters = map();
      parameters.putAll(parameters());
      parameters.putAll(mimeType(this, charset).parameters());

      return mimeType(this, parameters);
   }

   /**
    * Optional parameters.
    *
    * @return parameters map indexed by parameter name, never {@code null}
    */
   Map<String, String> parameters();

   /**
    * Optional parameter value for specified parameter.
    *
    * @param parameter parameter name
    *
    * @return parameter value or {@link Optional#empty}
    */
   Optional<String> parameter(String parameter);

   /**
    * Optional {@code charset} parameter.
    *
    * @return charset parameter value or {@link Optional#empty}
    *
    * @implSpec {@link Charset} instantiation must not fail at this level, parameters must be validated
    *       at start.
    */
   Optional<Charset> charset();

   /**
    * Checks if specified MIME type is included in this MIME type. e.g.: {@code application/*} includes
    * {@code application/pdf}. The inverse is not true because this operation is not symmetric.
    * <p>
    * Parameters are ignored by this operation.
    *
    * @param mimeType MIME type to check
    *
    * @return {@code true} if specified MIME type is included in this MIME type
    *
    * @see #matches(MimeType) for symmetric implementation
    */
   boolean includes(MimeType mimeType);

   /**
    * Checks if specified MIME type matches this MIME type. e.g.: {@code application/*} includes
    * {@code application/pdf}. The inverse is also true because this operation is symmetric.
    * <p>
    * Parameters are ignored by this operation.
    *
    * @param mimeType MIME type to check
    *
    * @return {@code true} if specified MIME type is included in this MIME type
    *
    * @see #includes(MimeType)
    */
   boolean matches(MimeType mimeType);

   /**
    * Checks if specified MIME type equals this MIME type, considering only the type and subtype and ignoring
    * parameters.
    *
    * @param mimeType optional mime type to compare to, can be {@code null}
    *
    * @return {@code true} if MIME types matches, or {@code false} if specified mime-type does not match, or
    *       is {@code null}
    */
   boolean equalsTypeAndSubtype(MimeType mimeType);

   /**
    * Returns {@code true} if type is a wildcard.
    *
    * @return {@code true} if type is a wildcard
    */
   boolean wildcardType();

   /**
    * Returns {@code true} if subtype is a wildcard.
    *
    * @return {@code true} if subtype is a wildcard
    */
   boolean wildcardSubtype();

   /**
    * Indicates whether this MIME type is concrete, i.e. whether neither the type
    * nor the subtype is a wildcard character.
    *
    * @return whether this MIME type is concrete
    */
   default boolean concrete() {
      return !wildcardType() && !wildcardSubtype();
   }

}
