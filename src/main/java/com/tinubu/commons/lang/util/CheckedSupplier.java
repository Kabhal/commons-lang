/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

/**
 * Represents a supplier of results.
 * This is a functional interface whose functional method is {@link #getChecked()}.
 * {@link #getChecked()} can throw checked exceptions.
 * If this class is used as {@link Supplier}, then unchecked {@link #get()} will be called.
 * If this class is used as {@link Callable}, then checked {@link #call()} will be called as an alias of
 * {@link #getChecked()}.
 * <p>
 * There is no requirement that a new or distinct result be returned each time the supplier is invoked.
 *
 * @param <T> the type of results supplied by this supplier
 */
@FunctionalInterface
public interface CheckedSupplier<T> extends XCheckedSupplier<T, Exception> {

   /**
    * Returns a rethrowing supplier.
    * This factory method is useful for the rethrow-with-finalizers pattern.
    *
    * @param exception the exception to rethrow
    * @param <X> the type of the exception to rethrow
    *
    * @return rethrowing supplier
    */
   static <X extends Exception> CheckedSupplier<X> rethrow(X exception) {
      return () -> {
         throw exception;
      };
   }

   /**
    * Returns a callable from an anonymous lambda.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> Callable<T> callable(XCheckedSupplier<? extends T, ? extends X> supplier) {
      return (Callable<T>) supplier;
   }

   /**
    * Returns a boolean supplier from an anonymous lambda.
    * Any instance of {@link XCheckedSupplier} will be unchecked.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   static <X extends Exception> BooleanSupplier booleanSupplier(XCheckedSupplier<Boolean, ? extends X> supplier) {
      return supplier::get;
   }

   /**
    * Returns a checked supplier from an {@link XCheckedSupplier} or a checked anonymous lambda.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> CheckedSupplier<T> checkedSupplier(XCheckedSupplier<? extends T, ? extends X> supplier) {
      return checkedSupplier((Supplier<T>) supplier);
   }

   /**
    * Returns a checked supplier from a {@link Supplier} or an unchecked anonymous lambda.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    *
    * @implSpec Unlike {@link XCheckedSupplier#checkedSupplier(Supplier)}, specified supplier
    *       instance of {@link XCheckedSupplier} are not unchecked, because it's not necessary to infer
    *       exception type which is always {@link Exception}.
    */
   @SuppressWarnings("unchecked")
   static <T> CheckedSupplier<T> checkedSupplier(Supplier<? extends T> supplier) {
      requireNonNull(supplier);
      if (supplier instanceof CheckedSupplier) {
         return (CheckedSupplier<T>) supplier;
      } else if (supplier instanceof XCheckedSupplier) {
         return ((XCheckedSupplier<T, ?>) supplier)::getChecked;
      } else {
         return supplier::get;
      }
   }

   /**
    * Returns a checked supplier from a {@link BooleanSupplier}.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   static CheckedSupplier<Boolean> checkedSupplier(BooleanSupplier supplier) {
      requireNonNull(supplier);
      return supplier::getAsBoolean;
   }

   /**
    * Returns a checked supplier from a standard callable.
    *
    * @param callable callable to adapt
    *
    * @return checked supplier
    *
    * @implSpec Unlike {@link XCheckedSupplier#checkedSupplier(Callable)}, specified supplier
    *       instance of {@link XCheckedSupplier} are not unchecked, because it's not necessary to infer
    *       exception type which is always {@link Exception}.
    */
   @SuppressWarnings("unchecked")
   static <T> CheckedSupplier<T> checkedSupplier(Callable<? extends T> callable) {
      requireNonNull(callable);
      if (callable instanceof CheckedSupplier) {
         return (CheckedSupplier<T>) callable;
      } else if (callable instanceof XCheckedSupplier) {
         return ((XCheckedSupplier<T, ?>) callable)::getChecked;
      } else {
         return callable::call;
      }
   }

   /**
    * Returns a checked supplier from an {@link XCheckedRunnable} or a checked anonymous lambda.
    *
    * @param runnable runnable to adapt
    *
    * @return checked supplier
    */
   static <X extends Exception> CheckedSupplier<Void> checkedSupplier(XCheckedRunnable<X> runnable) {
      requireNonNull(runnable);
      return () -> {
         runnable.runChecked();
         return null;
      };
   }

   /**
    * Returns a checked supplier from a {@link Runnable} or an unchecked anonymous lambda.
    *
    * @param runnable runnable to adapt
    *
    * @return checked supplier
    *
    * @implSpec Unlike {@link XCheckedSupplier#checkedSupplier(Runnable)}, specified runnable
    *       instance of {@link XCheckedRunnable} are not unchecked, because it's not necessary to infer
    *       exception type which is always {@link Exception}.
    */
   static CheckedSupplier<Void> checkedSupplier(Runnable runnable) {
      requireNonNull(runnable);
      if (runnable instanceof CheckedRunnable) {
         return () -> {
            ((CheckedRunnable) runnable).runChecked();
            return null;
         };
      } else if (runnable instanceof XCheckedRunnable) {
         return () -> {
            ((XCheckedRunnable<?>) runnable).runChecked();
            return null;
         };
      } else {
         return () -> {
            runnable.run();
            return null;
         };
      }
   }

   @Override
   @SuppressWarnings("ThrowFromFinallyBlock")
   default CheckedSupplier<T> tryFinally(List<? extends XCheckedRunnable<? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return () -> {
         Throwable throwable = null;
         try {
            return getChecked();
         } catch (Throwable t) {
            throwable = t;
            throw t;
         } finally {
            for (XCheckedRunnable<? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.runChecked();
               } catch (Throwable finalizerThrowable) {
                  if (throwable != null) {
                     throwable.addSuppressed(finalizerThrowable);
                  } else {
                     throwable = finalizerThrowable;
                  }
               }
            }
            if (throwable != null) {
               throw sneakyThrow(throwable);
            }
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedSupplier<T> tryFinally(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryFinally(list(finalizers));
   }

   @Override
   default CheckedSupplier<T> tryFinally(Runnable... finalizers) {
      return tryFinally(list(stream(finalizers).map(XCheckedRunnable::checkedRunnable)));
   }

   @Override
   default CheckedSupplier<T> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return () -> {
         try {
            return getChecked();
         } catch (Throwable throwable) {
            for (XCheckedConsumer<Throwable, ? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.acceptChecked(throwable);
               } catch (Throwable ft) {
                  throwable.addSuppressed(ft);
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedSupplier<T> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedSupplier<T> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default <SX extends Exception> CheckedSupplier<T> tryCatch(Class<? extends SX> exceptionClass,
                                                              List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return () -> {
         try {
            return getChecked();
         } catch (Exception exception) {
            if (exceptionClass.isAssignableFrom(exception.getClass()))
               for (XCheckedConsumer<? super SX, ? extends Exception> finalizer : finalizers) {
                  try {
                     finalizer.acceptChecked(exceptionClass.cast(exception));
                  } catch (Throwable ft) {
                     exception.addSuppressed(ft);
                  }
               }
            throw exception;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends Exception> CheckedSupplier<T> tryCatch(Class<? extends SX> exceptionClass,
                                                              XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedSupplier<T> tryCatch(Class<? extends Exception> exceptionClass,
                                       XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

}
