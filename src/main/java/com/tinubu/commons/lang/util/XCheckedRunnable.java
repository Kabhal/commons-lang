/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;
import static java.util.Objects.requireNonNull;

import java.io.UncheckedIOException;
import java.util.List;
import java.util.function.Supplier;

/**
 * Represents a checked {@link Runnable}.
 * This is a functional interface whose functional method is {@link #runChecked()}.
 * {@link #runChecked()} can throw {@link X genericized} exceptions.
 * If this class is used as {@link Runnable}, then unchecked {@link #run()} will be called.
 *
 * @param <X> the type of the checked exception possibly thrown by the runnable
 */
@FunctionalInterface
public interface XCheckedRunnable<X extends Exception> extends Runnable {

   /**
    * Noop runnable.
    *
    * @return noop runnable
    */
   static <X extends Exception> XCheckedRunnable<X> noop() {
      return () -> {};
   }

   /**
    * Run unchecked operation.
    */
   @Override
   default void run() {
      this.unchecked().run();
   }

   /**
    * Run checked operation.
    * This operation can throw {@link X} exceptions.
    *
    * @throws X if an error occurs
    */
   void runChecked() throws X;

   /**
    * Returns a runnable from an anonymous lambda.
    *
    * @param runnable runnable to adapt
    *
    * @return runnable
    */
   static <X extends Exception> Runnable runnable(XCheckedRunnable<? extends X> runnable) {
      return runnable;
   }

   /**
    * Returns a checked runnable from an {@link XCheckedRunnable} or a checked anonymous lambda.
    *
    * @param runnable runnable to adapt
    *
    * @return checked runnable
    */
   @SuppressWarnings("unchecked")
   static <X extends Exception> XCheckedRunnable<X> checkedRunnable(XCheckedRunnable<? extends X> runnable) {
      return (XCheckedRunnable<X>) runnable;
   }

   /**
    * Returns a checked runnable from an {@link XCheckedRunnable} or a checked anonymous lambda.
    *
    * @param runnable runnable to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked runnable
    */
   @SuppressWarnings("unchecked")
   static <X extends Exception> XCheckedRunnable<X> checkedRunnable(XCheckedRunnable<? extends X> runnable,
                                                                    Class<X> thrownExceptionClass) {
      return (XCheckedRunnable<X>) runnable;
   }

   /**
    * Returns a checked runnable from a {@link Runnable} or an unchecked anonymous lambda.
    *
    * @param runnable runnable to adapt
    *
    * @return checked runnable
    *
    * @implSpec Even if specified runnable is an instance of {@link CheckedRunnable} or
    *       {@link XCheckedRunnable}, implementation will uncheck it like any {@link Runnable}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <X extends Exception> XCheckedRunnable<X> checkedRunnable(Runnable runnable) {
      requireNonNull(runnable);
      return runnable::run;
   }

   /**
    * Returns a checked runnable from a {@link Runnable} or an unchecked anonymous lambda.
    *
    * @param runnable runnable to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked runnable
    *
    * @implSpec Even if specified runnable is an instance of {@link CheckedRunnable} or
    *       {@link XCheckedRunnable}, implementation will uncheck it like any {@link Runnable}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <X extends Exception> XCheckedRunnable<X> checkedRunnable(Runnable runnable,
                                                                    Class<X> thrownExceptionClass) {
      requireNonNull(runnable);
      return runnable::run;
   }

   /**
    * Returns a checked runnable from a checked supplier.
    *
    * @param supplier supplier to adapt
    *
    * @return checked runnable
    */
   static <T, X extends Exception> XCheckedRunnable<X> checkedRunnable(XCheckedSupplier<? super T, ? extends X> supplier) {
      requireNonNull(supplier);
      return supplier::getChecked;
   }

   /**
    * Returns a checked runnable from a checked supplier.
    *
    * @param supplier supplier to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked supplier
    */
   static <T, X extends Exception> XCheckedRunnable<X> checkedRunnable(XCheckedSupplier<? super T, ? extends X> supplier,
                                                                       Class<X> thrownExceptionClass) {
      requireNonNull(supplier);
      return supplier::getChecked;
   }

   /**
    * Returns a checked runnable from a standard supplier.
    * Any instance of {@link XCheckedSupplier} will be unchecked.
    *
    * @param supplier supplier to adapt
    *
    * @return checked runnable
    *
    * @implSpec Even if specified supplier is an instance of {@link CheckedSupplier} or
    *       {@link XCheckedSupplier}, implementation will uncheck it like any {@link Supplier}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends RuntimeException> XCheckedRunnable<X> checkedRunnable(Supplier<? super T> supplier) {
      requireNonNull(supplier);
      return supplier::get;
   }

   /**
    * Returns a checked runnable from a standard supplier.
    * Any instance of {@link XCheckedSupplier} will be unchecked.
    *
    * @param supplier supplier to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked supplier
    *
    * @implSpec Even if specified supplier is an instance of {@link CheckedSupplier} or
    *       {@link XCheckedSupplier}, implementation will uncheck it like any {@link Supplier}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends RuntimeException> XCheckedRunnable<X> checkedRunnable(Supplier<? super T> supplier,
                                                                              Class<X> thrownExceptionClass) {
      requireNonNull(supplier);
      return supplier::get;
   }

   /**
    * Returns an unchecked runnable from this checked runnable, using either :
    * <ul>
    * <li>{@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} : exception is converted from {@link Exception} to {@link RuntimeException} using best-fitting exceptions (e.g.: {@link UncheckedIOException}, ...)</li>
    * <li>{@link ExceptionUtils#sneakyThrow(Throwable) sneaky-throw} : checked exception is rethrown as-is but Java does not force to catch checked exceptions anymore using a hack</li>
    * </ul>
    *
    * @param sneakyThrow whether to use sneaky-throw unchecking
    *
    * @return unchecked runnable
    */
   default Runnable unchecked(boolean sneakyThrow) {
      return () -> {
         try {
            this.runChecked();
         } catch (Exception e) {
            if (sneakyThrow) {
               throw ExceptionUtils.sneakyThrow(e);
            } else {
               throw ExceptionUtils.runtimeThrow(e);
            }
         }
      };
   }

   /**
    * Returns an unchecked runnable from this checked runnable, using
    * default {@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} unchecking.
    *
    * @return unchecked runnable
    */
   default Runnable unchecked() {
      return unchecked(false);
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, then ensure all specified finalizers
    * are always called in specified order.
    * <p>
    * Wrapped checked runnable thrown exception is the first thrown exception, either by this
    * runnable, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked runnable with finalizers
    */
   default XCheckedRunnable<X> tryFinally(List<? extends XCheckedRunnable<? extends X>> finalizers) {
      requireNonNull(finalizers);
      return () -> checkedSupplier(this).tryFinally(finalizers).getChecked();
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, then ensure all specified finalizers
    * are always called in specified order.
    * <p>
    * Wrapped checked runnable thrown exception is the first thrown exception, either by this
    * runnable, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked runnable with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedRunnable<X> tryFinally(XCheckedRunnable<? extends X>... finalizers) {
      return tryFinally(list(finalizers));
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, and, only if a {@link Throwable} is
    * thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked runnable thrown exception is this runnable thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked runnable with finalizers
    */
   default XCheckedRunnable<X> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return () -> checkedSupplier(this).tryCatch(finalizers).getChecked();
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, and, only if a {@link Throwable} is
    * thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked runnable thrown exception is this runnable thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked runnable with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedRunnable<X> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, and, only if a {@link Throwable} is
    * thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked runnable thrown exception is this runnable thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked runnable with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedRunnable<X> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, and, only if an {@link Exception}
    * instance of {@code exceptionClass} is thrown, ensure all specified finalizers are always called in
    * specified order.
    * <p>
    * Wrapped checked runnable thrown exception is this runnable thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked runnable with finalizers
    */
   default <SX extends X> XCheckedRunnable<X> tryCatch(Class<? extends SX> exceptionClass,
                                                       List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return () -> checkedSupplier(this).tryCatch(exceptionClass, finalizers).getChecked();
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, and, only if an {@link Exception}
    * instance of {@code exceptionClass} is thrown, ensure all specified finalizers are always called in
    * specified order.
    * <p>
    * Wrapped checked runnable thrown exception is this runnable thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked runnable with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends X> XCheckedRunnable<X> tryCatch(Class<? extends SX> exceptionClass,
                                                       XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   /**
    * Returns a wrapped checked runnable calling {@link #runChecked()}, and, only if an {@link Exception}
    * instance of {@code exceptionClass} is thrown, ensure all specified finalizers are always called in
    * specified order.
    * <p>
    * Wrapped checked runnable thrown exception is this runnable thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked runnable with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedRunnable<X> tryCatch(Class<? extends X> exceptionClass,
                                        XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

}
