/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util.types;

import java.util.Base64;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * Base64 {@link String} holder.
 * Hold value can be blank, and is always valid Base64.
 * This holder can hold {@code null} values for convenience. Note that two {@link Base64String} with
 * {@code null} values are not {@link #equals(Object) equals}.
 */
public class Base64String {
   /** Maximum Base64 string length to display. */
   static final int MAX_BASE64_DISPLAY_LENGTH = 100;

   private final String value;

   private Base64String(String value) {
      this.value = value;
   }

   /**
    * Creates a new instance from specified Base64 value.
    *
    * @param value Base64 value
    *
    * @return new instance
    *
    * @throws IllegalArgumentException if value is not valid Base64
    */
   public static Base64String of(String value) {
      try {
         decode(value);
      } catch (IllegalArgumentException e) {
         throw new IllegalArgumentException(String.format("Can't parse '%s' Base64 format",
                                                          base64DisplayString(value)), e);
      }

      return new Base64String(value);
   }

   /**
    * Creates a new instance by Base64 encoding specified binary value.
    *
    * @param value binary value to encode, or {@code null}
    *
    * @return new instance
    */
   public static Base64String encode(byte[] value) {
      if (value == null) {
         return new Base64String(null);
      } else {
         return new Base64String(Base64.getEncoder().encodeToString(value));
      }
   }

   /**
    * Returns Base64 decoded value.
    *
    * @return decoded value, or {@code null} if hold value is {@code null}
    */
   public byte[] decode() {
      return decode(value);
   }

   /**
    * @throws IllegalArgumentException if value is not valid Base64
    */
   private static byte[] decode(String value) {
      if (value == null) {
         return null;
      } else {
         return Base64.getDecoder().decode(value);
      }
   }

   /**
    * Returns hold value, possibly {@code null}.
    *
    * @return hold value or {@code null}
    */
   public String value() {
      return value;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Base64String that = (Base64String) o;
      if (value == null && that.value == null) {
         return false;
      }
      return Objects.equals(value, that.value);
   }

   @Override
   public int hashCode() {
      return Objects.hash(value);
   }

   @Override
   public String toString() {
      return new StringJoiner(", ", Base64String.class.getSimpleName() + "[", "]")
            .add("value='" + base64DisplayString(value) + "'")
            .toString();
   }

   private static String base64DisplayString(String value) {
      return value == null || value.length() < MAX_BASE64_DISPLAY_LENGTH
             ? value
             : value.substring(0, MAX_BASE64_DISPLAY_LENGTH) + "...";
   }

}
