/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Objects.requireNonNull;

import java.io.UncheckedIOException;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Represents a predicate (boolean-valued function) of one argument.
 * This is a functional interface whose functional method is {@link #testChecked(Object)}.
 * {@link #testChecked(Object)} can throw {@link X genericized} exceptions.
 * If this class is used as {@link Predicate}, then unchecked {@link #test(Object)} will be called.
 *
 * @param <T> the type of the input to the operation
 * @param <X> the type of the checked exception possibly thrown by the predicate
 */
@FunctionalInterface
public interface XCheckedPredicate<T, X extends Exception> extends Predicate<T> {

   /**
    * Always {@code true} predicate.
    *
    * @param <T> the type of the input to the operation
    * @param <X> the type of the checked exception possibly thrown by the predicate
    *
    * @return always {@code true} predicate
    */
   static <T, X extends Exception> XCheckedPredicate<T, X> alwaysTrue() {
      return __ -> true;
   }

   /**
    * Always {@code false} predicate.
    *
    * @param <T> the type of the input to the operation
    * @param <X> the type of the checked exception possibly thrown by the predicate
    *
    * @return always {@code false} predicate
    */
   static <T, X extends Exception> XCheckedPredicate<T, X> alwaysFalse() {
      return __ -> false;
   }

   /**
    * Non-{@code null} elements predicate.
    *
    * @param <T> the type of the input to the operation
    * @param <X> the type of the checked exception possibly thrown by the predicate
    *
    * @return Non-{@code null} elements predicate
    */
   static <T, X extends Exception> XCheckedPredicate<T, X> nonNull() {
      return Objects::nonNull;
   }

   /**
    * Performs this unchecked operation on the given argument.
    *
    * @param t the input argument
    */
   @Override
   default boolean test(T t) {
      return this.unchecked().test(t);
   }

   /**
    * Performs this checked operation on the given argument.
    * This operation can throw {@link X} exceptions.
    *
    * @param t the input argument
    *
    * @throws X if an error occurs
    */
   boolean testChecked(T t) throws X;

   /**
    * Returns a checked predicate from a {@link Predicate} or an unchecked anonymous lambda.
    *
    * @param predicate predicate to adapt
    *
    * @return checked predicate
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> XCheckedPredicate<T, X> checkedPredicate(XCheckedPredicate<? super T, ? extends X> predicate) {
      return (XCheckedPredicate<T, X>) predicate;
   }

   /**
    * Returns a checked predicate from a {@link Predicate} or an unchecked anonymous lambda.
    *
    * @param predicate predicate to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked predicate
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> XCheckedPredicate<T, X> checkedPredicate(XCheckedPredicate<? super T, ? extends X> predicate,
                                                                            Class<X> thrownExceptionClass) {
      return (XCheckedPredicate<T, X>) predicate;
   }

   /**
    * Returns a checked predicate from a {@link Predicate} or an unchecked anonymous lambda.
    *
    * @param predicate predicate to adapt
    *
    * @return checked predicate
    *
    * @implSpec Even if specified predicate is an instance of {@link CheckedPredicate} or
    *       {@link XCheckedPredicate}, implementation will uncheck it like any {@link Predicate}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends Exception> XCheckedPredicate<T, X> checkedPredicate(Predicate<? super T> predicate) {
      requireNonNull(predicate);
      return predicate::test;
   }

   /**
    * Returns a checked predicate from a {@link Predicate} or an unchecked anonymous lambda.
    *
    * @param predicate predicate to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked predicate
    *
    * @implSpec Even if specified predicate is an instance of {@link CheckedPredicate} or
    *       {@link XCheckedPredicate}, implementation will uncheck it like any {@link Predicate}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends Exception> XCheckedPredicate<T, X> checkedPredicate(Predicate<? super T> predicate,
                                                                            Class<X> thrownExceptionClass) {
      requireNonNull(predicate);
      return predicate::test;
   }

   /**
    * Returns an unchecked predicate from this checked predicate, using either :
    * <ul>
    * <li>{@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} : exception is converted from {@link Exception} to {@link RuntimeException} using best-fitting exceptions (e.g.: {@link UncheckedIOException}, ...)</li>
    * <li>{@link ExceptionUtils#sneakyThrow(Throwable) sneaky-throw} : checked exception is rethrown as-is but Java does not force to catch checked exceptions anymore using a hack</li>
    * </ul>
    *
    * @param sneakyThrow whether to use sneaky-throw unchecking
    *
    * @return unchecked predicate
    */
   default Predicate<T> unchecked(boolean sneakyThrow) {
      return t -> {
         try {
            return this.testChecked(t);
         } catch (Exception e) {
            if (sneakyThrow) {
               throw ExceptionUtils.sneakyThrow(e);
            } else {
               throw ExceptionUtils.runtimeThrow(e);
            }
         }
      };
   }

   /**
    * Returns an unchecked predicate from this checked predicate, using
    * default {@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} unchecking.
    *
    * @return unchecked predicate
    */
   default Predicate<T> unchecked() {
      return unchecked(false);
   }

   /**
    * Returns a wrapped auto-closing checked predicate from this checked predicate.
    * Predicate input will be auto-closed after being processed by this predicate.
    * If {@link T} does not implement {@link AutoCloseable}, no {@link AutoCloseable#close()} will be called.
    *
    * @return auto-closing checked predicate
    */
   @SuppressWarnings("unchecked")
   default XCheckedPredicate<T, Exception> autoClose() {
      return t -> {
         if (t instanceof AutoCloseable) {
            try (AutoCloseable autoCloseable = (AutoCloseable) t) {
               return testChecked((T) autoCloseable);
            }
         } else {
            return testChecked(t);
         }
      };
   }

   /**
    * Returns a wrapped auto-closing-on-throw checked predicate from this checked predicate.
    * Predicate input will be auto-closed after being processed by this predicate, only if processing fails.
    * If {@link T} does not implement {@link AutoCloseable}, no {@link AutoCloseable#close()} will be called.
    *
    * @return auto-closing-on-throw checked predicate
    */
   default XCheckedPredicate<T, Exception> autoCloseOnThrow() {
      return t -> {
         try {
            return testChecked(t);
         } catch (Throwable throwable) {
            if (t instanceof AutoCloseable) {
               try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                  throw throwable;
               }
            }
            throw throwable;
         }
      };
   }

   /**
    * Returns a wrapped auto-closing-on-throw checked predicate from this checked predicate.
    * Predicate input will be auto-closed after being processed by this predicate, only if processing fails
    * with an exception extending specified exception class.
    * If {@link T} does not implement {@link AutoCloseable}, no {@link AutoCloseable#close()} will be called.
    *
    * @param exceptionClass auto-close only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    *
    * @return auto-closing-on-throw checked predicate
    */
   default XCheckedPredicate<T, Exception> autoCloseOnThrow(Class<? extends X> exceptionClass) {
      requireNonNull(exceptionClass);
      return t -> {
         try {
            return testChecked(t);
         } catch (Throwable throwable) {
            if (exceptionClass.isAssignableFrom(throwable.getClass())) {
               if (t instanceof AutoCloseable) {
                  try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                     throw throwable;
                  }
               }
            }
            throw throwable;
         }
      };
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, then ensure all specified
    * finalizers are always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is the first thrown exception, either by this
    * predicate, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked predicate with finalizers
    *
    * @implNote The second catch is a {@link Throwable}, so that some {@link Error} would be ignored as
    *       suppressed, because we consider more important to execute all finalizers than re-throw a nth
    *       {@link Error}, in the special case the first exception that will be finally rethrown is an
    *       {@link Exception}. The final {@link ExceptionUtils#sneakyThrow(Throwable)} is required in the
    *       case the first exception is thrown by a finalizer, not the predicate, and is an {@link Error}, in
    *       this case it's not a problem to sneak it as you're not supposed to catch it.
    */
   @SuppressWarnings("ThrowFromFinallyBlock")
   default XCheckedPredicate<T, X> tryFinally(List<? extends XCheckedRunnable<? extends X>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         Throwable throwable = null;
         try {
            return testChecked(v);
         } catch (Throwable t) {
            throwable = t;
            throw t;
         } finally {
            for (XCheckedRunnable<? extends X> finalizer : finalizers) {
               try {
                  finalizer.runChecked();
               } catch (Throwable finalizerThrowable) {
                  if (throwable != null) {
                     throwable.addSuppressed(finalizerThrowable);
                  } else {
                     throwable = finalizerThrowable;
                  }
               }
            }
            if (throwable != null) {
               throw sneakyThrow(throwable);
            }
         }
      };
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, then ensure all specified
    * finalizers are always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is the first thrown exception, either by this
    * predicate, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked predicate with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedPredicate<T, X> tryFinally(XCheckedRunnable<? extends X>... finalizers) {
      return tryFinally(list(finalizers));
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, and, only if a
    * {@link Throwable} is thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is this predicate thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked predicate with finalizers
    */
   default XCheckedPredicate<T, X> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         try {
            return testChecked(v);
         } catch (Throwable throwable) {
            for (XCheckedConsumer<Throwable, ? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.acceptChecked(throwable);
               } catch (Throwable ft) {
                  throwable.addSuppressed(ft);
               }
            }
            throw throwable;
         }
      };
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, and, only if a
    * {@link Throwable} is thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is this predicate thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked predicate with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedPredicate<T, X> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, and, only if a
    * {@link Throwable} is thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is this predicate thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked predicate with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedPredicate<T, X> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, and, only if an
    * {@link Exception} instance of {@code exceptionClass} is thrown, ensure all specified finalizers are
    * always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is this predicate thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked predicate with finalizers
    */
   @SuppressWarnings("unchecked")
   default <SX extends X> XCheckedPredicate<T, X> tryCatch(Class<? extends SX> exceptionClass,
                                                           List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return v -> {
         try {
            return testChecked(v);
         } catch (Exception exception) {
            if (exceptionClass.isAssignableFrom(exception.getClass()))
               for (XCheckedConsumer<? super SX, ? extends Exception> finalizer : finalizers) {
                  try {
                     finalizer.acceptChecked(exceptionClass.cast(exception));
                  } catch (Throwable ft) {
                     exception.addSuppressed(ft);
                  }
               }
            throw (X) exception;
         }
      };
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, and, only if an
    * {@link Exception}instance of {@code exceptionClass} is thrown, ensure all specified finalizers are
    * always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is this predicate thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked predicate with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends X> XCheckedPredicate<T, X> tryCatch(Class<? extends SX> exceptionClass,
                                                           XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   /**
    * Returns a wrapped checked predicate calling {@link #testChecked(Object)}, and, only if an
    * {@link Exception} instance of {@code exceptionClass} is thrown, ensure all specified finalizers are
    * always called in specified order.
    * <p>
    * Wrapped checked predicate thrown exception is this predicate thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked predicate with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedPredicate<T, X> tryCatch(Class<? extends X> exceptionClass,
                                            XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default XCheckedPredicate<T, X> and(Predicate<? super T> other) {
      Objects.requireNonNull(other);
      return t -> test(t) && other.test(t);
   }

   default XCheckedPredicate<T, X> and(XCheckedPredicate<? super T, ? extends X> other) {
      Objects.requireNonNull(other);
      return t -> test(t) && other.testChecked(t);
   }

   @Override
   default XCheckedPredicate<T, X> negate() {
      return (t) -> !test(t);
   }

   @Override
   default XCheckedPredicate<T, X> or(Predicate<? super T> other) {
      Objects.requireNonNull(other);
      return t -> test(t) || other.test(t);
   }

   default XCheckedPredicate<T, X> or(XCheckedPredicate<? super T, ? extends X> other) {
      Objects.requireNonNull(other);
      return t -> test(t) || other.testChecked(t);
   }

}
