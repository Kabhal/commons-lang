/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.XCheckedSupplier.checkedSupplier;
import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.function.Supplier;

/**
 * Represents a checked {@link Runnable}.
 * This is a functional interface whose functional method is {@link #runChecked()}.
 * {@link #runChecked()} can throw checked exceptions.
 * If this class is used as {@link Runnable}, then unchecked {@link #run()} will be called.
 */
@FunctionalInterface
public interface CheckedRunnable extends XCheckedRunnable<Exception> {

   /**
    * Noop runnable.
    *
    * @return noop runnable
    */
   static CheckedRunnable noop() {
      return () -> {};
   }

   /**
    * Returns a runnable from an anonymous lambda.
    *
    * @param runnable runnable to adapt
    *
    * @return runnable
    */
   static <X extends Exception> Runnable runnable(XCheckedRunnable<? extends X> runnable) {
      return runnable;
   }

   /**
    * Returns a checked runnable from an anonymous lambda.
    *
    * @param runnable runnable to adapt
    *
    * @return checked runnable
    */
   static <X extends Exception> CheckedRunnable checkedRunnable(XCheckedRunnable<? extends X> runnable) {
      return checkedRunnable((Runnable) runnable);
   }

   /**
    * Returns a checked runnable from a standard runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked runnable
    *
    * @implSpec Unlike {@link XCheckedRunnable#checkedRunnable(Runnable)}, specified runnable
    *       instance of {@link XCheckedRunnable} are not unchecked, because it's not necessary to infer
    *       exception type which is always {@link Exception}.
    */
   static CheckedRunnable checkedRunnable(Runnable runnable) {
      requireNonNull(runnable);
      if (runnable instanceof CheckedRunnable) {
         return (CheckedRunnable) runnable;
      } else if (runnable instanceof XCheckedRunnable) {
         return ((XCheckedRunnable<?>) runnable)::runChecked;
      } else {
         return runnable::run;
      }
   }

   /**
    * Returns a checked runnable from an {@link XCheckedSupplier} or a checked anonymous lambda.
    *
    * @param supplier runnable to adapt
    *
    * @return checked runnable
    */
   static <T, X extends Exception> CheckedRunnable checkedRunnable(XCheckedSupplier<? super T, X> supplier) {
      requireNonNull(supplier);
      return supplier::getChecked;
   }

   /**
    * Returns a checked runnable from a {@link Supplier} or an unchecked anonymous lambda.
    *
    * @param supplier supplier to adapt
    *
    * @return checked runnable
    *
    * @implSpec Unlike {@link XCheckedRunnable#checkedRunnable(Supplier)}, specified supplier
    *       instance of {@link XCheckedSupplier} are not unchecked, because it's not necessary to infer
    *       exception type which is always {@link Exception}.
    */
   @SuppressWarnings("unchecked")
   static <T> CheckedRunnable checkedRunnable(Supplier<? super T> supplier) {
      requireNonNull(supplier);
      if (supplier instanceof CheckedSupplier) {
         return () -> ((CheckedSupplier<T>) supplier).getChecked();
      } else if (supplier instanceof XCheckedSupplier) {
         return () -> ((XCheckedSupplier<T, ?>) supplier).getChecked();
      } else {
         return supplier::get;
      }
   }

   @Override
   default CheckedRunnable tryFinally(List<? extends XCheckedRunnable<? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return () -> checkedSupplier(this).tryFinally(finalizers).getChecked();
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedRunnable tryFinally(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryFinally(list(finalizers));
   }

   @Override
   default CheckedRunnable tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return () -> checkedSupplier(this).tryCatch(finalizers).getChecked();
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedRunnable tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedRunnable tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default <SX extends Exception> CheckedRunnable tryCatch(Class<? extends SX> exceptionClass,
                                                           List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return () -> checkedSupplier(this).tryCatch(exceptionClass, finalizers).getChecked();
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends Exception> CheckedRunnable tryCatch(Class<? extends SX> exceptionClass,
                                                           XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedRunnable tryCatch(Class<? extends Exception> exceptionClass,
                                    XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

}
