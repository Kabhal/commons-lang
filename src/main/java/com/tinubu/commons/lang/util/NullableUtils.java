/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.or;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

/**
 * Conveys the nullability concept of an object.
 * This class aim is transpose object nullity to something else, e.g. {@link Optional}, or default value, ...
 * so that resulting object is not {@code null}. The only exception is {@link #nullable(Object)} that can
 * return {@code null}, and can be used for internal fields.
 */
public final class NullableUtils {

   private NullableUtils() {
   }

   /**
    * Converts an object that can be nullable to an {@link Optional} so that the result is never {@code null}.
    *
    * @param object nullable object
    * @param <T> object type
    *
    * @return {@link Optional} of object, never {@code null}
    */
   public static <T> Optional<T> nullable(T object) {
      return Optional.ofNullable(object);
   }

   /**
    * Converts an object that can be nullable to an {@link Optional} so that the result is never {@code null}.
    *
    * @param object nullable object
    * @param <T> object type
    *
    * @return {@link Optional} of object, never {@code null}
    */
   public static <T> Optional<T> nullable(Nullable<T> object) {
      return object == null ? Optional.empty() : object.optional();
   }

   /**
    * Provides a default value for a nullable object so that the result should not be {@code null}.
    *
    * @param object nullable object
    * @param defaultValue default value
    * @param <T> object type
    *
    * @return object value or default value
    */
   public static <T> T nullable(T object, T defaultValue) {
      return object != null ? object : defaultValue;
   }

   /**
    * Provides a default value for a nullable object.
    *
    * @param object nullable object
    * @param defaultValue default value, can be {@code null}
    * @param <T> object type
    *
    * @return object value or default value
    */
   public static <T> T nullable(Nullable<T> object, T defaultValue) {
      if (object != null) {
         return object.orElse(defaultValue);
      } else {
         return defaultValue;
      }
   }

   /**
    * Provides a lazy default value for a nullable object.
    *
    * @param object nullable object
    * @param defaultValue default value supplier, can return {@code null}
    * @param <T> object type
    *
    * @return object value or default value
    */
   public static <T> T nullable(T object, Supplier<T> defaultValue) {
      return object != null ? object : notNull(defaultValue, "defaultValue").get();
   }

   /**
    * Provides a lazy default value for a nullable object.
    *
    * @param object nullable object
    * @param defaultValue default value supplier, can return {@code null}
    * @param <T> object type
    *
    * @return object value or default value
    */
   public static <T> T nullable(Nullable<T> object, Supplier<T> defaultValue) {
      notNull(defaultValue, "defaultValue");

      return object != null ? object.orElseGet(defaultValue) : defaultValue.get();
   }

   /**
    * Applies the mapping function on nullable object only if present, or return the default value. If map
    * result is {@code null}, the default value will apply.
    *
    * @param <T> object type
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param defaultValue default value, can be {@code null}
    *
    * @return mapped object value or default value
    */
   public static <T> T nullable(T object, Function<? super T, ? extends T> map, T defaultValue) {
      return nullable(object == null ? object : notNull(map, "map").apply(object), defaultValue);
   }

   /**
    * Applies the mapping function on nullable object only if present, or return the default value. If map
    * result is {@code null}, the default value will apply.
    *
    * @param <T> object type
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param defaultValue default value, can be {@code null}
    *
    * @return mapped object value or default value
    */
   public static <T> T nullable(Nullable<T> object, Function<? super T, ? extends T> map, T defaultValue) {
      return nullable(object == null ? object : object.map(notNull(map, "map")), defaultValue);
   }

   /**
    * Applies the mapping function on nullable object only if present, or return the lazy default value.
    *
    * @param <T> object type
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param defaultValue default value supplier, can return {@code null}
    *
    * @return mapped object value or default value
    */
   public static <T> T nullable(T object, Function<? super T, ? extends T> map, Supplier<T> defaultValue) {
      return nullable(object == null ? object : notNull(map, "map").apply(object), defaultValue);
   }

   /**
    * Applies the mapping function on nullable object only if present, or return the lazy default value.
    *
    * @param <T> object type
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param defaultValue default value supplier, can return {@code null}
    *
    * @return mapped object value or default value
    */
   public static <T> T nullable(Nullable<T> object,
                                Function<? super T, ? extends T> map,
                                Supplier<T> defaultValue) {
      return nullable(object == null ? object : object.map(notNull(map, "map")), defaultValue);
   }

   /**
    * Identity function making explicit the nullable nature of {@code object}.
    *
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param <T> object type
    *
    * @return mapped object value or {@code null}
    */
   public static <T> T nullable(T object, Function<? super T, ? extends T> map) {
      return nullable(object, map, (T) null);
   }

   /**
    * Identity function making explicit the nullable nature of {@code object}.
    *
    * @param object nullable object
    * @param map mapping function to apply to non-null object
    * @param <T> object type
    *
    * @return mapped object value or {@code null}
    */
   public static <T> T nullable(Nullable<T> object, Function<? super T, ? extends T> map) {
      return nullable(object, map, (T) null);
   }

   /**
    * Casts an object to the specified class if it is an instance of it, otherwise, returns
    * {@link Optional#empty}.
    * Object can be nullable, in such case the method returns an empty optional so that the result is never
    * {@code null}.
    *
    * @param object nullable object
    * @param subclass subclass class
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return {@link Optional} of subclassed object, or {@link Optional#empty()}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Supplier, Class, Function)
    * @see OptionalUtils#instanceOf(Object, Class)
    */
   public static <T, U extends T> Optional<U> nullableInstanceOf(T object, Class<U> subclass) {
      notNull(subclass, "subclass");

      return nullable(object).filter(o -> subclass.isAssignableFrom(o.getClass())).map(subclass::cast);
   }

   /**
    * Object can be nullable, in such case return the supplied default value, and does not call subclass
    * factory.
    * If object is not {@code null}, casts it to the specified class if it is an instance of it, otherwise,
    * returns factory result.
    *
    * @param object nullable object
    * @param defaultValue default value supplier, can return {@code null}
    * @param subclass subclass class
    * @param subclassFactory subclass factory, can return {@code null}
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return subclassed object, or {@code null}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Class)
    * @see NullableUtils#nullableInstanceOf(Object, Class, Function)
    * @see OptionalUtils#instanceOf(Object, Class, Function)
    */
   public static <T, U extends T> U nullableInstanceOf(T object,
                                                       Supplier<U> defaultValue,
                                                       Class<U> subclass,
                                                       Function<? super T, U> subclassFactory) {
      notNull(subclass, "subclass");
      notNull(defaultValue, "defaultValue");
      notNull(subclassFactory, "subclassFactory");

      var objectOrDefault = nullable(object).orElseGet(defaultValue);

      if (objectOrDefault == null) {
         return null;
      } else {
         return optional(objectOrDefault)
               .filter(o -> subclass.isAssignableFrom(o.getClass()))
               .map(subclass::cast)
               .or(() -> nullable(subclassFactory.apply(object)))
               .orElse(null);
      }
   }

   /**
    * Object can be nullable, in such case return {@code null}, and does not call subclass
    * factory.
    * If object is not {@code null}, casts it to the specified class if it is an instance of it, otherwise,
    * returns factory result.
    *
    * @param object nullable object
    * @param subclass subclass class
    * @param subclassFactory subclass factory, can return {@code null}
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return subclassed object, or {@code null}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Class)
    * @see NullableUtils#nullableInstanceOf(Object, Supplier, Class, Function)
    * @see OptionalUtils#instanceOf(Object, Class, Function)
    */
   public static <T, U extends T> U nullableInstanceOf(T object,
                                                       Class<U> subclass,
                                                       Function<? super T, U> subclassFactory) {
      notNull(subclass, "subclass");
      notNull(subclassFactory, "subclassFactory");

      if (object == null) {
         return null;
      } else {
         return optional(object)
               .filter(o -> subclass.isAssignableFrom(o.getClass()))
               .map(subclass::cast)
               .or(() -> nullable(subclassFactory.apply(object)))
               .orElse(null);
      }
   }

   /**
    * Casts an object to the specified class if it is an instance of it, otherwise, returns
    * {@link Optional#empty}.
    * Object can be nullable, in such case the method returns an empty optional so that the result is never
    * {@code null}.
    *
    * @param object nullable object
    * @param subclass subclass class
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return {@link Optional} of subclassed object, or {@link Optional#empty()}
    *
    * @see OptionalUtils#instanceOf(Object, Class)
    */
   public static <T, U extends T> Optional<U> nullableInstanceOf(Nullable<T> object, Class<U> subclass) {
      return nullableInstanceOf(nullable(object), subclass);
   }

   /**
    * Returns an object wrapped in {@link Optional} if it is an instance of one of specified classes,
    * otherwise, returns {@link Optional#empty}.
    * Object can be nullable, in such case the method returns an empty optional so that the result is never
    * {@code null}.
    *
    * @param object nullable object
    * @param subclasses subclasses class
    * @param <T> object type
    *
    * @return {@link Optional} of object, or {@link Optional#empty()}
    *
    * @see OptionalUtils#instanceOf(Object, Class...)
    */
   @SafeVarargs
   @SuppressWarnings("unchecked")
   public static <T> Optional<Object> nullableInstanceOf(T object, Class<? extends T>... subclasses) {
      noNullElements(subclasses, "subclasses");

      return stream(subclasses).reduce(Optional.empty(),
                                       (o, targetClass) -> or(o,
                                                              () -> nullableInstanceOf(object,
                                                                                       (Class<Object>) targetClass)),
                                       (o1, o2) -> or(o1, () -> o2));
   }

   /**
    * Returns an object wrapped in {@link Optional} if it is an instance of one of specified classes,
    * otherwise, returns {@link Optional#empty}.
    * Object can be nullable, in such case the method returns an empty optional so that the result is never
    * {@code null}.
    *
    * @param object nullable object
    * @param subclasses subclasses class
    * @param <T> object type
    *
    * @return {@link Optional} of object, or {@link Optional#empty()}
    *
    * @see OptionalUtils#instanceOf(Object, Class...)
    */
   @SafeVarargs
   public static <T> Optional<Object> nullableInstanceOf(Nullable<T> object,
                                                         Class<? extends T>... subclasses) {
      return object == null ? Optional.empty() : nullableInstanceOf(object.get(), subclasses);
   }

   /**
    * Bridge from {@link Predicate} to {@link Optional}.
    * Predicate is not called if object is {@code null} and an empty optional is returned.
    *
    * @param object object to bridge
    * @param predicate predicate to apply to object
    *
    * @return object as an optional if predicate is satisfied, otherwise, an empty optional
    *
    * @see OptionalUtils#optionalPredicate(Object, Predicate)
    */
   public static <T> Optional<T> nullablePredicate(T object, Predicate<T> predicate) {
      notNull(predicate, "predicate");

      return nullable(object).filter(predicate);
   }

   /**
    * Handles nullable {@link Boolean} so that result is never {@code null}.
    * {@code null} Boolean are considered {@code false}.
    * Use standard {@link #nullable(Object)} if you want to set an alternative default value for {@code null}
    * object.
    *
    * @param object boolean object
    *
    * @return {@code true} if object is not {@code null} and {@code true}, {@code false} otherwise
    */
   public static boolean nullableBoolean(Boolean object) {
      return Boolean.TRUE.equals(object);
   }

   /**
    * A nullable value holder.
    */
   public static class Nullable<T> {
      private final T value;

      private Nullable(T value) {
         this.value = value;
      }

      public static <T> Nullable<T> of(T value) {
         return new Nullable<>(value);
      }

      public T get() {
         return value;
      }

      public Optional<T> optional() {
         return value == null ? Optional.empty() : Optional.of(value);
      }

      public boolean isNull() {
         return value == null;
      }

      public boolean isNotNull() {
         return !isNull();
      }

      /**
       * If a value is not null, returns the value, otherwise returns
       * {@code other}.
       *
       * @param other the value to be returned, if value is null.
       *       May be {@code null}.
       *
       * @return the value, if not null, otherwise {@code other}
       */
      public T orElse(T other) {
         return nullable(value).orElse(other);
      }

      /**
       * If a value is not null, returns the value, otherwise returns the result
       * produced by the supplying function.
       *
       * @param supplier the supplying function that produces a value to be returned
       *
       * @return the value, if not null, otherwise the result produced by the
       *       supplying function
       *
       * @throws NullPointerException if value is null and the supplying
       *       function is {@code null}
       */
      public T orElseGet(Supplier<? extends T> supplier) {
         return nullable(value).orElseGet(supplier);
      }

      public <U> Nullable<U> map(Function<? super T, ? extends U> mapper) {
         Objects.requireNonNull(mapper);
         return Nullable.of(mapper.apply(value));
      }

   }

}
