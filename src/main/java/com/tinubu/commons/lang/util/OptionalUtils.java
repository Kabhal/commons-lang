/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Objects.requireNonNull;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

/**
 * Optional operations for non-nullable objects.
 */
public final class OptionalUtils {

   private OptionalUtils() {
   }

   /**
    * Wraps non-nullable object to {@link Optional}
    *
    * @param object object to wrap
    * @param <T> object type
    *
    * @return optional object
    *
    * @throws NullPointerException if object is null
    * @see NullableUtils#nullable(Object) if object is nullable
    */
   public static <T> Optional<T> optional(T object) {
      return Optional.of(object);
   }

   /**
    * Returns an empty {@link Optional}.
    *
    * @param <T> optional type
    *
    * @return empty optional
    */
   public static <T> Optional<T> optional() {
      return Optional.empty();
   }

   /**
    * Returns optional value when you require optional to be present.
    *
    * @param optional optional
    * @param <T> optional type
    *
    * @return optional value
    *
    * @throws IllegalArgumentException if optional is empty
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public static <T> T requirePresent(Optional<T> optional) {
      return optional.orElseThrow(() -> new IllegalArgumentException("Optional must not be empty"));
   }

   /**
    * Casts an object to the specified class if it is an instance of it, otherwise, returns
    * {@link Optional#empty}.
    * Object can't be null.
    *
    * @param object object
    * @param subclass subclass class
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return {@link Optional} of subclassed object, or {@link Optional#empty()}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Class) if object can be null
    * @deprecated Use {@link #instanceOf(Object, Class)} instead
    */
   @Deprecated
   public static <T, U extends T> Optional<U> optionalInstanceOf(T object, Class<U> subclass) {
      return instanceOf(object, subclass);
   }

   /**
    * Casts an object to the specified class if it is an instance of it, otherwise, returns
    * factory result. If factory returns {@code null}, then returns {@link Optional#empty}.
    * Object can't be null.
    *
    * @param object object
    * @param subclass subclass class
    * @param subclassFactory subclass factory, can return {@code null}
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return {@link Optional} of subclassed object, or {@link Optional#empty()}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Class) if object can be null
    */
   public static <T, U extends T> Optional<U> instanceOf(T object,
                                                         Class<U> subclass,
                                                         Function<? super T, ? extends U> subclassFactory) {
      notNull(object, "object");
      notNull(subclass, "subclass");
      notNull(subclassFactory, "subclassFactory");
      return optional(object)
            .filter(o -> subclass.isAssignableFrom(o.getClass()))
            .map(subclass::cast)
            .or(() -> nullable(subclassFactory.apply(object)));
   }

   /**
    * Casts an object to the specified class if it is an instance of it, otherwise, returns
    * {@link Optional#empty}.
    * Object can't be null.
    *
    * @param object object
    * @param subclass subclass class
    * @param <T> object type
    * @param <U> subclass type
    *
    * @return {@link Optional} of subclassed object, or {@link Optional#empty()}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Class) if object can be null
    */
   public static <T, U extends T> Optional<U> instanceOf(T object, Class<U> subclass) {
      notNull(object, "object");
      notNull(subclass, "subclass");
      return optional(object).filter(o -> subclass.isAssignableFrom(o.getClass())).map(subclass::cast);
   }

   /**
    * Returns an object wrapped in {@link Optional} if it is an instance of one of specified classes,
    * otherwise, returns {@link Optional#empty}.
    * Object can't be null.
    *
    * @param object object
    * @param subclasses subclasses class
    * @param <T> object type
    *
    * @return {@link Optional} of object, or {@link Optional#empty()}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Class...) if object can be null
    * @deprecated Use {@link #instanceOf(Object, Class[])}
    */
   @Deprecated
   @SafeVarargs
   @SuppressWarnings("unchecked")
   public static <T> Optional<Object> optionalInstanceOf(T object, Class<? extends T>... subclasses) {
      return instanceOf(object, subclasses);
   }

   /**
    * Returns an object wrapped in {@link Optional} if it is an instance of one of specified classes,
    * otherwise, returns {@link Optional#empty}.
    * Object can't be null.
    *
    * @param object object
    * @param subclasses subclasses class
    * @param <T> object type
    *
    * @return {@link Optional} of object, or {@link Optional#empty()}
    *
    * @see NullableUtils#nullableInstanceOf(Object, Class...) if object can be null
    */
   @SafeVarargs
   @SuppressWarnings("unchecked")
   public static <T> Optional<Object> instanceOf(T object, Class<? extends T>... subclasses) {
      notNull(object, "object");
      noNullElements(subclasses, "subclasses");

      return stream(subclasses).reduce(Optional.empty(),
                                       (o, targetClass) -> or(o,
                                                              () -> optionalInstanceOf(object,
                                                                                       (Class<Object>) targetClass)),
                                       (o1, o2) -> or(o1, () -> o2));
   }

   /**
    * JDK < 9 {@link Optional#or(Supplier)} replacement implementation.
    *
    * @param optional optional to or
    * @param suppliers optional suppliers while optional is empty
    * @param <T> optional type
    *
    * @return resulting optional
    *
    * @see Optional#or(Supplier) for JDK >= 9
    */
   @SuppressWarnings({ "unchecked", "OptionalUsedAsFieldOrParameterType" })
   @SafeVarargs
   public static <T> Optional<T> or(Optional<T> optional,
                                    Supplier<? extends Optional<? extends T>>... suppliers) {
      notNull(optional, "optional");
      noNullElements(suppliers, "suppliers");

      if (optional.isPresent()) {
         return optional;
      } else {
         Optional<T> ret = optional;
         for (Supplier<? extends Optional<? extends T>> supplier : suppliers) {
            ret = requireNonNull((Optional<T>) supplier.get());
            if (ret.isPresent()) {
               break;
            }
         }

         return ret;
      }
   }

   /**
    * Apply consumer to optional value if any.
    *
    * @param optional optional to consume
    * @param consumer consumer to apply
    * @param <T> optional type
    *
    * @return specified optional
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public static <T> Optional<T> peek(Optional<T> optional, Consumer<? super T> consumer) {
      notNull(optional, "optional");
      notNull(consumer, "consumer");

      return optional.map(peek(consumer));
   }

   /**
    * Peek version to be embedded in {@link Optional#map}.
    *
    * @param consumer consumer to apply
    * @param <T> optional type
    *
    * @return unary operator of optional value
    */
   public static <T> UnaryOperator<T> peek(Consumer<? super T> consumer) {
      return value -> {
         consumer.accept(value);
         return value;
      };
   }

   /**
    * Apply runnable only if optional is empty.
    *
    * @param optional optional to consume
    * @param runnable runnable to apply
    * @param <T> optional type
    *
    * @return specified optional
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public static <T> Optional<T> peekEmpty(Optional<T> optional, Runnable runnable) {
      notNull(optional, "optional");
      notNull(runnable, "runnable");

      return or(optional, peekEmpty(runnable));
   }

   /**
    * Peek version to be embedded in {@link Optional#or}.
    *
    * @param runnable runnable to run
    * @param <T> optional type
    *
    * @return unary operator of optional value
    */
   public static <T> Supplier<Optional<T>> peekEmpty(Runnable runnable) {
      return () -> {
         runnable.run();
         return optional();
      };
   }

   /**
    * Call actions respectively if optional is present and empty.
    *
    * @param optional optional to consume
    * @param action consumer to apply to optional if present
    * @param emptyAction runnable to call if optional is empty
    * @param <T> optional type
    *
    * @return specified optional
    */
   @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
   public static <T> void ifPresentOrElse(Optional<T> optional,
                                          Consumer<? super T> action,
                                          Runnable emptyAction) {
      peekEmpty(peek(optional, action), emptyAction);
   }

   /**
    * Bridge from {@link Predicate} to {@link Optional} for a non-nullable object.
    *
    * @param object object to bridge
    * @param predicate predicate to apply to object
    *
    * @return object as an optional if predicate is satisfied, otherwise, an empty optional
    *
    * @throws NullPointerException if object is null
    * @see NullableUtils#nullablePredicate(Object, Predicate) (Object, Predicate) if object is nullable
    */
   public static <T> Optional<T> optionalPredicate(T object, Predicate<T> predicate) {
      notNull(predicate, "predicate");

      return optional(object).filter(predicate);
   }

}
