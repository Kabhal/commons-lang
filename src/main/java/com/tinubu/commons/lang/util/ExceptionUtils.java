/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.XCheckedSupplier.rethrow;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.List;

public final class ExceptionUtils {

   private ExceptionUtils() {
   }

   /**
    * Java 8 hack to disguise any checked exception into an unchecked exception. Note that {@link Exception}
    * are <em>not</em> transformed to {@link RuntimeException}.
    *
    * @param throwable throwable to disguise
    * @param <T> exception type
    *
    * @return (never return)
    *
    * @throws T as original exception
    */
   @SuppressWarnings("unchecked")
   public static <T extends Throwable> T sneakyThrow(Throwable throwable) throws T {
      throw (T) throwable;
   }

   /**
    * Rethrow any throwable as an unchecked exception. {@link Exception}
    * are transformed to {@link UncheckedException} or {@link UncheckedIOException}.
    * {@link RuntimeException} and {@link Error} exceptions are left unchanged.
    *
    * @param throwable throwable to wrap
    *
    * @return (never return)
    */
   public static <T extends Throwable> T runtimeThrow(Throwable throwable) throws T {
      if (throwable instanceof Error) {
         throw (Error) throwable;
      } else if (throwable instanceof RuntimeException) {
         throw (RuntimeException) throwable;
      } else if (throwable instanceof IOException) {
         throw copySuppressedThrowables(throwable,
                                        new UncheckedIOException(throwable.getMessage(),
                                                                 (IOException) throwable));
      } else {
         throw copySuppressedThrowables(throwable, new UncheckedException(throwable));
      }
   }

   private static <ST extends Throwable, DT extends Throwable> DT copySuppressedThrowables(ST source,
                                                                                           DT dest) {
      for (Throwable suppressed : source.getSuppressed()) {
         dest.addSuppressed(suppressed);
      }
      return dest;
   }

   /**
    * Rethrows an exception, ensuring all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param exception exception to rethrow
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws X is the rethrown exception
    */
   public static <X extends Exception> X rethrowFinally(X exception,
                                                        List<? extends XCheckedRunnable<? extends X>> finalizers)
         throws X {
      throw rethrow(exception).tryFinally(finalizers).getChecked();
   }

   /**
    * Rethrows an exception, ensuring all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param exception exception to rethrow
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws X is the rethrown exception
    */
   @SafeVarargs
   public static <X extends Exception> X rethrowFinally(X exception,
                                                        XCheckedRunnable<? extends X>... finalizers)
         throws X {
      throw rethrowFinally(exception, list(finalizers));
   }

   /**
    * Rethrows an exception, ensuring all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param throwable exception to rethrow
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Throwable is the rethrown exception
    * @implNote Hack : No XCheck* class is able to catch {@link Throwable}, so we use
    *       {@link #sneakyThrow(Throwable)} in checked supplier, because its
    *       {@link XCheckedSupplier#tryFinally(List)} implementation catches {@link Throwable} anyway.
    */
   public static <X extends Exception> Throwable rethrowFinally(Throwable throwable,
                                                                List<? extends XCheckedRunnable<? extends X>> finalizers)
         throws Throwable {
      throw XCheckedSupplier.<Throwable, X>checkedSupplier(() -> {
         throw sneakyThrow(throwable);
      }).tryFinally(finalizers).getChecked();
   }

   /**
    * Rethrows an exception, ensuring all specified finalizers are always, called in specified order.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added as
    * suppressed in the first actually thrown exception.
    *
    * @param throwable exception to rethrow
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @throws Throwable is the rethrown exception
    */
   @SafeVarargs
   public static <X extends Exception> Throwable rethrowFinally(Throwable throwable,
                                                                XCheckedRunnable<? extends X>... finalizers)
         throws Throwable {
      throw rethrowFinally(throwable, list(finalizers));
   }

}
