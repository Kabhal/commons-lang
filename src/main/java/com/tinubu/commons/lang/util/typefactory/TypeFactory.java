/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util.typefactory;

import java.util.function.Supplier;

/**
 * Type factory abstraction.
 *
 * @param <T> factory type
 */
@FunctionalInterface
public interface TypeFactory<T> extends Supplier<T> {

   /**
    * Creates a new type factory from a supplier.
    * Factory type is automatically generated once from supplied type so that generated type factory should
    * be stored in a constant.
    */
   @SuppressWarnings("unchecked")
   static <T> TypeFactory<T> typeFactory(Supplier<T> factory) {
      Class<T> factoryType = (Class<T>) factory.get().getClass();

      return new TypeFactory<>() {
         @Override
         public T get() {
            return factory.get();
         }

         @Override
         public Class<T> factoryType() {
            return factoryType;
         }
      };
   }

   /**
    * Instantiates type using default constructor.
    *
    * @return new instance
    */
   @Override
   T get();

   /**
    * Returns the factory type as {@link Class}.
    * <p>
    * Default implementation is costly as it instantiates the type, override default in specific factory
    * implementations for performance.
    *
    * @return factory type class
    */
   @SuppressWarnings("unchecked")
   default Class<T> factoryType() {
      return (Class<T>) get().getClass();
   }

   /**
    * Instantiates type using characteristics from specified instance (best-effort). This is not, and must
    * not, be a copy constructor, newly instantiated type must always be empty.
    * If type can't mirror instance low-critical characteristics (e.g.: load factor), just use the default
    * constructor. If type can't mirror high-critical characteristics (e.g.: sorted collection comparator),
    * you should throw an exception.
    *
    * @return new instance, with possibly the same characteristics that specified instance
    */
   default T get(T instance) {
      return get();
   }

}


