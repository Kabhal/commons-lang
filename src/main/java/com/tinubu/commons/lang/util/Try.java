/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StringUtils.isBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.util.Objects.requireNonNull;

import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Represents either a success with a value or a failure with a reason.
 *
 * @param <V> success value
 * @param <R> failure reason
 */
public abstract class Try<V, R> {

   /**
    * Handle exceptions that can be thrown by specified callable and returns it as a {@link Try} instance.
    *
    * @param callable callable to execute
    */
   public static <V> Try<V, Exception> ofThrownBy(Callable<? extends V> callable) {
      notNull(callable, "callable");

      try {
         return Success.of(callable.call());
      } catch (Exception e) {
         return ExceptionFailure.of(e);
      }
   }

   /**
    * Handle exceptions that can be thrown by specified callable and returns it as a {@link Try} instance.
    *
    * @param callable callable to execute
    * @param exceptionClass catch only exception that are instance of this one, letting other exception
    *       be thrown as regular exceptions
    *
    * @throws Exception if callable throw an exception that is not an instance of specified exception
    *       class.
    */
   public static <V, X extends Exception> Try<V, X> ofThrownBy(Callable<? extends V> callable,
                                                               Class<? extends X> exceptionClass)
         throws Exception {
      notNull(callable, "callable");
      notNull(exceptionClass, "exceptionClass");

      try {
         return Success.of(callable.call());
      } catch (Exception e) {
         if (exceptionClass.isAssignableFrom(e.getClass())) {
            return ExceptionFailure.of(exceptionClass.cast(e));
         } else {
            throw e;
         }
      }
   }

   /**
    * Handle exceptions that can be thrown by specified supplier and returns it as a {@link Try} instance.
    *
    * @param supplier supplier to execute
    */
   @SuppressWarnings("unchecked")
   public static <V, X extends Exception> Try<V, X> ofThrownBy(XCheckedSupplier<? extends V, ? extends X> supplier) {
      notNull(supplier, "supplier");

      try {
         return Success.of(supplier.getChecked());
      } catch (Exception e) {
         return ExceptionFailure.of((X) e);
      }
   }

   /**
    * Handle exceptions that can be thrown by specified supplier and returns it as a {@link Try} instance.
    *
    * @param supplier supplier to execute
    * @param exceptionClass catch only exception that are instance of this one, letting other exception
    *       be thrown as regular exceptions
    *
    * @throws Exception if supplier throw an exception that is not an instance of specified exception
    *       class.
    */
   public static <V, X extends Exception> Try<V, X> ofThrownBy(XCheckedSupplier<? extends V, ? extends X> supplier,
                                                               Class<? extends X> exceptionClass) throws X {
      notNull(supplier, "supplier");
      notNull(exceptionClass, "exceptionClass");

      try {
         return Success.of(supplier.getChecked());
      } catch (Exception e) {
         if (exceptionClass.isAssignableFrom(e.getClass())) {
            return ExceptionFailure.of(exceptionClass.cast(e));
         } else {
            throw e;
         }
      }
   }

   /**
    * Handle exceptions that can be thrown by specified runnable and returns it as a {@link Try} instance.
    * In case of success, {@code (Void)null} is returned.
    *
    * @param runnable runnable to execute
    */
   public static Try<Void, RuntimeException> ofThrownBy(Runnable runnable) {
      notNull(runnable, "runnable");

      try {
         runnable.run();
         return Success.of(null);
      } catch (RuntimeException e) {
         return ExceptionFailure.of(e);
      }
   }

   /**
    * Handle exceptions that can be thrown by specified runnable and returns it as a {@link Try} instance.
    * In case of success, {@code (Void)null} is returned.
    *
    * @param runnable runnable to execute
    * @param exceptionClass catch only exception that are instance of this one, letting other exception
    *       be thrown as regular exceptions
    */
   public static <X extends RuntimeException> Try<Void, X> ofThrownBy(Runnable runnable,
                                                                      Class<? extends X> exceptionClass) {
      notNull(runnable, "runnable");
      notNull(exceptionClass, "exceptionClass");

      try {
         runnable.run();
         return Success.of(null);
      } catch (RuntimeException e) {
         if (exceptionClass.isAssignableFrom(e.getClass())) {
            return ExceptionFailure.of(exceptionClass.cast(e));
         } else {
            throw e;
         }
      }
   }

   /**
    * Handle exceptions that can be thrown by specified runnable and returns it as a {@link Try} instance.
    * In case of success, {@code (Void)null} is returned.
    *
    * @param runnable runnable to execute
    */
   @SuppressWarnings("unchecked")
   public static <X extends Exception> Try<Void, X> ofThrownBy(XCheckedRunnable<? extends X> runnable) {
      notNull(runnable, "runnable");

      try {
         runnable.runChecked();
         return Success.of(null);
      } catch (Exception e) {
         return ExceptionFailure.of((X) e);
      }
   }

   /**
    * Handle exceptions that can be thrown by specified runnable and returns it as a {@link Try} instance.
    * In case of success, {@code (Void)null} is returned.
    *
    * @param runnable runnable to execute
    * @param exceptionClass catch only exception that are instance of this one, letting other exception
    *       be thrown as regular exceptions
    *
    * @throws X if runnable throw an exception that is not an instance of specified exception
    *       class.
    */
   public static <X extends Exception> Try<Void, X> ofThrownBy(XCheckedRunnable<? extends X> runnable,
                                                               Class<? extends X> exceptionClass) throws X {
      notNull(runnable, "runnable");
      notNull(exceptionClass, "exceptionClass");

      try {
         runnable.runChecked();
         return Success.of(null);
      } catch (Exception e) {
         if (exceptionClass.isAssignableFrom(e.getClass())) {
            return ExceptionFailure.of(exceptionClass.cast(e));
         } else {
            throw e;
         }
      }
   }

   /**
    * Handle exceptions that can be thrown by specified callable, or flatten it as a {@link Try} instance.
    *
    * @param callable callable to execute
    */
   public static <V> Try<V, Exception> flattenThrownBy(Callable<? extends Try<V, Exception>> callable) {
      notNull(callable, "callable");

      try {
         return callable.call();
      } catch (Exception e) {
         return ExceptionFailure.of(e);
      }
   }

   /**
    * Handle exceptions that can be thrown by specified callable, or flatten it as a {@link Try} instance.
    *
    * @param callable callable to execute
    * @param exceptionClass catch only exception that are instance of this one, letting other exception
    *       be thrown as regular exceptions
    *
    * @throws Exception if callable throw an exception that is not an instance of specified exception
    *       class.
    */
   public static <V, X extends Exception> Try<V, X> flattenThrownBy(Callable<? extends Try<V, X>> callable,
                                                                    Class<? extends X> exceptionClass)
         throws Exception {
      notNull(callable, "callable");
      notNull(exceptionClass, "exceptionClass");

      try {
         return callable.call();
      } catch (Exception e) {
         if (exceptionClass.isAssignableFrom(e.getClass())) {
            return ExceptionFailure.of(exceptionClass.cast(e));
         } else {
            throw e;
         }
      }
   }

   /**
    * Maps this {@link Try} instance using the specified mapper on success.
    *
    * @param mapper success value mapper
    * @param <T> mapped success type
    *
    * @return new try instance with mapped success, or previous failure
    */
   public abstract <T> Try<T, R> map(Function<? super V, ? extends T> mapper);

   /**
    * Maps this {@link Try} instance using the specified mapper on failure.
    *
    * @param mapper failure value mapper
    * @param <T> mapped failure type
    *
    * @return new try instance with mapped failure, or previous success
    */
   public abstract <T> Try<V, T> mapFailure(Function<? super R, ? extends T> mapper);

   /**
    * Maps this {@link Try} instance using the specified mapper on success.
    *
    * @param mapper success value mapper
    * @param <T> mapped success type
    *
    * @return new try instance with mapped success, or previous failure
    */
   public abstract <T> Try<T, R> flatMap(Function<? super V, ? extends Try<? extends T, ? extends R>> mapper);

   /**
    * Returns either {@link Success#value()} or failure reason mapper result.
    *
    * @param mapper failure reason mapper
    *
    * @return either success value or failure reason mapper result
    */
   public abstract V orElseGet(Function<? super R, ? extends V> mapper);

   /**
    * Returns either {@link Success#value()} or supplied value.
    *
    * @param supplier value supplier
    *
    * @return either success value or supplied value
    */
   public abstract V orElseGet(Supplier<? extends V> supplier);

   /**
    * Returns either {@link Success#value()} or specified value.
    *
    * @param value value
    *
    * @return either success value or failure reason mapper result
    */
   public abstract V orElse(V value);

   /**
    * Returns either {@link Success#value()} or throw exception.
    * <p>
    * If reason is a {@link Exception checked exception}, it is wrapped in an {@link IllegalStateException}.
    * If you want to throw the original exception, see {@link #orElseThrow(Function)} and use, for example,
    * the {@link Function#identity()} mapper.
    *
    * @return success value, or throw exception
    *
    * @throws RuntimeException if reason is instance of {@link RuntimeException}
    * @throws IllegalStateException if reason is any other type, with reason on failure. If reason is a
    *       {@link Exception checked exception}, it will be added as thrown exception cause
    */
   public abstract V orElseThrow();

   /**
    * Returns either {@link Success#value()} or throw mapped exception.
    * <p>
    * In the case reason is a {@link ExceptionFailure} or {@link Failure}&lt;{@link Exception}&gt;, you can
    * use the {@link Function#identity()} to throw the original exception :
    * {@code (Try<?, IOException>)try.orElseThrow(Function::identity) -> throws IOException}
    *
    * @return success value, or throw mapped exception
    *
    * @throws X with reason on failure
    */
   public abstract <X extends Exception> V orElseThrow(Function<? super R, ? extends X> exceptionMapper)
         throws X;

   /**
    * Calls specified callback on success.
    *
    * @param onSuccess on success callback
    *
    * @return this instance
    */
   public abstract Try<V, R> peek(Consumer<? super V> onSuccess);

   /**
    * Calls specified callback on failure.
    *
    * @param onFailure on failure callback
    *
    * @return this instance
    */
   public abstract Try<V, R> peekFailure(Consumer<? super R> onFailure);

   /**
    * Calls specified callback on success.
    *
    * @param onSuccess on success callback
    */
   public abstract void ifSuccess(Consumer<? super V> onSuccess);

   /**
    * Calls specified callback on failure.
    *
    * @param onFailure on failure callback
    */
   public abstract void ifFailure(Consumer<? super R> onFailure);

   /**
    * Calls specified callbacks on success or on failure
    *
    * @param onSuccess on success callback
    * @param onFailure on failure callback
    */
   public abstract void ifSuccessOrElse(Consumer<? super V> onSuccess, Consumer<? super R> onFailure);

   /**
    * Returns supplied result on failure.
    *
    * @param supplier on failure supplier
    *
    * @return new try instance
    */
   public abstract Try<V, R> or(Supplier<? extends Try<? extends V, ? extends R>> supplier);

   /**
    * Returns this object as a {@link Success#value()} {@link Stream} on success, or an empty stream on
    * failure.
    *
    * @return this object as a stream
    */
   public abstract Stream<V> stream();

   /**
    * Returns this object as a {@link Success#value()} {@link Optional} on success, or an empty optional on
    * failure.
    * <p>
    * Note that if successful value is {@code null}, an empty optional will also be returned.
    *
    * @return this object as an optional
    */
   public abstract Optional<V> optional();

   public static class Success<V, R> extends Try<V, R> {
      private final V value;

      private Success(V value) {
         this.value = value;
      }

      public static <V, R> Success<V, R> of(V value) {
         return new Success<>(value);
      }

      public V value() {
         return this.value;
      }

      @Override
      public <T> Success<T, R> map(Function<? super V, ? extends T> mapper) {
         notNull(mapper, "mapper");

         return Success.of(mapper.apply(value));
      }

      @Override
      public <T> Try<V, T> mapFailure(Function<? super R, ? extends T> mapper) {
         notNull(mapper, "mapper");

         return Success.of(value);
      }

      @Override
      public <T> Try<T, R> flatMap(Function<? super V, ? extends Try<? extends T, ? extends R>> mapper) {
         notNull(mapper, "mapper");

         @SuppressWarnings("unchecked")
         Try<T, R> apply = (Try<T, R>) mapper.apply(value);

         return requireNonNull(apply);
      }

      @Override
      public V orElseGet(Function<? super R, ? extends V> mapper) {
         notNull(mapper, "mapper");

         return this.value;
      }

      @Override
      public V orElseGet(Supplier<? extends V> supplier) {
         notNull(supplier, "supplier");

         return this.value;
      }

      @Override
      public V orElse(V value) {
         return this.value;
      }

      @Override
      public V orElseThrow() {
         return this.value;
      }

      @Override
      public <X extends Exception> V orElseThrow(Function<? super R, ? extends X> exceptionMapper) throws X {
         notNull(exceptionMapper, "exceptionMapper");

         return this.value;
      }

      @Override
      public Success<V, R> peek(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");

         onSuccess.accept(value);

         return this;
      }

      @Override
      public Success<V, R> peekFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");

         return this;
      }

      @Override
      public void ifSuccess(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");

         onSuccess.accept(value);
      }

      @Override
      public void ifFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");
      }

      @Override
      public void ifSuccessOrElse(Consumer<? super V> onSuccess, Consumer<? super R> onFailure) {
         notNull(onSuccess, "onSuccess");
         notNull(onFailure, "onFailure");

         onSuccess.accept(value);
      }

      @Override
      public Try<V, R> or(Supplier<? extends Try<? extends V, ? extends R>> supplier) {
         notNull(supplier, "supplier");

         return this;
      }

      public Stream<V> stream() {
         return StreamUtils.stream(value);
      }

      public Optional<V> optional() {
         return nullable(value);
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Success<?, ?> success = (Success<?, ?>) o;
         return Objects.equals(value, success.value);
      }

      @Override
      public int hashCode() {
         return Objects.hash(value);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Success.class.getSimpleName() + "[", "]")
               .add("value=" + value)
               .toString();
      }
   }

   public static class Failure<V, R> extends Try<V, R> {
      protected final R reason;
      protected final String reasonString;

      protected Failure(R reason, String reasonString) {
         this.reason = notNull(reason, "reason");
         this.reasonString = isBlank(reasonString) ? reason.toString() : reasonString;
      }

      protected Failure(R reason) {
         this(reason, null);
      }

      public static <V, R> Failure<V, R> of(R reason) {
         return new Failure<>(reason);
      }

      public static <V, R> Failure<V, R> of(R reason, String reasonString) {
         return new Failure<>(reason, reasonString);
      }

      public R reason() {
         return this.reason;
      }

      public String reasonString() {
         return reasonString;
      }

      @Override
      public <T> Failure<T, R> map(Function<? super V, ? extends T> mapper) {
         notNull(mapper, "mapper");

         return Failure.of(reason);
      }

      @Override
      public <T> Try<V, T> mapFailure(Function<? super R, ? extends T> mapper) {
         notNull(mapper, "mapper");

         return Failure.of(mapper.apply(reason));
      }

      @Override
      public <T> Try<T, R> flatMap(Function<? super V, ? extends Try<? extends T, ? extends R>> mapper) {
         notNull(mapper, "mapper");

         return Failure.of(reason);
      }

      @Override
      public V orElseGet(Function<? super R, ? extends V> mapper) {
         notNull(mapper, "mapper");

         return mapper.apply(reason);
      }

      @Override
      public V orElseGet(Supplier<? extends V> supplier) {
         notNull(supplier, "supplier");

         return supplier.get();
      }

      @Override
      public V orElse(V value) {
         return value;
      }

      @Override
      public V orElseThrow() {
         if (reason instanceof RuntimeException) {
            throw (RuntimeException) reason;
         } else if (reason instanceof Exception) {
            throw new IllegalStateException(reasonString(), (Exception) reason);
         } else {
            throw new IllegalStateException(reasonString());
         }
      }

      @Override
      public <X extends Exception> V orElseThrow(Function<? super R, ? extends X> exceptionMapper) throws X {
         notNull(exceptionMapper, "exceptionMapper");

         throw requireNonNull(exceptionMapper.apply(reason));
      }

      @Override
      public Failure<V, R> peek(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");

         return this;
      }

      @Override
      public Failure<V, R> peekFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");

         onFailure.accept(reason);

         return this;
      }

      @Override
      public void ifSuccess(Consumer<? super V> onSuccess) {
         notNull(onSuccess, "onSuccess");
      }

      @Override
      public void ifFailure(Consumer<? super R> onFailure) {
         notNull(onFailure, "onFailure");

         onFailure.accept(reason);
      }

      @Override
      public void ifSuccessOrElse(Consumer<? super V> onSuccess, Consumer<? super R> onFailure) {
         notNull(onSuccess, "onSuccess");
         notNull(onFailure, "onFailure");

         onFailure.accept(reason);
      }

      @Override
      public Try<V, R> or(Supplier<? extends Try<? extends V, ? extends R>> supplier) {
         notNull(supplier, "supplier");

         @SuppressWarnings("unchecked")
         Try<V, R> supply = (Try<V, R>) supplier.get();

         return requireNonNull(supply);
      }

      public Stream<V> stream() {
         return StreamUtils.stream();
      }

      public Optional<V> optional() {
         return OptionalUtils.optional();
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         Failure<?, ?> failure = (Failure<?, ?>) o;
         return Objects.equals(reason, failure.reason);
      }

      @Override
      public int hashCode() {
         return Objects.hash(reason);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", Failure.class.getSimpleName() + "[", "]")
               .add("reason=" + reason)
               .toString();
      }
   }

   public static class ExceptionFailure<V, E extends Exception> extends Failure<V, E> {

      protected ExceptionFailure(E exception) {
         super(exception, null);
      }

      public static <V, E extends Exception> ExceptionFailure<V, E> of(E exception) {
         return new ExceptionFailure<>(exception);
      }

      @Override
      public String reasonString() {
         return super.reason().getMessage();
      }

   }

}
