/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.nio.file.Path;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;

/**
 * Various {@link Path} helpers.
 */
public final class PathUtils {

   private static final Path ROOT_PATH = Path.of("/");
   private static final Path EMPTY_PATH = Path.of("");

   private PathUtils() {
   }

   public static Path rootPath() {
      return ROOT_PATH;
   }

   public static boolean isRoot(Path path) {
      notNull(path, "path");

      return path.equals(rootPath());
   }

   public static Path emptyPath() {
      return EMPTY_PATH;
   }

   /**
    * Checks whether specified path is empty.
    *
    * <ul>
    *    <li>{@code isEmpty("") -> true}</li>
    *    <li>{@code isEmpty(" ") -> false}</li>
    *    <li>{@code isEmpty("/") -> false}</li>
    *    <li>{@code isEmpty("path") -> false}</li>
    *    <li>{@code isEmpty("/path") -> false}</li>
    * </ul>
    *
    * @param path path
    *
    * @return {@code true} if path is empty
    */
   public static boolean isEmpty(Path path) {
      notNull(path, "path");

      return path.equals(emptyPath());
   }

   /**
    * Removes root from specified path.
    * If path is not absolute, return unchanged path.
    *
    * <ul>
    *    <li>{@code removeRoot("/base") -> "base"}</li>
    *    <li>{@code removeRoot("base") -> "base"}</li>
    *    <li>{@code removeRoot("/") -> ""}</li>
    *    <li>{@code removeRoot("") -> ""}</li>
    * </ul>
    *
    * @param path relative or absolute path
    *
    * @return path without root
    */
   public static Path removeRoot(Path path) {
      notNull(path, "path");

      if (path.isAbsolute()) {
         return removeStart(path, ROOT_PATH).orElseThrow();
      } else {
         return path;
      }
   }

   /**
    * Adds a {@code /} prefix to specified path, only if necessary to ensure returned path is absolute.
    * <ul>
    *    <li>{@code addRoot("/base") -> "/base"}</li>
    *    <li>{@code addRoot("base") -> "/base"}</li>
    *    <li>{@code addRoot("/") -> "/"}</li>
    *    <li>{@code addRoot("") -> "/"}</li>
    * </ul>
    *
    * @param path relative or absolute path
    *
    * @return absolute path
    */
   public static Path addRoot(Path path) {
      notNull(path, "path");

      if (path.isAbsolute()) {
         return path;
      } else {
         return rootPath().resolve(path);
      }
   }

   /**
    * Removes start path from base path.
    *
    * <ul>
    *    <li>{@code removeStart("/base/sub", "/base") -> "sub"}</li>
    *    <li>{@code removeStart("base/sub", "base") -> "sub"}</li>
    *    <li>{@code removeStart("base/sub", "base/sub") -> ""}</li>
    *    <li>{@code removeStart("/base", "/base") -> ""}</li>
    *    <li>{@code removeStart("/base", "/") -> "base"}</li>
    *    <li>{@code removeStart("/", "/") -> ""}</li>
    *    <li>{@code removeStart("", "") -> ""}</li>
    *    <li>{@code removeStart("/base/sub", "/other") -> ø}</li>
    *    <li>{@code removeStart("base/sub", "other") -> ø}</li>
    * </ul>
    * Results can vary depending on specified path element equator.
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * @param path base path, can be relative, absolute or empty
    * @param startPath start path to remove from base path, can be relative, absolute or empty
    * @param equator path element equator
    *
    * @return base path without end path, or {@link Optional#empty()} if end path does not end base path
    */
   public static Optional<Path> removeStart(Path path, Path startPath, Equator<Path> equator) {
      notNull(path, "path");
      notNull(startPath, "startPath");

      if (isEmpty(startPath)) {
         return optional(path);
      } else if (startsWith(path, startPath, equator)) {
         if (startPath.getNameCount() - path.getNameCount() == 0) {
            return optional(path.isAbsolute() && !startPath.isAbsolute() ? ROOT_PATH : EMPTY_PATH);
         }
         return optional(path.subpath(startPath.getNameCount(), path.getNameCount()));
      } else {
         return optional();
      }
   }

   /**
    * Removes start path from base path.
    *
    * <ul>
    *    <li>{@code removeStart("/base/sub", "/base") -> "sub"}</li>
    *    <li>{@code removeStart("base/sub", "base") -> "sub"}</li>
    *    <li>{@code removeStart("base/sub", "base/sub") -> ""}</li>
    *    <li>{@code removeStart("/base", "/base") -> ""}</li>
    *    <li>{@code removeStart("/base", "/") -> "base"}</li>
    *    <li>{@code removeStart("/", "/") -> ""}</li>
    *    <li>{@code removeStart("", "") -> ""}</li>
    *    <li>{@code removeStart("/base/sub", "/other") -> ø}</li>
    *    <li>{@code removeStart("base/sub", "other") -> ø}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * @param path base path, can be relative, absolute or empty
    * @param startPath start path to remove from base path, can be relative, absolute or empty
    * @param ignoreCase case-insensitive elements match
    *
    * @return base path without end path, or {@link Optional#empty()} if end path does not end base path
    */
   public static Optional<Path> removeStart(Path path, Path startPath, boolean ignoreCase) {
      notNull(path, "path");
      notNull(startPath, "startPath");

      return removeStart(path,
                         startPath,
                         (realPathSegment, subPathSegment) -> pathElementEquals(realPathSegment,
                                                                                subPathSegment,
                                                                                ignoreCase));

   }

   /**
    * Removes start path from base path (case-sensitive).
    *
    * <ul>
    *    <li>{@code removeStart("/base/sub", "/base") -> "sub"}</li>
    *    <li>{@code removeStart("base/sub", "base") -> "sub"}</li>
    *    <li>{@code removeStart("base/sub", "base/sub") -> ""}</li>
    *    <li>{@code removeStart("/base", "/base") -> ""}</li>
    *    <li>{@code removeStart("/base", "/") -> "base"}</li>
    *    <li>{@code removeStart("/", "/") -> ""}</li>
    *    <li>{@code removeStart("", "") -> ""}</li>
    *    <li>{@code removeStart("/base/sub", "/other") -> ø}</li>
    *    <li>{@code removeStart("base/sub", "other") -> ø}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * @param path base path, can be relative, absolute or empty
    * @param startPath start path to remove from base path, can be relative, absolute or empty
    *
    * @return base path without end path, or {@link Optional#empty()} if end path does not end base path
    */
   public static Optional<Path> removeStart(Path path, Path startPath) {
      notNull(path, "path");
      notNull(startPath, "startPath");

      return removeStart(path, startPath, false);
   }

   /**
    * Removes end path from base path.
    *
    * <ul>
    *    <li>{@code removeEnd("/base/sub", "sub") -> "/base"}</li>
    *    <li>{@code removeEnd("/base/sub", "base/sub") -> "/"}</li>
    *    <li>{@code removeEnd("base/sub", "sub") -> "base"}</li>
    *    <li>{@code removeEnd("base/sub", "base/sub") -> ""}</li>
    *    <li>{@code removeEnd("/base", "/base") -> ""}</li>
    *    <li>{@code removeEnd("/", "/") -> ""}</li>
    *    <li>{@code removeEnd("", "") -> ""}</li>
    *    <li>{@code removeEnd("/base/sub", "other") -> ø}</li>
    * </ul>
    * Results can vary depending on specified path element equator.
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * @param path base path, can be relative, absolute or empty
    * @param endPath end path to remove from base path, can be relative, absolute or empty
    * @param equator path element equator
    *
    * @return base path without end path, or {@link Optional#empty()} if end path does not end base path
    */
   public static Optional<Path> removeEnd(Path path, Path endPath, Equator<Path> equator) {
      notNull(path, "path");
      notNull(endPath, "endPath");

      if (isEmpty(endPath)) {
         return optional(path);
      } else if (endsWith(path, endPath, equator)) {
         if (endPath.isAbsolute()) {
            return optional(EMPTY_PATH);
         } else if (path.getNameCount() - endPath.getNameCount() == 0) {
            return optional(nullable(path.getRoot(), EMPTY_PATH));
         } else {
            Path subpath = path.subpath(0, path.getNameCount() - endPath.getNameCount());
            return nullable(path.getRoot()).map(root -> root.resolve(subpath)).or(() -> optional(subpath));
         }
      } else {
         return optional();
      }
   }

   /**
    * Removes end path from base path.
    *
    * <ul>
    *    <li>{@code removeEnd("/base/sub", "sub") -> "/base"}</li>
    *    <li>{@code removeEnd("/base/sub", "base/sub") -> "/"}</li>
    *    <li>{@code removeEnd("base/sub", "sub") -> "base"}</li>
    *    <li>{@code removeEnd("base/sub", "base/sub") -> ""}</li>
    *    <li>{@code removeEnd("/base", "/base") -> ""}</li>
    *    <li>{@code removeEnd("/", "/") -> ""}</li>
    *    <li>{@code removeEnd("", "") -> ""}</li>
    *    <li>{@code removeEnd("/base/sub", "other") -> ø}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * @param path base path, can be relative, absolute or empty
    * @param endPath end path to remove from base path, can be relative, absolute or empty
    * @param ignoreCase case-insensitive elements match
    *
    * @return base path without end path, or {@link Optional#empty()} if end path does not end base path
    */
   public static Optional<Path> removeEnd(Path path, Path endPath, boolean ignoreCase) {
      notNull(path, "path");
      notNull(endPath, "endPath");

      return removeEnd(path,
                       endPath,
                       (realPathSegment, subPathSegment) -> pathElementEquals(realPathSegment,
                                                                              subPathSegment,
                                                                              ignoreCase));
   }

   /**
    * Removes end path from base path (case-sensitive).
    *
    * <ul>
    *    <li>{@code removeEnd("/base/sub", "sub") -> "/base"}</li>
    *    <li>{@code removeEnd("/base/sub", "base/sub") -> "/"}</li>
    *    <li>{@code removeEnd("base/sub", "sub") -> "base"}</li>
    *    <li>{@code removeEnd("base/sub", "base/sub") -> ""}</li>
    *    <li>{@code removeEnd("/base", "/base") -> ""}</li>
    *    <li>{@code removeEnd("/", "/") -> ""}</li>
    *    <li>{@code removeEnd("", "") -> ""}</li>
    *    <li>{@code removeEnd("/base/sub", "other") -> ø}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * @param path base path, can be relative, absolute or empty
    * @param endPath end path to remove from base path, can be relative, absolute or empty
    *
    * @return base path without end path, or {@link Optional#empty()} if end path does not end base path
    */
   public static Optional<Path> removeEnd(Path path, Path endPath) {
      notNull(path, "path");
      notNull(endPath, "endPath");

      return removeEnd(path, endPath, false);
   }

   /**
    * Checks if paths are equal.
    * <ul>
    * <li>{@code equals("path", "path") -> true}</li>
    * <li>{@code equals("/path", "/path") -> true}</li>
    * <li>{@code equals("/path", "path") -> false}</li>
    * <li>{@code equals("/", "/") -> true}</li>
    * <li>{@code equals("", "") -> true}</li>
    * <li>{@code equals("", "") -> true}</li>
    * </ul>
    * Results can vary depending on specified path element equator.
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code equals("path/..", "path/..") -> true}</li>
    * <li>{@code equals("path", "path/.") -> false}</li>
    * </ul>
    *
    * @param path1 first path to check
    * @param path2 second path to check
    * @param equator path element equator
    *
    * @return {@code true} of paths are equal
    */
   public static boolean equals(Path path1, Path path2, Equator<Path> equator) {
      notNull(path1, "path1");
      notNull(path2, "path2");

      if (path1.isAbsolute() != path2.isAbsolute()) {
         return false;
      }

      Iterator<Path> path1Iterator = path1.iterator();
      Iterator<Path> path2Iterator = path2.iterator();

      while (path1Iterator.hasNext()) {
         if (path2Iterator.hasNext()) {
            Path path1Segment = path1Iterator.next();
            Path path2Segment = path2Iterator.next();
            if (!equator.equate(path1Segment, path2Segment)) {
               return false;
            }
         } else {
            break;
         }
      }
      return !path1Iterator.hasNext() && !path2Iterator.hasNext();
   }

   /**
    * Checks if paths are equal.
    * <ul>
    * <li>{@code equals("path", "path") -> true}</li>
    * <li>{@code equals("/path", "/path") -> true}</li>
    * <li>{@code equals("/path", "path") -> false}</li>
    * <li>{@code equals("/", "/") -> true}</li>
    * <li>{@code equals("", "") -> true}</li>
    * <li>{@code equals("", "") -> true}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code equals("path/..", "path/..") -> true}</li>
    * <li>{@code equals("path", "path/.") -> false}</li>
    * </ul>
    *
    * @param path1 first path to check
    * @param path2 second path to check
    * @param ignoreCase case-insensitive elements match
    *
    * @return {@code true} of paths are equal
    */
   public static boolean equals(Path path1, Path path2, boolean ignoreCase) {
      notNull(path1, "path1");
      notNull(path2, "path2");

      return equals(path1, path2, (p1s, p2s) -> pathElementEquals(p1s, p2s, ignoreCase));
   }

   /**
    * Checks if paths are equal (case-sensitive).
    * <ul>
    * <li>{@code equals("path", "path") -> true}</li>
    * <li>{@code equals("/path", "/path") -> true}</li>
    * <li>{@code equals("/path", "path") -> false}</li>
    * <li>{@code equals("/", "/") -> true}</li>
    * <li>{@code equals("", "") -> true}</li>
    * <li>{@code equals("", "") -> true}</li>
    * </ul>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code equals("path/..", "path/..") -> true}</li>
    * <li>{@code equals("path", "path/.") -> false}</li>
    * </ul>
    *
    * @param path1 first path to check
    * @param path2 second path to check
    *
    * @return {@code true} of paths are equal
    */
   public static boolean equals(Path path1, Path path2) {
      return equals(path1, path2, false);
   }

   /**
    * Checks if sub path is contained in path.
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <ul>
    * <li>{@code contains("path/subpath", "path") -> true}</li>
    * <li>{@code contains("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code contains("path/subpath", "path/sub") -> false}</li>
    * <li>{@code contains("path/subpath", "/path") -> false}</li>
    * <li>{@code contains("/path/subpath", "path") -> true}</li>
    * <li>{@code contains("/path/subpath", "/path") -> true}</li>
    * <li>{@code contains("/path/subpath", "/") -> true}</li>
    * <li>{@code contains("path/subpath", "/") -> false}</li>
    * <li>{@code contains("/", "/") -> true}</li>
    * <li>{@code contains("/path/subpath", "") -> true}</li>
    * <li>{@code contains("", "") -> true}</li>
    * </ul>
    * Results can vary depending on specified equator.
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code contains("path/..", "path") -> true}</li>
    * <li>{@code contains("path/../subpath", "path/..") -> true}</li>
    * <li>{@code contains("path/subpath", "path/.") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param equator path element equator
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean contains(Path path, Path subPath, Equator<Path> equator) {
      notNull(path, "path");
      notNull(subPath, "subPath");

      return indexOf(path, subPath, equator).isPresent();
   }

   /**
    * Checks if sub path is contained in path.
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <ul>
    * <li>{@code contains("path/subpath", "path") -> true}</li>
    * <li>{@code contains("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code contains("path/subpath", "path/sub") -> false}</li>
    * <li>{@code contains("path/subpath", "/path") -> false}</li>
    * <li>{@code contains("/path/subpath", "path") -> true}</li>
    * <li>{@code contains("/path/subpath", "/path") -> true}</li>
    * <li>{@code contains("/path/subpath", "/") -> true}</li>
    * <li>{@code contains("path/subpath", "/") -> false}</li>
    * <li>{@code contains("/", "/") -> true}</li>
    * <li>{@code contains("/path/subpath", "") -> true}</li>
    * <li>{@code contains("", "") -> true}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code contains("path/..", "path") -> true}</li>
    * <li>{@code contains("path/../subpath", "path/..") -> true}</li>
    * <li>{@code contains("path/subpath", "path/.") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param ignoreCase case-insensitive elements match
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean contains(Path path, Path subPath, boolean ignoreCase) {
      notNull(path, "path");
      notNull(subPath, "subPath");

      return contains(path,
                      subPath,
                      (realPathSegment, subPathSegment) -> pathElementEquals(realPathSegment,
                                                                             subPathSegment,
                                                                             ignoreCase));
   }

   /**
    * Checks if sub path is contained in path (case-sensitive).
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <ul>
    * <li>{@code contains("path/subpath", "path") -> true}</li>
    * <li>{@code contains("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code contains("path/subpath", "path/sub") -> false}</li>
    * <li>{@code contains("path/subpath", "/path") -> false}</li>
    * <li>{@code contains("/path/subpath", "path") -> true}</li>
    * <li>{@code contains("/path/subpath", "/path") -> true}</li>
    * <li>{@code contains("/path/subpath", "/") -> true}</li>
    * <li>{@code contains("path/subpath", "/") -> false}</li>
    * <li>{@code contains("/", "/") -> true}</li>
    * <li>{@code contains("/path/subpath", "") -> true}</li>
    * <li>{@code contains("", "") -> true}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code contains("path/..", "path") -> true}</li>
    * <li>{@code contains("path/../subpath", "path/..") -> true}</li>
    * <li>{@code contains("path/subpath", "path/.") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean contains(Path path, Path subPath) {
      return contains(path, subPath, false);
   }

   /**
    * Checks if sub path is contained in path and returns the index in path of the first matching element
    * from sub path.
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <p>
    * Index value starts from 0 to identify the first path element. The root component does not change the
    * index value.
    * <ul>
    * <li>{@code indexOf("path/subpath", "path") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "path/subpath") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "subpath") -> 1}</li>
    * <li>{@code indexOf("path/subpath", "path/sub") -> ø}</li>
    * <li>{@code indexOf("path/subpath", "/path") -> ø}</li>
    * <li>{@code indexOf("/path/subpath", "path") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "/path") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "/") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "/") -> ø}</li>
    * <li>{@code indexOf("/", "/") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "") -> 0}</li>
    * <li>{@code indexOf("", "") -> 0}</li>
    * </ul>
    * Results can vary depending on specified path element equator.
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code indexOf("path/..", "path") -> 0}</li>
    * <li>{@code indexOf("path/../subpath", "path/..") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "path/.") -> ø}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param equator path element equator
    *
    * @return {@code true} of sub path contained in path
    */
   public static Optional<Integer> indexOf(Path path, Path subPath, Equator<Path> equator) {
      notNull(path, "path");
      notNull(subPath, "subPath");
      notNull(equator, "equator");

      if (subPath.isAbsolute()) {
         return startsWith(path, subPath, equator) ? optional(0) : optional();
      } else if (isEmpty(subPath)) {
         return optional(0);
      }

      int pathPosition = 0;
      int pathBackwardPosition = 0;
      int subPathPosition = 0;

      while (pathPosition < path.getNameCount()) {
         Path pathSegment = path.getName(pathPosition);
         if (subPathPosition < subPath.getNameCount()) {
            Path subPathSegment = subPath.getName(subPathPosition);
            if (!equator.equate(pathSegment, subPathSegment)) {
               subPathPosition = 0;
               pathBackwardPosition++;
               pathPosition = pathBackwardPosition;
            } else {
               subPathPosition++;
               pathPosition++;
            }
         } else {
            break;
         }
      }
      return !(subPathPosition < subPath.getNameCount()) ? optional(pathBackwardPosition) : optional();
   }

   /**
    * Checks if sub path is contained in path and returns the index in path of the first matching element
    * from sub path.
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <p>
    * Index value starts from 0 to identify the first path element. The root component does not change the
    * index value.
    * <ul>
    * <li>{@code indexOf("path/subpath", "path") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "path/subpath") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "subpath") -> 1}</li>
    * <li>{@code indexOf("path/subpath", "path/sub") -> ø}</li>
    * <li>{@code indexOf("path/subpath", "/path") -> ø}</li>
    * <li>{@code indexOf("/path/subpath", "path") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "/path") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "/") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "/") -> ø}</li>
    * <li>{@code indexOf("/", "/") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "") -> 0}</li>
    * <li>{@code indexOf("", "") -> 0}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code indexOf("path/..", "path") -> 0}</li>
    * <li>{@code indexOf("path/../subpath", "path/..") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "path/.") -> ø}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param ignoreCase case-insensitive elements match
    *
    * @return {@code true} of sub path contained in path
    */
   public static Optional<Integer> indexOf(Path path, Path subPath, boolean ignoreCase) {
      notNull(path, "path");
      notNull(subPath, "subPath");

      return indexOf(path,
                     subPath,
                     (realPathSegment, subPathSegment) -> pathElementEquals(realPathSegment,
                                                                            subPathSegment,
                                                                            ignoreCase));
   }

   /**
    * Checks if sub path is contained (case-sensitive) in path and returns the index in path of the first
    * matching element from sub path.
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <p>
    * Index value starts from 0 to identify the first path element. The root component does not change the
    * index value.
    * <ul>
    * <li>{@code indexOf("path/subpath", "path") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "path/subpath") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "subpath") -> 1}</li>
    * <li>{@code indexOf("path/subpath", "path/sub") -> ø}</li>
    * <li>{@code indexOf("path/subpath", "/path") -> ø}</li>
    * <li>{@code indexOf("/path/subpath", "path") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "/path") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "/") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "/") -> ø}</li>
    * <li>{@code indexOf("/", "/") -> 0}</li>
    * <li>{@code indexOf("/path/subpath", "") -> 0}</li>
    * <li>{@code indexOf("", "") -> 0}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code indexOf("path/..", "path") -> 0}</li>
    * <li>{@code indexOf("path/../subpath", "path/..") -> 0}</li>
    * <li>{@code indexOf("path/subpath", "path/.") -> ø}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    *
    * @return {@code true} of sub path contained in path
    */
   public static Optional<Integer> indexOf(Path path, Path subPath) {
      return indexOf(path, subPath, false);
   }

   /**
    * Checks if path starts with specified sub path.
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <ul>
    * <li>{@code startsWith("path/subpath", "path") -> true}</li>
    * <li>{@code startsWith("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code startsWith("path/subpath", "subpath") -> false}</li>
    * <li>{@code startsWith("path/subpath", "path/sub") -> false}</li>
    * <li>{@code startsWith("path/subpath", "/path") -> false}</li>
    * <li>{@code startsWith("/path/subpath", "path") -> false}</li>
    * <li>{@code startsWith("/path/subpath", "/path") -> true}</li>
    * <li>{@code startsWith("/path/subpath", "/") -> true}</li>
    * <li>{@code startsWith("path/subpath", "/") -> false}</li>
    * <li>{@code startsWith("/", "/") -> true}</li>
    * <li>{@code startsWith("/path/subpath", "") -> true}</li>
    * <li>{@code startsWith("", "") -> true}</li>
    * </ul>
    * Results can vary depending on specified path element equator.
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code startsWith("path/..", "path") -> true}</li>
    * <li>{@code startsWith("path/../subpath", "path/..") -> true}</li>
    * <li>{@code startsWith("path/subpath", "path/.") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param equator path element equator
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean startsWith(Path path, Path subPath, Equator<Path> equator) {
      notNull(path, "path");
      notNull(subPath, "subPath");
      notNull(equator, "equator");

      if (isEmpty(subPath)) {
         return true;
      } else if (subPath.isAbsolute() != path.isAbsolute()) {
         return false;
      }

      Iterator<Path> realPathIterator = path.iterator();
      Iterator<Path> subPathIterator = subPath.iterator();

      while (realPathIterator.hasNext()) {
         Path realPathSegment = realPathIterator.next();
         if (subPathIterator.hasNext()) {
            Path subPathSegment = subPathIterator.next();
            if (!equator.equate(realPathSegment, subPathSegment)) {
               return false;
            }
         } else {
            break;
         }
      }
      return !subPathIterator.hasNext();
   }

   /**
    * Checks if path starts with specified sub path.
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <ul>
    * <li>{@code startsWith("path/subpath", "path") -> true}</li>
    * <li>{@code startsWith("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code startsWith("path/subpath", "subpath") -> false}</li>
    * <li>{@code startsWith("path/subpath", "path/sub") -> false}</li>
    * <li>{@code startsWith("path/subpath", "/path") -> false}</li>
    * <li>{@code startsWith("/path/subpath", "path") -> false}</li>
    * <li>{@code startsWith("/path/subpath", "/path") -> true}</li>
    * <li>{@code startsWith("/path/subpath", "/") -> true}</li>
    * <li>{@code startsWith("path/subpath", "/") -> false}</li>
    * <li>{@code startsWith("/", "/") -> true}</li>
    * <li>{@code startsWith("/path/subpath", "") -> true}</li>
    * <li>{@code startsWith("", "") -> true}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code startsWith("path/..", "path") -> true}</li>
    * <li>{@code startsWith("path/../subpath", "path/..") -> true}</li>
    * <li>{@code startsWith("path/subpath", "path/.") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param ignoreCase case-insensitive elements match
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean startsWith(Path path, Path subPath, boolean ignoreCase) {
      notNull(path, "path");
      notNull(subPath, "subPath");

      return startsWith(path,
                        subPath,
                        (realPathSegment, subPathSegment) -> pathElementEquals(realPathSegment,
                                                                               subPathSegment,
                                                                               ignoreCase));
   }

   /**
    * Checks if path starts with specified sub path (case-sensitive).
    * <p>
    * If sub path is absolute, path must be absolute and start with sub path.
    * <ul>
    * <li>{@code startsWith("path/subpath", "path") -> true}</li>
    * <li>{@code startsWith("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code startsWith("path/subpath", "subpath") -> false}</li>
    * <li>{@code startsWith("path/subpath", "path/sub") -> false}</li>
    * <li>{@code startsWith("path/subpath", "/path") -> false}</li>
    * <li>{@code startsWith("/path/subpath", "path") -> false}</li>
    * <li>{@code startsWith("/path/subpath", "/path") -> true}</li>
    * <li>{@code startsWith("/path/subpath", "/") -> true}</li>
    * <li>{@code startsWith("path/subpath", "/") -> false}</li>
    * <li>{@code startsWith("/", "/") -> true}</li>
    * <li>{@code startsWith("/path/subpath", "") -> true}</li>
    * <li>{@code startsWith("", "") -> true}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code startsWith("path/..", "path") -> true}</li>
    * <li>{@code startsWith("path/../subpath", "path/..") -> true}</li>
    * <li>{@code startsWith("path/subpath", "path/.") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean startsWith(Path path, Path subPath) {
      return startsWith(path, subPath, false);
   }

   /**
    * Checks if path ends with specified sub path.
    * <p>
    * If sub path is absolute, path must be absolute and must be equal with sub path.
    * <ul>
    * <li>{@code endsWith("path/subpath", "path") -> false}</li>
    * <li>{@code endsWith("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "th/subpath") -> false}</li>
    * <li>{@code endsWith("path/subpath", "/subpath") -> false}</li>
    * <li>{@code endsWith("/path/subpath", "subpath") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "/path") -> false}</li>
    * <li>{@code endsWith("/path/subpath", "/path/subpath") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "/") -> false}</li>
    * <li>{@code endsWith("path/subpath", "/") -> false}</li>
    * <li>{@code endsWith("/", "/") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "") -> true}</li>
    * <li>{@code endsWith("", "") -> true}</li>
    * </ul>
    * Results can vary depending on specified path element equator.
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code endsWith("../path", "path") -> true}</li>
    * <li>{@code endsWith("path/../subpath", "../subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "subpath/.") -> false}</li>
    * <li>{@code endsWith("path/subpath", "subpath/otherpath/..") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param equator path element equator
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean endsWith(Path path, Path subPath, Equator<Path> equator) {
      notNull(path, "path");
      notNull(subPath, "subPath");

      if (isEmpty(subPath)) {
         return true;
      } else if (subPath.isAbsolute()) {
         return equals(path, subPath, equator);
      }

      Iterator<Path> realPathIterator = reverseIterator(path);
      Iterator<Path> subPathIterator = reverseIterator(subPath);

      while (realPathIterator.hasNext()) {
         Path realPathSegment = realPathIterator.next();
         if (subPathIterator.hasNext()) {
            Path subPathSegment = subPathIterator.next();
            if (!equator.equate(realPathSegment, subPathSegment)) {
               return false;
            }
         } else {
            break;
         }
      }
      return !subPathIterator.hasNext();
   }

   /**
    * Checks if path ends with specified sub path.
    * <p>
    * If sub path is absolute, path must be absolute and must be equal with sub path.
    * <ul>
    * <li>{@code endsWith("path/subpath", "path") -> false}</li>
    * <li>{@code endsWith("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "th/subpath") -> false}</li>
    * <li>{@code endsWith("path/subpath", "/subpath") -> false}</li>
    * <li>{@code endsWith("/path/subpath", "subpath") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "/path") -> false}</li>
    * <li>{@code endsWith("/path/subpath", "/path/subpath") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "/") -> false}</li>
    * <li>{@code endsWith("path/subpath", "/") -> false}</li>
    * <li>{@code endsWith("/", "/") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "") -> true}</li>
    * <li>{@code endsWith("", "") -> true}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code endsWith("../path", "path") -> true}</li>
    * <li>{@code endsWith("path/../subpath", "../subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "subpath/.") -> false}</li>
    * <li>{@code endsWith("path/subpath", "subpath/otherpath/..") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    * @param ignoreCase case-insensitive elements match
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean endsWith(Path path, Path subPath, boolean ignoreCase) {
      notNull(path, "path");
      notNull(subPath, "subPath");

      return endsWith(path,
                      subPath,
                      (realPathSegment, subPathSegment) -> pathElementEquals(realPathSegment,
                                                                             subPathSegment,
                                                                             ignoreCase));

   }

   /**
    * Checks if path ends with specified sub path (case-sensitive).
    * <p>
    * If sub path is absolute, path must be absolute and must be equal with sub path.
    * <ul>
    * <li>{@code endsWith("path/subpath", "path") -> false}</li>
    * <li>{@code endsWith("path/subpath", "path/subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "th/subpath") -> false}</li>
    * <li>{@code endsWith("path/subpath", "/subpath") -> false}</li>
    * <li>{@code endsWith("/path/subpath", "subpath") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "/path") -> false}</li>
    * <li>{@code endsWith("/path/subpath", "/path/subpath") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "/") -> false}</li>
    * <li>{@code endsWith("path/subpath", "/") -> false}</li>
    * <li>{@code endsWith("/", "/") -> true}</li>
    * <li>{@code endsWith("/path/subpath", "") -> true}</li>
    * <li>{@code endsWith("", "") -> true}</li>
    * </ul>
    * <p>
    * Path are not normalized during comparison, so that traversals elements are matched as any other element.
    *
    * <ul>
    * <li>{@code endsWith("../path", "path") -> true}</li>
    * <li>{@code endsWith("path/../subpath", "../subpath") -> true}</li>
    * <li>{@code endsWith("path/subpath", "subpath/.") -> false}</li>
    * <li>{@code endsWith("path/subpath", "subpath/otherpath/..") -> false}</li>
    * </ul>
    *
    * @param path path to check
    * @param subPath sub path to search in path
    *
    * @return {@code true} of sub path contained in path
    */
   public static boolean endsWith(Path path, Path subPath) {
      return endsWith(path, subPath, false);
   }

   /**
    * Reverse iterator for {@link Path}.
    *
    * @param path path
    *
    * @return path reverse iterator
    */
   public static Iterator<Path> reverseIterator(Path path) {
      notNull(path, "path");

      return new Iterator<>() {
         private int i = path.getNameCount() - 1;

         @Override
         public boolean hasNext() {
            return i >= 0;
         }

         @Override
         public Path next() {
            if (i >= 0) {
               Path result = path.getName(i);
               i--;
               return result;
            } else {
               throw new NoSuchElementException();
            }
         }
      };
   }

   private static boolean pathElementEquals(Path path1, Path path2, boolean ignoreCase) {
      return ignoreCase
             ? StringUtils.equalsIgnoreCase(path1.toString(), path2.toString())
             : StringUtils.equals(path1.toString(), path2.toString());
   }

}
