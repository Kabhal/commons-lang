/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

/**
 * Represents a predicate (boolean-valued function) of one argument.
 * This is a functional interface whose functional method is {@link #testChecked(Object)}.
 * {@link #testChecked(Object)} can throw checked exceptions.
 * If this class is used as {@link Predicate}, then unchecked {@link #test(Object)} will be called.
 *
 * @param <T> the type of the input to the operation
 */
@FunctionalInterface
public interface CheckedPredicate<T> extends XCheckedPredicate<T, Exception> {

   /**
    * Always {@code true} predicate.
    *
    * @param <T> the type of the input to the operation
    *
    * @return always {@code true} predicate
    */
   static <T> CheckedPredicate<T> alwaysTrue() {
      return __ -> true;
   }

   /**
    * Always {@code false} predicate.
    *
    * @param <T> the type of the input to the operation
    *
    * @return always {@code false} predicate
    */
   static <T> CheckedPredicate<T> alwaysFalse() {
      return __ -> false;
   }

   /**
    * Non-{@code null} elements predicate.
    *
    * @param <T> the type of the input to the operation
    *
    * @return Non-{@code null} elements predicate
    */
   static <T> CheckedPredicate<T> nonNull() {
      return Objects::nonNull;
   }

   /**
    * Returns a checked predicate from a {@link Predicate} or an unchecked anonymous lambda.
    *
    * @param predicate predicate to adapt
    *
    * @return checked predicate
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> CheckedPredicate<T> checkedPredicate(XCheckedPredicate<? super T, ? extends X> predicate) {
      return checkedPredicate((Predicate<T>) predicate);
   }

   /**
    * Returns a checked predicate from a {@link Predicate} or an unchecked anonymous lambda.
    *
    * @param predicate predicate to adapt
    *
    * @return checked predicate
    *
    * @implSpec Unlike {@link XCheckedPredicate#checkedPredicate(Predicate)}, specified predicate
    *       instance of {@link XCheckedPredicate} are not unchecked, because it's not necessary to infer
    *       exception type which is always {@link Exception}.
    */
   @SuppressWarnings("unchecked")
   static <T> CheckedPredicate<T> checkedPredicate(Predicate<? super T> predicate) {
      requireNonNull(predicate);
      if (predicate instanceof CheckedPredicate) {
         return (CheckedPredicate<T>) predicate;
      } else if (predicate instanceof XCheckedPredicate) {
         return ((XCheckedPredicate<T, ?>) predicate)::testChecked;
      } else {
         return predicate::test;
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   default CheckedPredicate<T> autoClose() {
      return t -> {
         if (t instanceof AutoCloseable) {
            try (AutoCloseable autoCloseable = (AutoCloseable) t) {
               return testChecked((T) autoCloseable);
            }
         } else {
            return testChecked(t);
         }
      };
   }

   @Override
   default CheckedPredicate<T> autoCloseOnThrow() {
      return t -> {
         try {
            return testChecked(t);
         } catch (Throwable throwable) {
            if (t instanceof AutoCloseable) {
               try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                  throw throwable;
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   default CheckedPredicate<T> autoCloseOnThrow(Class<? extends Exception> exceptionClass) {
      requireNonNull(exceptionClass);
      return t -> {
         try {
            return testChecked(t);
         } catch (Throwable throwable) {
            if (exceptionClass.isAssignableFrom(throwable.getClass())) {
               if (t instanceof AutoCloseable) {
                  try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                     throw throwable;
                  }
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   @SuppressWarnings("ThrowFromFinallyBlock")
   default CheckedPredicate<T> tryFinally(List<? extends XCheckedRunnable<? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         Throwable throwable = null;
         try {
            return testChecked(v);
         } catch (Throwable t) {
            throwable = t;
            throw t;
         } finally {
            for (XCheckedRunnable<? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.runChecked();
               } catch (Throwable finalizerThrowable) {
                  if (throwable != null) {
                     throwable.addSuppressed(finalizerThrowable);
                  } else {
                     throwable = finalizerThrowable;
                  }
               }
            }
            if (throwable != null) {
               throw sneakyThrow(throwable);
            }
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedPredicate<T> tryFinally(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryFinally(list(finalizers));
   }

   @Override
   default CheckedPredicate<T> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         try {
            return testChecked(v);
         } catch (Throwable throwable) {
            for (XCheckedConsumer<Throwable, ? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.acceptChecked(throwable);
               } catch (Throwable ft) {
                  throwable.addSuppressed(ft);
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedPredicate<T> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedPredicate<T> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default <SX extends Exception> CheckedPredicate<T> tryCatch(Class<? extends SX> exceptionClass,
                                                               List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return v -> {
         try {
            return testChecked(v);
         } catch (Exception exception) {
            if (exceptionClass.isAssignableFrom(exception.getClass()))
               for (XCheckedConsumer<? super SX, ? extends Exception> finalizer : finalizers) {
                  try {
                     finalizer.acceptChecked(exceptionClass.cast(exception));
                  } catch (Throwable ft) {
                     exception.addSuppressed(ft);
                  }
               }
            throw exception;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends Exception> CheckedPredicate<T> tryCatch(Class<? extends SX> exceptionClass,
                                                               XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedPredicate<T> tryCatch(Class<? extends Exception> exceptionClass,
                                        XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default CheckedPredicate<T> and(Predicate<? super T> other) {
      Objects.requireNonNull(other);
      return t -> test(t) && other.test(t);
   }

   @Override
   default CheckedPredicate<T> and(XCheckedPredicate<? super T, ? extends Exception> other) {
      Objects.requireNonNull(other);
      return t -> test(t) && other.testChecked(t);
   }

   @Override
   default CheckedPredicate<T> negate() {
      return (t) -> !test(t);
   }

   @Override
   default CheckedPredicate<T> or(Predicate<? super T> other) {
      Objects.requireNonNull(other);
      return t -> test(t) || other.test(t);
   }

   @Override
   default CheckedPredicate<T> or(XCheckedPredicate<? super T, ? extends Exception> other) {
      Objects.requireNonNull(other);
      return t -> test(t) || other.testChecked(t);
   }
}
