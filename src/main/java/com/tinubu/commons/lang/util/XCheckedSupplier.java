/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.Try.ofThrownBy;
import static java.util.Objects.requireNonNull;

import java.io.UncheckedIOException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

/**
 * Represents a supplier of results.
 * This is a functional interface whose functional method is {@link #getChecked()}.
 * {@link #getChecked()} can throw {@link X genericized} exceptions.
 * If this class is used as {@link Supplier}, then unchecked {@link #get()} will be called.
 * If this class is used as {@link Callable}, then checked {@link #call()} (throwing {@link Exception}) will
 * be called.
 * <p>
 * There is no requirement that a new or distinct result be returned each time the supplier is invoked.
 *
 * @param <T> the type of results supplied by this supplier
 */
@FunctionalInterface
public interface XCheckedSupplier<T, X extends Exception> extends Supplier<T>, Callable<T> {

   /**
    * Returns a rethrowing supplier.
    * This factory method is useful for the rethrow-with-finalizers pattern.
    *
    * @param exception the exception to rethrow
    * @param <X> the type of the exception to rethrow
    *
    * @return rethrowing supplier
    */
   static <X extends Exception> XCheckedSupplier<X, X> rethrow(X exception) {
      return () -> {
         throw exception;
      };
   }

   /**
    * Gets a checked result.
    * This operation can throw {@link X} exceptions.
    *
    * @return a result
    *
    * @throws X if an error occurs
    */
   T getChecked() throws X;

   /**
    * Gets an unchecked result.
    *
    * @return a result
    */
   @Override
   default T get() {
      return unchecked().get();
   }

   /**
    * Gets a checked result to match {@link Callable} interface.
    * This operation can throw {@link Exception} exceptions.
    *
    * @return a result
    *
    * @throws Exception if an error occurs
    */
   @Override
   default T call() throws Exception {
      return getChecked();
   }

   /**
    * Adhering to functional style, returns a nullable value from unchecked supplier as an {@link Optional}.
    *
    * @return supplied value or {@link Optional#empty()} if supplied value is {@code null}
    */
   default Optional<T> getNullable() {
      return nullable(get());
   }

   /**
    * Adhering to functional style, returns a nullable value from supplier as an {@link Optional}, wrapped
    * into a {@link Try} to manage exceptions.
    *
    * @return supplied value or {@link Optional#empty()} if supplied value is {@code null}
    */
   default Try<Optional<T>, X> getCheckedNullable() {
      return ofThrownBy(() -> nullable(getChecked()));
   }

   /**
    * Returns a callable from an anonymous lambda.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> Callable<T> callable(XCheckedSupplier<? extends T, ? extends X> supplier) {
      return (Callable<T>) supplier;
   }

   /**
    * Returns a boolean supplier from an anonymous lambda.
    * Any instance of {@link XCheckedSupplier} will be unchecked.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   static <X extends Exception> BooleanSupplier booleanSupplier(XCheckedSupplier<Boolean, ? extends X> supplier) {
      return supplier::get;
   }

   /**
    * Returns a checked supplier from an {@link XCheckedSupplier} or a checked anonymous lambda.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> XCheckedSupplier<T, X> checkedSupplier(XCheckedSupplier<? extends T, ? extends X> supplier) {
      return (XCheckedSupplier<T, X>) supplier;
   }

   /**
    * Returns a checked supplier from an {@link XCheckedSupplier} or a checked anonymous lambda.
    *
    * @param supplier supplier to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked supplier
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> XCheckedSupplier<T, X> checkedSupplier(XCheckedSupplier<? extends T, ? extends X> supplier,
                                                                          Class<X> thrownExceptionClass) {
      return (XCheckedSupplier<T, X>) supplier;
   }

   /**
    * Returns a checked supplier from a {@link Supplier} or an unchecked anonymous lambda.
    * Any instance of {@link XCheckedSupplier} will be unchecked.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    *
    * @implSpec Even if specified supplier is an instance of {@link CheckedSupplier} or
    *       {@link XCheckedSupplier}, implementation will uncheck it like any {@link Supplier}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends Exception> XCheckedSupplier<T, X> checkedSupplier(Supplier<? extends T> supplier) {
      requireNonNull(supplier);
      return supplier::get;
   }

   /**
    * Returns a checked supplier from a {@link Supplier} or an unchecked anonymous lambda.
    * Any instance of {@link XCheckedSupplier} will be unchecked.
    *
    * @param supplier supplier to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked supplier
    *
    * @implSpec Even if specified supplier is an instance of {@link CheckedSupplier} or
    *       {@link XCheckedSupplier}, implementation will uncheck it like any {@link Supplier}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends Exception> XCheckedSupplier<T, X> checkedSupplier(Supplier<? extends T> supplier,
                                                                          Class<X> thrownExceptionClass) {
      requireNonNull(supplier);
      return supplier::get;
   }

   /**
    * Returns a checked supplier from a {@link BooleanSupplier}.
    *
    * @param supplier supplier to adapt
    *
    * @return checked supplier
    */
   static <X extends RuntimeException> XCheckedSupplier<Boolean, X> checkedSupplier(BooleanSupplier supplier) {
      requireNonNull(supplier);
      return supplier::getAsBoolean;
   }

   /**
    * Returns a checked supplier from a {@link BooleanSupplier}.
    *
    * @param supplier supplier to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked supplier
    */
   static <X extends RuntimeException> XCheckedSupplier<Boolean, X> checkedSupplier(BooleanSupplier supplier,
                                                                                    Class<X> thrownExceptionClass) {
      requireNonNull(supplier);
      return supplier::getAsBoolean;
   }

   /**
    * Returns a checked supplier from a standard callable.
    *
    * @param callable callable to adapt
    *
    * @return checked supplier
    */
   @SuppressWarnings("unchecked")
   static <T> XCheckedSupplier<T, Exception> checkedSupplier(Callable<? extends T> callable) {
      requireNonNull(callable);
      if (callable instanceof XCheckedSupplier) {
         return (XCheckedSupplier<T, Exception>) callable;
      } else {
         return callable::call;
      }
   }

   /**
    * Returns a checked supplier from a checked runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked supplier
    */
   static <X extends Exception> XCheckedSupplier<Void, X> checkedSupplier(XCheckedRunnable<? extends X> runnable) {
      requireNonNull(runnable);
      return () -> {
         runnable.runChecked();
         return null;
      };
   }

   /**
    * Returns a checked supplier from a checked runnable.
    *
    * @param runnable runnable to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked supplier
    */
   static <X extends Exception> XCheckedSupplier<Void, X> checkedSupplier(XCheckedRunnable<? extends X> runnable,
                                                                          Class<X> thrownExceptionClass) {
      requireNonNull(runnable);
      return () -> {
         runnable.runChecked();
         return null;
      };
   }

   /**
    * Returns a checked supplier from a standard runnable.
    * Any instance of {@link XCheckedRunnable} will be unchecked.
    *
    * @param runnable runnable to adapt
    *
    * @return checked supplier
    *
    * @implSpec Even if specified runnable is an instance of {@link CheckedRunnable} or
    *       {@link XCheckedRunnable}, implementation will uncheck it like any {@link Runnable}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <X extends RuntimeException> XCheckedSupplier<Void, X> checkedSupplier(Runnable runnable) {
      requireNonNull(runnable);
      return () -> {
         runnable.run();
         return null;
      };
   }

   /**
    * Returns a checked supplier from a standard runnable.
    * Any instance of {@link XCheckedRunnable} will be unchecked.
    *
    * @param runnable runnable to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked supplier
    *
    * @implSpec Even if specified runnable is an instance of {@link CheckedRunnable} or
    *       {@link XCheckedRunnable}, implementation will uncheck it like any {@link Runnable}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <X extends RuntimeException> XCheckedSupplier<Void, X> checkedSupplier(Runnable runnable,
                                                                                 Class<X> thrownExceptionClass) {
      requireNonNull(runnable);
      return () -> {
         runnable.run();
         return null;
      };
   }

   /**
    * Returns an unchecked function from this checked function, using either :
    * <ul>
    * <li>{@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} : exception is converted from {@link Exception} to {@link RuntimeException} using best-fitting exceptions (e.g.: {@link UncheckedIOException}, ...)</li>
    * <li>{@link ExceptionUtils#sneakyThrow(Throwable) sneaky-throw} : checked exception is rethrown as-is but Java does not force to catch checked exceptions anymore using a hack</li>
    * </ul>
    *
    * @param sneakyThrow whether to use sneaky-throw unchecking
    *
    * @return unchecked supplier
    */
   default Supplier<T> unchecked(boolean sneakyThrow) {
      return () -> {
         try {
            return this.getChecked();
         } catch (Exception e) {
            if (sneakyThrow) {
               throw ExceptionUtils.sneakyThrow(e);
            } else {
               throw ExceptionUtils.runtimeThrow(e);
            }
         }
      };
   }

   /**
    * Returns an unchecked supplier from this checked supplier, using
    * default {@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} unchecking.
    *
    * @return unchecked supplier
    */
   default Supplier<T> unchecked() {
      return unchecked(false);
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, then ensure all specified finalizers
    * are always called in specified order.
    * <p>
    * Wrapped checked supplier thrown exception is the first thrown exception, either by this
    * supplier, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked supplier with finalizers
    *
    * @implNote The second catch is a {@link Throwable}, so that some {@link Error} would be ignored as
    *       suppressed, because we consider more important to execute all finalizers than re-throw a nth
    *       {@link Error}, in the special case the first exception that will be finally rethrown is an
    *       {@link Exception}. The final {@link ExceptionUtils#sneakyThrow(Throwable)} is required in the
    *       case the first exception is thrown by a finalizer, not the supplier, and is an {@link Error}, in
    *       this case it's not a problem to sneak it as you're not supposed to catch it.
    */
   @SuppressWarnings("ThrowFromFinallyBlock")
   default XCheckedSupplier<T, X> tryFinally(List<? extends XCheckedRunnable<? extends X>> finalizers) {
      requireNonNull(finalizers);
      return () -> {
         Throwable throwable = null;
         try {
            return getChecked();
         } catch (Throwable t) {
            throwable = t;
            throw t;
         } finally {
            for (XCheckedRunnable<? extends X> finalizer : finalizers) {
               try {
                  finalizer.runChecked();
               } catch (Throwable finalizerThrowable) {
                  if (throwable != null) {
                     throwable.addSuppressed(finalizerThrowable);
                  } else {
                     throwable = finalizerThrowable;
                  }
               }
            }
            if (throwable != null) {
               throw sneakyThrow(throwable);
            }
         }
      };
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, then ensure all specified finalizers
    * are always called in specified order.
    * <p>
    * Wrapped checked supplier thrown exception is the first thrown exception, either by this
    * supplier, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked supplier with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedSupplier<T, X> tryFinally(XCheckedRunnable<? extends X>... finalizers) {
      return tryFinally(list(finalizers));
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, then ensure all specified finalizers
    * are always called in specified order.
    * <p>
    * Wrapped checked supplier thrown exception is the first thrown exception, either by this
    * supplier, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked supplier with finalizers
    */
   default XCheckedSupplier<T, X> tryFinally(Runnable... finalizers) {
      return tryFinally(list(stream(finalizers).map(XCheckedRunnable::checkedRunnable)));
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, and, only if a {@link Throwable} is
    * thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked supplier thrown exception is this supplier thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked supplier with finalizers
    */
   default XCheckedSupplier<T, X> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return () -> {
         try {
            return getChecked();
         } catch (Throwable throwable) {
            for (XCheckedConsumer<Throwable, ? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.acceptChecked(throwable);
               } catch (Throwable ft) {
                  throwable.addSuppressed(ft);
               }
            }
            throw throwable;
         }
      };
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, and, only if a {@link Throwable} is
    * thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked supplier thrown exception is this supplier thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked supplier with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedSupplier<T, X> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, and, only if a {@link Throwable} is
    * thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked supplier thrown exception is this supplier thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked supplier with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedSupplier<T, X> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, and, only if an {@link Exception}
    * instance of {@code exceptionClass} is thrown, ensure all specified finalizers are always called in
    * specified order.
    * <p>
    * Wrapped checked supplier thrown exception is this supplier thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked supplier with finalizers
    */
   @SuppressWarnings("unchecked")
   default <SX extends X> XCheckedSupplier<T, X> tryCatch(Class<? extends SX> exceptionClass,
                                                          List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return () -> {
         try {
            return getChecked();
         } catch (Exception exception) {
            if (exceptionClass.isAssignableFrom(exception.getClass()))
               for (XCheckedConsumer<? super SX, ? extends Exception> finalizer : finalizers) {
                  try {
                     finalizer.acceptChecked(exceptionClass.cast(exception));
                  } catch (Throwable ft) {
                     exception.addSuppressed(ft);
                  }
               }
            throw (X) exception;
         }
      };
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, and, only if an {@link Exception}
    * instance of {@code exceptionClass} is thrown, ensure all specified finalizers are always called in
    * specified order.
    * <p>
    * Wrapped checked supplier thrown exception is this supplier thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked supplier with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends X> XCheckedSupplier<T, X> tryCatch(Class<? extends SX> exceptionClass,
                                                          XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   /**
    * Returns a wrapped checked supplier calling {@link #getChecked()}, and, only if an {@link Exception}
    * instance of {@code exceptionClass} is thrown, ensure all specified finalizers are always called in
    * specified order.
    * <p>
    * Wrapped checked supplier thrown exception is this supplier thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked supplier with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedSupplier<T, X> tryCatch(Class<? extends X> exceptionClass,
                                           XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

}
