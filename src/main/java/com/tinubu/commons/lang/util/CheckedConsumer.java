/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.function.Consumer;

/**
 * Represents an operation that accepts a single input argument and returns no
 * result. Unlike most other functional interfaces, {@code CheckedConsumer} is expected
 * to operate via side effects.
 * This is a functional interface whose functional method is {@link #acceptChecked(Object)}.
 * {@link #acceptChecked(Object)} can throw checked exceptions.
 * If this class is used as {@link Consumer}, then unchecked {@link #accept(Object)} will be called.
 *
 * @param <T> the type of the input to the operation
 */
@FunctionalInterface
public interface CheckedConsumer<T> extends XCheckedConsumer<T, Exception> {

   /**
    * Noop consumer.
    *
    * @return noop consumer
    */
   static <T> CheckedConsumer<T> noop() {
      return __ -> { };
   }

   /**
    * Returns a checked consumer from an {@link XCheckedConsumer} or a checked anonymous lambda.
    *
    * @param consumer consumer to adapt
    *
    * @return checked consumer
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> CheckedConsumer<T> checkedConsumer(XCheckedConsumer<? super T, ? extends X> consumer) {
      return checkedConsumer((Consumer<T>) consumer);
   }

   /**
    * Returns a checked consumer from a {@link Consumer} or an unchecked anonymous lambda.
    *
    * @param consumer consumer to adapt
    *
    * @return checked consumer
    *
    * @implSpec Unlike {@link XCheckedConsumer#checkedConsumer(Consumer)}, specified consumer instance
    *       of {@link XCheckedConsumer} are not unchecked, because it's not necessary to infer exception type
    *       which is always {@link Exception}.
    */
   @SuppressWarnings("unchecked")
   static <T> CheckedConsumer<T> checkedConsumer(Consumer<? super T> consumer) {
      requireNonNull(consumer);
      if (consumer instanceof CheckedConsumer) {
         return (CheckedConsumer<T>) consumer;
      } else if (consumer instanceof XCheckedConsumer) {
         return ((XCheckedConsumer<T, ?>) consumer)::acceptChecked;
      } else {
         return consumer::accept;
      }
   }

   /**
    * Returns a checked consumer from a checked runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked consumer
    */
   static <T, X extends Exception> CheckedConsumer<T> checkedConsumer(XCheckedRunnable<? extends X> runnable) {
      requireNonNull(runnable);
      return __ -> runnable.runChecked();
   }

   /**
    * Returns a checked consumer from a standard runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked consumer
    *
    * @implSpec Unlike {@link XCheckedConsumer#checkedConsumer(Runnable)}, specified runnable instance
    *       of {@link XCheckedRunnable} are not unchecked, because it's not necessary to infer exception type
    *       which is always {@link Exception}.
    */
   static <T> CheckedConsumer<T> checkedConsumer(Runnable runnable) {
      requireNonNull(runnable);
      if (runnable instanceof CheckedRunnable) {
         return __ -> ((CheckedRunnable) runnable).runChecked();
      } else if (runnable instanceof XCheckedRunnable) {
         return __ -> ((XCheckedRunnable<?>) runnable).runChecked();
      } else {
         return __ -> runnable.run();
      }
   }

   /**
    * Returns a checked consumer from a checked function. Function result is discarded.
    *
    * @param function function to adapt
    *
    * @return checked consumer
    */
   static <T, R, X extends Exception> CheckedConsumer<T> checkedConsumer(XCheckedFunction<? super T, ? extends R, ? extends X> function) {
      requireNonNull(function);
      return function::applyChecked;
   }

   @Override
   @SuppressWarnings("unchecked")
   default CheckedConsumer<T> autoClose() {
      return t -> {
         if (t instanceof AutoCloseable) {
            try (AutoCloseable autoCloseable = (AutoCloseable) t) {
               acceptChecked((T) autoCloseable);
            }
         } else {
            acceptChecked(t);
         }
      };
   }

   @Override
   default CheckedConsumer<T> autoCloseOnThrow() {
      return t -> {
         try {
            acceptChecked(t);
         } catch (Throwable throwable) {
            if (t instanceof AutoCloseable) {
               try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                  throw throwable;
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   default CheckedConsumer<T> autoCloseOnThrow(Class<? extends Exception> exceptionClass) {
      requireNonNull(exceptionClass);
      return t -> {
         try {
            acceptChecked(t);
         } catch (Throwable throwable) {
            if (exceptionClass.isAssignableFrom(throwable.getClass())) {
               if (t instanceof AutoCloseable) {
                  try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                     throw throwable;
                  }
               }
               throw throwable;
            }
         }
      };
   }

   @Override
   @SuppressWarnings("ThrowFromFinallyBlock")
   default CheckedConsumer<T> tryFinally(List<? extends XCheckedRunnable<? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         Throwable throwable = null;
         try {
            acceptChecked(v);
         } catch (Throwable t) {
            throwable = t;
            throw t;
         } finally {
            for (XCheckedRunnable<? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.runChecked();
               } catch (Throwable finalizerThrowable) {
                  if (throwable != null) {
                     throwable.addSuppressed(finalizerThrowable);
                  } else {
                     throwable = finalizerThrowable;
                  }
               }
            }
            if (throwable != null) {
               throw sneakyThrow(throwable);
            }
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedConsumer<T> tryFinally(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryFinally(list(finalizers));
   }

   @Override
   default CheckedConsumer<T> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         try {
            acceptChecked(v);
         } catch (Throwable throwable) {
            for (XCheckedConsumer<Throwable, ? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.acceptChecked(throwable);
               } catch (Throwable ft) {
                  throwable.addSuppressed(ft);
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedConsumer<T> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedConsumer<T> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default <SX extends Exception> CheckedConsumer<T> tryCatch(Class<? extends SX> exceptionClass,
                                                              List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return v -> {
         try {
            acceptChecked(v);
         } catch (Exception exception) {
            if (exceptionClass.isAssignableFrom(exception.getClass()))
               for (XCheckedConsumer<? super SX, ? extends Exception> finalizer : finalizers) {
                  try {
                     finalizer.acceptChecked(exceptionClass.cast(exception));
                  } catch (Throwable ft) {
                     exception.addSuppressed(ft);
                  }
               }
            throw exception;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends Exception> CheckedConsumer<T> tryCatch(Class<? extends SX> exceptionClass,
                                                              XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedConsumer<T> tryCatch(Class<? extends Exception> exceptionClass,
                                       XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default CheckedConsumer<T> andThen(Consumer<? super T> after) {
      requireNonNull(after);
      return (T t) -> {
         acceptChecked(t);
         after.accept(t);
      };
   }

   @Override
   default CheckedConsumer<T> andThen(XCheckedConsumer<? super T, ? extends Exception> after) {
      requireNonNull(after);
      return (T t) -> {
         acceptChecked(t);
         after.acceptChecked(t);
      };
   }

}
