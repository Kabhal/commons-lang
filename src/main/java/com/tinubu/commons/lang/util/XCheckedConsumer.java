/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Objects.requireNonNull;

import java.io.UncheckedIOException;
import java.util.List;
import java.util.function.Consumer;

/**
 * Represents an operation that accepts a single input argument and returns no
 * result. Unlike most other functional interfaces, {@code CheckedConsumer} is expected
 * to operate via side effects.
 * This is a functional interface whose functional method is {@link #acceptChecked(Object)}.
 * {@link #acceptChecked(Object)} can throw {@link X genericized} exceptions.
 * If this class is used as {@link Consumer}, then unchecked {@link #accept(Object)} will be called.
 *
 * @param <T> the type of the input to the operation
 * @param <X> the type of the checked exception possibly thrown by the consumer
 */
@FunctionalInterface
public interface XCheckedConsumer<T, X extends Exception> extends Consumer<T> {

   /**
    * Noop consumer.
    *
    * @return noop consumer
    */
   static <T, X extends Exception> XCheckedConsumer<T, X> noop() {
      return __ -> { };
   }

   /**
    * Performs this unchecked operation on the given argument.
    *
    * @param t the input argument
    */
   @Override
   default void accept(T t) {
      this.unchecked().accept(t);
   }

   /**
    * Performs this checked  operation on the given argument.
    * This operation can throw {@link X} exceptions.
    *
    * @param t the input argument
    *
    * @throws X if an error occurs
    */
   void acceptChecked(T t) throws X;

   /**
    * Returns a checked consumer from an {@link XCheckedConsumer} or a checked anonymous lambda.
    *
    * @param consumer consumer to adapt
    *
    * @return checked consumer
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(XCheckedConsumer<? super T, ? extends X> consumer) {
      return (XCheckedConsumer<T, X>) consumer;
   }

   /**
    * Returns a checked consumer from an {@link XCheckedConsumer} or a checked anonymous lambda.
    *
    * @param consumer consumer to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked consumer
    */
   @SuppressWarnings("unchecked")
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(XCheckedConsumer<? super T, ? extends X> consumer,
                                                                          Class<X> thrownExceptionClass) {
      return (XCheckedConsumer<T, X>) consumer;
   }

   /**
    * Returns a checked consumer from a {@link Consumer} or an unchecked anonymous lambda.
    *
    * @param consumer consumer to adapt
    *
    * @return checked consumer
    *
    * @implSpec Even if specified consumer is an instance of {@link CheckedConsumer} or
    *       {@link XCheckedConsumer}, implementation will uncheck it like any {@link Consumer}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(Consumer<? super T> consumer) {
      requireNonNull(consumer);
      return consumer::accept;
   }

   /**
    * Returns a checked consumer from a {@link Consumer} or an unchecked anonymous lambda.
    *
    * @param consumer consumer to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked consumer
    *
    * @implSpec Even if specified consumer is an instance of {@link CheckedConsumer} or
    *       {@link XCheckedConsumer}, implementation will uncheck it like any {@link Consumer}, because
    *       exception type can't be inferred and there would have a risk to hide a checked exception to
    *       compiler.
    */
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(Consumer<? super T> consumer,
                                                                          Class<X> thrownExceptionClass) {
      requireNonNull(consumer);
      return consumer::accept;
   }

   /**
    * Returns a checked consumer from a checked runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked consumer
    */
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(XCheckedRunnable<? extends X> runnable) {
      requireNonNull(runnable);
      return __ -> runnable.runChecked();
   }

   /**
    * Returns a checked consumer from a checked runnable.
    *
    * @param runnable runnable to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked consumer
    */
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(XCheckedRunnable<? extends X> runnable,
                                                                          Class<X> thrownExceptionClass) {
      requireNonNull(runnable);
      return __ -> runnable.runChecked();
   }

   /**
    * Returns a checked consumer from a standard runnable.
    *
    * @param runnable runnable to adapt
    *
    * @return checked consumer
    */
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(Runnable runnable) {
      requireNonNull(runnable);
      return __ -> runnable.run();
   }

   /**
    * Returns a checked consumer from a standard runnable.
    *
    * @param runnable runnable to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked consumer
    */
   static <T, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(Runnable runnable,
                                                                          Class<X> thrownExceptionClass) {
      requireNonNull(runnable);
      return __ -> runnable.run();
   }

   /**
    * Returns a checked consumer from a checked function. Function result is discarded.
    *
    * @param function function to adapt
    *
    * @return checked consumer
    */
   static <T, R, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(XCheckedFunction<? super T, ? extends R, ? extends X> function) {
      requireNonNull(function);
      return function::applyChecked;
   }

   /**
    * Returns a checked consumer from a checked function. Function result is discarded.
    *
    * @param function function to adapt
    * @param thrownExceptionClass explicitly set thrown exception type to force inferred type
    *
    * @return checked consumer
    */
   static <T, R, X extends Exception> XCheckedConsumer<T, X> checkedConsumer(XCheckedFunction<? super T, ? extends R, ? extends X> function,
                                                                             Class<X> thrownExceptionClass) {
      requireNonNull(function);
      return function::applyChecked;
   }

   /**
    * Returns an unchecked consumer from this checked consumer, using either :
    * <ul>
    * <li>{@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} : exception is converted from {@link Exception} to {@link RuntimeException} using best-fitting exceptions (e.g.: {@link UncheckedIOException}, ...)</li>
    * <li>{@link ExceptionUtils#sneakyThrow(Throwable) sneaky-throw} : checked exception is rethrown as-is but Java does not force to catch checked exceptions anymore using a hack</li>
    * </ul>
    *
    * @param sneakyThrow whether to use sneaky-throw unchecking
    *
    * @return unchecked consumer
    */
   default Consumer<T> unchecked(boolean sneakyThrow) {
      return t -> {
         try {
            this.acceptChecked(t);
         } catch (Exception e) {
            if (sneakyThrow) {
               throw ExceptionUtils.sneakyThrow(e);
            } else {
               throw ExceptionUtils.runtimeThrow(e);
            }
         }
      };
   }

   /**
    * Returns an unchecked consumer from this checked consumer, using
    * default {@link ExceptionUtils#runtimeThrow(Throwable) runtime-throw} unchecking.
    *
    * @return unchecked consumer
    */
   default Consumer<T> unchecked() {
      return unchecked(false);
   }

   /**
    * Returns a wrapped auto-closing checked consumer from this checked consumer.
    * Consumer input will be auto-closed after being processed by this consumer.
    * If {@link T} does not implement {@link AutoCloseable}, no {@link AutoCloseable#close()} will be called.
    *
    * @return auto-closing checked consumer
    */
   default XCheckedConsumer<T, Exception> autoClose() {
      return CheckedConsumer.checkedConsumer(this).autoClose();
   }

   /**
    * Returns a wrapped auto-closing-on-throw checked consumer from this checked consumer.
    * Consumer input will be auto-closed after being processed by this consumer, only if processing fails.
    * If {@link T} does not implement {@link AutoCloseable}, no {@link AutoCloseable#close()} will be called.
    *
    * @return auto-closing-on-throw checked consumer
    */
   default XCheckedConsumer<T, Exception> autoCloseOnThrow() {
      return t -> {
         try {
            acceptChecked(t);
         } catch (Throwable throwable) {
            if (t instanceof AutoCloseable) {
               try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                  throw throwable;
               }
            }
            throw throwable;
         }
      };
   }

   /**
    * Returns a wrapped auto-closing-on-throw checked consumer from this checked consumer.
    * Consumer input will be auto-closed after being processed by this consumer, only if processing fails with
    * an exception extending specified exception class.
    * If {@link T} does not implement {@link AutoCloseable}, no {@link AutoCloseable#close()} will be called.
    *
    * @param exceptionClass auto-close only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    *
    * @return auto-closing-on-throw checked consumer
    */
   default XCheckedConsumer<T, Exception> autoCloseOnThrow(Class<? extends X> exceptionClass) {
      requireNonNull(exceptionClass);
      return t -> {
         try {
            acceptChecked(t);
         } catch (Throwable throwable) {
            if (exceptionClass.isAssignableFrom(throwable.getClass())) {
               if (t instanceof AutoCloseable) {
                  try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                     throw throwable;
                  }
               }
               throw throwable;
            }
         }
      };
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, then ensure all specified
    * finalizers are always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is the first thrown exception, either by this
    * consumer, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked consumer with finalizers
    *
    * @implNote The second catch is a {@link Throwable}, so that some {@link Error} would be ignored as
    *       suppressed, because we consider more important to execute all finalizers than re-throw a nth
    *       {@link Error}, in the special case the first exception that will be finally rethrown is an
    *       {@link Exception}. The final {@link ExceptionUtils#sneakyThrow(Throwable)} is required in the
    *       case the first exception is thrown by a finalizer, not the consumer, and is an {@link Error}, in
    *       this case it's not a problem to sneak it as you're not supposed to catch it.
    */
   @SuppressWarnings("ThrowFromFinallyBlock")
   default XCheckedConsumer<T, X> tryFinally(List<? extends XCheckedRunnable<? extends X>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         Throwable throwable = null;
         try {
            acceptChecked(v);
         } catch (Throwable t) {
            throwable = t;
            throw t;
         } finally {
            for (XCheckedRunnable<? extends X> finalizer : finalizers) {
               try {
                  finalizer.runChecked();
               } catch (Throwable finalizerThrowable) {
                  if (throwable != null) {
                     throwable.addSuppressed(finalizerThrowable);
                  } else {
                     throwable = finalizerThrowable;
                  }
               }
            }
            if (throwable != null) {
               throw sneakyThrow(throwable);
            }
         }
      };
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, then ensure all specified
    * finalizers are always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is the first thrown exception, either by this
    * consumer, or one of the specified finalizers.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked consumer with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedConsumer<T, X> tryFinally(XCheckedRunnable<? extends X>... finalizers) {
      return tryFinally(list(finalizers));
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, and, only if a
    * {@link Throwable} is thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is this consumer thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked consumer with finalizers
    */
   default XCheckedConsumer<T, X> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         try {
            acceptChecked(v);
         } catch (Throwable throwable) {
            for (XCheckedConsumer<Throwable, ? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.acceptChecked(throwable);
               } catch (Throwable ft) {
                  throwable.addSuppressed(ft);
               }
            }
            throw throwable;
         }
      };
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, and, only if a
    * {@link Throwable} is thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is this consumer thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked consumer with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedConsumer<T, X> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, and, only if a
    * {@link Throwable} is thrown, ensure all specified finalizers are always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is this consumer thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    *
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked consumer with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedConsumer<T, X> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, and, only if an
    * {@link Exception} instance of {@code exceptionClass} is thrown, ensure all specified finalizers are
    * always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is this consumer thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked consumer with finalizers
    */
   @SuppressWarnings("unchecked")
   default <SX extends X> XCheckedConsumer<T, X> tryCatch(Class<? extends SX> exceptionClass,
                                                          List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return v -> {
         try {
            acceptChecked(v);
         } catch (Exception exception) {
            if (exceptionClass.isAssignableFrom(exception.getClass()))
               for (XCheckedConsumer<? super SX, ? extends Exception> finalizer : finalizers) {
                  try {
                     finalizer.acceptChecked(exceptionClass.cast(exception));
                  } catch (Throwable ft) {
                     exception.addSuppressed(ft);
                  }
               }
            throw (X) exception;
         }
      };
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, and, only if an
    * {@link Exception}instance of {@code exceptionClass} is thrown, ensure all specified finalizers are
    * always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is this consumer thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    * @param <SX> exception class type as subtype of {@link X}
    *
    * @return wrapped checked consumer with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends X> XCheckedConsumer<T, X> tryCatch(Class<? extends SX> exceptionClass,
                                                          XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   /**
    * Returns a wrapped checked consumer calling {@link #acceptChecked(Object)}, and, only if an
    * {@link Exception} instance of {@code exceptionClass} is thrown, ensure all specified finalizers are
    * always called in specified order.
    * <p>
    * Wrapped checked consumer thrown exception is this consumer thrown exception.
    * All finalizers will be run even in case of exception for one of them. Subsequent exceptions are added
    * as suppressed in the first actually thrown exception.
    * <p>
    * Caught exception is passed as input parameter of finalizers.
    *
    * @param exceptionClass call finalizers only if thrown exception is instance of specified exception
    *       class. This has no impact on thrown exception.
    * @param finalizers optional finalizers to all apply once operation is executed
    *
    * @return wrapped checked consumer with finalizers
    */
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default XCheckedConsumer<T, X> tryCatch(Class<? extends X> exceptionClass,
                                           XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default XCheckedConsumer<T, X> andThen(Consumer<? super T> after) {
      requireNonNull(after);
      return (T t) -> {
         acceptChecked(t);
         after.accept(t);
      };
   }

   default XCheckedConsumer<T, X> andThen(XCheckedConsumer<? super T, ? extends X> after) {
      requireNonNull(after);
      return (T t) -> {
         acceptChecked(t);
         after.acceptChecked(t);
      };
   }

}
