/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

/**
 * A {@link RuntimeException} resulting from a checked {@link Exception} to unchecked
 * {@link RuntimeException} conversion.
 * Original checked exception is always set as {@link #getCause() cause} in this exception, and must be an
 * {@link Exception} and must not be a {@link RuntimeException}, nor an {@link Error}.
 *
 * @see ExceptionUtils#runtimeThrow(Throwable)
 */
public class UncheckedException extends RuntimeException {

   /**
    * @param cause original checked exception, instance of {@link Exception}, not
    *       instance of {@link RuntimeException}
    */
   public UncheckedException(Throwable cause) {
      super(cause.getMessage(), validateCheckedException(cause));
   }

   /**
    * Returns original checked exception.
    *
    * @return original checked exception
    */
   public Exception checkedException() {
      return (Exception) getCause();
   }

   private static Throwable validateCheckedException(Throwable cause) {
      if (cause instanceof RuntimeException || cause instanceof Error) {
         throw new IllegalArgumentException(String.format(
               "Specified cause '%s' exception must be a checked Exception",
               cause.getClass().getName()));
      }

      return cause;
   }
}
