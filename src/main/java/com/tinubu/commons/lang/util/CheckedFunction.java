/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.sneakyThrow;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.function.Function;

/**
 * Represents a function that accepts one argument and produces a result.
 * This is a functional interface whose functional method is {@link #applyChecked(Object)}.
 * {@link #applyChecked(Object)} can throw checked exceptions.
 * If this class is used as {@link Function}, then unchecked {@link #apply(Object)} will be called.
 *
 * @param <T> the type of the input to the function
 * @param <R> the type of the result of the function
 */
@FunctionalInterface
public interface CheckedFunction<T, R> extends XCheckedFunction<T, R, Exception> {

   /**
    * Returns a function that always returns its input argument.
    *
    * @param <T> the type of the input and output objects to the function
    *
    * @return a function that always returns its input argument
    */
   static <T> CheckedFunction<T, T> identity() {
      return t -> t;
   }

   /**
    * Returns a checked function from an {@link XCheckedFunction} or a checked anonymous lambda.
    *
    * @param function function to adapt
    *
    * @return checked function
    */
   @SuppressWarnings("unchecked")
   static <T, R, X extends Exception> CheckedFunction<T, R> checkedFunction(XCheckedFunction<? super T, ? extends R, ? extends X> function) {
      return checkedFunction((Function<T, R>) function);
   }

   /**
    * Returns a checked function from a {@link Function} or an unchecked anonymous lambda.
    *
    * @param function function to adapt
    *
    * @return checked function
    *
    * @implSpec Unlike {@link XCheckedFunction#checkedFunction(Function)}, specified function instance
    *       of {@link XCheckedFunction} are not unchecked, because it's not necessary to infer exception type
    *       which is always {@link Exception}.
    */
   @SuppressWarnings("unchecked")
   static <T, R> CheckedFunction<T, R> checkedFunction(Function<? super T, ? extends R> function) {
      requireNonNull(function);
      if (function instanceof CheckedFunction) {
         return (CheckedFunction<T, R>) function;
      } else if (function instanceof XCheckedFunction) {
         return ((XCheckedFunction<T, R, ?>) function)::applyChecked;
      } else {
         return function::apply;
      }
   }

   @Override
   @SuppressWarnings("unchecked")
   default CheckedFunction<T, R> autoClose() {
      return t -> {
         if (t instanceof AutoCloseable) {
            try (AutoCloseable autoCloseable = (AutoCloseable) t) {
               return applyChecked((T) autoCloseable);
            }
         } else {
            return applyChecked(t);
         }
      };
   }

   @Override
   default CheckedFunction<T, R> autoCloseOnThrow() {
      return t -> {
         try {
            return applyChecked(t);
         } catch (Throwable throwable) {
            if (t instanceof AutoCloseable) {
               try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                  throw throwable;
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   default CheckedFunction<T, R> autoCloseOnThrow(Class<? extends Exception> exceptionClass) {
      requireNonNull(exceptionClass);
      return t -> {
         try {
            return applyChecked(t);
         } catch (Throwable throwable) {
            if (exceptionClass.isAssignableFrom(throwable.getClass())) {
               if (t instanceof AutoCloseable) {
                  try (AutoCloseable ignoredAutoCloseable = (AutoCloseable) t) {
                     throw throwable;
                  }
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   @SuppressWarnings("ThrowFromFinallyBlock")
   default CheckedFunction<T, R> tryFinally(List<? extends XCheckedRunnable<? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         Throwable throwable = null;
         try {
            return applyChecked(v);
         } catch (Throwable t) {
            throwable = t;
            throw t;
         } finally {
            for (XCheckedRunnable<? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.runChecked();
               } catch (Throwable finalizerThrowable) {
                  if (throwable != null) {
                     throwable.addSuppressed(finalizerThrowable);
                  } else {
                     throwable = finalizerThrowable;
                  }
               }
            }
            if (throwable != null) {
               throw sneakyThrow(throwable);
            }
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedFunction<T, R> tryFinally(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryFinally(list(finalizers));
   }

   @Override
   default CheckedFunction<T, R> tryCatch(List<? extends XCheckedConsumer<Throwable, ? extends Exception>> finalizers) {
      requireNonNull(finalizers);
      return v -> {
         try {
            return applyChecked(v);
         } catch (Throwable throwable) {
            for (XCheckedConsumer<Throwable, ? extends Exception> finalizer : finalizers) {
               try {
                  finalizer.acceptChecked(throwable);
               } catch (Throwable ft) {
                  throwable.addSuppressed(ft);
               }
            }
            throw throwable;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedFunction<T, R> tryCatch(XCheckedConsumer<Throwable, ? extends Exception>... finalizers) {
      return tryCatch(list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default CheckedFunction<T, R> tryCatch(XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default <SX extends Exception> CheckedFunction<T, R> tryCatch(Class<? extends SX> exceptionClass,
                                                                 List<? extends XCheckedConsumer<? super SX, ? extends Exception>> finalizers) {
      requireNonNull(exceptionClass);
      requireNonNull(finalizers);
      return v -> {
         try {
            return applyChecked(v);
         } catch (Exception exception) {
            if (exceptionClass.isAssignableFrom(exception.getClass()))
               for (XCheckedConsumer<? super SX, ? extends Exception> finalizer : finalizers) {
                  try {
                     finalizer.acceptChecked(exceptionClass.cast(exception));
                  } catch (Throwable ft) {
                     exception.addSuppressed(ft);
                  }
               }
            throw exception;
         }
      };
   }

   @Override
   @SuppressWarnings("unchecked" /* @SafeVarargs */)
   default <SX extends Exception> CheckedFunction<T, R> tryCatch(Class<? extends SX> exceptionClass,
                                                                 XCheckedConsumer<? super SX, ? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(finalizers));
   }

   @Override
   @SuppressWarnings("unchecked")
   default CheckedFunction<T, R> tryCatch(Class<? extends Exception> exceptionClass,
                                          XCheckedRunnable<? extends Exception>... finalizers) {
      return tryCatch(exceptionClass, list(stream(finalizers).map(XCheckedConsumer::checkedConsumer)));
   }

   @Override
   default <V> CheckedFunction<V, R> compose(Function<? super V, ? extends T> before) {
      requireNonNull(before);
      return (V v) -> applyChecked(before.apply(v));
   }

   @Override
   default <V> CheckedFunction<V, R> compose(XCheckedFunction<? super V, ? extends T, ? extends Exception> before) {
      requireNonNull(before);
      return (V v) -> applyChecked(before.applyChecked(v));
   }

   @Override
   default <V> CheckedFunction<T, V> andThen(Function<? super R, ? extends V> after) {
      requireNonNull(after);
      return (T t) -> after.apply(applyChecked(t));
   }

   @Override
   default <V> CheckedFunction<T, V> andThen(XCheckedFunction<? super R, ? extends V, ? extends Exception> after) {
      requireNonNull(after);
      return (T t) -> after.applyChecked(applyChecked(t));
   }

}
