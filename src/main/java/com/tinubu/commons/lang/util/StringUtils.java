/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.util;

public final class StringUtils {

   private StringUtils() {
   }

   /**
    * Returns the specified char sequence, or the default one if {@link #isBlank(CharSequence)}>.
    *
    * @param <T> char sequence type
    * @param cs char sequence to check, or {@code null}
    * @param defaultCs default char sequence
    *
    * @return char sequence
    */
   public static <T extends CharSequence> T defaultIfBlank(final T cs, final T defaultCs) {
      return isBlank(cs) ? defaultCs : cs;
   }

   /**
    * Returns the specified char sequence, or the default one if {@link #isEmpty(CharSequence)}>.
    *
    * @param <T> char sequence type
    * @param cs char sequence to check, or {@code null}
    * @param defaultCs default char sequence
    *
    * @return char sequence
    */
   public static <T extends CharSequence> T defaultIfEmpty(final T cs, final T defaultCs) {
      return isEmpty(cs) ? defaultCs : cs;
   }

   /**
    * Checks if a char sequence is empty ({@code ""}) or {@code null}.
    *
    * @param cs char sequence to check, or {@code null}
    *
    * @return {@code true} if char sequence is empty
    */
   public static boolean isEmpty(CharSequence cs) {
      return cs == null || cs.length() == 0;
   }

   /**
    * Checks if a char sequence is blank or {@code null}.
    * Character is considered blank if {@link Character#isWhitespace(char)} matches.
    *
    * @param cs char sequence to check, or {@code null}
    *
    * @return {@code true} if char sequence is blank
    */
   public static boolean isBlank(CharSequence cs) {
      int strLen = cs == null ? 0 : cs.length();

      if (strLen == 0) {
         return true;
      }
      for (int i = 0; i < strLen; i++) {
         if (!Character.isWhitespace(cs.charAt(i))) {
            return false;
         }
      }
      return true;
   }

   public static String removeEnd(String string, String suffix) {
      if (isEmpty(string) || isEmpty(suffix)) {
         return string;
      }
      if (string.endsWith(suffix)) {
         return string.substring(0, string.length() - suffix.length());
      }
      return string;
   }

   public static String removeEndIgnoreCase(String string, String suffix) {
      if (isEmpty(string) || isEmpty(suffix)) {
         return string;
      }
      if (string.toLowerCase().endsWith(suffix.toLowerCase())) {
         return string.substring(0, string.length() - suffix.length());
      }
      return string;
   }

   /**
    * String equality.
    * Returns {@code false} if both string are {@code null}.
    *
    * @param s1 string
    * @param s2 string
    *
    * @return {@code true} if string are equals.
    */
   public static boolean equals(String s1, String s2) {
      if (s1 == s2) {
         return true;
      }
      if (s1 == null || s2 == null) {
         return false;
      }

      return s1.equals(s2);
   }

   /**
    * String equality ignoring case.
    * Returns {@code false} if both string are {@code null}.
    *
    * @param s1 string
    * @param s2 string
    *
    * @return {@code true} if string are equals ignoring case.
    */
   public static boolean equalsIgnoreCase(String s1, String s2) {
      if (s1 == s2) {
         return true;
      }
      if (s1 == null || s2 == null) {
         return false;
      }

      return s1.equalsIgnoreCase(s2);
   }

   /**
    * Capitalizes first Unicode code point of specified string, using {@link Character#toTitleCase(int)}.
    * Other characters are left unchanged.
    * In theory, returned string can have a different length (in unicode code units) depending on
    * capitalized Unicode code point and string implementation.
    *
    * @param s string to capitalize
    *
    * @return capitalized string
    */
   public static String capitalize(String s) {
      int strLen = s.length();

      if (strLen == 0) {
         return s;
      }

      int cp = s.codePointAt(0);
      int newCp = Character.toTitleCase(cp);

      if (cp == newCp) {
         return s;
      }

      var cpLength = Character.charCount(cp);
      var newChars = Character.toChars(newCp);

      char[] capitalized = new char[strLen - cpLength + newChars.length];

      System.arraycopy(newChars, 0, capitalized, 0, newChars.length);
      s.getChars(cpLength, s.length(), capitalized, newChars.length);

      return new String(capitalized);
   }

   /**
    * Un-capitalizes first Unicode code point of specified string, using {@link Character#toLowerCase(int)}.
    * Other characters are left unchanged.
    * In theory, returned string can have a different length (in unicode code units) depending on
    * capitalized Unicode code point and string implementation.
    *
    * @param s string to capitalize
    *
    * @return capitalized string
    */
   public static String uncapitalize(String s) {
      int strLen = s.length();

      if (strLen == 0) {
         return s;
      }

      int cp = s.codePointAt(0);
      int newCp = Character.toLowerCase(cp);

      if (cp == newCp) {
         return s;
      }

      var cpLength = Character.charCount(cp);
      var newChars = Character.toChars(newCp);

      char[] uncapitalized = new char[strLen - cpLength + newChars.length];

      System.arraycopy(newChars, 0, uncapitalized, 0, newChars.length);
      s.getChars(cpLength, s.length(), uncapitalized, newChars.length);

      return new String(uncapitalized);
   }

}
