/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.log;

import com.tinubu.commons.lang.log.ExtendedLogger.Level;
import com.tinubu.commons.lang.log.ExtendedLogger.LogMessage;

public interface ExtendedLoggerCallback {

   static ExtendedLoggerCallback noop() {
      return new ExtendedLoggerCallback() { };
   }

   /**
    * Callback called before log operation, so that callback is always called.
    *
    * @param level required log level
    * @param message log message
    *
    * @return {@code true} to continue to logger, or {@code false} to ignore logging operation, but not other
    *       callbacks
    */
   default boolean before(Level level, LogMessage message) {
      return true;
   }

   /**
    * Callback called after log level is checked, and before logger is called, so that callback is called only
    * if required log level is higher than current log level.
    *
    * @param level required log level
    * @param message log message
    *
    * @return {@code true} to continue to logger, or {@code false} to stop logging operation, but not other
    *       callbacks
    */
   default boolean beforeLog(Level level, LogMessage message) {
      return true;
   }

   /**
    * Callback called after logger is called, so that callback is called only if required log level is
    * higher than current log level.
    *
    * @param level required log level
    * @param message log message
    */
   default void afterLog(Level level, LogMessage message) { }

   /**
    * Callback called after log operation, so that callback is always called.
    *
    * @param level required log level
    * @param message log message
    */
   default void after(Level level, LogMessage message) { }

   /**
    * Returns a composed callback that first calls this callback operations, then the specified callback
    * operations.
    * This and after callbacks are always called in sequence, if no exception occur, but logging will be
    * granted only if both callbacks return {@code true}.
    *
    * @param after after callback
    *
    * @return composed callback
    */
   default ExtendedLoggerCallback andThen(ExtendedLoggerCallback after) {
      return new ExtendedLoggerCallback() {
         @Override
         public boolean before(Level level, LogMessage message) {
            var r1 = ExtendedLoggerCallback.super.before(level, message);
            var r2 = after.before(level, message);

            return r1 && r2;
         }

         @Override
         public boolean beforeLog(Level level, LogMessage message) {
            var r1 = ExtendedLoggerCallback.super.beforeLog(level, message);
            var r2 = after.beforeLog(level, message);

            return r1 && r2;
         }

         @Override
         public void afterLog(Level level, LogMessage message) {
            ExtendedLoggerCallback.super.afterLog(level, message);
            after.afterLog(level, message);
         }

         @Override
         public void after(Level level, LogMessage message) {
            ExtendedLoggerCallback.super.after(level, message);
            after.after(level, message);
         }
      };
   }
}
