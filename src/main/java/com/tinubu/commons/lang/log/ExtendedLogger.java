/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.log;

import static com.tinubu.commons.lang.log.ExtendedLogger.Level.DEBUG;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.ERROR;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.INFO;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.TRACE;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.WARN;
import static com.tinubu.commons.lang.log.ExtendedLogger.LogMessage.ArgumentSignature.ARG0;
import static com.tinubu.commons.lang.log.ExtendedLogger.LogMessage.ArgumentSignature.ARG1;
import static com.tinubu.commons.lang.log.ExtendedLogger.LogMessage.ArgumentSignature.ARG1T;
import static com.tinubu.commons.lang.log.ExtendedLogger.LogMessage.ArgumentSignature.ARG2;
import static com.tinubu.commons.lang.log.ExtendedLogger.LogMessage.ArgumentSignature.ARGN;
import static com.tinubu.commons.lang.log.ExtendedLoggerCallback.noop;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.helpers.MessageFormatter;

/**
 * Extends delegated {@link Logger} with new log operations, supporting lazy evaluation of both message and
 * arguments.
 */
public class ExtendedLogger implements Logger {

   protected final Logger logger;
   protected final ExtendedLoggerCallback callback;

   protected ExtendedLogger(Logger logger, ExtendedLoggerCallback callback) {
      this.logger = notNull(logger, "logger");
      this.callback = callback;
   }

   protected ExtendedLogger(Logger logger) {
      this(logger, noop());
   }

   public static ExtendedLogger of(Logger logger, ExtendedLoggerCallback callback) {
      return new ExtendedLogger(logger, callback);
   }

   public static ExtendedLogger of(Logger logger) {
      return of(logger, noop());
   }

   public static ExtendedLogger of(Class<?> clazz, ExtendedLoggerCallback callback) {
      return new ExtendedLogger(LoggerFactory.getLogger(clazz), callback);
   }

   public static ExtendedLogger of(Class<?> clazz) {
      return of(clazz, noop());
   }

   public static ExtendedLogger of(String name) {
      return new ExtendedLogger(LoggerFactory.getLogger(name));
   }

   public enum Level {
      OFF, ERROR, WARN, INFO, DEBUG, TRACE;

      /**
       * Returns {@code true} if specified level is loggable considering this level is the underlying logger
       * actual level.
       * Always return {@code false} if current log level is {@link Level#OFF} or specified level is
       * {@link Level#OFF}, excepting if both are {@link Level#OFF}.
       */
      public boolean isEnabled(Level level) {
         if ((this == OFF || level == OFF) && !(this == OFF && level == OFF)) {
            return false;
         }
         return ordinal() >= level.ordinal();
      }

   }

   /**
    * Return the name of this {@link Logger} instance.
    *
    * @return name of this logger instance
    */
   @Override
   public String getName() {
      return logger.getName();
   }

   /**
    * Main log operation.
    *
    * @param level log level
    * @param message log message
    */
   public void log(Level level, LogMessage message) {
      notNull(level, "level");
      notNull(message, "message");

      switch (level) {
         case OFF:
            log(level, message, m -> logOff(null, m));
            break;
         case ERROR:
            log(level, message, m -> logError(null, m));
            break;
         case WARN:
            log(level, message, m -> logWarn(null, m));
            break;
         case INFO:
            log(level, message, m -> logInfo(null, m));
            break;
         case DEBUG:
            log(level, message, m -> logDebug(null, m));
            break;
         case TRACE:
            log(level, message, m -> logTrace(null, m));
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported %s log level", level));
      }

   }

   public void error(LogMessage message) {
      log(ERROR, message);
   }

   public void warn(LogMessage message) {
      log(WARN, message);
   }

   public void info(LogMessage message) {
      log(INFO, message);
   }

   public void debug(LogMessage message) {
      log(DEBUG, message);
   }

   public void trace(LogMessage message) {
      log(TRACE, message);
   }

   /**
    * Main log operation with marker.
    *
    * @param marker marker
    * @param level log level
    * @param message log message
    */
   public void log(Level level, Marker marker, LogMessage message) {
      notNull(level, "level");
      notNull(marker, "marker");
      notNull(message, "message");

      switch (level) {
         case OFF:
            log(level, message, m -> logOff(marker, m));
            break;
         case ERROR:
            log(level, message, m -> logError(marker, m));
            break;
         case WARN:
            log(level, message, m -> logWarn(marker, m));
            break;
         case INFO:
            log(level, message, m -> logInfo(marker, m));
            break;
         case DEBUG:
            log(level, message, m -> logDebug(marker, m));
            break;
         case TRACE:
            log(level, message, m -> logTrace(marker, m));
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported %s log level", level));
      }

   }

   public void error(Marker marker, LogMessage message) {
      log(ERROR, marker, message);
   }

   public void warn(Marker marker, LogMessage message) {
      log(WARN, marker, message);
   }

   public void info(Marker marker, LogMessage message) {
      log(INFO, marker, message);
   }

   public void debug(Marker marker, LogMessage message) {
      log(DEBUG, marker, message);
   }

   public void trace(Marker marker, LogMessage message) {
      log(TRACE, marker, message);
   }

   public void log(Level level, Callable<String> message) {
      notNull(level, "level");
      notNull(message, "message");

      log(level, LogMessage.of(message));
   }

   public void error(Callable<String> message) {
      log(ERROR, message);
   }

   public void warn(Callable<String> message) {
      log(WARN, message);
   }

   public void info(Callable<String> message) {
      log(INFO, message);
   }

   public void debug(Callable<String> message) {
      log(DEBUG, message);
   }

   public void trace(Callable<String> message) {
      log(TRACE, message);
   }

   public void log(Level level, String message) {
      notNull(level, "level");
      notNull(message, "message");

      log(level, LogMessage.of(message));
   }

   @Override
   public void error(String message) {
      log(ERROR, message);
   }

   @Override
   public void warn(String message) {
      log(WARN, message);
   }

   @Override
   public void info(String message) {
      log(INFO, message);
   }

   @Override
   public void debug(String message) {
      log(DEBUG, message);
   }

   @Override
   public void trace(String message) {
      log(TRACE, message);
   }

   public void log(Level level, Marker marker, String message) {
      notNull(level, "level");
      notNull(marker, "marker");
      notNull(message, "message");

      log(level, marker, LogMessage.of(message));
   }

   @Override
   public void error(Marker marker, String message) {
      log(ERROR, marker, message);
   }

   @Override
   public void warn(Marker marker, String message) {
      log(WARN, marker, message);
   }

   @Override
   public void info(Marker marker, String message) {
      log(INFO, marker, message);
   }

   @Override
   public void debug(Marker marker, String message) {
      log(DEBUG, marker, message);
   }

   @Override
   public void trace(Marker marker, String message) {
      log(TRACE, marker, message);
   }

   public void log(Level level, Callable<String> message, Callable<Object> arg) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(arg, "arg");

      log(level, LogMessage.of(message, arg));
   }

   public void error(Callable<String> message, Callable<Object> arg) {
      log(ERROR, message, arg);
   }

   public void warn(Callable<String> message, Callable<Object> arg) {
      log(WARN, message, arg);
   }

   public void info(Callable<String> message, Callable<Object> arg) {
      log(INFO, message, arg);
   }

   public void debug(Callable<String> message, Callable<Object> arg) {
      log(DEBUG, message, arg);
   }

   public void trace(Callable<String> message, Callable<Object> arg) {
      log(TRACE, message, arg);
   }

   public void log(Level level, String message, Callable<Object> arg) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(arg, "arg");

      log(level, LogMessage.of(message, arg));
   }

   public void error(String message, Callable<Object> arg) {
      log(ERROR, message, arg);
   }

   public void warn(String message, Callable<Object> arg) {
      log(WARN, message, arg);
   }

   public void info(String message, Callable<Object> arg) {
      log(INFO, message, arg);
   }

   public void debug(String message, Callable<Object> arg) {
      log(DEBUG, message, arg);
   }

   public void trace(String message, Callable<Object> arg) {
      log(TRACE, message, arg);
   }

   public void log(Level level, String message, Object arg) {
      notNull(level, "level");
      notNull(message, "message");

      log(level, LogMessage.of(message, arg));
   }

   @Override
   public void error(String message, Object arg) {
      log(ERROR, message, arg);
   }

   @Override
   public void warn(String message, Object arg) {
      log(WARN, message, arg);
   }

   @Override
   public void info(String message, Object arg) {
      log(INFO, message, arg);
   }

   @Override
   public void debug(String message, Object arg) {
      log(DEBUG, message, arg);
   }

   @Override
   public void trace(String message, Object arg) {
      log(TRACE, message, arg);
   }

   public void log(Level level, Marker marker, String message, Object arg) {
      notNull(level, "level");
      notNull(marker, "marker");
      notNull(message, "message");

      log(level, marker, LogMessage.of(message, arg));
   }

   @Override
   public void error(Marker marker, String message, Object arg) {
      log(ERROR, marker, message, arg);
   }

   @Override
   public void warn(Marker marker, String message, Object arg) {
      log(WARN, marker, message, arg);
   }

   @Override
   public void info(Marker marker, String message, Object arg) {
      log(INFO, marker, message, arg);
   }

   @Override
   public void debug(Marker marker, String message, Object arg) {
      log(DEBUG, marker, message, arg);
   }

   @Override
   public void trace(Marker marker, String message, Object arg) {
      log(TRACE, marker, message, arg);
   }

   public void log(Level level, Callable<String> message, Callable<Object> arg1, Callable<Object> arg2) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(arg1, "arg1");
      notNull(arg2, "arg2");

      log(level, LogMessage.of(message, arg1, arg2));
   }

   public void error(Callable<String> message, Callable<Object> arg1, Callable<Object> arg2) {
      log(ERROR, message, arg1, arg2);
   }

   public void warn(Callable<String> message, Callable<Object> arg1, Callable<Object> arg2) {
      log(WARN, message, arg1, arg2);
   }

   public void info(Callable<String> message, Callable<Object> arg1, Callable<Object> arg2) {
      log(INFO, message, arg1, arg2);
   }

   public void debug(Callable<String> message, Callable<Object> arg1, Callable<Object> arg2) {
      log(DEBUG, message, arg1, arg2);
   }

   public void trace(Callable<String> message, Callable<Object> arg1, Callable<Object> arg2) {
      log(TRACE, message, arg1, arg2);
   }

   public void log(Level level, String message, Callable<Object> arg1, Callable<Object> arg2) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(arg1, "arg1");
      notNull(arg2, "arg2");

      log(level, LogMessage.of(message, arg1, arg2));
   }

   public void error(String message, Callable<Object> arg1, Callable<Object> arg2) {
      log(ERROR, message, arg1, arg2);
   }

   public void warn(String message, Callable<Object> arg1, Callable<Object> arg2) {
      log(WARN, message, arg1, arg2);
   }

   public void info(String message, Callable<Object> arg1, Callable<Object> arg2) {
      log(INFO, message, arg1, arg2);
   }

   public void debug(String message, Callable<Object> arg1, Callable<Object> arg2) {
      log(DEBUG, message, arg1, arg2);
   }

   public void trace(String message, Callable<Object> arg1, Callable<Object> arg2) {
      log(TRACE, message, arg1, arg2);
   }

   public void log(Level level, String message, Object arg1, Object arg2) {
      notNull(level, "level");
      notNull(message, "message");

      log(level, LogMessage.of(message, arg1, arg2));
   }

   @Override
   public void error(String message, Object arg1, Object arg2) {
      log(ERROR, message, arg1, arg2);
   }

   @Override
   public void warn(String message, Object arg1, Object arg2) {
      log(WARN, message, arg1, arg2);
   }

   @Override
   public void info(String message, Object arg1, Object arg2) {
      log(INFO, message, arg1, arg2);
   }

   @Override
   public void debug(String message, Object arg1, Object arg2) {
      log(DEBUG, message, arg1, arg2);
   }

   @Override
   public void trace(String message, Object arg1, Object arg2) {
      log(TRACE, message, arg1, arg2);
   }

   public void log(Level level, Marker marker, String message, Object arg1, Object arg2) {
      notNull(level, "level");
      notNull(marker, "marker");
      notNull(message, "message");

      log(level, marker, LogMessage.of(message, arg1, arg2));
   }

   @Override
   public void error(Marker marker, String message, Object arg1, Object arg2) {
      log(ERROR, marker, message, arg1, arg2);
   }

   @Override
   public void warn(Marker marker, String message, Object arg1, Object arg2) {
      log(WARN, marker, message, arg1, arg2);
   }

   @Override
   public void info(Marker marker, String message, Object arg1, Object arg2) {
      log(INFO, marker, message, arg1, arg2);
   }

   @Override
   public void debug(Marker marker, String message, Object arg1, Object arg2) {
      log(DEBUG, marker, message, arg1, arg2);
   }

   @Override
   public void trace(Marker marker, String message, Object arg1, Object arg2) {
      log(TRACE, marker, message, arg1, arg2);
   }

   @SafeVarargs
   public final void log(Level level, Callable<String> message, Callable<Object>... args) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(args, "args");

      log(level, LogMessage.of(message, args));
   }

   @SafeVarargs
   public final void error(Callable<String> message, Callable<Object>... args) {
      log(ERROR, message, args);
   }

   @SafeVarargs
   public final void warn(Callable<String> message, Callable<Object>... args) {
      log(WARN, message, args);
   }

   @SafeVarargs
   public final void info(Callable<String> message, Callable<Object>... args) {
      log(INFO, message, args);
   }

   @SafeVarargs
   public final void debug(Callable<String> message, Callable<Object>... args) {
      log(DEBUG, message, args);
   }

   @SafeVarargs
   public final void trace(Callable<String> message, Callable<Object>... args) {
      log(TRACE, message, args);
   }

   @SafeVarargs
   public final void log(Level level, String message, Callable<Object>... args) {
      notNull(level, "level");
      notNull(message, "message");
      notNull(args, "args");

      log(level, LogMessage.of(message, args));
   }

   @SafeVarargs
   public final void error(String message, Callable<Object>... args) {
      log(ERROR, message, args);
   }

   @SafeVarargs
   public final void warn(String message, Callable<Object>... args) {
      log(WARN, message, args);
   }

   @SafeVarargs
   public final void info(String message, Callable<Object>... args) {
      log(INFO, message, args);
   }

   @SafeVarargs
   public final void debug(String message, Callable<Object>... args) {
      log(DEBUG, message, args);
   }

   @SafeVarargs
   public final void trace(String message, Callable<Object>... args) {
      log(TRACE, message, args);
   }

   public void log(Level level, String message, Object... args) {
      notNull(level, "level");
      notNull(message, "message");

      log(level, LogMessage.of(message, args));
   }

   @Override
   public void error(String message, Object... args) {
      log(ERROR, message, args);
   }

   @Override
   public void warn(String message, Object... args) {
      log(WARN, message, args);
   }

   @Override
   public void info(String message, Object... args) {
      log(INFO, message, args);
   }

   @Override
   public void debug(String message, Object... args) {
      log(DEBUG, message, args);
   }

   @Override
   public void trace(String message, Object... args) {
      log(TRACE, message, args);
   }

   public void log(Level level, Marker marker, String message, Object... args) {
      notNull(level, "level");
      notNull(marker, "marker");
      notNull(message, "message");

      log(level, LogMessage.of(message, args));
   }

   @Override
   public void error(Marker marker, String message, Object... args) {
      log(ERROR, marker, message, args);
   }

   @Override
   public void warn(Marker marker, String message, Object... args) {
      log(WARN, marker, message, args);
   }

   @Override
   public void info(Marker marker, String message, Object... args) {
      log(INFO, marker, message, args);
   }

   @Override
   public void debug(Marker marker, String message, Object... args) {
      log(DEBUG, marker, message, args);
   }

   @Override
   public void trace(Marker marker, String message, Object... args) {
      log(TRACE, marker, message, args);
   }

   public void log(Level level, Callable<String> message, Throwable throwable) {
      notNull(level, "level");
      notNull(message, "message");

      log(level, LogMessage.of(message, throwable));
   }

   public void error(Callable<String> message, Throwable throwable) {
      log(ERROR, message, throwable);
   }

   public void warn(Callable<String> message, Throwable throwable) {
      log(WARN, message, throwable);
   }

   public void info(Callable<String> message, Throwable throwable) {
      log(INFO, message, throwable);
   }

   public void debug(Callable<String> message, Throwable throwable) {
      log(DEBUG, message, throwable);
   }

   public void trace(Callable<String> message, Throwable throwable) {
      log(TRACE, message, throwable);
   }

   public void log(Level level, String message, Throwable throwable) {
      notNull(level, "level");
      notNull(message, "message");

      log(level, LogMessage.of(message, throwable));
   }

   @Override
   public void error(String message, Throwable throwable) {
      log(ERROR, message, throwable);
   }

   @Override
   public void warn(String message, Throwable throwable) {
      log(WARN, message, throwable);
   }

   @Override
   public void info(String message, Throwable throwable) {
      log(INFO, message, throwable);
   }

   @Override
   public void debug(String message, Throwable throwable) {
      log(DEBUG, message, throwable);
   }

   @Override
   public void trace(String message, Throwable throwable) {
      log(TRACE, message, throwable);
   }

   public void log(Level level, Marker marker, String message, Throwable throwable) {
      notNull(level, "level");
      notNull(marker, "marker");
      notNull(message, "message");

      log(level, LogMessage.of(message, throwable));
   }

   @Override
   public void error(Marker marker, String message, Throwable throwable) {
      log(ERROR, marker, message, throwable);
   }

   @Override
   public void warn(Marker marker, String message, Throwable throwable) {
      log(WARN, marker, message, throwable);
   }

   @Override
   public void info(Marker marker, String message, Throwable throwable) {
      log(INFO, marker, message, throwable);
   }

   @Override
   public void debug(Marker marker, String message, Throwable throwable) {
      log(DEBUG, marker, message, throwable);
   }

   @Override
   public void trace(Marker marker, String message, Throwable throwable) {
      log(TRACE, marker, message, throwable);
   }

   /**
    * Returns current log level.
    *
    * @return current log level
    *
    * @implNote {@link Level#OFF} is assumed if no other level is detected.
    */
   public Level logLevel() {
      if (logger.isTraceEnabled()) return TRACE;
      else if (logger.isDebugEnabled()) return DEBUG;
      else if (logger.isInfoEnabled()) return INFO;
      else if (logger.isWarnEnabled()) return WARN;
      else if (logger.isErrorEnabled()) return ERROR;
      else return Level.OFF;
   }

   /**
    * Returns current log level.
    *
    * @param marker marker
    *
    * @return current log level
    *
    * @implNote {@link Level#OFF} is assumed if no other level is detected.
    */
   public Level logLevel(Marker marker) {
      if (isTraceEnabled(marker)) return TRACE;
      else if (isDebugEnabled(marker)) return DEBUG;
      else if (isInfoEnabled(marker)) return INFO;
      else if (isWarnEnabled(marker)) return WARN;
      else if (isErrorEnabled(marker)) return ERROR;
      else return Level.OFF;
   }

   @Override
   public boolean isErrorEnabled() {
      return logger.isErrorEnabled();
   }

   @Override
   public boolean isErrorEnabled(Marker marker) {
      return logger.isErrorEnabled(marker);
   }

   @Override
   public boolean isWarnEnabled() {
      return logger.isWarnEnabled();
   }

   @Override
   public boolean isWarnEnabled(Marker marker) {
      return logger.isWarnEnabled(marker);
   }

   @Override
   public boolean isInfoEnabled() {
      return logger.isInfoEnabled();
   }

   @Override
   public boolean isInfoEnabled(Marker marker) {
      return logger.isInfoEnabled(marker);
   }

   @Override
   public boolean isDebugEnabled() {
      return logger.isDebugEnabled();
   }

   @Override
   public boolean isDebugEnabled(Marker marker) {
      return logger.isDebugEnabled(marker);
   }

   @Override
   public boolean isTraceEnabled() {
      return logger.isTraceEnabled();
   }

   @Override
   public boolean isTraceEnabled(Marker marker) {
      return logger.isTraceEnabled(marker);
   }

   /**
    * Returns {@code true} if specified level is loggable depending on underlying logger actual level.
    * Always return {@code false} if current log level is {@link Level#OFF} or specified level is
    * {@link Level#OFF}, excepting if both are {@link Level#OFF}.
    */
   public boolean isLogLevelEnabled(Level level) {
      return logLevel().isEnabled(level);
   }

   /**
    * Lazy parameter evaluation for loggers that takes any {@link Object} in parameter and calls
    * {@link Object#toString()} on it if log level is sufficient. As {@link Callable} operation will be
    * evaluated only if current log level is sufficient, it's not required to wrap the log into a condition,
    * testing the current log level, when parameter evaluation is costly.
    * <p>
    * This operation is not required when {@link ExtendedLogger} API is used, only for external, unrelated,
    * usages.
    *
    * @param callable lazy parameter evaluation
    *
    * @return parameter value lazily evaluated when {@link Object#toString} will be called
    */
   public static Object lazy(Callable<Object> callable) {
      notNull(callable, "callable");

      return new Object() {
         @Override
         public String toString() {
            var resolved = LogMessage.resolve(callable);

            return resolved != null ? resolved.toString() : null;
         }
      };
   }

   /**
    * Sets underlying logger default level for specified name. Underlying logger must be supported.
    *
    * @param name log prefix name, or {@link Logger#ROOT_LOGGER_NAME}
    * @param level level value
    */
   public static void setLoggerLevel(String name, ExtendedLogger.Level level) {
      notBlank(name, "name");
      notNull(level, "level");

      Logger logger = LoggerFactory.getLogger(name);

      if (logger instanceof ch.qos.logback.classic.Logger) {
         ch.qos.logback.classic.Logger loggerImpl = (ch.qos.logback.classic.Logger) logger;
         loggerImpl.setLevel(logbackLevel(level));
      } else {
         throw new IllegalStateException(String.format("Unknown '%s' logger implementation",
                                                       logger.getClass().getName()));
      }

      //       <dependency>
      //          <groupId>org.apache.logging.log4j</groupId>
      //          <artifactId>log4j-core</artifactId>
      //          2.11.2
      //       </dependency>
      //       Configurator.setLevel("com.tinubu.logexplorer", Boolean.TRUE.equals(debug) ? Level.DEBUG : Level.INFO);
   }

   /**
    * Maps level to Logback level.
    *
    * @param level level to map
    *
    * @return logback level
    */
   protected static ch.qos.logback.classic.Level logbackLevel(Level level) {
      ch.qos.logback.classic.Level logbackLevel;
      switch (level) {
         case OFF:
            logbackLevel = ch.qos.logback.classic.Level.OFF;
            break;
         case ERROR:
            logbackLevel = ch.qos.logback.classic.Level.ERROR;
            break;
         case WARN:
            logbackLevel = ch.qos.logback.classic.Level.WARN;
            break;
         case INFO:
            logbackLevel = ch.qos.logback.classic.Level.INFO;
            break;
         case DEBUG:
            logbackLevel = ch.qos.logback.classic.Level.DEBUG;
            break;
         case TRACE:
            logbackLevel = ch.qos.logback.classic.Level.TRACE;
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported '%s' log level", level));
      }
      return logbackLevel;
   }

   /**
    * Maps Logback level to level.
    *
    * @param level Logback level to map
    *
    * @return level
    */
   protected static Level level(ch.qos.logback.classic.Level level) {
      Level logbackLevel;
      switch (level.toInt()) {
         case ch.qos.logback.classic.Level.OFF_INT:
            logbackLevel = Level.OFF;
            break;
         case ch.qos.logback.classic.Level.ERROR_INT:
            logbackLevel = Level.ERROR;
            break;
         case ch.qos.logback.classic.Level.WARN_INT:
            logbackLevel = Level.WARN;
            break;
         case ch.qos.logback.classic.Level.INFO_INT:
            logbackLevel = Level.INFO;
            break;
         case ch.qos.logback.classic.Level.DEBUG_INT:
            logbackLevel = Level.DEBUG;
            break;
         case ch.qos.logback.classic.Level.TRACE_INT:
            logbackLevel = Level.TRACE;
            break;
         default:
            throw new IllegalStateException(String.format("Unsupported '%s' log level", level));
      }
      return logbackLevel;
   }

   /**
    * Core logging operation, including callback calls.
    */
   protected void log(Level level, LogMessage message, Consumer<LogMessage> logging) {
      if (callback.before(level, message)) {
         if (isLogLevelEnabled(level)) {
            if (callback.beforeLog(level, message)) {
               logging.accept(message);
            }
            callback.afterLog(level, message);
         }
      }
      callback.after(level, message);
   }

   protected void logOff(Marker marker, LogMessage m) {
   }

   protected void logError(Marker marker, LogMessage m) {
      switch (m.argumentSignature()) {
         case ARG0:
            if (marker != null) {
               logger.error(marker, m.message());
            } else {
               logger.error(m.message());
            }
            break;
         case ARG1:
            if (marker != null) {
               logger.error(marker, m.message(), m.arg1());
            } else {
               logger.error(m.message(), m.arg1());
            }
            break;
         case ARG2:
            if (marker != null) {
               logger.error(marker, m.message(), m.arg1(), m.arg2());
            } else {
               logger.error(m.message(), m.arg1(), m.arg2());
            }
            break;
         case ARGN:
            if (marker != null) {
               logger.error(marker, m.message(), m.args());
            } else {
               logger.error(m.message(), m.args());
            }
            break;
         case ARG1T:
            if (marker != null) {
               logger.error(marker, m.message(), m.throwable());
            } else {
               logger.error(m.message(), m.throwable());
            }
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' argument signature",
                                                          m.argumentSignature()));
      }
   }

   protected void logWarn(Marker marker, LogMessage m) {
      switch (m.argumentSignature()) {
         case ARG0:
            if (marker != null) {
               logger.warn(marker, m.message());
            } else {
               logger.warn(m.message());
            }
            break;
         case ARG1:
            if (marker != null) {
               logger.warn(marker, m.message(), m.arg1());
            } else {
               logger.warn(m.message(), m.arg1());
            }
            break;
         case ARG2:
            if (marker != null) {
               logger.warn(marker, m.message(), m.arg1(), m.arg2());
            } else {
               logger.warn(m.message(), m.arg1(), m.arg2());
            }
            break;
         case ARGN:
            if (marker != null) {
               logger.warn(marker, m.message(), m.args());
            } else {
               logger.warn(m.message(), m.args());
            }
            break;
         case ARG1T:
            if (marker != null) {
               logger.warn(marker, m.message(), m.throwable());
            } else {
               logger.warn(m.message(), m.throwable());
            }
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' argument signature",
                                                          m.argumentSignature()));
      }
   }

   protected void logInfo(Marker marker, LogMessage m) {
      switch (m.argumentSignature()) {
         case ARG0:
            if (marker != null) {
               logger.info(marker, m.message());
            } else {
               logger.info(m.message());
            }
            break;
         case ARG1:
            if (marker != null) {
               logger.info(marker, m.message(), m.arg1());
            } else {
               logger.info(m.message(), m.arg1());
            }
            break;
         case ARG2:
            if (marker != null) {
               logger.info(marker, m.message(), m.arg1(), m.arg2());
            } else {
               logger.info(m.message(), m.arg1(), m.arg2());
            }
            break;
         case ARGN:
            if (marker != null) {
               logger.info(marker, m.message(), m.args());
            } else {
               logger.info(m.message(), m.args());
            }
            break;
         case ARG1T:
            if (marker != null) {
               logger.info(marker, m.message(), m.throwable());
            } else {
               logger.info(m.message(), m.throwable());
            }
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' argument signature",
                                                          m.argumentSignature()));
      }
   }

   protected void logDebug(Marker marker, LogMessage m) {
      switch (m.argumentSignature()) {
         case ARG0:
            if (marker != null) {
               logger.debug(marker, m.message());
            } else {
               logger.debug(m.message());
            }
            break;
         case ARG1:
            if (marker != null) {
               logger.debug(marker, m.message(), m.arg1());
            } else {
               logger.debug(m.message(), m.arg1());
            }
            break;
         case ARG2:
            if (marker != null) {
               logger.debug(marker, m.message(), m.arg1(), m.arg2());
            } else {
               logger.debug(m.message(), m.arg1(), m.arg2());
            }
            break;
         case ARGN:
            if (marker != null) {
               logger.debug(marker, m.message(), m.args());
            } else {
               logger.debug(m.message(), m.args());
            }
            break;
         case ARG1T:
            if (marker != null) {
               logger.debug(marker, m.message(), m.throwable());
            } else {
               logger.debug(m.message(), m.throwable());
            }
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' argument signature",
                                                          m.argumentSignature()));
      }
   }

   protected void logTrace(Marker marker, LogMessage m) {
      switch (m.argumentSignature()) {
         case ARG0:
            if (marker != null) {
               logger.trace(marker, m.message());
            } else {
               logger.trace(m.message());
            }
            break;
         case ARG1:
            if (marker != null) {
               logger.trace(marker, m.message(), m.arg1());
            } else {
               logger.trace(m.message(), m.arg1());
            }
            break;
         case ARG2:
            if (marker != null) {
               logger.trace(marker, m.message(), m.arg1(), m.arg2());
            } else {
               logger.trace(m.message(), m.arg1(), m.arg2());
            }
            break;
         case ARGN:
            if (marker != null) {
               logger.trace(marker, m.message(), m.args());
            } else {
               logger.trace(m.message(), m.args());
            }
            break;
         case ARG1T:
            if (marker != null) {
               logger.trace(marker, m.message(), m.throwable());
            } else {
               logger.trace(m.message(), m.throwable());
            }
            break;
         default:
            throw new IllegalStateException(String.format("Unknown '%s' argument signature",
                                                          m.argumentSignature()));
      }
   }

   /**
    * Lazy message and argument resolution abstraction.
    * Message and arguments resolving is mutable to ensure resolving is always evaluated once, wherever
    * evaluation is made.
    *
    * @implNote Differentiates argument signatures for optimization purpose (prevents array creation
    *       when not required).
    */
   public static class LogMessage {

      public enum ArgumentSignature {
         /** 0 args */
         ARG0,
         /** 1 arg */
         ARG1,
         /** 2 args */
         ARG2,
         /** n args */
         ARGN,
         /** 1 arg (Throwable) */
         ARG1T
      }

      private final Object message;
      private final ArgumentSignature argumentSignature;
      private final Object[] args;
      private final Object arg1;
      private final Object arg2;
      private final Throwable throwable;

      private String resolvedMessage;
      private Object[] resolvedArgs;
      private Object resolvedArg1;
      private Object resolvedArg2;

      private LogMessage(Object message,
                         ArgumentSignature argumentSignature,
                         Object[] args,
                         Object arg1,
                         Object arg2,
                         Throwable throwable) {
         this.argumentSignature = notNull(argumentSignature, "argumentSignature");
         this.message = notNull(message, "message");
         this.args = args;
         this.arg1 = arg1;
         this.arg2 = arg2;
         this.throwable = throwable;
      }

      /* ARG0 */

      public static LogMessage of(Callable<String> message) {
         return new LogMessage(message, ARG0, null, null, null, null);
      }

      public static LogMessage of(String message) {
         return new LogMessage(message, ARG0, null, null, null, null);
      }

      /* ARGN */

      public static LogMessage of(Callable<String> message, Callable<Object>[] args) {
         return new LogMessage(message, ARGN, args, null, null, null);
      }

      public static LogMessage of(String message, Callable<Object>[] args) {
         return new LogMessage(message, ARGN, args, null, null, null);
      }

      public static LogMessage of(Callable<String> message, Object[] args) {
         return new LogMessage(message, ARGN, args, null, null, null);
      }

      public static LogMessage of(String message, Object[] args) {
         return new LogMessage(message, ARGN, args, null, null, null);
      }

      /* ARG1 */

      public static LogMessage of(Callable<String> message, Callable<Object> arg1) {
         return new LogMessage(message, ARG1, null, arg1, null, null);
      }

      public static LogMessage of(String message, Callable<Object> arg1) {
         return new LogMessage(message, ARG1, null, arg1, null, null);
      }

      public static LogMessage of(Callable<String> message, Object arg1) {
         return new LogMessage(message, ARG1, null, arg1, null, null);
      }

      public static LogMessage of(String message, Object arg1) {
         return new LogMessage(message, ARG1, null, arg1, null, null);
      }

      /* ARG2 */

      public static LogMessage of(Callable<String> message, Callable<Object> arg1, Callable<Object> arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      public static LogMessage of(String message, Callable<Object> arg1, Callable<Object> arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      public static LogMessage of(Callable<String> message, Object arg1, Callable<Object> arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      public static LogMessage of(String message, Object arg1, Callable<Object> arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      public static LogMessage of(Callable<String> message, Callable<Object> arg1, Object arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      public static LogMessage of(String message, Callable<Object> arg1, Object arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      public static LogMessage of(Callable<String> message, Object arg1, Object arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      public static LogMessage of(String message, Object arg1, Object arg2) {
         return new LogMessage(message, ARG2, null, arg1, arg2, null);
      }

      /* ARG1T */

      public static LogMessage of(Callable<String> message, Throwable throwable) {
         return new LogMessage(message, ARG1T, null, null, null, throwable);
      }

      public static LogMessage of(String message, Throwable throwable) {
         return new LogMessage(message, ARG1T, null, null, null, throwable);
      }

      public String message() {
         return resolvedMessage = nullable(resolvedMessage, () -> {
            var resolved = resolve(message);

            return resolved != null ? resolved.toString() : null;
         });
      }

      public Object[] args() {
         if (argumentSignature != ARGN) {
            throw new IllegalStateException("Bad argument signature");
         }
         return resolvedArgs =
               nullable(resolvedArgs, () -> stream(args).map(LogMessage::resolve).toArray(Object[]::new));
      }

      public Object arg1() {
         if (argumentSignature != ARG1 && argumentSignature != ARG2) {
            throw new IllegalStateException("Bad argument signature");
         }
         return resolvedArg1 = nullable(resolvedArg1, () -> resolve(arg1));
      }

      public Object arg2() {
         if (argumentSignature != ARG2) {
            throw new IllegalStateException("Bad argument signature");
         }
         return resolvedArg2 = nullable(resolvedArg2, () -> resolve(arg2));
      }

      public Throwable throwable() {
         if (argumentSignature != ARG1T) {
            throw new IllegalStateException("Bad argument signature");
         }
         return throwable;
      }

      public ArgumentSignature argumentSignature() {
         return argumentSignature;
      }

      public String formattedMessage() {
         switch (argumentSignature) {
            case ARG0:
               return message();
            case ARG1:
               return MessageFormatter.format(message(), arg1()).getMessage();
            case ARG2:
               return MessageFormatter.format(message(), arg1(), arg2()).getMessage();
            case ARGN:
               return MessageFormatter.arrayFormat(message(), args()).getMessage();
            case ARG1T:
               return MessageFormatter.format(message(), throwable()).getMessage();
            default:
               throw new IllegalStateException(String.format("Unknown '%s' argument signature",
                                                             argumentSignature));
         }

      }

      /**
       * Resolve argument to {@link Object}.
       *
       * @param argument logger argument to resolve, can be any object, or a {@link Callable}, or a
       *       {@link Supplier}, in such cases, argument is lazily, and recursively evaluated
       *
       * @return resolved argument as object
       *
       * @implSpec If an exception occurs while resolving argument, the exception message is returned as
       *       the resolved value for argument
       */
      @SuppressWarnings("unchecked")
      protected static Object resolve(Object argument) {
         if (argument instanceof Callable || argument instanceof Supplier) {
            Object lazyArgument;

            try {
               if (argument instanceof Callable) {
                  lazyArgument = ((Callable<Object>) argument).call();
               } else {
                  lazyArgument = ((Supplier<Object>) argument).get();
               }
            } catch (Exception e) {
               lazyArgument = "[" + e.getClass().getSimpleName() + ":" + e.getMessage() + "]";
            }

            return resolve(lazyArgument);
         } else {
            return argument;
         }
      }

   }

}
