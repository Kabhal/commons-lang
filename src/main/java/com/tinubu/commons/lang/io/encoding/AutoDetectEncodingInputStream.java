/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.encoding;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.lang.System.arraycopy;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import com.tinubu.commons.lang.io.encoding.EncodingDetector.DetectedEncoding;

/**
 * Wraps a source {@link InputStream} with this encoding auto-detector.
 * <p>
 * A sample of source content, of configurable length, is used for detection using BOMs or simple decoding.
 * <p>
 * Supports {@link StandardCharsets#UTF_8} and {@link StandardCharsets#UTF_16} BOM detection, if detection
 * length is sufficient (2 to 3 bytes are necessary).
 * <p>
 * All encoding/bom detection operations can be called at any time and in any order, one or several times. No
 * detection result is cached between detection operations.
 * <p>
 * A side feature is to remove BOM after detection, this operation works only if no external read occurred
 * yet.
 */
public class AutoDetectEncodingInputStream extends PushbackInputStream {

   /** Default detection length in bytes. */
   private static final int DEFAULT_DETECTION_LENGTH = 4096;

   private static final EncodingDetector DEFAULT_DETECTOR = EncodingDetectorFactory
         .instance()
         .encodingDetector()
         .orElseThrow(() -> new IllegalStateException("No EncodingDetector detected"));

   private final EncodingDetector detector;
   private final int detectionLength;

   /** Current detection buffer, filled as we read input stream. */
   private final byte[] detectionBuffer;
   /** Current write position in detection buffer. */
   private int detectionBufferPos;
   /**
    * Detection buffer size, once detection length is reached, or no more bytes are available. Otherwise,
    * value is always -1.
    */
   private int detectionBufferLength;
   private int pos = 0;

   /**
    * Creates encoding detector stream with configurable detection length.
    *
    * @param inputStream source stream
    * @param detector encoding detector
    * @param detectionLength detection length in bytes, must be > 0
    */
   public AutoDetectEncodingInputStream(InputStream inputStream,
                                        EncodingDetector detector,
                                        int detectionLength) {
      super(notNull(inputStream, "inputStream"),
            satisfies(detectionLength, l -> l > 0, "detectionLength", "must be > 0"));
      this.detector = notNull(detector, "detector");
      this.detectionLength = detectionLength;

      detectionBuffer = new byte[detectionLength];
      detectionBufferLength = -1;
      detectionBufferPos = 0;
   }

   /**
    * Creates encoding detector stream with default detection length ({@value #DEFAULT_DETECTION_LENGTH}).
    *
    * @param inputStream source stream
    * @param detector encoding detector
    */
   public AutoDetectEncodingInputStream(InputStream inputStream, EncodingDetector detector) {
      this(inputStream, detector, DEFAULT_DETECTION_LENGTH);
   }

   /**
    * Creates encoding detector stream with default detector and configurable detection length.
    *
    * @param inputStream source stream
    * @param detectionLength detection length in bytes, must be > 0
    */
   public AutoDetectEncodingInputStream(InputStream inputStream, int detectionLength) {
      this(inputStream, DEFAULT_DETECTOR, detectionLength);
   }

   /**
    * Creates encoding detector stream with default detector and default detection length
    * ({@value #DEFAULT_DETECTION_LENGTH}).
    *
    * @param inputStream source stream
    */
   public AutoDetectEncodingInputStream(InputStream inputStream) {
      this(inputStream, DEFAULT_DETECTOR, DEFAULT_DETECTION_LENGTH);
   }

   /**
    * Detects first matching encoding among specified encodings, only using BOM when available for encoding.
    * If encoding has no known BOM, it is not matched.
    * <p>
    * Remove BOM only works if no external read occurred yet since stream initialization.
    *
    * @param encodings restricts encodings to detect, or empty list for no restriction
    * @param removeBom whether to remove BOM for subsequent reads, when available for encoding, and if
    *       BOM is detected
    *
    * @return first matching encoding, or {@link Optional#empty()} if none encoding match
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasBom(List<Charset> encodings, boolean removeBom) throws IOException {
      noNullElements(encodings, "encodings");

      fillDetectionBuffer();

      var detected = detector.hasBom(detectionBuffer(), encodings);

      if (removeBom && detected.isPresent()) {
         removeBom(detected.get().requireBom());
      }

      return detected;
   }

   /**
    * Detects first matching encoding among specified encodings, only using BOM when available for encoding.
    * If encoding has no known BOM, it is not matched.
    *
    * @param encodings restricts encodings to detect, or empty list for no restriction
    *
    * @return first matching encoding, or {@link Optional#empty()} if none encoding match
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasBom(List<Charset> encodings) throws IOException {
      return hasBom(encodings, false);
   }

   /**
    * Detects specified encoding only using BOM when available for encoding.
    * If encoding has no known BOM, returns {@code false}.
    * <p>
    * Remove BOM only works if no external read occurred yet since stream initialization.
    *
    * @param encoding encoding to detect
    * @param removeBom whether to remove BOM for subsequent reads, when available for encoding, and if
    *       BOM is detected
    *
    * @return detected encoding, or {@link Optional#empty}
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasBom(Charset encoding, boolean removeBom) throws IOException {
      notNull(encoding, "encoding");

      return hasBom(list(encoding), removeBom);
   }

   /**
    * Detects specified encoding only using BOM when available for encoding.
    * If encoding has no known BOM, it is not matched.
    *
    * @param encoding encoding to detect
    *
    * @return detected encoding, or {@link Optional#empty}
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasBom(Charset encoding) throws IOException {
      notNull(encoding, "encoding");

      return hasBom(encoding, false);
   }

   /**
    * Detects first matching encoding among specified encodings.
    * <p>
    * Remove BOM only works if no external read occurred yet since stream initialization.
    *
    * @param encodings restricts encodings to detect, or empty list for no restriction
    * @param removeBom whether to remove BOM for subsequent reads, when available for encoding, and if
    *       BOM is detected. BOM removal is ignored if {@code ignoreBom} is set
    *
    * @return first matching encoding, or {@link Optional#empty()} if none encoding match
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasEncoding(List<Charset> encodings, boolean removeBom)
         throws IOException {
      noNullElements(encodings, "encodings");

      fillDetectionBuffer();

      var detected = detector.hasEncoding(detectionBuffer(), encodings);

      if (removeBom && detected.isPresent() && detected.get().bom().isPresent()) {
         removeBom(detected.get().requireBom());
      }

      return detected;
   }

   /**
    * Detects first matching encoding among specified encodings.
    * Use BOM detection when available for encoding.
    *
    * @param encodings restricts encodings to detect, or empty list for no restriction
    *
    * @return first matching encoding, or {@link Optional#empty()} if none encoding match
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasEncoding(List<Charset> encodings) throws IOException {
      noNullElements(encodings, "encodings");

      return hasEncoding(encodings, false);
   }

   /**
    * Detects specified encoding.
    * Use BOM detection when available for encoding.
    * <p>
    * Remove BOM only works if no external read occurred yet since stream initialization.
    *
    * @param encoding encoding to detect
    * @param removeBom whether to remove BOM for subsequent reads, when available for encoding, and if
    *       BOM is detected. BOM removal is ignored if {@code ignoreBom} is set
    *
    * @return detected encoding, or {@link Optional#empty}
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasEncoding(Charset encoding, boolean removeBom) throws IOException {
      notNull(encoding, "encoding");

      return hasEncoding(list(encoding), removeBom);
   }

   /**
    * Detects specified encoding.
    * Use BOM detection when available for encoding.
    *
    * @param encoding encoding to detect
    *
    * @return detected encoding, or {@link Optional#empty}
    *
    * @throws IOException if an I/O error occurs while reading input stream
    */
   public Optional<DetectedEncoding> hasEncoding(Charset encoding) throws IOException {
      notNull(encoding, "encoding");

      return hasEncoding(list(encoding));
   }

   @Override
   public int read() throws IOException {
      int read = super.read();

      if (read != -1) {
         pos++;
      }

      if (detectionBufferLength == -1) {
         if (read != -1 && detectionBufferPos < detectionLength) {
            detectionBuffer[detectionBufferPos++] = (byte) read;
         }

         if (read == -1 || detectionBufferPos >= detectionLength) {
            detectionBufferLength = detectionBufferPos;
         }
      }

      return read;
   }

   @Override
   public int read(byte[] b, int off, int len) throws IOException {
      int read = super.read(b, off, len);

      if (read != -1) {
         pos += read;
      }

      if (detectionBufferLength == -1) {
         if (read != -1 && detectionBufferPos < detectionLength) {
            var advanceLength = Integer.min(read, detectionLength - detectionBufferPos);
            arraycopy(b, off, detectionBuffer, detectionBufferPos, advanceLength);
            detectionBufferPos += advanceLength;
         }

         if (read == -1 || detectionBufferPos >= detectionLength) {
            detectionBufferLength = detectionBufferPos;
         }
      }

      return read;
   }

   /**
    * Reads from input stream as many bytes as necessary to complete detection buffer, if not already
    * completed.
    * Read bytes are then unread for further reading.
    *
    * @return number of bytes read to complete detection buffer, or {@code 0} if detection buffer is already
    *       filled
    */
   protected int fillDetectionBuffer() throws IOException {
      if (detectionBufferLength == -1) {
         int completingBufferPos = 0;
         int read;
         byte[] completingBuffer = new byte[detectionLength];
         do {
            read = read(completingBuffer, completingBufferPos, detectionLength - detectionBufferPos);

            if (read != -1) {
               completingBufferPos += read;
            }
         } while (read != -1 && detectionBufferPos < detectionLength);

         unread(completingBuffer, 0, completingBufferPos);
         pos -= completingBufferPos;

         return completingBufferPos;
      } else {
         return 0;
      }
   }

   protected ByteBuffer detectionBuffer() {
      if (detectionBufferLength == -1) {
         throw new IllegalStateException("Detection buffer must be filled first");
      }
      return ByteBuffer.wrap(detectionBuffer, 0, detectionBufferLength);
   }

   protected void removeBom(byte[] bom) throws IOException {
      if (pos == 0) {
         var bomLength = bom.length;
         int read = read(new byte[bomLength], 0, bomLength);
         if (read != bomLength) {
            throw new IllegalStateException("BOM removal failure");
         }
      }
   }

}
