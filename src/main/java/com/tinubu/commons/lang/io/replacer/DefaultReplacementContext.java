/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer;

import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;

import java.io.IOException;
import java.io.Reader;

import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.ReplacementContext;

public class DefaultReplacementContext implements ReplacementContext, AutoCloseable {

   private boolean stopReadingText;
   private boolean stopParsingToken;
   private int suppressFollowingWhiteSpace;
   private Reader injectedReader;

   private DefaultReplacementContext(boolean stopReadingText,
                                     boolean stopParsingToken,
                                     int suppressFollowingWhiteSpace,
                                     Reader injectedReader) {
      this.stopReadingText = stopReadingText;
      this.stopParsingToken = stopParsingToken;
      this.suppressFollowingWhiteSpace = suppressFollowingWhiteSpace;
      this.injectedReader = injectedReader;
   }

   public DefaultReplacementContext() {
      this(false, false, 0, null);
   }

   @Override
   public DefaultReplacementContext stopReadingText(boolean stopReadingText) {
      this.stopReadingText = stopReadingText;
      return this;
   }

   @Override
   public DefaultReplacementContext stopParsingToken(boolean stopParsingToken) {
      this.stopParsingToken = stopParsingToken;
      return this;
   }

   @Override
   public DefaultReplacementContext suppressFollowingWhitespace(int suppressFollowingWhitespace) {
      this.suppressFollowingWhiteSpace = suppressFollowingWhitespace;
      return this;
   }

   @Override
   public DefaultReplacementContext injectReader(Reader reader) {
      if (this.injectedReader != null) {
         try {
            this.injectedReader.close();
         } catch (IOException e) {
            throw runtimeThrow(e);
         }
      }
      this.injectedReader = reader;
      return this;
   }

   @Override
   public boolean stopReadingText() {
      return stopReadingText;
   }

   @Override
   public boolean stopParsingToken() {
      return stopParsingToken;
   }

   @Override
   public boolean suppressFollowingWhitespace(char[] whitespace) {
      if (whitespace == null) {
         suppressFollowingWhiteSpace = 0;
         return false;
      } else {
         if (suppressFollowingWhiteSpace == 0) {
            return false;
         } else if (suppressFollowingWhiteSpace == -1) {
            return true;
         } else {
            suppressFollowingWhiteSpace--;
            return true;
         }
      }
   }

   @Override
   public int readInjectedReader() throws IOException {
      return injectedReader != null ? injectedReader.read() : -1;
   }

   @Override
   public int readInjectedReader(char[] cbuf, int off, int len) throws IOException {
      return injectedReader != null ? injectedReader.read(cbuf, off, len) : -1;
   }

   @Override
   public void close() throws IOException {
      if (injectedReader != null) {
         injectedReader.close();
      }
   }
}
