/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.encoding;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;

/** Generic interface for any encoding detector. */
public interface EncodingDetector {

   /**
    * Detects sample buffer encoding among specified encodings.
    *
    * @param buffer sample buffer
    * @param encodings restricts encodings to detect, or empty list for no restriction
    *
    * @return detected encoding, with or without {@link DetectedEncoding#bom()}, or {@link Optional#empty}
    *       if no encoding detected or if detected encoding is not in restricted list
    */
   Optional<DetectedEncoding> hasEncoding(ByteBuffer buffer, List<Charset> encodings);

   /**
    * Detects sample buffer encoding among specified encodings.
    *
    * @param buffer sample buffer
    *
    * @return detected encoding, with or without {@link DetectedEncoding#bom()}, or {@link Optional#empty}
    *       if no encoding detected or if detected encoding is not in restricted list
    */
   default Optional<DetectedEncoding> hasEncoding(ByteBuffer buffer) {
      return hasEncoding(buffer, list());
   }

   /**
    * Detects sample buffer BOM for any specified encoding.
    *
    * @param buffer sample buffer
    * @param encodings restricts encodings to detect BOM for, or empty list for no restriction
    *
    * @return detected encoding, with {@link DetectedEncoding#bom()}, or {@link Optional#empty} if no BOM
    *       detected or if detected BOM encoding is not in restricted encoding list
    */
   Optional<DetectedEncoding> hasBom(ByteBuffer buffer, List<Charset> encodings);

   /**
    * Detects sample buffer BOM for any specified encoding.
    *
    * @param buffer sample buffer
    *
    * @return detected encoding, with {@link DetectedEncoding#bom()}, or {@link Optional#empty} if no BOM
    *       detected or if detected BOM encoding is not in restricted encoding list
    */
   default Optional<DetectedEncoding> hasBom(ByteBuffer buffer) {
      return hasBom(buffer, list());
   }

   class DetectedEncoding {
      private final Charset encoding;
      private final byte[] bom;
      private final float confidence;

      public DetectedEncoding(Charset encoding, byte[] bom, float confidence) {
         this.encoding = notNull(encoding, "encoding");
         this.bom = nullable(bom, b -> notEmpty(b, "bom"));
         this.confidence = satisfies(confidence, c -> c >= 0 && c <= 1, "confidence", "must be in [0, 1]");
      }

      public DetectedEncoding(Charset encoding, byte[] bom) {
         this(encoding, bom, 0);
      }

      public DetectedEncoding(Charset encoding, float confidence) {
         this(encoding, null, confidence);
      }

      public DetectedEncoding(Charset encoding) {
         this(encoding, null, 0);
      }

      public Charset encoding() {
         return encoding;
      }

      public Optional<byte[]> bom() {
         return nullable(bom);
      }

      public byte[] requireBom() {
         return nullable(bom).orElseThrow(() -> new IllegalStateException("Missing BOM"));
      }

      /**
       * Detection confidence. Value in [0-1].
       * If no confidence is provided by implementation, set providence to 0 conservatively.
       */
      public float confidence() {
         return confidence;
      }

      @Override
      public boolean equals(Object o) {
         if (this == o) return true;
         if (o == null || getClass() != o.getClass()) return false;
         DetectedEncoding that = (DetectedEncoding) o;
         return Float.compare(confidence, that.confidence) == 0
                && Objects.equals(encoding, that.encoding)
                && Objects.deepEquals(bom, that.bom);
      }

      @Override
      public int hashCode() {
         return Objects.hash(encoding, Arrays.hashCode(bom), confidence);
      }

      @Override
      public String toString() {
         return new StringJoiner(", ", DetectedEncoding.class.getSimpleName() + "[", "]")
               .add("encoding=" + encoding)
               .add("bom=" + Arrays.toString(bom))
               .add("confidence=" + confidence)
               .toString();
      }
   }

}
