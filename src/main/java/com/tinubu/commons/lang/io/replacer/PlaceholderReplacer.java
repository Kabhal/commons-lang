/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalPredicate;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.lang.System.arraycopy;
import static java.util.Objects.requireNonNull;

import java.util.Deque;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;

import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.ReplacementContext;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.TokenReplacer;

// Support escaping of ending token ? use case : $include(uri://query=\$dollar)$ , or ${{include(uri)\}} ?
public class PlaceholderReplacer implements TokenReplacer {

   private static final String DEFAULT_PLACEHOLDER_BEGIN = "${";
   private static final String DEFAULT_PLACEHOLDER_END = "}";
   private static final char DEFAULT_ESCAPE_CHAR = '\\';

   private final int bufferSize;
   private final char[] placeholderBegin;
   private final char[] placeholderEnd;
   private final BiFunction<Deque<ReplacementContext>, String, Object> replacementFunction;

   /** {@link #placeholderBegin} matching index. */
   private int phbIndex = 0;
   /** {@link #placeholderEnd} matching index. */
   private int pheIndex = 0;
   private boolean matches = false;
   /** {@link #content} insertion index. */
   private int cIndex = 0;
   private final char[] content;
   /** Number of escaping characters before placeholder begin token. */
   private int escaped = 0;

   /**
    * Creates placeholder replacer instance.
    *
    * @param bufferSize internal buffer size to adapt to your requirements
    * @param placeholderBegin placeholder begin char sequence
    * @param placeholderEnd placeholder end char sequence
    * @param replacementFunction replacement function, taking key value in parameter
    */
   public PlaceholderReplacer(int bufferSize,
                              String placeholderBegin,
                              String placeholderEnd,
                              BiFunction<Deque<ReplacementContext>, String, Object> replacementFunction) {
      this.placeholderBegin = notBlank(placeholderBegin, "placeholderBegin").toCharArray();
      this.placeholderEnd = notBlank(placeholderEnd, "placeholderEnd").toCharArray();
      this.replacementFunction = notNull(replacementFunction, "replacementFunction");
      this.bufferSize = satisfies(bufferSize, bs -> bs > 0, "bufferSize", "must be > 0");
      this.content = new char[bufferSize];
   }

   /**
    * Creates placeholder replacer instance.
    *
    * @param bufferSize internal buffer size to adapt to your requirements
    * @param replacementFunction replacement function, taking key value in parameter
    */
   public PlaceholderReplacer(int bufferSize,
                              BiFunction<Deque<ReplacementContext>, String, Object> replacementFunction) {
      this(bufferSize, DEFAULT_PLACEHOLDER_BEGIN, DEFAULT_PLACEHOLDER_END, replacementFunction);
   }

   /**
    * Simple replacement function adapter for {@link Map} model.
    *
    * @param model map model
    *
    * @return replacement function
    */
   public static BiFunction<Deque<ReplacementContext>, String, Object> mapModelReplacement(Map<String, Object> model) {
      notNull(model, "model");

      return (__, key) -> model.get(requireNonNull(key));
   }

   public int bufferSize() {
      return bufferSize;
   }

   public char[] placeholderBegin() {
      return placeholderBegin;
   }

   public char[] placeholderEnd() {
      return placeholderEnd;
   }

   public BiFunction<Deque<ReplacementContext>, String, Object> replacementFunction() {
      return replacementFunction;
   }

   @Override
   public void reset() {
      escaped = 0;
      phbIndex = 0;
      pheIndex = 0;
      cIndex = 0;
      matches = false;
   }

   @Override
   public boolean isPremise(int c) {
      return c != -1 && (placeholderBegin[0] == c || c == '\\');
   }

   @Override
   public boolean append(int c) {
      if (phbIndex == 0 && c == '\\') {
         escaped++;
      } else {
         if (matchingPhBegin()) {
            return placeholderBegin[phbIndex++] == c;
         } else if (startMatchingPhEnd(c)) {
            pheIndex++;
            verifyMatch();
         } else if (matchingPhEnd()) {
            if (placeholderEnd[pheIndex++] == c) {
               verifyMatch();
            } else {
               retrogradePhEndToContent(c);
            }
         } else {
            addToContent(c);
         }
      }
      return true;
   }

   private boolean matchingPhBegin() {
      return phbIndex < placeholderBegin.length;
   }

   private boolean startMatchingPhEnd(int c) {
      return pheIndex == 0 && placeholderEnd[pheIndex] == c;
   }

   private boolean matchingPhEnd() {
      return pheIndex > 0;
   }

   private void addToContent(int c) {
      content[cIndex++] = (char) c;
   }

   /**
    * phEnd has been partially matched, but this last character does not match.
    * We retrograde already matched phEnd to regular content, and reset phe index.
    * Last character is also added to content.
    */
   private void retrogradePhEndToContent(int c) {
      arraycopy(placeholderEnd, 0, content, cIndex, pheIndex - 1);
      cIndex += pheIndex - 1;
      addToContent(c);
      pheIndex = 0;
   }

   /**
    * Verify if we have a match after last action.
    */
   private void verifyMatch() {
      if (pheIndex >= placeholderEnd.length) {
         matches = true;
      }
   }

   @Override
   public boolean matches() {
      return matches;
   }

   /*
     key     replace
     -key    key
     --key   -replace
     ---key  -key
     ----key --replace

    */
   @Override
   public Optional<char[]> replacement(Deque<ReplacementContext> contextStack) {
      boolean replace = escaped % 2 == 0;

      if (escaped == 0) {
         return replacement(contextStack, new String(this.content, 0, cIndex));
      } else if (escaped % 2 == 1) {
         return optional((String.valueOf(DEFAULT_ESCAPE_CHAR).repeat(escaped / 2) + String.valueOf(
               unReplacedToken())).toCharArray());
      } else if (escaped % 2 == 0) {
         return optional((String.valueOf(DEFAULT_ESCAPE_CHAR).repeat(escaped / 2)
                          + String.valueOf(replacement(contextStack,
                                                       new String(this.content,
                                                                  0,
                                                                  cIndex)).orElseGet(this::unReplacedToken))).toCharArray());
      } else {
         throw new IllegalStateException();
      }
   }

   /** Reconstitutes original token. */
   private char[] unReplacedToken() {
      return (String.valueOf(placeholderBegin) + new String(this.content, 0, cIndex) + String.valueOf(
            placeholderEnd)).toCharArray();
   }

   protected Optional<char[]> replacement(Deque<ReplacementContext> contextStack, String content) {
      return nullable(replacementFunction.apply(contextStack, content))
            .map(String::valueOf)
            .flatMap(replacementString -> optionalPredicate(replacementString,
                                                            __ -> replacementString.length() <= bufferSize))
            .map(String::toCharArray);
   }
}
