/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer.directives;

import static com.tinubu.commons.lang.util.CheckedFunction.checkedFunction;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.ExceptionUtils.runtimeThrow;
import static com.tinubu.commons.lang.util.NullableUtils.nullableBoolean;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.Directive;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveEvalContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveReplacementContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveServiceLoader;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.ReplacementContext;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Failure;
import com.tinubu.commons.lang.util.Try.Success;

/**
 * Conditional directive.
 * <p>
 * Evaluated condition is first evaluated, and if successful, its is content replaced before if or else
 * block.
 * <p>
 * Usage, given {@code key}={@code value} in model :
 * <ul>
 *    <li>Simple evaluation : {@code ${if(has(key))} text ${/if}} &nbsp;-&gt; " text "</li>
 *    <li>Injecting evaluation : {@code ${if(key)} text ${/if}} &nbsp;-&gt; "value text "</li>
 *    <li>Negated injecting evaluation : {@code ${if!(key)} text ${/if}} &nbsp;-&gt; " text "</li>
 *    <li>if/else evaluation : {@code ${if(has(key))} text ${else} alternative ${/if}} &nbsp;-&gt; " text "</li>
 *    <li>More powerful injecting evaluation :<br>
 *    {@code ${if!(include(file://missing-import.txt, ignoreMissingResource))} alternative ${/if}} &nbsp;-&gt; " alternative "</li>
 * </ul>
 */
public class IfDirective extends AbstractDirective implements DirectiveServiceLoader {

   @Override
   public List<String> names() {
      return list("if", "if!", "else", "/if");
   }

   @Override
   public Try<Boolean, String> nonEscapedApply(DirectiveEvalContext evalContext) {
      ReplacementContext context = evalContext.currentContext();

      switch (evalContext.directive()) {
         case "if":
         case "if!":
            return context.ifReadingText(ctx -> ctx instanceof IfDirectiveReplacementContext,
                                         ifDirective(evalContext));
         case "else":
            return context.ifReadingText(ctx -> ctx instanceof IfDirectiveReplacementContext,
                                         elseDirective(evalContext));
         case "/if":
            return context.ifReadingText(ctx -> ctx instanceof IfDirectiveReplacementContext,
                                         endifDirective(evalContext));
         default:
            throw new IllegalStateException("Unknown directive");
      }

   }

   private static Function<ReplacementContext, Try<Boolean, String>> endifDirective(DirectiveEvalContext evalContext) {
      return ctx -> evalContext
            .currentContext(IfDirectiveReplacementContext.class)
            .<Try<Boolean, String>>map(checkedFunction(__ -> {
               evalContext.contextStack().pop().close();
               return Success.of(true);
            }))
            .orElseGet(() -> {
               return Failure.of("Misplaced directive");
            });
   }

   private static Function<ReplacementContext, Try<Boolean, String>> elseDirective(DirectiveEvalContext evalContext) {
      return ctx -> evalContext
            .currentContext(IfDirectiveReplacementContext.class)
            .<Try<Boolean, String>>map(ifContext -> {
               try (IfDirectiveReplacementContext pop = (IfDirectiveReplacementContext) evalContext
                     .contextStack()
                     .pop()) {
                  IfDirectiveReplacementContext elseContext = new IfDirectiveReplacementContext()
                        .outerCondition(pop.outerCondition())
                        .innerCondition(!pop.innerCondition());
                  elseContext.stopReadingText(!elseContext.innerCondition() || !elseContext.outerCondition());
                  evalContext.contextStack().push(elseContext);
               } catch (IOException e) {
                  throw runtimeThrow(e);
               }
               return Success.of(true);
            })
            .orElseGet(() -> Failure.of("Misplaced directive"));
   }

   private static Function<ReplacementContext, Try<Boolean, String>> ifDirective(DirectiveEvalContext evalContext) {
      return ctx -> {
         if (evalContext.arguments().isEmpty()) {
            return Failure.of("Missing condition");
         } else {
            IfDirectiveReplacementContext newIfContext =
                  new IfDirectiveReplacementContext().outerCondition(optionalInstanceOf(ctx,
                                                                                        IfDirectiveReplacementContext.class)
                                                                           .map(ifContext -> ifContext.outerCondition()
                                                                                             && ifContext.innerCondition())
                                                                           .orElse(!ctx.stopReadingText()));
            evalContext.contextStack().push(newIfContext);

            String conditionDirective = String.join(",", evalContext.arguments());
            Try<Boolean, String> conditionEvaluation =
                  evalContext.evaluate(conditionDirective).map(success -> success != null && success);

            boolean condition = conditionEvaluation instanceof Success
                                && nullableBoolean(((Success<Boolean, String>) conditionEvaluation).value());

            if (evalContext.directive().equals("if!")) {
               condition = !condition;
            }

            newIfContext.innerCondition(condition);

            newIfContext.stopReadingText(!newIfContext.innerCondition() || !newIfContext.outerCondition());

            return conditionEvaluation;
         }
      };
   }

   @Override
   public Stream<Directive> loadDirectives() {
      return stream(new IfDirective());
   }

   public static class IfDirectiveReplacementContext extends DirectiveReplacementContext {

      /**
       * Maintains outer status of stopReading, used for deep-stacking commands like if, else, ...
       */
      private boolean outerCondition = false;
      private boolean innerCondition = false;

      public boolean outerCondition() {
         return outerCondition;
      }

      public boolean innerCondition() {
         return innerCondition;
      }

      public IfDirectiveReplacementContext outerCondition(boolean outerStopReading) {
         this.outerCondition = outerStopReading;
         return this;
      }

      public IfDirectiveReplacementContext innerCondition(boolean innerCondition) {
         this.innerCondition = innerCondition;
         return this;
      }

      //@Override
      //public boolean stopReadingText() {
      //   return !outerCondition /*|| !innerCondition*/ || super.stopReadingText();
      //}

   }

}

