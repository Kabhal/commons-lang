/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer.directives;

import static com.tinubu.commons.lang.util.CheckedFunction.checkedFunction;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.io.StringReader;
import java.util.List;
import java.util.stream.Stream;

import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.Directive;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveEvalContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveReplacementContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveServiceLoader;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.ReplacementContext;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Failure;
import com.tinubu.commons.lang.util.Try.Success;

/**
 * Escape directive to escape block content.
 * <p>
 * Usage:
 * <ul>
 *    <li>Escape parameter : {@code ${escape(${has(key)})}}</li>
 *    <li>Escape the remaining document content (${/escape} is just escaped): {@code ${escape(final)}}</li>
 *    <li>Escape block : {@code ${escape}${has(key)}${/escape}}</li>
 * </ul>
 */
public class EscapeDirective extends AbstractDirective implements DirectiveServiceLoader {

   private static final String FINAL_OPTION = "final";

   @Override
   public List<String> names() {
      return list("escape", "/escape");
   }

   @Override
   public Try<Boolean, String> apply(DirectiveEvalContext evalContext) {

      if (evalContext.directive().equals("/escape")) {
         return evalContext
               .currentContext(EscapeDirectiveReplacementContext.class)
               .<Try<Boolean, String>>map(checkedFunction(__ -> {
                  evalContext.contextStack().pop().close();
                  return Success.of(true);
               }))
               .orElseGet(() -> Failure.of("Misplaced directive"));
      } else {
         return super.apply(evalContext);
      }
   }

   @Override
   public Try<Boolean, String> nonEscapedApply(DirectiveEvalContext evalContext) {
      ReplacementContext context = evalContext.currentContext();

      if (evalContext.directive().equals("escape")) {
         return context.ifReadingText(checkedFunction(ctx -> {
            if (!evalContext.arguments().isEmpty()) {
               if (evalContext.hasOption(0, FINAL_OPTION)) {
                  ctx.stopParsingToken(true);
               } else {
                  context.injectReader(new StringReader(String.join(" ", evalContext.arguments())));
               }
            } else {
               evalContext
                     .contextStack()
                     .push(new EscapeDirectiveReplacementContext().stopReadingText(false).escapeTokens(true));
            }
            return Success.of(true);
         }));
      } else {
         throw new IllegalStateException("Unknown directive");
      }
   }

   @Override
   public Stream<Directive> loadDirectives() {
      return stream(new EscapeDirective());
   }

   private static class EscapeDirectiveReplacementContext extends DirectiveReplacementContext {}
}
