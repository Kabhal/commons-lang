/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;

import java.util.Deque;
import java.util.Optional;

import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.ReplacementContext;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.TokenReplacer;

public class StringReplacer implements TokenReplacer {
   private final String matchString;
   private final String replacementString;
   private final boolean ignoreCase;

   private int length = 0;
   private boolean matches = false;

   public StringReplacer(String matchString, String replacementString, boolean ignoreCase) {
      this.matchString = notEmpty(matchString, "matchString");
      this.replacementString = replacementString;
      this.ignoreCase = ignoreCase;
   }

   public StringReplacer(String matchString, String replacementString) {
      this(matchString, replacementString, false);
   }

   @Override
   public void reset() {
      length = 0;
      matches = false;
   }

   @Override
   public boolean isPremise(int c) {
      return c != -1 && matchCharacter(c, matchString.charAt(0));
   }

   @Override
   public boolean append(int c) {
      if (matches) {
         throw new IllegalStateException();
      }

      if (c == -1) {
         return false;
      } else {
         length++;

         if (!matchCharacter(c, matchString.charAt(length - 1))) {
            return false;
         }

         if (matchString.length() == length) {
            matches = true;
         }
         return true;
      }
   }

   @Override
   public boolean matches() {
      return matches;
   }

   @Override
   public Optional<char[]> replacement(Deque<ReplacementContext> contextStack) {
      if (!matches) {
         throw new IllegalStateException();
      }

      return nullable(replacementString).map(String::toCharArray);
   }

   private boolean matchCharacter(int c1, char c2) {
      if (!ignoreCase) {
         return c1 == c2;
      } else {
         return Character.toLowerCase(c1) == Character.toLowerCase(c2)
                || Character.toUpperCase(c1) == Character.toUpperCase(c2);
      }
   }

}
