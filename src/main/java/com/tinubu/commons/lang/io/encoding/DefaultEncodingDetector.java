/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.encoding;

import static com.tinubu.commons.lang.util.CollectionUtils.entry;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listIntersection;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.CollectionUtils.set;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.CodingErrorAction.REPORT;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.US_ASCII;
import static java.nio.charset.StandardCharsets.UTF_16;
import static java.nio.charset.StandardCharsets.UTF_16BE;
import static java.nio.charset.StandardCharsets.UTF_16LE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Comparator.comparing;
import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import com.tinubu.commons.lang.util.Pair;

/**
 * Basic encoding detector using :
 * <ul>
 *    <li>{@link StandardCharsets#UTF_8},{@link StandardCharsets#UTF_16LE}, {@link StandardCharsets#UTF_16BE} BOMs detection</li>
 *    <li>Decoding of detection sample with higher confidence</li>
 * </ul>
 * <p>
 * If no restricted encodings list is provided, detects encoding among {@link StandardCharsets} charsets.
 * Non-standard charset detection has a confidence set to 0 conservatively.
 */
// FIXME optimization : read BOM once char-per-char while matching one of searched BOM, instead of matching each BOM one by one
public class DefaultEncodingDetector implements EncodingDetector {

   /**
    * Base encoding list to detect with associated confidence (empirical).
    * Caution : map must be ordered from higher to lower confidence.
    */
   protected static final Map<Charset, Float> STANDARD_CHARSETS = map(LinkedHashMap::new,
                                                                      entry(UTF_8, 0.3f),
                                                                      entry(UTF_16, 0.2f),
                                                                      entry(UTF_16BE, 0.2f),
                                                                      entry(UTF_16LE, 0.2f),
                                                                      entry(US_ASCII, 0.1f),
                                                                      entry(ISO_8859_1, 0f));

   protected static final byte[] UTF_8_BOM = { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF };
   protected static final byte[] UTF_16LE_BOM = { (byte) 0xFF, (byte) 0xFE };
   protected static final byte[] UTF_16BE_BOM = { (byte) 0xFE, (byte) 0xFF };

   protected static final Map<Charset, List<byte[]>> BOM_MAP = map(entry(UTF_8, list(UTF_8_BOM)),
                                                                   entry(UTF_16, list(UTF_16BE_BOM)),
                                                                   entry(UTF_16LE, list(UTF_16LE_BOM)),
                                                                   entry(UTF_16BE, list(UTF_16BE_BOM)));
   // FIXME auto-compute at start
   protected static final Map<byte[], List<Charset>> REVERSED_BOM_MAP = map(entry(UTF_8_BOM, list(UTF_8)),
                                                                            entry(UTF_16LE_BOM,
                                                                                  list(UTF_16LE)),
                                                                            entry(UTF_16BE_BOM,
                                                                                  list(UTF_16, UTF_16BE)));

   protected final boolean ignoreBom;

   /**
    * Default encoding detector.
    *
    * @param ignoreBom whether to ignore bom when using {@link #hasEncoding(ByteBuffer, List)}
    */
   public DefaultEncodingDetector(boolean ignoreBom) {
      this.ignoreBom = ignoreBom;
   }

   /**
    * Default encoding detector, using BOM when available.
    */
   public DefaultEncodingDetector() {
      this(false);
   }

   @Override
   public Optional<DetectedEncoding> hasEncoding(ByteBuffer buffer, List<Charset> encodings) {
      notNull(buffer, "buffer");
      noNullElements(encodings, "encodings");

      var detectEncodings = encodings(encodings);

      if (!ignoreBom) {

         var detectedBom = REVERSED_BOM_MAP.entrySet().stream().flatMap(bomEntry -> {
            return detectBom(buffer, bomEntry.getKey()) ? stream(bomEntry) : stream();
         }).findFirst();

         if (detectedBom.isPresent()) {
            var bomCharsets = detectedBom.get().getValue();
            return detectEncodings
                  .keySet()
                  .stream()
                  .filter(bomCharsets::contains)
                  .map(encoding -> new DetectedEncoding(encoding, detectedBom.get().getKey(), 1f))
                  .findFirst();
         }
      }

      return detectEncodings
            .entrySet()
            .stream()
            .flatMap(encoding -> detectEncoding(buffer, encoding.getKey()) ? stream(new DetectedEncoding(
                  encoding.getKey(),
                  encoding.getValue())) : stream())
            .findFirst();
   }

   @Override
   public Optional<DetectedEncoding> hasBom(ByteBuffer buffer, List<Charset> encodings) {
      notNull(buffer, "buffer");
      noNullElements(encodings, "encodings");

      var detectBomEncodings = encodings == null || encodings.isEmpty()
                               ? BOM_MAP.keySet()
                               : listIntersection(BOM_MAP.keySet(), encodings);

      return detectBomEncodings
            .stream()
            .flatMap(encoding -> detectBom(buffer, encoding)
                  .map(bom -> new DetectedEncoding(encoding, bom, 1))
                  .stream())
            .findFirst();
   }

   /**
    * Computes an encoding list to detect, using restricted encodings, and preserving confidence.
    * Specified encodings, if not in standard charset, have a confidence of 0 conservatively.
    *
    * @param encodings restricted encoding list, or empty list to not restrict the list
    *
    * @return encodings to detect
    */
   protected Map<Charset, Float> encodings(List<Charset> encodings) {
      if (encodings == null || encodings.isEmpty()) {
         return STANDARD_CHARSETS;
      } else {
         return map(LinkedHashMap::new,
                    stream(encodings)
                          .map(encoding -> entry(encoding, STANDARD_CHARSETS.getOrDefault(encoding, 0f)))
                          .sorted(Entry.<Charset, Float>comparingByValue().reversed()));
      }
   }

   /**
    * Detects encoding from detection buffer. If detection buffer length is <= 0, returns false.
    */
   protected boolean detectEncoding(ByteBuffer buffer, Charset encoding) {
      var detectionBuffer = buffer.slice();

      if (detectionBuffer.remaining() > 0) {
         return compatibleEncoding(detectionBuffer, encoding);
      } else {
         return false;
      }
   }

   protected static boolean compatibleEncoding(ByteBuffer buffer, Charset encoding) {
      try {
         encoding.newDecoder().onMalformedInput(REPORT).onUnmappableCharacter(REPORT).decode(buffer);
      } catch (CharacterCodingException e) {
         return false;
      }

      return true;
   }

   protected Optional<byte[]> detectBom(ByteBuffer buffer, Charset encoding) {
      return nullable(BOM_MAP.get(encoding)).flatMap(boms -> stream(boms)
            .filter(bom -> detectBom(buffer, bom))
            .findFirst());
   }

   /**
    * Detects specified BOM in detection buffer.
    * If detection buffer length is < bom length, returns false.
    *
    * @param bom bom to detect
    */
   protected boolean detectBom(ByteBuffer buffer, byte[] bom) {
      if (buffer.remaining() < bom.length) {
         return false;
      }

      return Arrays.equals(bom, 0, bom.length, buffer.array(), buffer.arrayOffset(), bom.length);
   }

}
