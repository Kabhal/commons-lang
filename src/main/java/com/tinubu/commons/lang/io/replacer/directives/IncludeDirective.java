/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer.directives;

import static com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader.SourceContentServiceLoader;
import static com.tinubu.commons.lang.util.CheckedFunction.checkedFunction;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.ServiceLoader;
import java.util.stream.Stream;

import com.tinubu.commons.lang.io.contentloader.ContentLoader;
import com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.Directive;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveEvalContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveServiceLoader;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Failure;
import com.tinubu.commons.lang.util.Try.Success;

/**
 * Include third-party content at current directive position.
 * <p>
 * Note that imported content will be streamed into Reader without extra memory consumption, whatever the
 * imported content size.
 * <p>
 * Content URI is specified in parameter, and is loaded using
 * {@link ServiceLoaderContentLoader#loadContent(URI)}. You can register new {@link ContentLoader}
 * {@link ServiceLoaderContentLoader#loadContentLoaders(ContentLoader...) manually}, or using
 * {@link SourceContentServiceLoader com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader$SourceContentServiceLoader} interface with {@link ServiceLoader} framework.
 * <p>
 * Usage :
 * <ul>
 *    <li>Include with failure if resource is missing : {@code ${include(file://import.txt)}}</li>
 *    <li>Include with not failure if resource is missing : {@code ${include(file://import.txt, ignoreMissingResource)}}</li>
 * </ul>
 */
public class IncludeDirective extends AbstractDirective implements DirectiveServiceLoader {

   private static final String IGNORE_MISSING_RESOURCE_OPTION = "ignoreMissingResource";

   private final ContentLoader contentLoader;

   public IncludeDirective(ContentLoader contentLoader) {
      this.contentLoader = notNull(contentLoader, "contentLoader");
   }

   public IncludeDirective() {
      this(new ServiceLoaderContentLoader());
   }

   @Override
   public List<String> names() {
      return list("include");
   }

   @Override
   public Try<Boolean, String> nonEscapedApply(DirectiveEvalContext evalContext) {
      return evalContext.currentContext().ifReadingText(ctx -> {
         if (!evalContext.arguments().isEmpty()) {
            String includeUri = evalContext.requireArgument(0);

            try {
               return contentLoader
                     .loadContent(new URI(includeUri))
                     .<Try<Boolean, String>>map(checkedFunction(content -> {
                        Reader contentReader =
                              new InputStreamReader(content.inputStream(), content.encoding().orElse(UTF_8));
                        ctx.injectReader(contentReader);
                        return Success.of(true);
                     })).orElseGet(() -> {
                        if (evalContext.hasOption(1, IGNORE_MISSING_RESOURCE_OPTION)) {
                           return Success.of(false);
                        } else {
                           return Failure.of(String.format("'%s' resource not found", includeUri));
                        }
                     });
            } catch (URISyntaxException e) {
               return Failure.of(String.format("Can't parse '%s' URI", includeUri));
            }
         } else {
            return Failure.of("Missing URI parameter");
         }
      });
   }

   @Override
   public Stream<Directive> loadDirectives() {
      return stream(new IncludeDirective());
   }

}
