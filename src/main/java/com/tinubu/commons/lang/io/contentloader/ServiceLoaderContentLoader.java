/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.contentloader;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.stream.Stream;

/**
 * {@link ContentLoader} implementation loading source loaders from {@link ServiceLoader} framework.
 * <p>
 * No default loaders provided, registers custom
 * {@link SourceContentServiceLoader
 * com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader$SourceContentServiceLoader}
 * implementations using {@link ServiceLoader} framework.
 */
public class ServiceLoaderContentLoader extends ResponsibilityChainContentLoader {

   private static final List<ContentLoader> contentLoaders = loadContentLoaders();

   public static List<ContentLoader> loadContentLoaders() {
      return list(stream(ServiceLoader.load(SourceContentServiceLoader.class)).flatMap(
            SourceContentServiceLoader::loadSourceContentLoaders));
   }

   public static void loadContentLoaders(ContentLoader... contentLoaders) {
      noNullElements(contentLoaders, "contentLoaders");
      stream(contentLoaders).forEach(ServiceLoaderContentLoader.contentLoaders::add);
   }

   public static void clearContentLoaders() {
      contentLoaders.clear();
   }

   public ServiceLoaderContentLoader() {
      super(contentLoaders);
   }

   /**
    * Interface to load some {@link ContentLoader} using {@link ServiceLoader} framework.
    */
   public interface SourceContentServiceLoader {
      Stream<ContentLoader> loadSourceContentLoaders();
   }

}

