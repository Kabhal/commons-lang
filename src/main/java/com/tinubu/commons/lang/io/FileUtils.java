/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io;

import java.io.File;
import java.nio.file.Path;

public class FileUtils {

   private static final char EXTENSION_SEPARATOR = '.';

   /** Unix separator character. */
   private static final char UNIX_SEPARATOR = '/';

   /** Windows separator character. */
   private static final char WINDOWS_SEPARATOR = '\\';

   public static String fileExtension(String path) {
      if (path == null) {
         return null;
      }
      final int index = indexOfExtension(path);
      if (index == -1) {
         return "";
      } else {
         return path.substring(index + 1);
      }
   }

   public static String fileExtension(Path path) {
      return fileExtension(path.toString());
   }

   public static String fileExtension(File file) {
      return fileExtension(file.toString());
   }

   private static int indexOfLastSeparator(String filename) {
      final int lastUnixPos = filename.lastIndexOf(UNIX_SEPARATOR);
      final int lastWindowsPos = filename.lastIndexOf(WINDOWS_SEPARATOR);
      return Math.max(lastUnixPos, lastWindowsPos);
   }

   private static int indexOfExtension(String filename) {
      final int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);
      final int lastSeparator = indexOfLastSeparator(filename);
      return lastSeparator > extensionPos ? -1 : extensionPos;
   }

}
