/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer;

import static com.tinubu.commons.lang.log.ExtendedLogger.Level.DEBUG;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.ERROR;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.INFO;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.TRACE;
import static com.tinubu.commons.lang.log.ExtendedLogger.Level.WARN;
import static com.tinubu.commons.lang.util.CheckedFunction.identity;
import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.CollectionUtils.listConcat;
import static com.tinubu.commons.lang.util.CollectionUtils.map;
import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.OptionalUtils.optionalInstanceOf;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.Try.ofThrownBy;
import static com.tinubu.commons.lang.validation.Validate.noNullElements;
import static com.tinubu.commons.lang.validation.Validate.notBlank;
import static com.tinubu.commons.lang.validation.Validate.notEmpty;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.util.Objects.requireNonNull;

import java.io.Reader;
import java.io.StringReader;
import java.util.Deque;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ServiceLoader;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.ReplacementContext;
import com.tinubu.commons.lang.io.replacer.GeneralReplacerReader.TokenReplacer;
import com.tinubu.commons.lang.io.replacer.directives.ValueDirective;
import com.tinubu.commons.lang.log.ExtendedLogger;
import com.tinubu.commons.lang.log.ExtendedLogger.Level;
import com.tinubu.commons.lang.util.Pair;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Failure;
import com.tinubu.commons.lang.util.Try.Success;
import com.tinubu.commons.lang.util.regex.PatternUtils;

/**
 * Advanced {@link TokenReplacer} using pluggable directives for {@link GeneralReplacerReader}.
 * <p>
 * General syntax is :
 * <ul>
 *    <li>{@code <token>} := {@code <phBegin>} {@code <directiveBegin>} {@code <directive>} {@code <directiveEnd>} {@code <suppressWhitespaces>} {@code <phEnd>}</li>
 *    <li>{@code <phBegin>} := '${' (default)</li>
 *    <li>{@code <directiveBegin>} := '' (default)</li>
 *    <li>{@code <directive>} := {@code <name>} [{@code <arguments>}]</li>
 *    <li>{@code <arguments>} := {@code <argumentsBegin>} {@code <argument>} ( ',' {@code <argument>} )* {@code <argumentsEnd>}</li>
 *    <li>{@code <argumentsBegin>} := '(' (default)</li>
 *    <li>{@code <argumentsEnd>} := ')' (default)</li>
 *    <li>{@code <directiveEnd>} := '' (default)</li>
 *    <li>{@code <suppressWhitespaces>} := '-' [ '*' | [0-9]+ ] </li>
 *    <li>{@code <phEnd>} := '}' (default)</li>
 * </ul>
 * <p>
 * For examples :
 * <ul>
 *    <li>{@code ${simpleModelKey} } : simple placeholder replacer for key/value model using fallback mode</li>
 *    <li>{@code ${&(simpleModelKey)} } : simple placeholder replacer for key/value model using explicit '&' directive</li>
 *    <li>{@code ${include(file://import.txt)} } : 'include' directive</li>
 *    <li>{@code ${if(key)} text ${/if} } : 'if' directive illustrating the "block" mode</li>
 * </ul>
 */
// FIXME replace LOG_* with systematic event system and remain with INLINE and THROW only ?
// FIXME document+line+col in messages/events/context ?
public class DirectiveReplacer implements TokenReplacer {

   private static final ExtendedLogger log = ExtendedLogger.of(DirectiveReplacer.class);

   private static final String DEFAULT_PLACEHOLDER_BEGIN = "${";
   private static final String DEFAULT_PLACEHOLDER_END = "}";
   private static final String DEFAULT_DIRECTIVE_BEGIN = "";
   private static final String DEFAULT_DIRECTIVE_END = "";
   private static final String DEFAULT_DIRECTIVE_SUPPRESS_WP = "-";
   private static final String DEFAULT_DIRECTIVE_ARGUMENT_BEGIN = "(";
   private static final String DEFAULT_DIRECTIVE_ARGUMENT_END = ")";
   private static final String DEFAULT_DIRECTIVE_ARGUMENT_SEPARATOR = ",";
   private static final ErrorMode DEFAULT_ERROR_MODE = ErrorMode.THROW;

   private static final Pattern ARGUMENT_SPLIT = Pattern.compile("\\s*,\\s*");

   /** Delegated placeholder replacer. */
   private final PlaceholderReplacer placeholderReplacer;
   /**
    * Extra prefix to identify directives among placeholders. Can be empty.
    */
   private String directiveBegin;
   /**
    * Extra suffix to identify directives among placeholders. Can be empty.
    */
   private String directiveEnd;
   /**
    * Sequence ending placeholder content, or directive content (within directive prefix/suffix) to identify
    * a directive that should suppress following whitespaces. Can't be empty.
    */
   private String directiveSuppressWhitespace;
   /** Directive error management mode. */
   private ErrorMode errorMode;

   private final Map<String, Directive> directives = map();
   private Pattern directivePattern;
   private Pattern suppressWhitespacePattern;

   private DirectiveReplacer(int bufferSize,
                             String placeholderBegin,
                             String placeholderEnd,
                             String directiveBegin,
                             String directiveEnd,
                             String directiveSuppressWhitespace,
                             BiFunction<Deque<ReplacementContext>, String, Object> modelReplacementFunction,
                             ErrorMode errorMode) {
      this.placeholderReplacer = new PlaceholderReplacer(bufferSize,
                                                         placeholderBegin,
                                                         placeholderEnd,
                                                         (contextStack, content) -> directiveReplacementFunction(
                                                               modelReplacementFunction,
                                                               contextStack,
                                                               content));
      this.directiveBegin = notNull(directiveBegin, "directiveBegin");
      this.directiveEnd = notNull(directiveEnd, "directiveEnd");
      this.directiveSuppressWhitespace = notBlank(directiveSuppressWhitespace, "directiveSuppressWhitespace");
      this.errorMode = notNull(errorMode, "errorMode");
      computePatterns();
   }

   public DirectiveReplacer(int bufferSize,
                            String placeholderBegin,
                            String placeholderEnd,
                            BiFunction<Deque<ReplacementContext>, String, Object> modelReplacementFunction) {
      this(bufferSize,
           placeholderBegin,
           placeholderEnd,
           DEFAULT_DIRECTIVE_BEGIN,
           DEFAULT_DIRECTIVE_END,
           DEFAULT_DIRECTIVE_SUPPRESS_WP,
           modelReplacementFunction,
           DEFAULT_ERROR_MODE);
   }

   public DirectiveReplacer(int bufferSize,
                            BiFunction<Deque<ReplacementContext>, String, Object> modelReplacementFunction) {
      this(bufferSize,
           DEFAULT_PLACEHOLDER_BEGIN,
           DEFAULT_PLACEHOLDER_END,
           DEFAULT_DIRECTIVE_BEGIN,
           DEFAULT_DIRECTIVE_END,
           DEFAULT_DIRECTIVE_SUPPRESS_WP,
           modelReplacementFunction,
           DEFAULT_ERROR_MODE);
   }

   public DirectiveReplacer(int bufferSize) {
      this(bufferSize,
           DEFAULT_PLACEHOLDER_BEGIN,
           DEFAULT_PLACEHOLDER_END,
           DEFAULT_DIRECTIVE_BEGIN,
           DEFAULT_DIRECTIVE_END,
           DEFAULT_DIRECTIVE_SUPPRESS_WP,
           null,
           DEFAULT_ERROR_MODE);
   }

   /**
    * Simple replacement function adapter for {@link Map} model.
    *
    * @param model map model
    *
    * @return replacement function
    */
   public static BiFunction<Deque<ReplacementContext>, String, Object> mapModelReplacement(Map<String, Object> model) {
      notNull(model, "model");

      return (__, key) -> model.get(requireNonNull(key));
   }

   public DirectiveReplacer directiveTokens(String directiveBegin,
                                            String directiveEnd,
                                            String directiveSuppressWhitespace) {
      this.directiveBegin = directiveBegin;
      this.directiveEnd = directiveEnd;
      this.directiveSuppressWhitespace = directiveSuppressWhitespace;
      computePatterns();
      return this;
   }

   public DirectiveReplacer errorMode(ErrorMode errorMode) {
      this.errorMode = errorMode;
      return this;
   }

   @Override
   public void reset() {
      placeholderReplacer.reset();
   }

   @Override
   public boolean isPremise(int c) {
      return placeholderReplacer.isPremise(c);
   }

   @Override
   public boolean append(int c) {
      return placeholderReplacer.append(c);
   }

   @Override
   public boolean matches() {
      return placeholderReplacer.matches();
   }

   @Override
   public Optional<char[]> replacement(Deque<ReplacementContext> contextStack) {
      return placeholderReplacer.replacement(contextStack);
   }

   /**
    * Manually loads directives
    *
    * @param directives directives
    *
    * @return this replacer
    */
   public DirectiveReplacer loadDirectives(Directive... directives) {
      noNullElements(directives, "directives");

      stream(directives).forEach(directive -> {
         directive.names().forEach(name -> this.directives.put(name, directive));
      });
      return this;
   }

   /**
    * Exclude library default directives when using {@link #loadDirectivePlugins(Predicate)}.
    *
    * @return directive predicate excluding default directives.
    */
   public Predicate<Directive> excludeDefaultDirectives() {
      return directive -> directive
            .getClass()
            .getName()
            .startsWith("com.tinubu.commons.lang.io.replacer.directives");
   }

   /**
    * Loads directives from {@link ServiceLoader} services implementing {@link DirectiveServiceLoader}
    * interface.
    *
    * @param directiveFilter filter loaded directives
    *
    * @return this replacer
    */
   public DirectiveReplacer loadDirectivePlugins(Predicate<Directive> directiveFilter) {
      notNull(directiveFilter, "directiveFilter");
      stream(ServiceLoader.load(DirectiveServiceLoader.class))
            .flatMap(DirectiveServiceLoader::loadDirectives)
            .filter(directiveFilter)
            .forEach(this::loadDirectives);
      return this;
   }

   /**
    * Unload all loaded directives, manually or from plugins.
    *
    * @return this replacer
    */
   public DirectiveReplacer unloadDirectives() {
      directives.clear();
      return this;
   }

   private Object directiveReplacementFunction(BiFunction<Deque<ReplacementContext>, String, Object> modelReplacementFunction,
                                               Deque<ReplacementContext> contextStack,
                                               String content) {
      Pair<Boolean, Integer> replacement = suppressWhitespace(content).mapLeft(c -> {
         return evaluateDirective(modelReplacementFunction, contextStack, c);
      });

      if (replacement.getRight() != 0) {
         ReplacementContext context = requireNonNull(contextStack.peek());
         context.suppressFollowingWhitespace(replacement.getRight());
      }

      return replacement.getLeft() == null ? null : "";
   }

   private Boolean evaluateDirective(BiFunction<Deque<ReplacementContext>, String, Object> modelReplacementFunction,
                                     Deque<ReplacementContext> contextStack,
                                     String content) {
      ParsedDirective parsedDirective =
            isDirective(content).orElseGet(() -> new ParsedDirective(new ValueDirective(),
                                                                     list("&", content)));

      return manageDirectiveError(new DirectiveEvalContext(contextStack,
                                                           modelReplacementFunction,
                                                           parsedDirective.arguments),
                                  evalContext -> evaluateDirective(parsedDirective.directive, evalContext));
   }

   private Boolean manageDirectiveError(DirectiveEvalContext evalContext,
                                        Function<? super DirectiveEvalContext, Try<Boolean, String>> directive) {
      return directive.apply(evalContext).orElseGet(failure -> {

         Level logLevel = null;
         switch (errorMode) {
            case THROW:
               throw new IllegalStateException(String.format("'%s' directive error: %s",
                                                             evalContext.directive(),
                                                             failure));
            case LOG_TRACE:
               logLevel = nullable(logLevel, TRACE);
            case LOG_DEBUG:
               logLevel = nullable(logLevel, DEBUG);
            case LOG_INFO:
               logLevel = nullable(logLevel, INFO);
            case LOG_WARN:
               logLevel = nullable(logLevel, WARN);
            case LOG_ERROR:
               logLevel = nullable(logLevel, ERROR);
               log.log(logLevel, String.format("'%s' directive error: %s", evalContext.directive(), failure));
               return false;
            case INLINE:
               String inlineMessage =
                     String.valueOf(placeholderReplacer.placeholderBegin()) + directiveBegin + String.format(
                           "%s error: %s",
                           evalContext.directive(),
                           failure) + directiveEnd + String.valueOf(placeholderReplacer.placeholderEnd());

               requireNonNull(evalContext
                                    .contextStack()
                                    .peek()).injectReader(new StringReader(inlineMessage));

               return false;
            default:
               throw new IllegalStateException(String.format("Unsupported '%s' error mode", errorMode));
         }
      });
   }

   /**
    * @return trimmed content. Pair right value is the suppress whitespace parameter
    */
   private Pair<String, Integer> suppressWhitespace(String content) {
      Matcher fallbackMatcher = suppressWhitespacePattern.matcher(content);

      if (fallbackMatcher.matches()) {
         int suppressFollowingWhitespace =
               fallbackMatcher.group(2) != null
               ? (fallbackMatcher.group(3) != null ? (fallbackMatcher
                                                            .group(3)
                                                            .equals("*")
                                                      ? -1
                                                      : Integer.parseInt(fallbackMatcher.group(3))) : 1)
               : 0;

         return Pair.of(fallbackMatcher.group(1), suppressFollowingWhitespace);
      } else {
         throw new IllegalStateException();
      }
   }

   /**
    * Returns parsed directive content, if specified content has valid syntax.
    * <p>
    * Empty content is not supported and will return {@link Optional#empty()}.
    * <p>
    * Directive syntax is {@code function 'directiveBegin' arg1 ('directiveSeparator' argN)* 'directiveEnd'}
    * Spaces are trimmed from :
    * <ul>
    *    <li>around function name</li>
    *    <li>after argument list if any</li>
    *    <li>around each argument</li>
    *    <li>around suppress whitespace selector</li>
    * </ul>
    *
    * @return parsed directive as function name and argument list, or {@link Optional#empty()} if directive
    *       does not match valid syntax. Pair right value is the suppress whitespace parameter
    */
   private Optional<ParsedDirective> isDirective(String content) {
      Matcher directiveMatcher = directivePattern.matcher(content);

      if (directiveMatcher.matches()) {

         String directiveName = directiveMatcher.group(1);
         Directive directive = directives.get(directiveName);

         if (directive == null) {
            return optional();
         }

         List<String> arguments;
         if (directiveMatcher.group(2) != null) {
            arguments = listConcat(stream(directiveName),
                                   stream(ARGUMENT_SPLIT.split(directiveMatcher.group(2).trim())));
         } else {
            arguments = list(directiveName);
         }

         return optional(new ParsedDirective(directive, arguments));
      } else {
         return optional();
      }
   }

   /**
    * A directive decomposition. Arguments first element is the parsed directive name.
    */
   private static class ParsedDirective {
      public Directive directive;
      public List<String> arguments;

      public ParsedDirective(Directive directive, List<String> arguments) {
         this.directive = directive;
         this.arguments = arguments;
      }
   }

   private void computePatterns() {
      this.directivePattern = directivePattern();
      this.suppressWhitespacePattern = suppressWhitespacePattern();
   }

   private Pattern directivePattern() {
      String db = PatternUtils.escapeRegexp(directiveBegin);
      String de = PatternUtils.escapeRegexp(directiveEnd);
      String ab = PatternUtils.escapeRegexp(DEFAULT_DIRECTIVE_ARGUMENT_BEGIN);
      String ae = PatternUtils.escapeRegexp(DEFAULT_DIRECTIVE_ARGUMENT_END);
      String as = PatternUtils.escapeRegexp(DEFAULT_DIRECTIVE_ARGUMENT_SEPARATOR);
      String fun = "[^" + ab + "\\s]+";
      String arg = "[^" + as + "]+";
      return Pattern.compile("\\s*"
                             + db
                             + "\\s*("
                             + fun
                             + ")\\s*(?:"
                             + ab
                             + "("
                             + arg
                             + "(?:,"
                             + arg
                             + ")*)?"
                             + ae
                             + ")?\\s*"
                             + de
                             + "\\s*");
   }

   private Pattern suppressWhitespacePattern() {
      String dwsp = PatternUtils.escapeRegexp(directiveSuppressWhitespace);
      return Pattern.compile("\\s*(.*?)\\s*(" + dwsp + "([0-9*]+)?)?\\s*");
   }

   /**
    * @implSpec Per convention, returns {@code false} if reading text is disabled.
    */
   private Boolean modelReplacement(String content, DirectiveEvalContext evalContext) {
      ReplacementContext context = evalContext.currentContext();

      if (evalContext
            .currentDirectiveContext()
            .map(DirectiveReplacementContext::escapeTokens)
            .orElse(false)) {
         return null;
      } else {
         if (context.stopReadingText()) {
            return false;
         }
      }

      return evalContext.modelValue(content).map(value -> {
         context.injectReader(new StringReader(value.toString()));
         return true;
      }).orElse(null);
   }

   private Try<Boolean, String> evaluateDirective(Directive directive, DirectiveEvalContext evalContext) {
      Try<Try<Boolean, String>, RuntimeException> evaluate = ofThrownBy(() -> directive.apply(evalContext));

      return evaluate.map(identity()).orElseGet(failure -> Failure.of(failure.getMessage()));
   }

   /**
    * Contains the context during the evaluation of a {@link Directive}.
    */
   public class DirectiveEvalContext {
      private final Deque<ReplacementContext> contextStack;
      private final BiFunction<Deque<ReplacementContext>, String, Object> modelReplacementFunction;
      private final String directive;
      private final List<String> arguments;

      public DirectiveEvalContext(Deque<ReplacementContext> contextStack,
                                  BiFunction<Deque<ReplacementContext>, String, Object> modelReplacementFunction,
                                  List<String> arguments) {
         this.contextStack = notEmpty(contextStack, "contextStack");
         this.modelReplacementFunction = nullable(modelReplacementFunction, (ctx, c) -> null);

         List<String> args = satisfies(notNull(arguments, "arguments"),
                                       a -> !a.isEmpty(),
                                       "arguments",
                                       "must have directive set at arguments[0]");
         this.directive = args.get(0);
         this.arguments = args.subList(1, args.size());
      }

      public Try<Boolean, String> evaluate(String function) {
         ParsedDirective parsedDirective =
               isDirective(function).orElseGet(() -> new ParsedDirective(new ValueDirective(),
                                                                         list("&", function)));

         DirectiveEvalContext evalContext =
               new DirectiveEvalContext(contextStack, modelReplacementFunction, parsedDirective.arguments);
         return evaluateDirective(parsedDirective.directive, evalContext).mapFailure(failure -> {
            return String.format("'%s' directive error: %s", evalContext.directive(), failure);
         });
      }

      public String directive() {
         return directive;
      }

      /**
       * Replacer context stack. Stack is never empty, but can contain any implementations of
       * {@link ReplacementContext}.
       *
       * @return replacer context stack
       */
      public Deque<ReplacementContext> contextStack() {
         return contextStack;
      }

      /**
       * Current context, from the head of the context stack.
       *
       * @return current context
       */
      public ReplacementContext currentContext() {
         return requireNonNull(contextStack.peek());
      }

      /**
       * Current context, from the head of the context stack, if its type is an instance of the specified one.
       *
       * @return current directive replacement context, or {@link Optional#empty()} if current context is not
       *       of the specified type.
       */
      public <T extends ReplacementContext> Optional<T> currentContext(Class<T> contextType) {
         return optionalInstanceOf(currentContext(), contextType);
      }

      /**
       * Current context, from the head of the context stack, if its type is an instance of
       * {@link DirectiveReplacementContext}.
       *
       * @return current directive replacement context, or {@link Optional#empty()} if current context is not
       *       of this type.
       */
      public Optional<DirectiveReplacementContext> currentDirectiveContext() {
         return currentContext(DirectiveReplacementContext.class);
      }

      /**
       * Returns specified variable value
       *
       * @param name variable name
       *
       * @return variable value, or {@link Optional#empty()} if variable is not present
       */
      public Optional<Object> modelValue(String name) {
         return nullable(modelReplacementFunction.apply(contextStack, name));
      }

      /**
       * Trimmed arguments for the directive, never {@code null}.
       *
       * @return trimmed arguments list
       */
      public List<String> arguments() {
         return arguments;
      }

      /**
       * Returns trimmed argument at specified position if present.
       *
       * @param position argument position starting from 0
       *
       * @return trimmed argument value, or {@link Optional#empty()} if no argument at specified position
       */
      public Optional<String> argument(int position) {
         return arguments.size() > position ? optional(arguments.get(position)) : optional();
      }

      /**
       * Returns trimmed argument at specified position, or throw.
       *
       * @param position argument position starting from 0
       *
       * @return trimmed argument value
       *
       * @throws IllegalStateException if no argument at specified position
       */
      public String requireArgument(int position) {
         return argument(position).orElseThrow(() -> new IllegalStateException(String.format(
               "No argument at #%d",
               position)));
      }

      /**
       * Checks if an argument is an option with the specified name.
       * If argument is {@code true}, the option is always considered has present.
       *
       * @param position argument position starting from 0
       * @param name option name for argument value
       *
       * @return {@code true} if argument exists and has a value equals (case-insensitive) to specified name
       */
      public boolean hasOption(int position, String name) {
         return argument(position)
               .filter(opt -> opt.equalsIgnoreCase(name) || opt.equalsIgnoreCase("true"))
               .isPresent();
      }
   }

   public enum ErrorMode {
      /** Throw {@link IllegalStateException} on error. */
      THROW,
      /** Log at trace level on error. */
      LOG_TRACE,
      /** Log at debug level on error. */
      LOG_DEBUG,
      /** Log at info level on error. */
      LOG_INFO,
      /** Log at warn level on error. */
      LOG_WARN,
      /** Log at error level on error. */
      LOG_ERROR,
      /** Inline error in content at the faulty directive position. */
      INLINE
   }

   /**
    * A directive evaluator.
    * Directive instances are reused between calls, so no internal state should be maintained.
    */
   public interface Directive extends Function<DirectiveEvalContext, Try<Boolean, String>> {

      List<String> names();

      @Override
      Try<Boolean, String> apply(DirectiveEvalContext evalContext);
   }

   /**
    * Replacement context with extensions to enable new behavior for directives.
    */
   public static class DirectiveReplacementContext extends DefaultReplacementContext {

      /**
       * Whether to stop replacing tokens content.
       */
      private boolean escapeTokens = false;

      public boolean escapeTokens() {
         return escapeTokens;
      }

      public DirectiveReplacementContext escapeTokens(boolean escapeTokens) {
         this.escapeTokens = escapeTokens;
         return this;
      }

      public Try<Boolean, String> escapeTokens(Supplier<Try<Boolean, String>> delegate) {
         notNull(delegate, "delegate");

         if (escapeTokens) {
            return Success.of(null);
         } else {
            return delegate.get();
         }
      }

      @Override
      public DirectiveReplacementContext stopReadingText(boolean stopReadingText) {
         return (DirectiveReplacementContext) super.stopReadingText(stopReadingText);
      }

      @Override
      public DirectiveReplacementContext stopParsingToken(boolean stopParsingToken) {
         return (DirectiveReplacementContext) super.stopParsingToken(stopParsingToken);
      }

      @Override
      public DirectiveReplacementContext suppressFollowingWhitespace(int suppressFollowingWhitespace) {
         return (DirectiveReplacementContext) super.suppressFollowingWhitespace(suppressFollowingWhitespace);
      }

      @Override
      public DirectiveReplacementContext injectReader(Reader reader) {
         return (DirectiveReplacementContext) super.injectReader(reader);
      }

   }

   /**
    * {@link ServiceLoader} loading interface.
    * <p>
    * Use {@link #loadDirectivePlugins(Predicate)} to load directives using service loader.
    */
   public interface DirectiveServiceLoader {

      Stream<Directive> loadDirectives();

   }

}
