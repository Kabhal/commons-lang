/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer.directives;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;

import java.util.List;
import java.util.stream.Stream;

import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.Directive;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveEvalContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveReplacementContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveServiceLoader;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Success;

/**
 * Function evaluated as boolean result.
 * <p>
 * Usage :
 * <ul>
 *    <li>{@code ${true} }</li>
 *    <li>{@code ${false} }</li>
 * </ul>
 */
public class BooleanDirective extends AbstractDirective implements DirectiveServiceLoader {

   @Override
   public List<String> names() {
      return list("true", "false");
   }

   @Override
   public Try<Boolean, String> nonEscapedApply(DirectiveEvalContext evalContext) {
      switch (evalContext.directive()) {
         case "true":
            return Success.of(true);
         case "false":
            return Success.of(false);
         default:
            throw new IllegalStateException("Unknown directive");
      }
   }

   @Override
   public Stream<Directive> loadDirectives() {
      return stream(new BooleanDirective());
   }

   private static class CommentDirectiveReplacementContext extends DirectiveReplacementContext {

   }
}

