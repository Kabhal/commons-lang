/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.contentloader;

import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Optional;

/**
 * General interface for content loading from a URI representing a content source.
 */
public interface ContentLoader {

   /**
    * Returns a {@link Content} from specified source.
    *
    * @param source source URI. supported URI format depends on implementation
    *
    * @return content input stream, or {@link Optional#empty()} if source URI is not supported or available
    */
   Optional<Content> loadContent(URI source);

   /**
    * Structure to store content and metadata.
    */
   interface Content {
      /**
       * Content input stream.
       * Returned input stream should be closed once used.
       *
       * @return content input stream
       */
      InputStream inputStream();

      /**
       * Content encoding, if available
       *
       * @return content encoding, or {@link Optional#empty} if encoding is unknown
       */
      Optional<Charset> encoding();
   }

}

