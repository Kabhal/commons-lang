/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.validation.Validate.notNull;
import static com.tinubu.commons.lang.validation.Validate.satisfies;
import static java.lang.Math.min;
import static java.util.Objects.requireNonNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Success;

/**
 * General replacer for reader based on {@link PushbackReader} to replace arbitrary tokens in large
 * documents.
 * Note that you should wrap the provided input reader in a {@link BufferedReader} for performance purpose.
 * You must provide a {@link TokenReplacer} implementation to implement your token matching and
 * replacement algorithm.
 * <p>
 * Limitation : section length and replacement text is limited to {@value #DEFAULT_BUFFER_SIZE} bytes by
 * default. You can configure an alternative buffer size depending on your requirements. In case of
 * buffer overflow, token replacement string will be ignored and original text left as-is.
 */
public class GeneralReplacerReader extends PushbackReader {

   /**
    * Default buffer size. Default buffer size is set to a "reasonably" big value because of particular
    * implementation limitations due to buffer size.
    */
   private static final int DEFAULT_BUFFER_SIZE = 32768;
   private static final Supplier<ReplacementContext> DEFAULT_REPLACEMENT_CONTEXT =
         DefaultReplacementContext::new;

   private final TokenReplacer tokenReplacer;

   /**
    * Maximum length of pushback buffer, constraining all theses :
    * <ul>
    *    <li>the maximum read length in {@link #read(char[], int, int)}</li>
    *    <li>the maximum length of section to replace, and</li>
    *    <li>the maximum length of replacement text</li>
    * </ul>
    */
   private final int bufferSize;

   /** Tracks last computed section remaining length to read before computing the next section. */
   private int currentSectionLen = 0;

   private final Deque<ReplacementContext> replacementContextStack =
         new ArrayDeque<>(list(DEFAULT_REPLACEMENT_CONTEXT.get()));

   /**
    * Creates general replacer reader instance.
    *
    * @param in reader to read from
    * @param tokenReplacerFactory token replacer factory
    * @param bufferSize internal buffer size to adapt to your requirements
    */
   public GeneralReplacerReader(Reader in, TokenReplacerFactory tokenReplacerFactory, int bufferSize) {
      super(notNull(in, "in"), satisfies(bufferSize, bs -> bs > 0, "bufferSize", "must be > 0"));

      this.tokenReplacer = notNull(tokenReplacerFactory, "tokenReplacerFactory").instance(bufferSize);
      this.bufferSize = bufferSize;
   }

   /**
    * Creates general replacer reader instance with default buffer size.
    *
    * @param in reader to read from
    * @param tokenReplacerFactory token replacer factory
    */
   public GeneralReplacerReader(Reader in, TokenReplacerFactory tokenReplacerFactory) {
      this(in, tokenReplacerFactory, DEFAULT_BUFFER_SIZE);
   }

   @Override
   public int read() throws IOException {
      if (replacementContext().stop()) {
         return -1;
      }

      int readInjected = replacementContext().readInjectedReader();
      if (readInjected != -1) {
         return readInjected;
      }

      /* Because read method must always return at least one char or -1, switch to next section while
       * regular text section returns 0 length because of ReplacementContext::stopReading.
       * We have to extract check the injected reader here or there's a chance to read the next section
       * before injecting the reader.
       */
      while (nextSection(1) == 0) {
         readInjected = replacementContext().readInjectedReader();
         if (readInjected != -1) {
            return readInjected;
         }
      }
      return super.read();
   }

   @Override
   public int read(char[] cbuf, int off, int len) throws IOException {
      if (replacementContext().stop()) {
         return -1;
      }

      int readInjected = replacementContext().readInjectedReader(cbuf, off, len);
      if (readInjected != -1) {
         return readInjected;
      }

      return super.read(cbuf, off, nextSection(len));
   }

   @Override
   public void close() throws IOException {
      try {
         for (ReplacementContext context : replacementContextStack) {
            context.close();
         }
      } finally {
         super.close();
      }
   }

   /**
    * Main section dispatcher based on next character.
    *
    * @return the number of bytes to read for the next section
    *
    * @throws IOException if an I/O error occurs
    * @implNote Because of pushback buffer size limitation, the entire buffer size must be used for a
    *       single section at a time, so that the next section must be completely read before starting a new
    *       section.
    */
   private int nextSection(int len) throws IOException {
      int c = super.read();
      int sectionLen;

      if (c == -1) {
         sectionLen = len;
      } else if (currentSectionLen > 0) {
         super.unread(c);
         sectionLen = currentSectionLen;
      } else {
         tokenReplacer.reset();

         if (!replacementContext().stopParsingToken() && tokenReplacer.isPremise(c)) {
            super.unread(c);
            sectionLen = currentSectionLen = tokenSection(tokenReplacer);
         } else {
            super.unread(c);
            sectionLen = currentSectionLen = regularTextSection(tokenReplacer);
         }
      }

      sectionLen = min(len, sectionLen);
      currentSectionLen -= sectionLen;
      return sectionLen;
   }

   /**
    * Identifies the next regular text section until a premise character is found.
    *
    * @return the number of bytes to read for this section
    *
    * @throws IOException if an I/O error occurs
    * @implNote section length is constrained by pushback buffer size. Buffer size has no impact on
    *       final result : more iterations will be just required if buffer size is too low.
    */
   private int regularTextSection(TokenReplacer tokenReplacer) throws IOException {
      int c;
      boolean token = false;
      int constrainedLength = bufferSize;
      char[] textBuffer = new char[constrainedLength];
      int textBufferLength = 0;

      do {
         c = super.read();

         if (!replacementContext().stopParsingToken() && tokenReplacer.isPremise(c)) {
            token = true;
         }
         if (c != -1) {
            textBuffer[textBufferLength] = (char) c;
            textBufferLength++;
         }
      } while (c != -1 && textBufferLength < constrainedLength && !token);

      if (replacementContext().stopReadingText()) {
         if (token && c != -1) {
            super.unread(textBuffer, textBufferLength - 1, 1);
         }
         return 0;
      } else {
         int wspLength = whiteSpaceLength(textBuffer, textBufferLength);

         super.unread(textBuffer, wspLength, textBufferLength - wspLength);
         if (token && c != -1) {
            return textBufferLength - wspLength - 1;
         } else {
            return textBufferLength - wspLength;
         }
      }
   }

   /**
    * Returns the length of a "whitespace" section, at the beginning of the specified buffer.
    * Note that there's no guarantee that whitespace section is at the beginning of the current section all
    * the time, however it is always the case just after a "token" section for example. So you should disable
    * {@link ReplacementContext#suppressFollowingWhitespace(char[])} once it has been called one or more
    * time, or  once a {@code null} value is sent to it.
    * <p>
    * The following whitespaces are supported by the reader :
    *  <ul>
    *     <li>{@code \r\n}</li>
    *     <li>{@code \r}</li>
    *     <li>{@code \n}</li>
    *     <li>{@code \t}</li>
    *     <li>{@code SPACE}</li>
    *     <li>{@code NBSP(0x00A0)}</li>
    *  </ul>
    * <p>
    * However, you can decide to restrict this list, and support different section lengths in
    * {@link ReplacementContext#suppressFollowingWhitespace(char[])} implementation.
    * The reader will call the context following these rules :
    * <ul>
    *    <li>each whitespace is added to section length while context returns {@code true}</li>
    *    <li>if no more whitespace is available in buffer, {@code null} value is sent to context, so that it
    *        can change its internal state</li>
    * </ul>
    *
    * @param textBuffer text buffer to search whitespace section from the start
    * @param textBufferLength text buffer length
    *
    * @return whitespace section length, whose length is lesser than or equal to text buffer length
    */
   private int whiteSpaceLength(char[] textBuffer, int textBufferLength) {
      int nlLength = 0;
      int nlIndex = 0;

      do {
         String nl;
         if (textBufferLength > nlIndex + 1
             && textBuffer[nlIndex] == '\r'
             && textBuffer[nlIndex + 1] == '\n') {
            nl = "\r\n";
         } else if (textBufferLength > nlIndex && (textBuffer[nlIndex] == ' '
                                                   || textBuffer[nlIndex] == '\u00A0'
                                                   || textBuffer[nlIndex] == '\t'
                                                   || textBuffer[nlIndex] == '\n'
                                                   || textBuffer[nlIndex] == '\r')) {
            nl = String.valueOf(textBuffer[nlIndex]);
         } else {
            replacementContext().suppressFollowingWhitespace(null);
            break;
         }

         if (replacementContext().suppressFollowingWhitespace(nl.toCharArray())) {
            nlLength += nl.length();
         } else {
            break;
         }
         nlIndex += nl.length();
      } while (true);
      return nlLength;
   }

   /**
    * Identifies the next token section.
    *
    * @param tokenReplacer current token replacer instance for this section
    *
    * @return the number of bytes to read for this section
    *
    * @throws IOException if an I/O error occurs
    * @implNote If buffer size is too low to handle this section in one iteration, the token won't be
    *       replaced. The same if token replacement string is larger than buffer.
    */
   private int tokenSection(TokenReplacer tokenReplacer) throws IOException {
      int c;
      boolean parseFinished = false;
      char[] tokenBuffer = new char[bufferSize];
      int tokenBufferLength = 0;

      do {
         c = super.read();

         if (c != -1) {
            tokenBuffer[tokenBufferLength] = (char) c;
            tokenBufferLength++;
         }

         if (!tokenReplacer.append(c)) {
            parseFinished = true;

            if (c != -1) {
               super.unread(c);
               tokenBufferLength--;
            }
         } else if (tokenReplacer.matches()) {
            parseFinished = true;

            Optional<char[]> value = tokenReplacer.replacement(replacementContextStack);
            ensureReplacementContextStackNotEmpty();
            if (value.isPresent() && value.get().length <= bufferSize) {
               tokenBuffer = value.get();
               tokenBufferLength = value.get().length;
            }
         }

      } while (!parseFinished && tokenBufferLength < bufferSize && c != -1);

      super.unread(tokenBuffer, 0, tokenBufferLength);
      return tokenBufferLength;
   }

   private void ensureReplacementContextStackNotEmpty() {
      if (replacementContextStack.isEmpty()) {
         replacementContextStack.push(DEFAULT_REPLACEMENT_CONTEXT.get());
      }
   }

   private ReplacementContext replacementContext() {
      return requireNonNull(replacementContextStack.peek());
   }

   /**
    * {@link TokenReplacer} factory for a given buffer size. Buffer size is a primary information for
    * {@link GeneralReplacerReader} implementation, so that token replacer should take buffer size into
    * account for its own implementation.
    */
   @FunctionalInterface
   public interface TokenReplacerFactory {

      /**
       * Instantiates a new instance.
       *
       * @param bufferSize current buffer size configured in {@link GeneralReplacerReader}, can be
       *       useful to align replacer implementation with it.
       *
       * @return new token replacer instance
       */
      TokenReplacer instance(int bufferSize);
   }

   /**
    * Token matching and replacement algorithm.
    */
   public interface TokenReplacer {

      /**
       * Resets token replacer internal state for next token matching.
       * This operation is called before a new token is analyzed to reuse the same token replacer instance.
       */
      void reset();

      /**
       * Returns {@code true} if specified character is a token premise. Replacer state should not be modified
       * at this level but in {@link #append(int)} operation.
       *
       * @param c character to match, is never {@code -1}
       *
       * @return whether the specified character is a token premise
       */
      boolean isPremise(int c);

      /**
       * Appends the specified character to token matching. Returns {@code false} if matching fails with this
       * new character, {@code true} if matching is still successful and matching can continue.
       * Note that {@code -1} character can be received if no more characters are available, in this case you
       * can decide to return {@code true} or {@code false} depending on your matching algorithm.
       * <p>
       * Note that if you return {@code false} here, no replacement will occur. Otherwise, a call to
       * {@link #matches()} will be tried after this call, so if you consider the token as "matched" with
       * this last character append, just return {@code true} and also makes {@link #matches()} return
       * {@code true}.
       *
       * @param c new character to append to token matcher, can be -1 if no more characters are
       *       available in reader
       */
      boolean append(int c);

      /**
       * Returns {@code true} if token is considered as "matched" since the last {@link #append(int)} call.
       * The general replacer will then call {@link #replacement(Deque)} to retrieve replacement
       * string for matched token.
       *
       * @return {@code true} if token is considered as "matched"
       */
      boolean matches();

      /**
       * Returns replacement string for matched token. You can return {@link Optional#empty()} if replacement
       * string can't be internally generated for the matched token, in this case, token string won't be
       * replaced and left as-is.
       *
       * @param contextStack current replacement context stack to peek, push or pop. Stack is guaranteed
       *       to always contain at least the default context.
       *
       * @return token replacement string or {@link Optional#empty()} if no replacement string is available
       *       for matched token
       */
      Optional<char[]> replacement(Deque<ReplacementContext> contextStack);
   }

   /**
    * Base replacement context for reader and directives.
    */
   public interface ReplacementContext extends AutoCloseable {

      /**
       * Mutates "stop reading text" flag.
       *
       * @param stopReadingText stop reading text flag
       *
       * @return this context
       */
      ReplacementContext stopReadingText(boolean stopReadingText);

      /**
       * Mutates "stop parsing token" flag.
       *
       * @param stopParsingToken stop parsing token flag
       *
       * @return this context
       */
      ReplacementContext stopParsingToken(boolean stopParsingToken);

      /**
       * Mutates "suppress following whitespace" flag.
       * Values can be :
       * <ul>
       *    <li>-1 : Suppress all following whitespaces</li>
       *    <li>0 : Does not suppress any following whitespace</li>
       *    <li>1... : Suppress this number of following whitespace</li>
       * </ul>
       *
       * @param suppressFollowingWhitespace suppress following whitespace flag
       *
       * @return this context
       */
      ReplacementContext suppressFollowingWhitespace(int suppressFollowingWhitespace);

      /**
       * Sets a reader to inject content from.
       * Previous reader must be {@link Reader#close()} if overridden with this method.
       * Specified reader must be closed in {@link ReplacementContext#close()} that must be called each time
       * a context is removed from context stack. {@link GeneralReplacerReader} will also close all remaining
       * contexts in the stack on {@link GeneralReplacerReader#close()}.
       */
      ReplacementContext injectReader(Reader reader);

      /**
       * Whether to stop read next regular text sections.
       *
       * @return {@code true} if next regular text sections should be skipped by the reader
       */
      boolean stopReadingText();

      /**
       * Whether to stop parsing next token sections. Token sections are identified by
       * {@link TokenReplacer#isPremise(int)}.
       * <p>
       * When this flag is enabled, tokens are not parsed, not replaced and read as-is from source.
       *
       * @return {@code true} if next token sections should be not parsed and not replaced by the reader
       */
      boolean stopParsingToken();

      /**
       * Convenience flag to indicate to the reader to stop reading from source.
       *
       * @return {@code true} if reader should stop reading from source
       */
      default boolean stop() {
         return stopReadingText() && stopParsingToken();
      }

      /**
       * Convenience operation to evaluate specified function only if {@link #stopReadingText()} is false.
       * Per convention, returns {@link Success} with {@code false} value if text reading is disabled.
       */
      default Try<Boolean, String> ifReadingText(Function<ReplacementContext, Try<Boolean, String>> action) {
         notNull(action, "action");
         if (!stopReadingText()) {
            return action.apply(this);
         } else {
            return Success.of(false);
         }
      }

      /**
       * Convenience operation to evaluate specified function only if {@link #stopReadingText()} is false.
       * Per convention, returns {@link Success} with {@code false} value if text reading is disabled.
       */
      default Try<Boolean, String> ifReadingText(Predicate<ReplacementContext> orPredicate,
                                                 Function<ReplacementContext, Try<Boolean, String>> action) {
         notNull(action, "action");
         notNull(orPredicate, "orPredicate");

         if (!stopReadingText() || orPredicate.test(this)) {
            return action.apply(this);
         } else {
            return Success.of(false);
         }
      }

      /**
       * Reader requests if specified white space should be suppressed. Reader will ask for subsequent
       * whitespaces until this method returns {@code false}.
       * If this is the reader that encounter a non-whitespace character, a call to this method with a
       * {@code null} whitespace is made to have a chance to reset implementation state.
       *
       * @param whitespace whitespace to suppress, or {@code null} if subsequent character is not a
       *       supported whitespace.
       *
       * @return {@code true} if the specified whitespace should be suppressed
       */
      boolean suppressFollowingWhitespace(char[] whitespace);

      /**
       * Reader will call this operation when trying to inject a third-party reader. This operation should be
       * called by {@link GeneralReplacerReader#read()}.
       *
       * @return next character from injected reader or {@code -1} if there's no reader to inject, or reader
       *       has no more characters remanining to read
       *
       * @throws IOException if an I/O error occurs while reading from injected reader
       */
      int readInjectedReader() throws IOException;

      /**
       * Reader will call this operation when trying to inject a third-party reader. This operation should be
       * called by {@link GeneralReplacerReader#read(char[], int, int)}.
       *
       * @return next character from injected reader or {@code -1} if there's no reader to inject, or reader
       *       has no more characters remanining to read
       *
       * @throws IOException if an I/O error occurs while reading from injected reader
       */
      int readInjectedReader(char[] cbuf, int off, int len) throws IOException;

      /**
       * {@inheritDoc}
       *
       * @throws IOException
       * @implNote Injected reader should be closed here if present.
       */
      @Override
      void close() throws IOException;
   }
}
