/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.contentloader;

import static com.tinubu.commons.lang.util.NullableUtils.nullable;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Optional;

/**
 * {@link ContentLoader} implementation using a byte array source.
 */
public class ByteArrayContentLoader implements ContentLoader {

   private final byte[] content;
   private final Charset encoding;

   public ByteArrayContentLoader(byte[] content, Charset encoding) {
      this.content = notNull(content, "content");
      this.encoding = encoding;
   }

   public ByteArrayContentLoader(byte[] content) {
      this(content, null);
   }

   @Override
   public Optional<Content> loadContent(URI ignored) {
      return optional(new Content() {
         @Override
         public InputStream inputStream() {
            return new ByteArrayInputStream(content);
         }

         @Override
         public Optional<Charset> encoding() {
            return nullable(encoding);
         }
      });
   }

}

