/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.encoding;

import static com.tinubu.commons.lang.util.CheckedPredicate.alwaysTrue;
import static com.tinubu.commons.lang.util.OptionalUtils.optional;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.util.StreamUtils.streamConcat;

import java.util.Optional;
import java.util.ServiceLoader;
import java.util.function.Predicate;

public class EncodingDetectorFactory {

   public static final DefaultEncodingDetector DEFAULT_ENCODING_DETECTOR = new DefaultEncodingDetector();

   private static class SingletonHolder {
      private static final EncodingDetectorFactory INSTANCE = new EncodingDetectorFactory();
   }

   public static EncodingDetectorFactory instance() {
      return SingletonHolder.INSTANCE;
   }

   public Optional<EncodingDetector> encodingDetector(Predicate<EncodingDetector> filter,
                                                      EncodingDetector defaultEncodingDetector) {
      ServiceLoader<EncodingDetector> serviceLoader = ServiceLoader.load(EncodingDetector.class);

      Predicate<EncodingDetector> f = filter != null ? filter : alwaysTrue();

      return streamConcat(stream(serviceLoader), stream(defaultEncodingDetector)).filter(f).findFirst();
   }

   public Optional<EncodingDetector> encodingDetector(Predicate<EncodingDetector> filter) {
      return encodingDetector(filter, DEFAULT_ENCODING_DETECTOR);
   }

   public Optional<EncodingDetector> encodingDetector(EncodingDetector defaultEncodingDetector) {
      return encodingDetector(null, defaultEncodingDetector);
   }

   public Optional<EncodingDetector> encodingDetector() {
      return encodingDetector(null, DEFAULT_ENCODING_DETECTOR);
   }
}
