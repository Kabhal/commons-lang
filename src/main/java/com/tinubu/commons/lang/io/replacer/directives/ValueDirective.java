/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.io.replacer.directives;

import static com.tinubu.commons.lang.util.CollectionUtils.list;
import static com.tinubu.commons.lang.util.StreamUtils.stream;
import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.io.StringReader;
import java.util.List;
import java.util.stream.Stream;

import com.tinubu.commons.lang.io.contentloader.ContentLoader;
import com.tinubu.commons.lang.io.contentloader.ServiceLoaderContentLoader;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.Directive;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveEvalContext;
import com.tinubu.commons.lang.io.replacer.DirectiveReplacer.DirectiveServiceLoader;
import com.tinubu.commons.lang.util.Try;
import com.tinubu.commons.lang.util.Try.Failure;
import com.tinubu.commons.lang.util.Try.Success;

/**
 * Directive to inject a model variable value into a document.
 * <p>
 * Usage, given {@code key}={@code value} in model :
 * <ul>
 *    <li>Checks if model variable is present : {@code ${if(has(key))} text ${/if}} &nbsp;-&gt; " text "</li>
 *    <li>Injects model variable if present : {@code ${key}} or {@code ${&(key)}} &nbsp;-&gt; "value"</li>
 *    <li>Escape model variable if not present : {@code ${unknown}} &nbsp;-&gt; "${unknown}"</li>
 *    <li>Injects and/or test model variable :
 *       <ul>
 *         <li>{@code ${if!(unknown)} text ${/if}} &nbsp;-&gt; " text "</li>
 *         <li>{@code ${if!(key)} text ${else} is present${/if}} &nbsp;-&gt; "value is present"</li>
 *       </ul>
 *    </li>
 * </ul>
 */
public class ValueDirective extends AbstractDirective implements DirectiveServiceLoader {

   private final ContentLoader contentLoader;

   public ValueDirective(ContentLoader contentLoader) {
      this.contentLoader = notNull(contentLoader, "contentLoader");
   }

   public ValueDirective() {
      this(new ServiceLoaderContentLoader());
   }

   @Override
   public List<String> names() {
      return list("has", "&");
   }

   @Override
   @SuppressWarnings("resource")
   public Try<Boolean, String> nonEscapedApply(DirectiveEvalContext evalContext) {
      return evalContext.currentContext().ifReadingText(ctx -> evalContext.argument(0).map(name -> {
         return evalContext.modelValue(name).<Try<Boolean, String>>map(value -> {
            if (evalContext.directive().equals("&")) {
               ctx.injectReader(new StringReader(value.toString()));
            }
            return Success.of(true);
         }).orElseGet(() -> {
            if (evalContext.directive().equals("&")) {
               return Success.of(null);
            } else {
               return Success.of(false);
            }
         });
      }).orElseGet(() -> Failure.of("Missing model variable name")));
   }

   @Override
   public Stream<Directive> loadDirectives() {
      return stream(new ValueDirective());
   }

}
