/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.variant;

import static com.tinubu.commons.lang.validation.Validate.notNull;

import java.math.BigDecimal;
import java.math.BigInteger;

import com.tinubu.commons.lang.convert.ConversionService;
import com.tinubu.commons.lang.convert.services.StandardConversionService;

/**
 * Specialized variant to manage {@link Number} types.
 * <p>
 * Conversion supported for types :
 * <ul>
 *    <li>{@link Long}</li>
 *    <li>{@link Integer}</li>
 *    <li>{@link Short}</li>
 *    <li>{@link Byte}</li>
 *    <li>{@link Float}</li>
 *    <li>{@link Double}</li>
 *    <li>{@link BigDecimal}</li>
 *    <li>{@link BigInteger}</li>
 * </ul>
 *
 * @apiNote Generally speaking variant is not about conversion, but this is a common use case for
 *       Number's. Note that if you do not need a {@link Variant} in first place, you can just use a simple
 *       {@link Number}.
 */
public class NumberVariant extends BoundVariant<Number> {

   private final ConversionService conversionService;

   public <T extends Number> NumberVariant(T value, Class<T> type, ConversionService conversionService) {
      super(value, type);
      this.conversionService = conversionService;
   }

   public <T extends Number> NumberVariant(T value, ConversionService conversionService) {
      super(value);
      this.conversionService = conversionService;
   }

   public <T extends Number> NumberVariant(T value, Class<T> type) {
      this(value, type, new StandardConversionService());
   }

   public <T extends Number> NumberVariant(T value) {
      this(value, new StandardConversionService());
   }

   public Long longValue() {
      return conversionService.convert(value(), Long.class);
   }

   public Integer integerValue() {
      return conversionService.convert(value(), Integer.class);
   }

   public Float floatValue() {
      return conversionService.convert(value(), Float.class);
   }

   public Double doubleValue() {
      return conversionService.convert(value(), Double.class);
   }

   public Short shortValue() {
      return conversionService.convert(value(), Short.class);
   }

   public Byte byteValue() {
      return conversionService.convert(value(), Byte.class);
   }

   public BigDecimal bigDecimalValue() {
      return conversionService.convert(value(), BigDecimal.class);
   }

   public BigInteger bigIntegerValue() {
      return conversionService.convert(value(), BigInteger.class);
   }

   @SuppressWarnings("unchecked")
   public <T extends Number> T convertValue(Class<T> type) {
      notNull(type, "type");

      if (type.equals(Long.class)) {
         return (T) longValue();
      } else if (type.equals(Integer.class)) {
         return (T) integerValue();
      } else if (type.equals(Float.class)) {
         return (T) floatValue();
      } else if (type.equals(Double.class)) {
         return (T) doubleValue();
      } else if (type.equals(Short.class)) {
         return (T) shortValue();
      } else if (type.equals(Byte.class)) {
         return (T) byteValue();
      } else if (type.equals(BigDecimal.class)) {
         return (T) bigDecimalValue();
      } else if (type.equals(BigInteger.class)) {
         return (T) bigIntegerValue();
      } else {
         return super.value(type);
      }
   }
}