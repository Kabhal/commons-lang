/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.mapper;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toMap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

/**
 * Helper class for enums with mapped values.
 * You can use {@link #map(Object)} to return the enum entry corresponding to mapped value.
 * <p>
 * Uniqueness of mapped values is checked at class creation (fail-fast), hence mapped values class must
 * implement {@link Objects#equals} accordingly.
 *
 * @param <T> enum type
 * @param <V> mapped value type
 *
 * @apiNote Mapping complexity is in O(1).
 */
public class EnumMapper<T extends Enum<T>, V> {

   private final Class<T> enumClass;
   private final Function<T, V> mapper;

   private Map<V, T> enumMap;

   public EnumMapper(Class<T> enumClass, Function<T, V> mapper) {
      this.enumClass = enumClass;
      this.mapper = mapper;

      checkMappedValues(mapper);
   }

   public T map(V v) {
      if (v == null) {
         return null;
      }

      return enumMap().get(v);
   }

   private void checkMappedValues(Function<T, V> mapper) {
      List<V> uniqueMappedValues = new ArrayList<>();
      Set<V> nonUniqueMappedValues = new HashSet<>();
      Stream.of(enumClass.getEnumConstants()).map(mapper).forEach(mappedValue -> {
         if (uniqueMappedValues.contains(mappedValue)) {
            nonUniqueMappedValues.add(mappedValue);
         } else {
            uniqueMappedValues.add(mappedValue);
         }
      });

      if (nonUniqueMappedValues.size() > 0) {
         throw new IllegalArgumentException(String.format("Mapped values must be unique : %s",
                                                          nonUniqueMappedValues
                                                                .stream()
                                                                .map(Objects::toString)
                                                                .collect(joining(", ", "[ ", " ]"))));
      }
   }

   private Map<V, T> enumMap() {
      if (enumMap == null) {
         enumMap = Stream.of(enumClass.getEnumConstants()).collect(toMap(mapper, identity()));
      }

      return enumMap;
   }

}
