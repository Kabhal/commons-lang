/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.tinubu.commons.lang.beans;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Configures accessor naming for declaration scope.
 * <p>
 * This annotation can be used on enclosing class, or package-info.java's package directive.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.METHOD, ElementType.TYPE, ElementType.TYPE_USE, ElementType.PACKAGE })
public @interface AccessorConfig {
   String DEFAULT_GETTER_PREFIX = "get";
   String DEFAULT_SETTER_PREFIX = "set";
   String DEFAULT_BOOLEAN_GETTER_PREFIX = "is";

   /**
    * Getter accessor prefix used.
    * Set this prefix to correctly identify the property name depending on your
    * accessor naming scheme.
    *
    * @return getter accessor prefix
    */
   String getterPrefix() default DEFAULT_GETTER_PREFIX;

   /**
    * Setter accessor prefix used.
    * Set this prefix to correctly identify the property name depending on your
    * accessor naming scheme.
    *
    * @return setter accessor prefix
    */
   String setterPrefix() default DEFAULT_SETTER_PREFIX;

   /**
    * Getter accessor prefix used for boolean accessors.
    * Set this prefix to correctly identify the property name depending on your
    * accessor naming scheme.
    *
    * @return boolean getter accessor prefix
    */
   String booleanGetterPrefix() default DEFAULT_BOOLEAN_GETTER_PREFIX;
}
